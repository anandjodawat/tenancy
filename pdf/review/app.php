<?php
include("../../admin/Common.php");
if(!isset($_SESSION['ManagerID']) || $_SESSION["ManagerID"] == false){
	if(!isset($_SESSION['TenantID']) || $_SESSION["TenantID"] == false){
		redirect("../../index.php");
	}
}
require_once('tcpdf_include.php');
$msg="";
$ID=0;

if(isset($_REQUEST["ID"]))
		$AppGUID=trim($_REQUEST["ID"]);

$ID=mysqli_fetch_array(mysqli_query($dbh,"SELECT ID FROM application WHERE GUID='".$AppGUID."'"));
$AppID=$ID['ID'];	
$result=mysqli_query($dbh,"Select * FROM application Where ID=".(int)$AppID." AND Status != 0 ") or die('Failed due to:'.mysqli_error($dbh));
$row = mysqli_fetch_array($result);
$TenantID=$row["TenantID"];
$Address=$row["Address"];

$result2=mysqli_query($dbh,"Select * FROM users Where ID=".(int)$TenantID."") or die('Failed due to:'.mysqli_error($dbh));
$row2 = mysqli_fetch_array($result2);

$result3=mysqli_query($dbh,"Select * FROM app_inspection Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
$row3 = mysqli_fetch_array($result3);

$result4=mysqli_query($dbh,"Select * FROM tenant_profile Where UserID=".(int)$TenantID."") or die('Failed due to:'.mysqli_error($dbh));
$row4 = mysqli_fetch_array($result4);				

if($result){
$result5=mysqli_query($dbh,"Select * FROM app_questions Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
$row5 = mysqli_fetch_array($result5);
}

$photo=mysqli_query($dbh,"Select * FROM app_documents WHERE Application=".(int)$AppID."") or die ('Failed due to:'.mysqli_error ($dbh));
$nikal=mysqli_fetch_array($photo);

$File=$nikal["File"];
if($row3["Inspected"]==1)
	$row3["Inspected"]="I declare that I have physically inspected the inside of this property";
if($row3["Inspected"]==2)
	$row3["Inspected"]="I have not yet inspected this property";
if($row3["Inspected"]==3)
	$row3["Inspected"]="I am currently interstate/overseas and will physically inspect the property upon my arrival";
if($row3["Inspected"]==4)
	$row3["Inspected"]="An associate/other occupant has inspected the property on my behalf";
if($row3["Inspected"]==5)
	$row3["Inspected"]="I am currently interstate/overseas but accept the property in its current state";

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tenancy Application');
$pdf->SetTitle('Review Application');
$pdf->SetSubject('PDF View');
$pdf->SetKeywords('Tenancy, PDF, Application');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

 $html='<html class="no-js" lang="">

<head>

<meta charset="utf-8">

<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title>Application</title>
<link rel="stylesheet" href="css/bootstrap.min.css">

<style>
body {
    margin: 0px;
    padding: 0px;
    background-color: #f9f9fa;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    overflow-x: hidden;
}
.container {
width:900px;
margin:0 auto;
padding:20px 20px 10px 20px;
background:#fff;
}
#targetbody {
	width:100%;
	height:auto;
	display:inline-block;
}
#gethtml {
	width:100%;
    display: inline-block;
	border-bottom:1px solid #ccc;
	margin-right:0;
    padding-bottom: 5px;	
}
#gethtml td {
font-size:17px;
}
.reviewapp_content {
	width:100%;
	height:auto;
	display:inline-block;
	border-bottom:1px solid #ccc;
	padding-bottom:15px;
	margin-top:20px;
}
.review_left {
	width:49%;
	float:left;
	margin-right:1%;
}
.review_right {
	width:49%;
	float:right;
}
th,td {
	padding:8px 0 7px 8px;
}
tbody, tr {
	width:100%;
	height:auto;
	display:inline-block;
}
.table_half {
	width:100%;
/*	float:left;
	display:inline-block;*/
}
.table_half td {
	width:47%;
	display:inline-block;
	font-size: 15px;
}
.table_half td:nth-child(2) {
    color:#5d77b3 !important;
	-webkit-print-color-adjust: exact;
	font-weight: bold;
    font-size: 16px;
}
.black_bg {
	width:auto;
	background:#bebebe !important;
	color:#000 !important;
	display:block;
	-webkit-print-color-adjust: exact;
	padding:2px 9px;
	font-size:18px;
    margin: 4px 0;
	font-weight:500;
	text-align:left;
}
.td60 {
    width:60% !important;
}
.td90 {
    width:72% !important;
}
.td10 {
    width:10% !important;
}

</style>

</head>

<body>

<div class="container">

<div id="targetbody">


<div style="margin: 0px;    padding: 0px;    background-color: #f9f9fa;    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;    overflow-x: hidden;">
<div style="width:900px;margin:0 auto;padding:20px 20px 10px 20px;background:#fff;">

<div style="width:100%;	height:auto;	display:inline-block;">
<table id="gethtml" style="width:100%;display: inline-block;border-bottom:1px solid #ccc;margin-right:0;padding-bottom: 5px">

			<tr>

				<td style="padding-top:15px !important;color:#666666;font-size:22px;float:left;font-weight:bold;">Residential Tenancy Application

                    <span style="color:#999999 !important;-webkit-print-color-adjust: exact;display:block;font-size:14px;font-weight:500;">Powered by: Tenancy</span>

                </td>

				<td style="float:right;"><img src="../../img/logo.png"></td>

			</tr>

			<tr>

				<td style="padding-bottom:0 !important;float:left;"><strong>Property Manager:</strong> '. dboutput($row["PM_Name"]) .'</td>

				<td style="float:right;padding-bottom:0 !important;"><strong>Sent to:</strong> '.dboutput($row["PM_Email"]) .'</td>

			</tr>

			<tr>

				<td style="float:left;padding-top:5px;"><strong>Received:</strong> '.$row["DateAdded"].'</td>

				<td style="float:right;padding-top:5px;"><strong>Application ID:</strong> '.$row["ID"] .'</td>

			</tr>

		</table> <div style="width:100%;height:auto;display:inline-block;border-bottom:1px solid #ccc;padding-bottom:15px;margin-top:20px;">';
		$pdf->writeHTML($html, true, false, true, false, '');
		
        $html = '<div class="review_left"> <table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Property Details</th></tr>

			<tr>

				<td>Application Address:</td>

				<td>'.dboutput($row["Address"]).'</td>

            </tr>

			<tr>

				<td>Property ID: </td>	

				<td>'. dboutput($row["ID"]).'</td>

			</tr>

			<tr>            

				<td>Inspection Details</td>

				<td>'. dboutput($row3["Inspected"]).'</td>

			</tr>

			<tr>

				<td>Weekly Rent: </td>	

				<td>$'. dboutput($row["WeeklyRent"]).'</td>

			</tr>

			<tr>

				<td>Monthly Rent: </td>	

				<td>$'. dboutput($row["MonthlyRent"]).'</td>

			</tr>

			<tr>

				<td>Bond: </td>	

				<td>$'. dboutput($row["Bond"]).'</td>

			</tr>

			<tr>

				<td>Commencement Date: </td>

				<td>'. dboutput($row["CommDate"]).'</td>

			</tr>

			<tr>

				<td>Preferred Lease Term: </td>

				<td>'. dboutput($row["LeaseLength"]).'</td>

			</tr>

			<tr>

				<td>No. of Occupants:</td>

				<td>'. dboutput($row["Occupants"]).'</td>

			</tr>

			<tr>

				<td>No. of Vehicles</td>

				<td>'. dboutput($row["Vehicles"]).'</td>

			</tr>

			<tr>

				<td>No. of Pets</td>

				<td>'. dboutput($row["Pets"]).'</td>

			</tr>

			<tr>

				<td>How did you find out about this property?</td>

				<td>'. dboutput($row["Find"]).'-'.dboutput($row["Source"]).'</td>

			</tr>

		</table>';

		
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->AddPage();
		
        $html ='<table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Personal Details</th></tr>

			<tr>

				<td>Name: </td>

				<td>'. dboutput($row4["Fname"]).' '. dboutput($row4["Mname"]).' '. dboutput($row4["Lname"]).'</td>

			</tr>

			<tr>

				<td>Sex</td>

				<td>'. dboutput($row4["Gender"]).'</td>

			</tr>

			<tr>

				<td>Date of Birth</td>

				<td>'. dboutput($row4["DOB"]).'</td>

			</tr>

			<tr>

				<td>Smoker</td>

				<td>'. dboutput($row4["LSmoke"]).'</td>

			</tr>

			<tr>

				<td>State of Issue</td>

				<td>'. dboutput($row4["SOI"]).'</td>

			</tr>

			<tr>

				<td>Expiry</td>

				<td>'. dboutput($row4["EDate"]).'</td>

			</tr>

			<tr>

				<td>Identification & Supporting Docs</td>

				<td>';
			if($nikal["Upload"] == "1"){

					$html .='Applicant will provide ID and Docs by hand or fax by ".$nikal["Date"]';

				}else{

					$html .='Applicant has attached Docs';

				}

				
				$html .= '</td>

			</tr>

			<tr>

				<td>All Day Phone/Mobile</td>

				<td>'. dboutput($row4["Mnumber"]).'</td>

			</tr>

			<tr>

				<td>Home/Work Phone</td>

				<td>'. dboutput($row4["Hnumber"]).'</td>

			</tr>

			<tr>

				<td>Email</td>

				<td>'. dboutput($row2["Email"]).'</td>

			</tr>

			<tr>

				<td>Current Address</td>

				<td>'. dboutput($row4["CurrAddress"]).'</td>

			</tr>

			<tr>

				<td>Postal Address</td>

				<td>'. dboutput($row4["CurrPostAdd"]).'</td>

			</tr>

			<tr>

				<td>Emergency Contact</td>

				<td>'. dboutput($row4["NameEC"]).'</td>

			</tr>

			<tr>

				<td>Relationship</td>

				<td>'. dboutput($row4["ECRelation"]).'</td>

			</tr>

			<tr>

				<td>Phone</td>

				<td>'. dboutput($row4["MAcon"]).'</td>

			</tr>

			<tr>

				<td>Address</td>

				<td>'. dboutput($row4["AddEC"]).'</td>

			</tr>

		</table>     

        <table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Address</th></tr>

			<tr>

				<td>I am currently: </td>

				<td>'. dboutput($row4["LivingArr"]).'</td>		

			</tr>

			<tr>

				<td>Address: </td>

				<td>'. dboutput($row4["Addx"]).'</td>		

			</tr>

			<tr>

				<td>Monthly Rent: </td>

				<td>'. dboutput($row4["CurrMonRent"]).'</td>

			</tr>

			<tr>

				<td>Was Bond fully refunded? </td>

				<td>'. dboutput($row4["CurrBond"]).'</td>

			</tr>

			<tr>

				<td>Agents Name: </td>

				<td>'. dboutput($row4["CurrAgName"]).'</td>	

			</tr>

			<tr>

				<td>Phone: </td>

				<td>'. dboutput($row4["CurrPhn"]).'</td>

			</tr>

			<tr>

				<td>Email: </td>

				<td>'. dboutput($row4["CurrEmail"]).'</td>			

			</tr>

			<tr>

               <td>Reason for leaving: </td>

			   <td>'. dboutput($row4["ReasonFL"]).'</td>            

            </tr>            

            <tr>

				<td>Previous Address: </td>

				<td>'. dboutput($row4["PrLivArr"]).'</td>            

            </tr>

		</table>';


		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->AddPage();
		   

        $html ='<table class="table_half">

		  <tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Current Employment</th></tr>

			<tr>

			  <td style="width:60% !important;color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">'. dboutput($row4["EmCurrWork"]).'</td>

			</tr>

		  </table>    

			  <table class="table_half">

				<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Previous Employment</th></tr>

				<tr>

				  <td style="width:60% !important;color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">'.dboutput($row4["EmPrWork"]).'</td>

				</tr>

			  </table> </div>

			  

			  

	<div class="review_right">	<table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Utility Connections</th></tr>

			<tr><td style="width:100% !important;color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">';

			 if($row["Utility"] == "No"){

				 $html .= 'This applicant WOULD NOT like to be contacted about utilities.To integrate your utility 

				connection provider please contact us.';

			}

			else {

				$html .= 'Required';

			}

				

				$html .='</td></tr></table>';

		

        if ($row4["Student"] !="") {

				$html .= '<table class="table_half">

					<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Is A Student?</th></tr>

					<tr><td style="color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;"><?php if(dboutput($row4["Student"])=="Y"){ echo "Yes"; } else{ echo "No"; } ?></td></tr>

				  </table>';

				}   

		 if($row["Occupants"] > 0) {

		$html .='<table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Other Occupants</th></tr>';

			

				$result6=mysqli_query($dbh,"Select * FROM app_occupants Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));

				while($row6=mysqli_fetch_array($result6)){

			

				$html .='<tr>

					<td>Name: </td>

					<td>'.dboutput($row6["Name"]).'</td>

				</tr>

				<tr>

					<td>Phone: </td>

					<td>'.dboutput($row6["Mobile"]).'</td>

				</tr>

				<tr>

					<td>Relationship: </td>

					<td>'.dboutput($row6["Relationship"]).'</td>

				</tr>

				<tr>

					<td>Age: </td>

					<td>'.dboutput($row6["Age"]).'</td>

				</tr>';

				

					if($row6["Age"] > 18){

					$html .='<tr>

						<td>On the lease? </td>

						<td>'.dboutput($row6["Lease"]).'</td>

					</tr>';

						if($row6["Lease"] == "Yes"){

						

							$html .='<tr>

								<td>Email </td>

								<td>'.dboutput($row6["Email"]).'</td>

							</tr>

							<tr>

								<td>Contact No </td>

								<td>'.dboutput($row6["Mobile"]).'</td>

							</tr>';

						}

					}

				}

		$html .='</table>';

		} 

		 if($row["Vehicles"] > 0) {

		$html .='<table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Vehicles</th></tr>';

			

				$result7=mysqli_query($dbh,"Select * FROM app_vehicles Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));

				while($row7=mysqli_fetch_array($result7)){

			

				$html .='<tr>

					<td>Type: </td>

					<td>'.dboutput($row7["Type"]).'</td>

				</tr>

				<tr>

					<td>Model: </td>

					<td>'.dboutput($row7["Model"]).'</td>

				</tr>

				<tr>

					<td>Registration: </td>

					<td>'.dboutput($row7["Registration"]).'</td>

				</tr>';

					}

		$html .='</table>';

		 } 



		if ($row["Pets"] > 0) { 

		$html .='<table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Pets</th></tr>';

				$result8=mysqli_query($dbh,"Select * FROM app_pets Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));

				while($row8=mysqli_fetch_array($result8)){

				$html .='<tr>

					<td>Type: </td>';

						if($row8["Other"] == ""){

					

					$html .='<td>'.dboutput($row8["Type"]).'</td>';

					 } else{  

					$html .='<td>'.dboutput($row8["Type"]).'</td>';

						}

				$html .='</tr>

				<tr>

					<td>Breed: </td>

					<td>'.dboutput($row8["Breed"]).'</td>

				</tr>';

				}

			  

		$html .='</table>';

		 } 

		 $pdf->writeHTML($html, true, false, true, false, '');
		$pdf->AddPage();

                $html ='<table class="table_half">

                <tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Other Details</th></tr>

                <tr>

					<td style="width:72% !important;">Applications pending on other properties?</td>

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q1"]).'</span></td>

                </tr>

				 <tr>

					<td style="width:72% !important;">Have you ever been refused a rental property?</td>

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q2"]).'</span></td>

                 </tr>

				 <tr>

					<td style="width:72% !important;">Are you in debt to another landlord or agent?</td>

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q3"]).'</span></td>

                 </tr>

				 <tr>

					<td style="width:72% !important;">Deductions ever taken from your Bond ?</td>

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q4"]).'</span></td>

                 </tr>

				  <tr>

					<td style="width:72% !important;">Anything affecting future rental payments?</td>

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q5"]).'</span></td>

                  </tr>

				  <tr>

					<td style="width:72% !important;">Considering buying a property soon?</td> 

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q6"]).'</span></td>

                  </tr>

				  <tr>

					<td style="width:72% !important;">Do you currently own a property?</td>

					<td style="width:10% !important;"><span class="darkblue">'.dboutput($row5["Q7"]).'</span></td>

                  </tr>

			  </table>            

          <table class="table_half">

			<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">References</th></tr>

			<tr>

				<td>Personal Ref.</td>

				<td>'.dboutput($row4["PerRef"]).'</td>			

			</tr>

			<tr>

				<td>Phone</td>

				<td>'.dboutput($row4["PerPh"]).'</td>			

			</tr>

			<tr>

				<td>Relationship</td>

				<td>'.dboutput($row4["PerRel"]).'</td>			

			</tr>

			<tr>

				<td>Email</td>

				<td>'.dboutput($row4["PerEmail"]).'</td>			

			</tr>

			<tr>

				<td>Personal Ref. 1</td>

				<td>'.dboutput($row4["RefName1"]).'</td>			

			</tr>

			<tr>

				<td>Phone</td>

				<td>'.dboutput($row4["1PhnNo"]).'</td>			

			</tr>

			<tr>

				<td>Relationship</td>

				<td>'.dboutput($row4["1Relation"]).'</td>			

			</tr>

			<tr>

				<td>Email</td>

				<td>'.dboutput($row4["1Email"]).'</td>			

			</tr>

			<tr>

				<td>Personal Ref. 2</td>

				<td>'.dboutput($row4["RefName2"]).'</td>			

			</tr>

			<tr>

				<td>Phone</td>

				<td>'.dboutput($row4["2PhnNo"]).'</td>			

			</tr>

			<tr>

				<td>Relationship</td>

				<td>'.dboutput($row4["2Relation"]).'</td>			

			</tr>

			<tr>

				<td>Email</td>

				<td>'.dboutput($row4["2Email"]).'</td>			

			</tr>

         </table>';

		 

		 if($File != "") {

			

			$html .= '<table class="table_half">

				<tr><th style="width:auto;background:#bebebe !important;color:#000 !important;display:block;-webkit-print-color-adjust: exact;padding:2px 9px;font-size:18px;margin: 4px 0;font-weight:500;text-align:left;">Files Uploaded</th></tr>

				<tr><td>

				<a href="../../uploads/'.$nikal["File"].'" download><img src="../../uploads/'.$File.'" style="width:100px;"/> </a></td>

				</tr></table></div> </div>';

			}
		
		$html .='</div>
</div>
</body>
</html>';
		// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();
 

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');
?>