<?php
include("../../admin/Common.php");
if(!isset($_SESSION['ManagerID']) || $_SESSION["ManagerID"] == false){
	if(!isset($_SESSION['TenantID']) || $_SESSION["TenantID"] == false){
		redirect("../../index.php");
	}
}
$msg="";
$ID=0;

if(isset($_REQUEST["ID"]))
		$AppGUID=trim($_REQUEST["ID"]);

$ID=mysqli_fetch_array(mysqli_query($dbh,"SELECT ID FROM application WHERE GUID='".$AppGUID."'"));
$AppID=$ID['ID'];	
$result=mysqli_query($dbh,"Select * FROM application Where ID=".(int)$AppID." AND Status != 0 ") or die('Failed due to:'.mysqli_error($dbh));
$row = mysqli_fetch_array($result);
$TenantID=$row["TenantID"];
$Address=$row["Address"];



$result2=mysqli_query($dbh,"Select * FROM users Where ID=".(int)$TenantID."") or die('Failed due to:'.mysqli_error($dbh));
$row2 = mysqli_fetch_array($result2);

$result3=mysqli_query($dbh,"Select * FROM app_inspection Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
$row3 = mysqli_fetch_array($result3);

$result4=mysqli_query($dbh,"Select * FROM tenant_profile Where UserID=".(int)$TenantID."") or die('Failed due to:'.mysqli_error($dbh));
$row4 = mysqli_fetch_array($result4);				

if($result){
$result5=mysqli_query($dbh,"Select * FROM app_questions Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
$row5 = mysqli_fetch_array($result5);
}

$photo=mysqli_query($dbh,"Select * FROM app_documents WHERE Application=".(int)$AppID."") or die ('Failed due to:'.mysqli_error ($dbh));
$nikal=mysqli_fetch_array($photo);

$File=$nikal["File"];
if($row3["Inspected"]==1)
	$row3["Inspected"]="I declare that I have physically inspected the inside of this property";
if($row3["Inspected"]==2)
	$row3["Inspected"]="I have not yet inspected this property";
if($row3["Inspected"]==3)
	$row3["Inspected"]="I am currently interstate/overseas and will physically inspect the property upon my arrival";
if($row3["Inspected"]==4)
	$row3["Inspected"]="An associate/other occupant has inspected the property on my behalf";
if($row3["Inspected"]==5)
	$row3["Inspected"]="I am currently interstate/overseas but accept the property in its current state";


$html = '<table border="1" cellspacing="3" cellpadding="4">
			<tr>
				<td style="padding-top:15px !important;color:#666666;font-size:22px;float:left;font-weight:bold;">Residential Tenancy Application
                    <span style="color:#999999 !important;-webkit-print-color-adjust: exact;display:block;font-size:14px;font-weight:500;">Powered by: Tenancy</span>
                </td>
				<td style="float:right;"><img src="../../img/logo.png"></td>
			</tr>
			<tr>
				<td style="padding-bottom:0 !important;float:left;"><strong>Property Manager:</strong> '. dboutput($row["PM_Name"]) .'</td>
				<td style="float:right;padding-bottom:0 !important;"><strong>Sent to:</strong> '.dboutput($row["PM_Email"]) .'</td>
			</tr>
			<tr>
				<td style="float:left;padding-top:0;"><strong>Received:</strong> '.$row["DateAdded"].'</td>
				<td style="float:right;padding-top:0;"><strong>Application ID:</strong> '.$row["ID"] .'</td>
			</tr>
		</table>
        <table border="1" cellspacing="3" cellpadding="4">
			<tr><th colspan="2">Property Details</th></tr>
			<tr>
				<td>Application Address:</td>
				<td>'.dboutput($row["Address"]).'</td>
            </tr>
			<tr>
				<td>Property ID: </td>	
				<td>'. dboutput($row["ID"]).'</td>
			</tr>
			<tr>            
				<td>Inspection Details</td>
				<td>'. dboutput($row3["Inspected"]).'</td>
			</tr>
			<tr>
				<td>Weekly Rent: </td>	
				<td>$'. dboutput($row["WeeklyRent"]).'</td>
			</tr>
			<tr>
				<td>Monthly Rent: </td>	
				<td>$'. dboutput($row["MonthlyRent"]).'</td>
			</tr>
			<tr>
				<td>Bond: </td>	
				<td>$'. dboutput($row["Bond"]).'</td>
			</tr>
			<tr>
				<td>Commencement Date: </td>
				<td>'. dboutput($row["CommDate"]).'</td>
			</tr>
			<tr>
				<td>Preferred Lease Term: </td>
				<td>'. dboutput($row["LeaseLength"]).'</td>
			</tr>
			<tr>
				<td>No. of Occupants:</td>
				<td>'. dboutput($row["Occupants"]).'</td>
			</tr>
			<tr>
				<td>No. of Vehicles</td>
				<td>'. dboutput($row["Vehicles"]).'</td>
			</tr>
			<tr>
				<td>No. of Pets</td>
				<td>'. dboutput($row["Pets"]).'</td>
			</tr>
			<tr>
				<td>How did you find out about this property?</td>
				<td>'. dboutput($row["Find"]).'-'.dboutput($row["Source"]).'</td>
			</tr>
		</table>
        <table border="1" cellspacing="3" cellpadding="4">
			<tr><th colspan="2">Personal Details</th></tr>
			<tr>
				<td>Name: </td>
				<td>'. dboutput($row4["Fname"]).' '. dboutput($row4["Mname"]).' '. dboutput($row4["Lname"]).'</td>
			</tr>
			<tr>
				<td>Sex</td>
				<td>'. dboutput($row4["Gender"]).'</td>
			</tr>
			<tr>
				<td>Date of Birth</td>
				<td>'. dboutput($row4["DOB"]).'</td>
			</tr>
			<tr>
				<td>Smoker</td>
				<td>'. dboutput($row4["LSmoke"]).'</td>
			</tr>
			<tr>
				<td>State of Issue</td>
				<td>'. dboutput($row4["SOI"]).'</td>
			</tr>
			<tr>
				<td>Expiry</td>
				<td>'. dboutput($row4["EDate"]).'</td>
			</tr>
			<tr>
				<td>Identification & Supporting Docs</td>
				<td><?php if($nikal["Upload"] == "1"){
					echo "Applicant will provide ID and Docs by hand or fax by ".$nikal["Date"];
				}else{
					echo "Applicant has attached Docs";
				}
				?></td>
			</tr>
			<tr>
				<td>All Day Phone/Mobile</td>
				<td>'. dboutput($row4["Mnumber"]).'</td>
			</tr>
			<tr>
				<td>Home/Work Phone</td>
				<td>'. dboutput($row4["Hnumber"]).'</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>'. dboutput($row2["Email"]).'</td>
			</tr>
			<tr>
				<td>Current Address</td>
				<td>'. dboutput($row4["CurrAddress"]).'</td>
			</tr>
			<tr>
				<td>Postal Address</td>
				<td>'. dboutput($row4["CurrPostAdd"]).'</td>
			</tr>
			<tr>
				<td>Emergency Contact</td>
				<td>'. dboutput($row4["NameEC"]).'</td>
			</tr>
			<tr>
				<td>Relationship</td>
				<td>'. dboutput($row4["ECRelation"]).'</td>
			</tr>
			<tr>
				<td>Phone</td>
				<td>'. dboutput($row4["MAcon"]).'</td>
			</tr>
			<tr>
				<td>Address</td>
				<td>'. dboutput($row4["AddEC"]).'</td>
			</tr>
		</table>        
        <table border="1" cellspacing="3" cellpadding="4">
			<tr><th colspan="2">Address</th></tr>
			<tr>
				<td>I am currently: </td>
				<td>'. dboutput($row4["LivingArr"]).'</td>		
			</tr>
			<tr>
				<td>Address: </td>
				<td>'. dboutput($row4["Addx"]).'</td>		
			</tr>
			<tr>
				<td>Monthly Rent: </td>
				<td>'. dboutput($row4["CurrMonRent"]).'</td>
			</tr>
			<tr>
				<td>Was Bond fully refunded? </td>
				<td>'. dboutput($row4["CurrBond"]).'</td>
			</tr>
			<tr>
				<td>Agents Name: </td>
				<td>'. dboutput($row4["CurrAgName"]).'</td>	
			</tr>
			<tr>
				<td>Phone: </td>
				<td>'. dboutput($row4["CurrPhn"]).'</td>
			</tr>
			<tr>
				<td>Email: </td>
				<td>'. dboutput($row4["CurrEmail"]).'</td>			
			</tr>
			<tr>
               <td>Reason for leaving: </td>
			   <td>'. dboutput($row4["ReasonFL"]).'</td>            
            </tr>            
            <tr>
				<td>Previous Address: </td>
				<td>'. dboutput($row4["PrLivArr"]).'</td>            
            </tr>
		</table>        
        <table border="1" cellspacing="3" cellpadding="4">
		  <tr><th colspan="2">Current Employment</th></tr>
			<tr>
			  <td class="td60" style="color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">'. dboutput($row4["EmCurrWork"]).'</td>
			</tr>
		  </table>
			<table border="1" cellspacing="3" cellpadding="4">
			  <tr><th colspan="2">Current Employment</th></tr>
				<tr>
				  <td class="td60" style="color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">'.dboutput($row4["EmCurrWork"]).'</td>
				</tr>
			  </table>
			  <table border="1" cellspacing="3" cellpadding="4">
				<tr><th colspan="2">Previous Employment</th></tr>
				<tr>
				  <td class="td60" style="color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">'.dboutput($row4["EmPrWork"]).'</td>
				</tr>
			  </table>';
			  
			  if($File != "") {
			
			$html .= '<table border="1" cellspacing="3" cellpadding="4">
				<tr><th  colspan="2">Files Uploaded</th></tr>
				<tr><td>
				<a href="../../uploads/'.$nikal["File"].'" download><img src="../../uploads/'.$File.'" style="width:100px;"/> </a></td>
				</tr></table>';
			}

		  $html .= '<table border="1" cellspacing="3" cellpadding="4">
			<tr><th  colspan="2"">Utility Connections</th></tr>
			<tr><td style="width:100% !important;color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;">';
			 if($row["Utility"] == "No"){
				 $html .= 'This applicant WOULD NOT like to be contacted about utilities.To integrate your utility 
				connection provider please contact us.';
			}
			else {
				$html .= 'Required';
			}
				
				$html .='</td></tr>
		</table>';
        if ($row4["Student"] !="") {
				$html .= '<table border="1" cellspacing="3" cellpadding="4">
					<tr><th  colspan="2">Is A Student?</th></tr>
					<tr><td style="color:#5d77b3 !important;font-weight:bold;font-size:16px;-webkit-print-color-adjust: exact;"><?php if(dboutput($row4["Student"])=="Y"){ echo "Yes"; } else{ echo "No"; } ?></td></tr>
				  </table>';
				} 
		 if($row["Occupants"] > 0) {
		$html .='<table border="1" cellspacing="3" cellpadding="4">
			<tr><th  colspan="2">Other Occupants</th></tr>';
			
				$result6=mysqli_query($dbh,"Select * FROM app_occupants Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
				while($row6=mysqli_fetch_array($result6)){
			
				$html .='<tr>
					<td>Name: </td>
					<td>'.dboutput($row6["Name"]).'</td>
				</tr>
				<tr>
					<td>Phone: </td>
					<td>'.dboutput($row6["Mobile"]).'</td>
				</tr>
				<tr>
					<td>Relationship: </td>
					<td>'.dboutput($row6["Relationship"]).'</td>
				</tr>
				<tr>
					<td>Age: </td>
					<td>'.dboutput($row6["Age"]).'</td>
				</tr>';
				
					if($row6["Age"] > 18){
					$html .='<tr>
						<td>On the lease? </td>
						<td>'.dboutput($row6["Lease"]).'</td>
					</tr>';
						if($row6["Lease"] == "Yes"){
						
							$html .='<tr>
								<td>Email </td>
								<td>'.dboutput($row6["Email"]).'</td>
							</tr>
							<tr>
								<td>Contact No </td>
								<td>'.dboutput($row6["Mobile"]).'</td>
							</tr>';
						}
					}
				}
		$html .='</table>';
		} 
		 if($row["Vehicles"] > 0) {
		$html .='<table class="table_half">
			<tr><th  colspan="2">Vehicles</th></tr>';
			
				$result7=mysqli_query($dbh,"Select * FROM app_vehicles Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
				while($row7=mysqli_fetch_array($result7)){
			
				$html .='<tr>
					<td>Type: </td>
					<td>'.dboutput($row7["Type"]).'</td>
				</tr>
				<tr>
					<td>Model: </td>
					<td>'.dboutput($row7["Model"]).'</td>
				</tr>
				<tr>
					<td>Registration: </td>
					<td>'.dboutput($row7["Registration"]).'</td>
				</tr>';
					}
		$html .='</table>';
		 } 

		if ($row["Pets"] > 0) { 
		$html .='<table class="table_half">
			<tr><th  colspan="2">Pets</th></tr>';
				$result8=mysqli_query($dbh,"Select * FROM app_pets Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
				while($row8=mysqli_fetch_array($result8)){
				$html .='<tr>
					<td>Type: </td>';
						if($row8["Other"] == ""){
					
					$html .='<td>'.dboutput($row8["Type"]).'</td>';
					 } else{  
					$html .='<td>'.dboutput($row8["Type"]).'</td>';
						}
				$html .='</tr>
				<tr>
					<td>Breed: </td>
					<td>'.dboutput($row8["Breed"]).'</td>
				</tr>';
				}
		$html .='</table>';
		 } 
                $html .='<table class="table_half">
                <tr><th colspan="2">Other Details</th></tr>
                <tr>
					<td class="td90">Applications pending on other properties?</td>
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q1"]).'</span></td>
                </tr>
				 <tr>
					<td class="td90">Have you ever been refused a rental property?</td>
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q2"]).'</span></td>
                 </tr>
				 <tr>
					<td class="td90">Are you in debt to another landlord or agent?</td>
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q3"]).'</span></td>
                 </tr>
				 <tr>
					<td class="td90">Deductions ever taken from your Bond ?</td>
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q4"]).'</span></td>
                 </tr>
				  <tr>
					<td class="td90">Anything affecting future rental payments?</td>
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q5"]).'</span></td>
                  </tr>
				  <tr>
					<td class="td90">Considering buying a property soon?</td> 
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q6"]).'</span></td>
                  </tr>
				  <tr>
					<td class="td90">Do you currently own a property?</td>
					<td class="td10"><span class="darkblue">'.dboutput($row5["Q7"]).'</span></td>
                  </tr>
			  </table>            
          <table class="table_half">
			<tr><th  colspan="2">References</th></tr>
			<tr>
				<td>Personal Ref.</td>
				<td>'.dboutput($row4["PerRef"]).'</td>			
			</tr>
			<tr>
				<td>Phone</td>
				<td>'.dboutput($row4["PerPh"]).'</td>			
			</tr>
			<tr>
				<td>Relationship</td>
				<td>'.dboutput($row4["PerRel"]).'</td>			
			</tr>
			<tr>
				<td>Email</td>
				<td>'.dboutput($row4["PerEmail"]).'</td>			
			</tr>
			<tr>
				<td>Personal Ref. 1</td>
				<td>'.dboutput($row4["RefName1"]).'</td>			
			</tr>
			<tr>
				<td>Phone</td>
				<td>'.dboutput($row4["1PhnNo"]).'</td>			
			</tr>
			<tr>
				<td>Relationship</td>
				<td>'.dboutput($row4["1Relation"]).'</td>			
			</tr>
			<tr>
				<td>Email</td>
				<td>'.dboutput($row4["1Email"]).'</td>			
			</tr>
			<tr>
				<td>Personal Ref. 2</td>
				<td>'.dboutput($row4["RefName2"]).'</td>			
			</tr>
			<tr>
				<td>Phone</td>
				<td>'.dboutput($row4["2PhnNo"]).'</td>			
			</tr>
			<tr>
				<td>Relationship</td>
				<td>'.dboutput($row4["2Relation"]).'</td>			
			</tr>
			<tr>
				<td>Email</td>
				<td>'.dboutput($row4["2Email"]).'</td>			
			</tr>
         </table>';

echo $html;
