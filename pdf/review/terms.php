<?php
include("../../admin/functions.php");
if(!isset($_SESSION['ADMIN']) || $_SESSION["ADMIN"] == false){
	if(!isset($_SESSION['ManagerID']) || $_SESSION["ManagerID"] == false){
		if(!isset($_SESSION['TenantID']) || $_SESSION["TenantID"] == false){
			redirect("../../index.php");
		}
	}
}
include '../../includes/tenancy-terms.php';
require_once('tcpdf_include.php');
$msg="";
$ID=0;

if(isset($_REQUEST["ID"]))
		$AppGUID=trim($_REQUEST["ID"]);
	
$row=mysqli_fetch_assoc(mysqli_query($dbh,"SELECT TenantID, DATE_FORMAT(DateAdded, '%D %b %Y <br/> %l:%i %p') AS DateAdd  FROM application WHERE 
GUID='".$AppGUID."'"));
$TenantID=$row["TenantID"];

$result4=mysqli_query($dbh,"Select Fname,Lname FROM tenant_profile Where UserID=".(int)$TenantID) or die('Failed due to:'.mysqli_error($dbh));
$row4 = mysqli_fetch_array($result4);
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tenancy Application');
$pdf->SetTitle('Review Application');
$pdf->SetSubject('PDF View');
$pdf->SetKeywords('Tenancy, PDF, Application');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------



$signature = TCPDF_FONTS::addTTFfont('../../fonts/signature/allura-regular.ttf', 'TrueTypeUnicode', '', 96);
$pdf->SetFont($signature, '', 25, '', false);

// set font
$pdf->SetFont('dejavusans', '', 10);


// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content

		
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->AddPage();
		// convert TTF font to TCPDF format and store it on the fonts folder
		
		$html ='<table>
				<tr>
					<td colspan="2">
						<h3>Terms and Conditions</h3>
						<p><img src="../../img/checkbox.png" alt="checkbox" width="15px;"> I acknowledge that I have Read, Understood and Agree with the Tenancy Privacy Statement / Collection Notice & Tenant Declaration </p>
					</td>
				</tr>
				<tr>
					<td style="width:75%;">
						<p>
					<br/><span style="font-family:'.$signature.'; font-size:30px; font-weight:bold">'. $row4["Fname"] .' '. $row4["Lname"].'</span>
						<br/>(Digital representation of tenants signature, approved by tenant)<br/></p>
					</td>
					<td style="width:25%;">
						<p><br/>'.$row["DateAdd"].'<br/>(Date Signed)</p>   
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p class="praone">'. $row4["Fname"] .' '. $row4["Lname"].' Acknowledges that they have Read, Understood and Agree with the following Terms and Conditions </p>
						<p class="pratwo">'. $row4["Fname"] .' '. $row4["Lname"].' also acknowledges that the agent in question cannot confirm that any phone lines to the property are operable or able to be reconnected. '. $row4["Fname"] .' '. $row4["Lname"].' understands that it is his/her responsibility to check with the telephone provider before proceeding with the tenancy to confirm the situation with the telephone line. Ensuring the main switch is in the off position for power connection remains the responsibility of the tenant. 
						Please note that the tenant listed above has submitted their application using our online tenancy application system tenancyapplication.com.au. All tenants that use our system are made to electronically sign off on a Tenancy Privacy Statement / Collection Notice and a Tenant Declaration and our system is fully compliant with all industry standards and regulations. This act of electronically signing the form does allow third parties to release details of the tenant listed above for the purposes of processing a tenancy application under the Privacy Act. </p>
						'
						.$tenancy_terms.
						'
					</td>
				</tr>
			</table>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();
 

//Close and output PDF document
$pdf->Output('signed_terms.pdf', 'I');
$pdf->Output("/home/tenancyapplicati/public_html/pdf/review/data/signed_terms.pdf", "F"); 


//============================================================+
// END OF FILE
//============================================================+
