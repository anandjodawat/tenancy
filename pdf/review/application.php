<?php
include("../../admin/functions.php");
if(!isset($_SESSION['TENANCY_ADMIN']) || $_SESSION["TENANCY_ADMIN"] == false){
    if(!isset($_SESSION['ManagerID']) || $_SESSION["ManagerID"] == false){
        if(!isset($_SESSION['TenantID']) || $_SESSION["TenantID"] == false){
            redirect("../../index.php");
        }
    }
}
include '../../includes/tenancy-terms.php';
require_once('tcpdf_include.php');
$msg="";
$ID=0;
$rguid = mt_rand(100000, 999999);
if(isset($_REQUEST["ID"]))
    $AppGUID=trim($_REQUEST["ID"]);

$ID=mysqli_fetch_array(mysqli_query($dbh,"SELECT ID FROM application WHERE GUID='".$AppGUID."'"));
$AppID=$ID['ID'];
$result=mysqli_query($dbh,"Select *, DATE_FORMAT(DateAdded, '%D %b %Y - %l:%i %p') AS DateAdd FROM application Where ID=".(int)$AppID." AND Status != 0 ") or die('Failed due to:'.mysqli_error($dbh));
$row = mysqli_fetch_array($result);
$TenantID=$row["TenantID"];
$Address=$row["Address"];

$fahad = mysqli_query($dbh,"SELECT TenantID FROM application WHERE ID='".$AppID."'");
$rowF = mysqli_fetch_array($fahad);
$TID = $rowF["TenantID"];

$result2=mysqli_query($dbh,"Select * FROM users Where ID=".(int)$TenantID) or die('Failed due to:'.mysqli_error($dbh));
$row2 = mysqli_fetch_array($result2);

$result3=mysqli_query($dbh,"Select * FROM app_inspection Where Application=".(int)$AppID) or die('Failed due to:'.mysqli_error($dbh));
$row3 = mysqli_fetch_array($result3);
$manager=[];

$manageLogo='';
$propertyManager=mysqli_fetch_array(mysqli_query($dbh,"SELECT PM_Email as managerEmail FROM application WHERE ID='".$AppID."'"));
$propertyManagerID=mysqli_fetch_array(mysqli_query($dbh,"SELECT ID as managerID FROM users WHERE Email='".$propertyManager['managerEmail']."'"));
$propertyManagerLogo=mysqli_fetch_array(mysqli_query($dbh,"SELECT OfficeLogo as managerLogo FROM manager_profile WHERE UserID='".$propertyManagerID['managerID']."'"));


//$manageLogo='<img src="../../uploads/manager/officelogo/'.$propertyManagerLogo['managerLogo'].'" width="100" />';
if(array_key_exists('managerLogo',$propertyManagerLogo))
{

    $manageLogo='<img src="../../uploads/manager/officelogo/'.$propertyManagerLogo['managerLogo'].'" height="50" />';

}

if($row3["Inspected"]==1){
    $rowIns="I declare that I have physically inspected the inside of this property";
}
else if($row3["Inspected"]==2){
    $rowIns="I have not yet inspected this property";
}
else if($row3["Inspected"]==3){
    $rowIns="I am currently interstate/overseas and will physically inspect the property upon my arrival";
}
else if($row3["Inspected"]==4){
    $rowIns="An associate/other occupant has inspected the property on my behalf";
}
else if($row3["Inspected"]==5){
    $rowIns="I am currently interstate/overseas but accept the property in its current state";
}
else{
    $rowIns="";
}

$result4=mysqli_query($dbh,"Select * FROM tenant_profile Where UserID=".(int)$TenantID) or die('Failed due to:'.mysqli_error($dbh));
$row4 = mysqli_fetch_array($result4);



if($row4["Smoke"] == 'Y'){ $Smoke="Yes";}else{ $Smoke="No"; }
if($row4["LSmoke"] == 'Y'){ $LSmoke="Yes";}else{ $LSmoke="No"; }
if($row4["Student"] == 'Y'){ $Student="Yes";}else{ $Student="No"; }
if($row4["Pension"] == 'Y'){ $Pension="Yes";}else{ $Pension="No"; }

if($result){
    $result5=mysqli_query($dbh,"Select * FROM app_questions Where Application=".(int)$AppID) or die('Failed due to:'.mysqli_error($dbh));
    $row5 = mysqli_fetch_array($result5);
}



if(dboutput($row4["LivingArr"])== 'Renting - Through an Agent'){
    $KisKaNaam="Agent's Name";
}
else if(dboutput($row4["LivingArr"])== 'Renting - Through a private landlord'){
    $KisKaNaam="Landlord's Name";
}
else if(dboutput($row4["LivingArr"])== 'With Parents'){
    $KisKaNaam="Parents's Name";
}
else if(dboutput($row4["LivingArr"])== 'Sharing'){
    $KisKaNaam="Partner's Name";
}
else{
    $KisKaNaam="Name";
}
ob_start();
// create new PDF document
$pdf = new TCPDF('P', 'px', 'A4', true, 'UTF-8', false);
$colWidth=650;
$margin=30;
$pagetopmargin = 45;
$pagefootermargin = 20;
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tenancy Application');
$pdf->SetTitle('Review Application');
$pdf->SetSubject('PDF View');
$pdf->SetKeywords('Tenancy, PDF, Application');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, 100, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, -10, 0);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetTopMargin($pagetopmargin);
$pdf->SetFooterMargin($pagefootermargin);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------



$signature = TCPDF_FONTS::addTTFfont('../../fonts/signature/allura-regular.ttf', 'TrueTypeUnicode', '', 96);
$pdf->SetFont($signature, '', 25, '', false);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// set font
$pdf->SetFont('dejavusans', '', 10);

$pdf->AddPage();

// define some HTML content with style

// output the HTML content
$pdf->writeHTMLCell(0,0,0,300,'<div style="width:100%" align="center"><img src="https://www.tenancyapplication.com.au/admin/assets/images/logo-side.png" alt=""><div style="background-color: purple;color:#f5f5f5;font-size:30px;">Residential Tenancy Application<br><span style="font-size:11px">www.tenancyapplication.com.au</span></div> </div> ');

//$name='';
//$pdf->writeHTMLCell(500,232,$margin,240,$name);
$page1Bottom='<table cellpadding="5" cellspacing="2">
<tr>
<td>'.$manageLogo.'</td>
</tr>
<tr>
	<td style="width:320px;height:17px;" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >AGENT DETAILS</p></td>	
</tr>

<tr>
<td style="width:320px;height:22px;background-color: #f1f1f2;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Agency Name: '.dboutput($row["Agency"]).'</p></td>
</tr>
<tr>
<td style="width:320px;height:22px;background-color: #f1f1f2;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Property Manager: '.dboutput($row["PM_Name"]).'</p></td>
</tr>
<tr>
<td style="width:320px;height:22px;background-color: #f1f1f2;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Email: '.dboutput($row["PM_Email"]).'</p></td>
</tr>
<tr>
<td style="width:320px;height:22px;background-color: #f1f1f2;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Recieved: '.dboutput($row["DateAdd"]).'</p></td>
</tr>
<tr>
<td style="width:320px;height:22px;background-color: #f1f1f2;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Application ID: '.dboutput($row["ID"]).'</p></td>
</tr>
</table>';

$pdf->writeHTMLCell(200,100,$margin,640,$page1Bottom);
//$pdf->writeHTMLCell(100,0,495,10,$manageLogo,0,0,false,true,'right center');
$pdf->SetPrintHeader(true);
$pdf->SetPrintFooter(true);
//second page
$pdf->AddPage();

//$pdf->writeHTMLCell(235,232,495,10,$manageLogo);
$propertyDetails='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >PROPERTY DETAILS</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Address: &nbsp;'.dboutput($row["Address"]).', '.dboutput($row["Suburb"]).', '.dboutput($row["State"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width: 40%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >PostCode: &nbsp;'.dboutput($row["Postcode"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width: 60%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Property Type: &nbsp;'.dboutput($row["PropType"]).'</p></td>
	<td></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >No. of Bedroom: &nbsp;'.dboutput($row["Bedrooms"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Bond: &nbsp; $'.dboutput($row["Bond"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;width: 20%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Rent: &nbsp;</p></td>
	<td style="height:22px;background-color: #f1f1f2;width: 40%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10" alt=""/>&nbsp;Weekly $'. dboutput($row["WeeklyRent"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width: 40%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10" alt=""/>&nbsp;Monthly $'. dboutput($row["MonthlyRent"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Commencement Date: &nbsp;'. date('d-m-Y',strtotime(dboutput($row["CommDate"]))).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Preferred Lease Term: &nbsp;'. dboutput($row["LeaseLength"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >No. of Occupants: &nbsp;'.dboutput($row["Occupants"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >No. of Vehicles: &nbsp;'.dboutput($row["Vehicles"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >No. of Pets: &nbsp;'. dboutput($row["Pets"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">How did you find out about this property? : &nbsp;<br>'. dboutput($row4["2Relation"]).'</p></td>

</tr>
</table>';
$personalDetails='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >PERSONAL DETAILS</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Name: &nbsp;'.dboutput($row4["Title"])." ".dboutput($row4["Fname"])." ".dboutput($row4["Mname"])." ".dboutput($row4["Lname"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Gender: &nbsp;'.dboutput($row4["Gender"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Date of Birth: &nbsp;'.date('d-m-Y',strtotime(dboutput($row4["DOB"]))).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Home Phone: &nbsp;'.dboutput($row4["Mnumber"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Work Phone: &nbsp;'.dboutput($row4["Hnumber"]).'</p></td>

</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Mobile: &nbsp;'.dboutput($row4["Mobile"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width: 50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Fax: &nbsp;'.dboutput($row4["Fax"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Email: &nbsp;'. dboutput($row2["Email"]).'</p></td>

</tr>

</table>';


$emergencyContact='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >EMERGENCY CONTACT</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Name: &nbsp;'.dboutput($row4["NameEC"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Relationship: &nbsp;'.dboutput($row4["ECRelation"]).'</p></td>

</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Contact Address: &nbsp;'.dboutput($row4["AddEC"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Home Phone: &nbsp;'.dboutput($row4["MAcon"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Work Phone: &nbsp;'.dboutput($row4["E_phone"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Mobile: &nbsp;'.dboutput($row4["E_mobile"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Email: &nbsp;'.dboutput($row4["E_email"]).'</p></td>

</tr>

</table>';
$selectUtilites=explode(',',$row['Connection']);
$selectUtilites=array_map('trim',$selectUtilites);

$utilites='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >UTILITIES CONNECTION</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Selecting the right utility provider can make your move easier and may save you money.<br/>
                            Would you like a call closer to your moving date to help arrange connection to your household services? You\'ll get a phone call,
                            email and help to compare and select a plan from available suppliers.</p></td>
</tr>

<tr>';

if(in_array('Electricity',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Electricity</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Electricity</p></td>';
}
if(in_array('Gas',$selectUtilites))
{

    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Gas</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Gas</p></td>';
}
if(in_array('Telephone',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Telephone</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Telephone</p></td>';
}
$utilites.='</tr>';
$utilites.='<tr>';
if(in_array('Internet',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Internet</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Internet</p></td>';
}
if(in_array('Bottled gas',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Bottled gas</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Bottled gas</p></td>';
}
if(in_array('Pay TV',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Pay TV</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Pay TV</p></td>';
}
$utilites.='</tr>';
$utilites.='<tr>';
if(in_array('Disconnections',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Disconnections</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Disconnections</p></td>';
}
if(in_array('Removalist',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Removalist</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Removalist</p></td>';
}
if(in_array('Vehicle hire',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Vehicle hire</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Vehicle hire</p></td>';
}
$utilites.='</tr>';
$utilites.='<tr>';
if(in_array('Cleaning services',$selectUtilites))
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/tick-check-box-md.png" width="10">&nbsp;Cleaning services</p></td>';
}
else
{
    $utilites.='<td style="height:22px;background-color: #f1f1f2;"  colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" ><img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Cleaning services</p></td>';
}

$utilites.='</tr></table>';


$currentAddress='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >CURRENT ADDRESS DETAILS</p></td>

</tr>';
if($row4['LivingArr']=='Other')
{
    $currentAddress.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">What are your current living arrangements?: <br>'.dboutput($row4["LivingArr"]).'</p></td>
</tr>';
}
else
{
    $currentAddress.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">What are your current living arrangements?: <br>'.dboutput($row4["LivingArr"]).'</p></td>
</tr>';
}
$currentAddress.='<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Unit:&nbsp;'.dboutput($row4["Unit"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Street:&nbsp;'.dboutput($row4["Street"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Street Name:&nbsp;'.dboutput($row4["StreetN"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Street Type:&nbsp;'.dboutput($row4["StreetT"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Suburb:&nbsp;'.dboutput($row4["Suburb"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">State:&nbsp;'.dboutput($row4["State"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">PostCode:&nbsp;'.dboutput($row4["PostCode"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">When did you move in?:&nbsp;'.dboutput($row4["DateMoveIn"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Reason for leaving:<br>'.dboutput($row4["ReasonFL"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Landlord\'s Name:&nbsp;'.dboutput($row4["Landlord_Name"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Monthly Rent:&nbsp;$'.dboutput($row4["Monthly_Rent"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:&nbsp;'.dboutput($row4["Phone"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Email:&nbsp;'.dboutput($row4["Email"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Was your bond fully refunded?:&nbsp;'.dboutput($row4["Refund"]).'</p></td>

</tr>';
if($row4['CurrPostDiff']=='Yes')
{
    $currentAddress.='
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Is your postal address different?:&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No</p></td>

</tr>';
}
else{
    $currentAddress.='
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Is your postal address different?:&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No</p></td>

</tr>';

}
$currentAddress.='
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Post Address:&nbsp;'.dboutput($row4['CurrPostAdd']).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Post Suburb:&nbsp;'.dboutput($row4['CurrPostSub']).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Post State:&nbsp;'.dboutput($row4['CurrPostState']).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">PostCode:&nbsp;'.dboutput($row4['CurrPostCode']).'</p></td>

</tr>';

$currentAddress.='</table>';


$previousAddress='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >PREVIOUS ADDRESS DETAILS</p></td>

</tr>';
if($row4['LivingArr']=='Other')
{
    $previousAddress.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">What was your previous living arrangements?: <br>'.dboutput($row4["PrAdd"]).'</p></td>
</tr>';
}
else
{
    $previousAddress.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">What was your previous living arrangements?: <br>'.dboutput($row4["PrAdd"]).'</p></td>
</tr>';
}
$previousAddress.='<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Unit:&nbsp;'.dboutput($row4["PrUnit"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Street:&nbsp;'.dboutput($row4["PrStreet"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Street Name:&nbsp;'.dboutput($row4["PrStreetN"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Street Type:&nbsp;'.dboutput($row4["PrStreetT"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Suburb:&nbsp;'.dboutput($row4["PrSuburb"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">State:&nbsp;'.dboutput($row4["PrState"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:33.33%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">PostCode:&nbsp;'.dboutput($row4["PrPostCode"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">From Date:&nbsp;'.dboutput($row4["PrFrmDate"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%" colspan="2"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">To Date:&nbsp;'.dboutput($row4["PrToDate"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Reason for leaving:<br>'.dboutput($row4["PrReasonFL"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Agent\'s Name:&nbsp;'.dboutput($row4["PrAgName"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Monthly Rent:&nbsp;$'.dboutput($row4["PrMR"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:&nbsp;'.dboutput($row4["PrPhone"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Email:&nbsp;'.dboutput($row4["PrEmail"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Was your bond fully refunded?:&nbsp;'.dboutput($row4["PrBond"]).'</p></td>

</tr>';
if($row4['PPrextra']=='Yes')
{
    $previousAddress.='
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Additional information about your living arrangements?:&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>Detail:<br>'.dboutput($row4['PrExtra']).'</p></td>

</tr>';
}
else{
    $previousAddress.='
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Additional information about your living arrangements?:&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No<br>Detail:<br>'.dboutput($row4['PrExtra']).'</p></td>

</tr>';

}

$previousAddress.='</table>';

$currentEmployment='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >CURRENT EMPLOYMENT DETAILS</p></td>

</tr>';

$currentEmployment.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">What is your current work situation?: <br>'.dboutput($row4["EmCurrWork"]).'</p></td>
</tr>';
if($row4['EmCurrWork']=='I am currently employed'):
    $currentEmployment.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Company Name:&nbsp;'.dboutput($row4["Emcompany_name"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Manager/Contact Name:&nbsp;'.dboutput($row4["EmManager_name"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:&nbsp;'.dboutput($row4["EmPhone"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Email:&nbsp;'.dboutput($row4["EmEmail"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Current Address:&nbsp;'.dboutput($row4["EmCurr_address"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Industry:&nbsp;'.dboutput($row4["Emindustry"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Occupation/Position:&nbsp;'.dboutput($row4["EmOccup_Post"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Nature of Employment:&nbsp;'.dboutput($row4["EmNOE"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">When did you start?:&nbsp;'.dboutput($row4["EmDatestart"]).'</p></td>
</tr>';
    if($row4['Emjob']=='still'):
        $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px"><img src="../../images/tick-check-box-md.png" alt="" width="10"/>&nbsp;Still in this job &nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10" alt=""/>&nbsp;Finished this job</p></td>
</tr>';
    else:
        $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px"><img src="../../images/checkbox-unchecked-md.png" alt="" width="10"/>&nbsp;Still in this job &nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10" alt=""/>&nbsp;Finished this job: Date:'.dboutput($row4['EmDatefinish']).'</p></td>
</tr>';

    endif;
    $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Gross Annual Salary Before Tax:'.dboutput($row4['EmGross']).'</p></td>
</tr>';
endif;

if($row4['EmCurrWork']=='I run my own buisness'):
    $currentEmployment.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Company Name:&nbsp;'.dboutput($row4["mycompany_name"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Industry:&nbsp;'.dboutput($row4["myindustry"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Occupation/Position:&nbsp;'.dboutput($row4["myOccup_Post"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Nature of Employment:&nbsp;'.dboutput($row4["myNOE"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">When did you start?:&nbsp;'.dboutput($row4["myDatestart"]).'</p></td>
</tr>';
    if($row4['myjob']=='still'):
        $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px"><img src="../../images/tick-check-box-md.png" alt="" width="10"/>&nbsp;Still in this job &nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10" alt=""/>&nbsp;Finished this job</p></td>
</tr>';
    else:
        $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px"><img src="../../images/checkbox-unchecked-md.png" alt="" width="10"/>&nbsp;Still in this job &nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10" alt=""/>&nbsp;Finished this job: Date:'.dboutput($row4['myDatefinish']).'</p></td>
</tr>';

    endif;
    $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Gross Annual Salary Before Tax:'.dboutput($row4['myGross']).'</p></td>
</tr>';
    $currentEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Date Company Est.:'.dboutput($row4['myDateEST']).'</p></td>
</tr>
 <tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">ACN:'.dboutput($row4['myACN']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">ABN:'.dboutput($row4['myABN']).'</p></td>
</tr>
 <tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Accountant\'s Name.:'.dboutput($row4['myAccountant_name']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:'.dboutput($row4['A_Phone']).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Lawyer\'s Name.:'.dboutput($row4['myLawyer']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:'.dboutput($row4['L_Phone']).'</p></td>
</tr>';
endif;


$currentEmployment.='</table>';

$previousEmployment='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >PREVIOUS EMPLOYMENT DETAILS</p></td>

</tr>';

$previousEmployment.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">What was your previous work?: <br>'.dboutput($row4["EmPrWork"]).'</p></td>
</tr>';
if($row4['EmPrWork']=='I was previously employed'):
    $previousEmployment.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Company Name:&nbsp;'.dboutput($row4["Pre_company_name"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Manager/Contact Name:&nbsp;'.dboutput($row4["Pre_Manager_name"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:&nbsp;'.dboutput($row4["Pre_Phone"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Email:&nbsp;'.dboutput($row4["Pre_Email"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Current Address:&nbsp;'.dboutput($row4["Pre_Curr_address"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Industry:&nbsp;'.dboutput($row4["Pre_industry"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Occupation/Position:&nbsp;'.dboutput($row4["Pre_Occup_Post"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Nature of Employment:&nbsp;'.dboutput($row4["Pre_NOE"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">When did you start?:&nbsp;'.dboutput($row4["Pre_Datestart"]).'</p></td>
</tr>';

    $previousEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">When did you finish?:'.dboutput($row4['Pre_Datefinish']).'</p></td>
</tr>';


    $previousEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Gross Annual Salary Before Tax:&nbsp;'.dboutput($row4['Pre_Gross']).'</p></td>
</tr>';
endif;

if($row4['EmPrWork']=='I previously ran my own business'):
    $previousEmployment.='<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Company Name:&nbsp;'.dboutput($row4["pre_mycompany_name"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Industry:&nbsp;'.dboutput($row4["pre_myindustry"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Occupation/Position:&nbsp;'.dboutput($row4["pre_myOccup_Post"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Nature of Employment:&nbsp;'.dboutput($row4["pre_myNOE"]).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">When did you start?:&nbsp;'.dboutput($row4["pre_myDatestart"]).'</p></td>
</tr>';
    if($row4['pre_myjob']=='still'):
        $previousEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px"><img src="../../images/tick-check-box-md.png" alt="" width="10"/>&nbsp;Still in this job &nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10" alt=""/>&nbsp;Finished this job</p></td>
</tr>';
    else:
        $previousEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px"><img src="../../images/checkbox-unchecked-md.png" alt="" width="10"/>&nbsp;Still in this job &nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10" alt=""/>&nbsp;Finished this job: Date:'.dboutput($row4['pre_myDatefinish']).'</p></td>
</tr>';

    endif;
    $previousEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Gross Annual Salary Before Tax:&nbsp;'.dboutput($row4['pre_myGross']).'</p></td>
</tr>';
    $previousEmployment.='
    <tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Date Company Est.:&nbsp;'.dboutput($row4['pre_myDateEST']).'</p></td>
</tr>
 <tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">ACN:&nbsp;'.dboutput($row4['pre_myACN']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">ABN:&nbsp;'.dboutput($row4['pre_myABN']).'</p></td>
</tr>
 <tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Accountant\'s Name.:<br>'.dboutput($row4['pre_myAccountant_name']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:&nbsp;'.dboutput($row4['pre_A_Phone']).'</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Lawyer\'s Name.:<br>'.dboutput($row4['pre_myLawyer']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px">Phone:&nbsp;'.dboutput($row4['pre_L_Phone']).'</p></td>
</tr>';
endif;


$previousEmployment.='</table>';

$personalRef='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >PERSONAL REFERENCES</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Someone who knows you well who will not be living with you</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Personal Reference: &nbsp;'.dboutput($row4["PerRef"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Occupation: &nbsp;'.dboutput($row4["PerOcc"]).'</p></td>

</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Relationship: &nbsp;'.dboutput($row4["PerRel"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%;"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Phone: &nbsp;'.dboutput($row4["PerPh"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Mobile: &nbsp;'.dboutput($row4["PerMob"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Email: &nbsp;'.dboutput($row4["PerEmail"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Address: &nbsp;'.dboutput($row4["PerAdd"]).'</p></td>

</tr>


</table>';
$professionalRef='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >PROFESSIONAL REFERENCES</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Work colleagues, associates, etc</p></td>
</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Name of Professional Reference 1: &nbsp;'.dboutput($row4["RefName1"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Company Name: &nbsp;'.dboutput($row4["RefCName1"]).'</p></td>

</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Relationship: &nbsp;'.dboutput($row4["1Relation"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%;"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Phone: &nbsp;'.dboutput($row4["1PhnNo"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Mobile: &nbsp;'.dboutput($row4["1MobNo"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Email: &nbsp;'.dboutput($row4["1Email"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Company Address: &nbsp;'.dboutput($row4["1Address"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Name of Professional Reference 2: &nbsp;'.dboutput($row4["RefName2"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Company Name: &nbsp;'.dboutput($row4["RefCName2"]).'</p></td>

</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Relationship: &nbsp;'.dboutput($row4["2Relation"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%;"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Phone: &nbsp;'.dboutput($row4["2PhnNo"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Mobile: &nbsp;'.dboutput($row4["2MobNo"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Email: &nbsp;'.dboutput($row4["2Email"]).'</p></td>

</tr>
<tr>
	<td style="height:22px;background-color: #f1f1f2;" colspan="3" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Company Address: &nbsp;'.dboutput($row4["2Address"]).'</p></td>

</tr>


</table>';

$income='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >INCOME DETAILS</p></td>

</tr><tr>
  <td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >All Figures entered below are:&nbsp;'.dboutput($row4['below']).'</p></td>

</tr><tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Salary/Wages: &nbsp;'.dboutput($row4["sal"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Self Employed: &nbsp;'.dboutput($row4["selfemp"]).'</p></td>
</tr>

<tr>
	<td style="height:22px;background-color: #f1f1f2; width:50%;"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Benefits: &nbsp;'.dboutput($row4["benefit"]).'</p></td>
	<td style="height:22px;background-color: #f1f1f2; width:50%;" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Others: &nbsp;'.dboutput($row4["other"]).'</p></td>

</tr>

</table>';
$expense='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;">
<tr>
	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >EXPENSE DETAILS</p></td>

</tr><tr>
  <td style="height:22px;background-color: #f1f1f2;" colspan="3"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Current Weekly Rent:&nbsp;'.dboutput($row4['Tamount']).'</p></td>

</tr><tr>
	<td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Vehicle Information: &nbsp;'.dboutput($row4["car"]).'&nbsp;&nbsp;$&nbsp;'.dboutput($row4['Tvalue']).'</p></td>
	<td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Others: &nbsp;'.dboutput($row4["Eother"]).'</p></td>
</tr>

</table>';
$SelectU=mysqli_query($dbh,"Select * FROM tenant_image WHERE UserID=".$TenantID."");

$rowU=mysqli_fetch_array($SelectU);
if($rowU['Image']):
    $image1='<div style="width:650px" >
<p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px;" >Primary/Photo ID:</p>
<img src="../../uploads/photos/'.$rowU["Image"].'" style="width:650px">
</div>';

endif;
if($rowU['Image2']):
    $image2='<div style="width:650px" >
<p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px;" >Secondary ID:</p>
<img src="../../uploads/secondary/'.$rowU["Image2"].'" style="width:650px">
</div>';

endif;
if($rowU['Image3']):
    $image3='<div style="width:650px" >
<p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px;" >Rental History/Proof of Address:</p>
<img src="../../uploads/history/'.$rowU["Image3"].'" style="width:650px">
</div>';

endif;
if($rowU['Image4']):
    $image4='<div style="width:650px" >
<p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px;" >Employment/Proof of Income:</p>
<img src="../../uploads/employments/'.$rowU["Image4"].'" style="width:650px">
</div>';

endif;
//
//$identification='<table cellpadding="5" cellspacing="3" style="font-size:11px;font-family:Arial;width:650px;">
//<tr>
//	<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >SUPPORTING DOCUMENTS</p></td>
//
//</tr>
//
//<tr>
//  <td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Primary/Photo ID:<br><img src="../../uploads/photos/'.$rowU["Image"].'" style="width:320px"></p></td>
//
//
//  <td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Secondary ID:<br><img src="../../uploads/secondary/'.$rowU["Image2"].'" style="width:320px"></p></td>
//
//</tr>
//<tr>
//  <td style="height:22px;background-color: #f1f1f2;width:50%" ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Rental History/Proof of Address:<br><img src="../../uploads/history/'.$rowU["Image3"].'" style="width:320px"></p></td>
//
//
//  <td style="height:22px;background-color: #f1f1f2;width:50%" colspan="2"><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Employment/Proof of Income:<br><img src="../../uploads/employments/'.$rowU["Image4"].'" style="width:320px"></p></td>
//
//</tr>
//</table>';
$page1='<table cellpadding="5" cellspacing="2" style="font-size:11px;font-family:Arial;">
<tr>
<td style="width: 320px;">'.$propertyDetails.$utilites.$previousAddress.'</td>
<td style="width: 320px">'.$personalDetails.$emergencyContact.$currentAddress.'</td>
</tr>


</table>';
$pdf->writeHTMLCell($colWidth,300,$margin,$pagetopmargin,$page1);

$page2='<table cellpadding="5" cellspacing="2" style="font-size:11px;font-family:Arial;">

<tr>
<td style="width: 320px;">'.$personalRef.$professionalRef.$income.$expense.'</td>
<td style="width: 320px;">'.$currentEmployment.$previousEmployment.'</td>
</tr>

</table>';
$pdf->AddPage();
//$pdf->writeHTMLCell(235,232,495,10,$manageLogo);
$pdf->writeHTMLCell($colWidth,300,$margin,$pagetopmargin,$page2);
if($rowU['Image']):
    $pdf->AddPage();

    $pdf->writeHTMLCell(650,0,$margin,$pagetopmargin,$image1);
endif;
if($rowU['Image2']):
    $pdf->AddPage();
    $pdf->writeHTMLCell(650,0,$margin,$pagetopmargin,$image2);
endif;
if($rowU['Image3']):
    $pdf->AddPage();
    $pdf->writeHTMLCell(650,0,$margin,$pagetopmargin,$image3);
endif;
if($rowU['Image4']):
    $pdf->AddPage();
    $pdf->writeHTMLCell(650,0,$margin,$pagetopmargin,$image4);
endif;


$questions='<table cellpadding="5" cellspacing="2" style="font-size:11px;width:650px;">
<tr>
<td style="height:17px;" colspan="3" ><p style="font-size:11px;font-family:Arial;color: #6d6e71;line-height: 14px; " >FURTHER QUESTIONS</p></td>
</tr>
<tr>';
if($row5["Q1"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:320px"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Has your tenancy ever been terminated by a landlord or agent?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q1']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:320px"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Has your tenancy ever been terminated by a landlord or agent?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='<td style="width:10px;"></td>';
if($row5["Q2"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Have you ever been refused a property by any landlord or agent?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q2']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Have you ever been refused a property by any landlord or agent?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='</tr>';
$questions.='<tr>';

if($row5["Q3"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:320px"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Are you in debt to another landlord or agent?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q3']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:320px"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Are you in debt to another landlord or agent?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='<td style="width:10px;"></td>';
if($row5["Q4"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Have any deductions ever been made from your rental bond?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q4']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Have any deductions ever been made from your rental bond?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='</tr>';
$questions.='<tr>';
if($row5["Q5"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Is there any reason known to you that would affect your future rental payments?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q5']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Is there any reason known to you that would affect your future rental payments?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='<td style="width:10px;"></td>';
if($row5["Q6"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Do you have any other applications pending on other properties?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q6']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Do you have any other applications pending on other properties?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='</tr>';
$questions.='<tr>';
if($row5["Q7"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:320px"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Do you currently own a property?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q7']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:320px"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Do you currently own a property?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='<td style="width:10px;"></td>';
if($row5["Q8"]=='Yes'):
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Are you considering buying a property after this tenancy or in the near future?: &nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;No<br>'.dboutput($row5['Details_Q8']).' </p></td>';
else:
    $questions.='
<td style="height:22px;background-color: #f1f1f2;width:50%"  ><p style="font-size:10px;font-family:Arial;color:#808285;line-height: 13px" >Are you considering buying a property after this tenancy or in the near future?: &nbsp;<img src="../../images/checkbox-unchecked-md.png" width="10">&nbsp;Yes&nbsp;&nbsp;<img src="../../images/tick-check-box-md.png" width="10">&nbsp;No </p></td>';
endif;
$questions.='</tr>';


$questions.='</table>';
$pdf->AddPage();
//$pdf->writeHTMLCell(235,232,495,10,$manageLogo);
$pdf->writeHTMLCell($colWidth,300,$margin,$pagetopmargin,$questions.$identification);
$page3='<table style="font-size:11px;width:650px;">
            <tr>
                <td>
                    <h3>General Lease Terms</h3>
                </td>
            </tr>
            <tr>
                <td><h4 style="margin-bottom: 1px;">Bond & Initial Payments</h4>
                <p>On approval of an application a tenant is required to pay a holding deposit in accordance with the above provisions, to commence the lease 4 weeks’ bond and 2 weeks rent in advance must be paid to the agent.</p>
                   <ol type="a"  class="top_list" style="margin-left: 20px;">
                                                        <li>Bond payments can be made direct to the Bond Board or by Bank Cheque or Money Order.</li>
                                                        <li>The first 2 weeks rent are to be paid by Visa/MasterCard Credit Card or Debit Card, AMEX or Diners.</li>
                                                    </ol>
                    <p>Upon approval, you’ll receive a credit card authorization form to complete within 24 hours and send it back to us so we can assist you with processing this payment.
                                                        As a part of our fraud prevention policy, you’ll be required to validate your credit card with supporting documents
                                                    </p>
                                                    <b>Holding Deposit.</b>
                                                    <p>The holding deposit can only be accepted after the application for tenancy has been approved.<br>The holding fee of 1 WEEKS RENT keeps the premises off the market for the prospective tenant for 7 days (or longer by agreement). In consideration of an approved holding fee paid by a prospective tenant, the agent acknowledges that:
                                                     <ol type="i" style="margin-left: 20px;">
                                                        <li>The application for tenancy has been approved by the landlord; and</li>

                                                        <li>The premises will not be let during the above period, pending the making of a residential tenancy agreement; and</li>
                                                        <li>If the prospective tenant(s) decide not to enter into such an agreement, the landlord may retain the whole fee; and</li>
                                                        <li>If a residential tenancy agreement is entered into, the holding fee is to be paid towards rent for the residential premises concerned.</li>
                                                          <li>The whole of the fee will be refunded to the prospective tenant if:<br>
                                                            <ol type="a"  style="margin-left: 20px;">
                                                                <li>the entering into of the residential tenancy agreement is conditional on the landlord carrying out repairs or other work and the landlord does not carry out the repairs or other work during the specified period</li>
                                                                <li>the landlord/landlord’s agent have failed to disclose a material fact(s) or made misrepresentation(s) before entering into the residential tenancy agreement.</li>
                                                            </ol>
                                                            </li>
                                                        </ol>
                                                     </p>
                                                    <p><img src="../../img/checkbox.png" alt="checkbox" width="15px;"> I the tenant authorize the debit of my credit/debit card for the amount equivalent to 2 week’s rent once the application has been approved by the landlord.</p>
                                                    <p><img src="../../img/checkbox.png" alt="checkbox" width="15px;"> I understand a card convenience fee applies to all transactions; 2% for Visa & MasterCard and 2.65% for AMEX & Diners Club, and that these fees are charged by a third-party payment processor – Rental Rewards for the convenience of the service.</p>
                                                    <p><img src="../../img/checkbox.png" alt="checkbox" width="15px;"> I understand that my Rental Rewards account will be activated to process my initial payments.</p>
                                                    <p><strong>Keys will not be provided for the property until the lease agreement has been signed by all tenants and the bond and first 2 weeks rent have been received by the agent in full.</strong></p>
                                                    <h4>Ongoing Rent Payments</h4>
            <p>Our preferred payment method for rent is Rental Rewards. (Please note that there is a small convenience fee charged for the use of the system. These fees are charged by a third-party payment processor – Rental Rewards</p>
                                                    <p>The fees for the convenience of the use of the services are: (must be able to set-up fee at agent level, as there is variation)</p>
                                                    <ol type="a"  style="margin-left: 20px;">
                                                        <li>Bank Account: $2</li>
                                                        <li>BPAY: $2.20 inc GST (internet banking using Rental Rewards Biller and Reference)</li>
                                                        <li>Credit Card: 2% for Visa & MasterCard and 2.65% for AMEX & Diners Club</li>
                                                    </ol>
                                                    </td></tr></table>';
$pdf->AddPage();
//$pdf->writeHTMLCell(235,232,495,10,$manageLogo);
$pdf->writeHTMLCell($colWidth,300,$margin,$pagetopmargin-20,$page3);
$page4='<table style="font-size:11px;width:650px;"><tr><td>
<p>Upon approval, you will be directed to the Rental Rewards Tenant Set-up form or provided/emailed which you’ll need to complete and along with other supporting documents as outlined in Identification when you come in to sign the lease, if not already provided. Other accepted payment methods (that do not incur a charge other than standard bank fee) to pay rental and other payments to us is by Cheque (Bank or Personnel), Deduction from Pay (organised through your employer), and Centrepay (organised through Centrelink).We have provided options for your convenience and ask that you consider what is most suitable to your circumstances.
                                                    </p>
                                                    <p>We have provided options for your convenience and ask that you consider what is most suitable to your circumstances.<br>
                                                    </p>
                                                     <h4>Payment Processing & Time Frames</h4>
                                                       <p>Please note that due to bank processing time frames, payments may take up to 3 business days to reach us.</p><p> Please factor this when you commence your payments.</p>
                                                        <p><strong>Please be aware that we will do our utmost to confirm personal details, current income and rental history but the final decision whether to approve this application lies with the landlord.</strong></p>
                                                        <p><strong>Declaration</strong></p>
                                                        <p>I <span style="display:inline-block; font-style:italic">'.dboutput($row4["Fname"]).' '.dboutput($row4["Mname"]).' '.dboutput($row4["Lname"]).'</span> the tenant, understand that by submitting this application I am accepting the property in its current condition unless negotiated otherwise. I accept that I have viewed the property or had a trusted person view the property on my behalf and I agree the premises are reasonably clean and tidy and in a condition suitable for me to occupy.</p>
                                                        <p>I understand the payment methods for rent and agree to them.</p>

</td>
</tr>
        </table>';
$pdf->AddPage();
//$pdf->writeHTMLCell(235,232,495,10,$manageLogo);
$pdf->writeHTMLCell($colWidth,300,$margin,$pagetopmargin,$page4);
$page5 ='<table style="font-size:11px;width:650px;">
				<tr>
					<td colspan="2">
						<h3>Terms and Conditions</h3>
						<p><img src="../../img/checkbox.png" alt="checkbox" width="15px;"> I acknowledge that I have Read, Understood and Agree with the Tenancy Privacy Statement / Collection Notice & Tenant Declaration </p>
					</td>
				</tr>
				<tr>
					<td style="width:75%;">
						<p>
					<br/><span style="font-family:'.$signature.'; font-size:30px; font-weight:bold">'. $row4["Fname"] .' '. $row4["Mname"].' '.$row4["Lname"].'</span>
						<br/>(Digital representation of tenants signature, approved by tenant)<br/></p>
					</td>
					<td style="width:25%;">
						<p><br/>'.$row["DateAdd"].'<br/>(Date Signed)</p>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p class="praone">'. $row4["Fname"] .' '. $row4["Lname"].' Acknowledges that they have Read, Understood and Agree with the following Terms and Conditions </p>
						<p class="pratwo">'. $row4["Fname"] .' '. $row4["Lname"].' also acknowledges that the agent in question cannot confirm that any phone lines to the property are operable or able to be reconnected. '. $row4["Fname"] .' '. $row4["Lname"].' understands that it is his/her responsibility to check with the telephone provider before proceeding with the tenancy to confirm the situation with the telephone line. Ensuring the main switch is in the off position for power connection remains the responsibility of the tenant.
						Please note that the tenant listed above has submitted their application using our online tenancy application system www.tenancyapplication.com.au.au. All tenants that use our system are made to electronically sign off on a Tenancy Privacy Statement / Collection Notice and a Tenant Declaration and our system is fully compliant with all industry standards and regulations. This act of electronically signing the form does allow third parties to release details of the tenant listed above for the purposes of processing a tenancy application under the Privacy Act. </p>'
						.$tenancy_terms.
						'
					</td>
				</tr>
			</table>';

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content


// reset pointer to the last page
//$pdf->lastPage();
$pdf->AddPage();
//$pdf->writeHTMLCell(235,232,495,10,$manageLogo);
$pdf->writeHTMLCell($colWidth,300,$margin,$pagetopmargin,$page5);


ob_end_clean();
$pdf->Output('tenancy_application.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+
