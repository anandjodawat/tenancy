<?php
include("../../admin/functions.php");
if(!isset($_SESSION['TENANCY_ADMIN']) || $_SESSION["TENANCY_ADMIN"] == false){
    if(!isset($_SESSION['ManagerID']) || $_SESSION["ManagerID"] == false){
        if(!isset($_SESSION['TenantID']) || $_SESSION["TenantID"] == false){
            redirect("../../index.php");
        }
    }
}ob_start();
require_once('tcpdf_include.php');
$msg="";
$ID=0;
$rguid = mt_rand(100000, 999999);
if(isset($_REQUEST["ID"]))
    $AppGUID=trim($_REQUEST["ID"]);

$ID=mysqli_fetch_array(mysqli_query($dbh,"SELECT ID FROM application WHERE GUID='".$AppGUID."'"));
$AppID=$ID['ID'];
$result=mysqli_query($dbh,"Select *, DATE_FORMAT(DateAdded, '%D %b %Y - %l:%i %p') AS DateAdd FROM application Where ID=".(int)$AppID." AND Status != 0 ") or die('Failed due to:'.mysqli_error($dbh));
$row = mysqli_fetch_array($result);
$TenantID=$row["TenantID"];
$Address=$row["Address"];

$fahad = mysqli_query($dbh,"SELECT TenantID FROM application WHERE ID='".$AppID."'");
$rowF = mysqli_fetch_array($fahad);
$TID = $rowF["TenantID"];

$result2=mysqli_query($dbh,"Select * FROM users Where ID=".(int)$TenantID) or die('Failed due to:'.mysqli_error($dbh));
$row2 = mysqli_fetch_array($result2);

$result3=mysqli_query($dbh,"Select * FROM app_inspection Where Application=".(int)$AppID) or die('Failed due to:'.mysqli_error($dbh));
$row3 = mysqli_fetch_array($result3);
if($row3["Inspected"]==1){
    $rowIns="I declare that I have physically inspected the inside of this property";
}
else if($row3["Inspected"]==2){
    $rowIns="I have not yet inspected this property";
}
else if($row3["Inspected"]==3){
    $rowIns="I am currently interstate/overseas and will physically inspect the property upon my arrival";
}
else if($row3["Inspected"]==4){
    $rowIns="An associate/other occupant has inspected the property on my behalf";
}
else if($row3["Inspected"]==5){
    $rowIns="I am currently interstate/overseas but accept the property in its current state";
}
else{
    $rowIns="";
}

$result4=mysqli_query($dbh,"Select * FROM tenant_profile Where UserID=".(int)$TenantID) or die('Failed due to:'.mysqli_error($dbh));
$row4 = mysqli_fetch_array($result4);
if($row4["Smoke"] == 'Y'){ $Smoke="Yes";}else{ $Smoke="No"; }
if($row4["LSmoke"] == 'Y'){ $LSmoke="Yes";}else{ $LSmoke="No"; }
if($row4["Student"] == 'Y'){ $Student="Yes";}else{ $Student="No"; }
if($row4["Pension"] == 'Y'){ $Pension="Yes";}else{ $Pension="No"; }

if($result){
    $result5=mysqli_query($dbh,"Select * FROM app_questions Where Application=".(int)$AppID) or die('Failed due to:'.mysqli_error($dbh));
    $row5 = mysqli_fetch_array($result5);
}



if(dboutput($row4["LivingArr"])== 'Renting - Through an Agent'){
    $KisKaNaam="Agent's Name";
}
else if(dboutput($row4["LivingArr"])== 'Renting - Through a private landlord'){
    $KisKaNaam="Landlord's Name";
}
else if(dboutput($row4["LivingArr"])== 'With Parents'){
    $KisKaNaam="Parents's Name";
}
else if(dboutput($row4["LivingArr"])== 'Sharing'){
    $KisKaNaam="Partner's Name";
}
else{
    $KisKaNaam="Name";
}
ob_start();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tenancy Application');
$pdf->SetTitle('Review Application');
$pdf->SetSubject('PDF View');
$pdf->SetKeywords('Tenancy, PDF, Application');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------



$signature = TCPDF_FONTS::addTTFfont('../../fonts/signature/allura-regular.ttf', 'TrueTypeUnicode', '', 96);
$pdf->SetFont($signature, '', 25, '', false);

// set font
$pdf->SetFont('dejavusans', '', 10);

$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
// create some HTML content
// get current vertical position
//$y = $pdf->getY();
//
$pdf->SetFillColor(255, 255, 255);
$pdf->setEqualColumns(2, 89);
$html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
			<th colspan="2" style="background-color:#bebebe;color:#000">Agent Details</th>
			</tr>
				<tr>
					<td colspan="2" style="padding-bottom:0;!important;float:left;">Property Manager:&nbsp;<span style="color:blue;">'. dboutput($row["PM_Name"]) .'</span></td>
				</tr>
				<tr>
					<td colspan="2" style="float:right;padding-bottom:0; !important;">Sent to:&nbsp;<span style="color:blue;">'.dboutput($row["PM_Email"]) .'</span></td>
				</tr>
				<tr>
					<td colspan="2"style="float:left;padding-top:0;">Received:&nbsp;<span style="color:blue;"> '.$row["DateAdd"].'</span></td>
				</tr>
				<tr>
					<td colspan="2" style="float:right;padding-top:0;">Application ID:&nbsp;<span style="color:blue;"> '.$row["ID"] .'</span>
				</td>
				</tr>
		</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');

$html= '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Property Details</th>
			</tr>
			<tr>
				<td colspan="2">Application Address</td>
			</tr>
			<tr>	
				<td colspan="2" style="color:blue; border:1px solid #000000;padding:20px;">'.dboutput($row["Address"]).', '.dboutput($row["Suburb"]).', '.dboutput($row["State"]).'</td>
			</tr>				
			<tr>
				<td>Post Code </td>	
				<td>Property Type</td>									
			</tr>
			<tr>
				<td style="color:blue; border:1px solid #000000;">'. dboutput($row["Postcode"]).'</td>
				<td style="color:blue;border:1px solid #000000;">'. dboutput($row["PropType"]).'</td>					
			</tr>            
			<tr>
				<td colspan="2" style="border:1px solid #000000;">No. of Bedrooms:&nbsp;&nbsp;
				<span style="color:blue;">'. dboutput($row["Bedrooms"]).'</span></td>			
			</tr>
			<tr>
				<td>Weekly Rent</td>	
				<td>Monthly Rent</td>											
			</tr>
			<tr>
				<td style="color:blue;border:1px solid #000000;">$'. dboutput($row["WeeklyRent"]).'</td>	
				<td style="color:blue;border:1px solid #000000">$'. dboutput($row["MonthlyRent"]).'</td>												
			</tr>			
			<tr>
				<td>Bond</td>	
				<td>Commencement Date</td>																
			</tr>           
			<tr>
				<td style="color:blue;border:1px solid #000000;">$'. dboutput($row["Bond"]).'</td>
				<td style="color:blue;border:1px solid #000000;">'. date('d-m-Y',strtotime(dboutput($row["CommDate"]))).'</td>												
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;">Preferred Lease Term:&nbsp;&nbsp;
				<span style="color:blue;">'. dboutput($row["LeaseLength"]).'</span></td>												
			</tr>
			<tr>
				<td>No. of Occupants</td>
				<td>No. of Vehicles</td>												
			</tr>
            <tr>
				<td style="color:blue;border:1px solid #000000;">'. dboutput($row["Occupants"]).'</td>	
				<td style="color:blue;border:1px solid #000000;">'. dboutput($row["Vehicles"]).'</td>	
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;">No. of Pets: &nbsp;&nbsp;
				<span style="color:blue">'. dboutput($row["Pets"]).'</span></td>				
			</tr>
			<tr>
				<td colspan="2">How did you find out about this property?</td>				
			</tr>
		    <tr>
				<td colspan="2" style="color:blue;border:1px solid #000000;">'. dboutput($row["Find"]).' '.dboutput($row["Source"]).'</td>				
			</tr>            
		</table><br/>';
$pdf->writeHTML($html, true, false, true, false, '');
$html= '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Utility Connections</th>
			</tr>
			<tr>
				<td colspan="2" style="width:50%;color:blue;-webkit-print-color-adjust: exact;">';
if($row["Utility"] == "No"){
    $html .= 'Not Required.';
}
else {
    $html .= 'Required <br>'.$row['Connection'];
}

$html .='</td>
			</tr>
		</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$html= '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>				
				<th colspan="2" style="background-color:#bebebe;color:#000;">References</th>
			</tr>
			<tr>
				<td><strong>Personal Ref.</strong></td> 
				<td style="color:blue;border:1px solid #000000;">'.dboutput($row4["PerRef"]).'</td>
			</tr>
			<tr>
				<td>Occupation</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["PerOcc"]).'</td>	
			</tr>
			<tr>
				<td>Phone</td>
				<td>Mobile</td>					
			</tr>
			<tr>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["PerPh"]).'</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["PerMob"]).'</td>	
			</tr>
			<tr>
			<td>Relationship</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["1Relation"]).'</td>
			</tr>
			<tr>
			<td colspan="2">Email</td>
			</tr>	
			<tr>		
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["PerEmail"]).'</td>
			</tr>
			<tr>
			<td colspan="2">Address</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["PerAdd"]).'</td>
			</tr>
			<tr>
			<td><strong>Professional Ref. 1</strong></td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["RefName1"]).'</td>	
			</tr>
			<tr>
			<td colspan="2">Company Name</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["RefCName1"]).'</td>
			</tr>
			<tr>
				<td>Phone</td>
				<td>Mobile</td>
			</tr>
			<tr>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["1PhnNo"]).'</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["1MobNo"]).'</td>	
			</tr>
			<tr>
				<td>Relationship</td>
				<td style="color:blue:;border:1px solid #000000">'.dboutput($row4["1Relation"]).'</td>
			</tr>
			<tr>
				<td>Email</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["1Email"]).'</td>
			</tr>
			<tr>
			<td colspan="2">Address</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["1Address"]).'</td>	
			</tr>
			<tr>
				<td><strong>Professional Ref. 2</strong></td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["RefName2"]).'</td>
			</tr>
			<tr>
			<td colspan="2">Company Name</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["RefCName2"]).'</td>
			</tr>
			<tr>
				<td>Phone</td>
				<td>Mobile</td>
			</tr>
			<tr>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["2PhnNo"]).'</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["2MobNo"]).'</td>
			</tr>
			<tr>
				<td>Relationship</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["2Relation"]).'</td>
			</tr>
			<tr>
				<td>Email</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["2Email"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Address</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["2Address"]).'</td>	
			</tr>
		</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->addPage();
//$pdf->resetColumns();
$pdf->setEqualColumns(2, 89);
$html='<table style="border:1px solid #000000"  cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Personal Details</th>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #757575;">Name:&nbsp;<span style="color:blue;">'.dboutput($row4["Title"])." ".dboutput($row4["Fname"])." ".dboutput($row4["Mname"])." ".dboutput($row4["Lname"]).'</span></td>
								
			</tr>	
			<tr>
				<td colspan="2" style="border:1px solid #000000">Gender:&nbsp;<span style="color:blue;">'. dboutput($row4["Gender"]).'</span></td>
			</tr>	
			<tr>
				<td colspan="2" style="border:1px solid #000000">Date of Birth:&nbsp;<span style="color:blue">'. date('d-m-Y',strtotime(dboutput($row4["DOB"]))).'</span></td>
			</tr>
			<tr>
				<td>Home Phone</td>
				<td>Work Phone</td>
			</tr>	
			<tr>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Mnumber"]).'</td>	
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Hnumber"]).'</td>				
			</tr>
			<tr>
				<td>Mobile</td>
				<td>Fax</td>
			</tr>			
			<tr>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Mobile"]).'</td>	
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Fax"]).'</td>				
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000">Email:&nbsp;<span style="color:blue">'. dboutput($row2["Email"]).'</span></td>
			</tr>
			<tr>
          		<td colspan="2">Emergency Contact Name</td>
          	</tr>
          	<tr>
          		<td colspan="2" style="color:blue;border:1px solid #000000">'. dboutput($row4["NameEC"]).'</td>
			</tr>	
			<tr>
        		<td>Relationship</td>
        		<td>Home Phone</td>
			</tr>
			<tr>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["ECRelation"]).'</td>	
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["MAcon"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Contact Address</td>
			</tr>
			<tr>	
				<td colspan="2" style="color:blue;border:1px solid #000000">'. dboutput($row4["AddEC"]).'</td>				
			</tr>
			<tr>
				<td>Work Phone</td>
				<td>Mobile</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["E_phone"]).'</td>
			</tr>
			<tr>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["E_phone"]).'</td>
				<td style="color:blue;border:1px solid #000000">'.dboutput($row4["E_mobile"]).'</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000">Email:&nbsp;<span style="color:blue">'. dboutput($row4["E_email"]).'</span></td>				
			</tr>
		</table></br>';
//$pdf->writeHTMLCell(90, '', '', $y, $html, 1, 0, 1, true, 'J', true);
$pdf->writeHTML($html, true, false, true, false, '');
$html='<table style="border:1px solid #000000"  cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Other Details</th>
			</tr>
			<tr>				
				<td colspan="2">Applications pending on other properties?</td>
			</tr>	
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q1"]);
if($row5["Q1"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q1"]);
}
$html.= '</td></tr>	
			<tr>
				<td colspan="2">Have you ever been refused a rental property?</td>               
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q2"]);
if($row5["Q2"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q2"]);
}
$html.= '</td></tr>			
			<tr>
				<td colspan="2">Are you in debt to another landlord or agent?</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q3"]);
if($row5["Q3"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q3"]);
}
$html.= '</td></tr>
			<tr>
				<td colspan="2">Deductions ever taken from your Bond ?</td>							
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q4"]);
if($row5["Q4"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q4"]);
}
$html.= '</td></tr>			
			<tr>
				<td colspan="2">Anything affecting future rental payments?</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q5"]);
if($row5["Q5"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q5"]);
}
$html.= '</td></tr>
			<tr>
				<td colspan="2">Considering buying a property soon?</td> 
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q6"]).'</td>				
			</tr>
			<tr>				
				<td colspan="2">Are you considering buying a property after this tenancy or in the near future?</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q8"]);

if($row5["Q8"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q8"]);
}
$html.= '</td></tr>
			<tr>
				<td colspan="2">Do you currently own a property?</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row5["Q7"]);
if($row5["Q7"]=="Yes"){
    $html .=': '.dboutput($row5["Details_Q7"]);
    $pdsql=mysqli_query($dbh,"SELECT * FROM app_documents WHERE Application=".(int)$AppID);
    if(mysqli_num_rows($pdsql) > 0){
        while($pdocs=mysqli_fetch_array($pdsql)){
            $html.= '<br/><a target="_blank" href="../../uploads/property/'.$pdocs['File'].'">
						'.$pdocs['File'].'</a>';
        }
    }
}
$html.= '</td></tr>
		</table></br>';
//$pdf->writeHTMLCell(90, '', '', $y, $html, 1, 0, 1, true, 'J', true);
$pdf->writeHTML($html, true, false, true, false, '');
$html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Current Employment</th>
			</tr>
			<tr>			
				';
$html.='<td>Current work situation</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["EmCurrWork"]).'</td>	
			</tr>	
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" ){
    $html .='Company Name';
}
$html.= '</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed" ){
    $html .=''.dboutput($row4["Emcompany_name"]);
}
else{
    $html .=''.dboutput($row4["mycompany_name"]);

}


$html.= '</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" ){
    $html .='Manager/Contact Name';
}


$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed" )
{
    $html .=''.dboutput($row4["EmManager_name"]);
}


$html.= '</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" ){
    $html .='Phone';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed" )
{
    $html .=''.dboutput($row4["EmPhone"]);
}

$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" ){
    $html .='Email';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed" )
{
    $html .=''.dboutput($row4["EmEmail"]);
}
$html.='</td>
			</tr>		
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" ){
    $html .='Current Address';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed" )
{
    $html .=''.dboutput($row4["EmCurr_address"]);
}

$html.='</td>
			</tr>	
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Industry';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["Emindustry"]);
}
else{
    $html .=''.dboutput($row4["Emindustry"]);

}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Occupation/Position';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';


if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["EmOccup_Post"]);
}
else{
    $html .=''.dboutput($row4["myOccup_Post"]);

}

$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Nature of Employment';
}
$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["EmNOE"]);
}
else{
    $html .=''.dboutput($row4["myNOE"]);

}

$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Start In';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["EmDatestart"]);
}
elseif($row4["EmCurrWork"]=="I run my own business"){
    $html .=''.dboutput($row4["myDatestart"]);

}

$html.='</td>	
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Job';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["Emjob"]);
}
elseif($row4["EmCurrWork"]=="I run my own business"){
    $html .=''.dboutput($row4["myjob"]);

}

$html.='</td>
			</tr>	
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Finish On';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["EmDatefinish"]);
}
elseif($row4["EmCurrWork"]=="I run my own business"){
    $html .=''.dboutput($row4["myDatefinish"]);

}

$html.='</td>	
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"]=="I am currently employed" || $row4["EmCurrWork"] == 'I run my own business'){
    $html .='Gross Annual Salary Before Tax';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I am currently employed"){
    $html .=''.dboutput($row4["EmGross"]);
}
else{
    $html .=''.dboutput($row4["myGross"]);

}

$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='Date Company Est.';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmCurrWork"]=="I run my own business"){
    $html .=''.dboutput($row4["myDateEST"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='ACN';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["myACN"]).'</td>
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='ABN';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["myABN"]).'</td>
			</tr>	
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='Accountant Name';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["myAccountant_name"]).'</td>	
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='Phone No';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["A_Phone"]).'</td>	
			</tr>
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='Lawyer Name';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["myLawyer"]).'</td>	
			</tr>			
			<tr>
				<td>';
if($row4["EmCurrWork"] == 'I run my own business'){
    $html .='Phone No';
}

$html.='</td>
				<td style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'. dboutput($row4["L_Phone"]).'</td>	
			</tr>						
			</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->addPage();
$pdf->setEqualColumns(2, 89);
$html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style=" background-color:#bebebe;color:#000">Previous Employment</th>
			</tr>
			<tr>
				<td colspan="2">Previous work situation</td>	
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">'.dboutput($row4["EmPrWork"]).'</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business"){
    $html .='Company Name';
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_company_name"]);
}
else{
    $html .=''.dboutput($row4["pre_mycompany_name"]);

}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" ){
    $html .='Manager/Contact Name';
}

$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Manager_name"]);
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" ){
    $html .='Phone';
}

$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Phone"]);
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" ){
    $html .='Email';
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Email"]);
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" ){
    $html .='Previous Address';
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Curr_address"]);
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Industry';
}

$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_industry"]);
}
elseif($row4["EmPrWork"]=="I previously ran my own business"){
    $html .=''.dboutput($row4["pre_myindustry"]);

}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Occupation/Position';
}

$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Occup_Post"]);
}
elseif($row4["EmPrWork"]=="I previously ran my own business"){
    $html .=''.dboutput($row4["pre_myOccup_Post"]);

}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Nature of Employment';
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_NOE"]);
}
elseif($row4["EmPrWork"]=="I previously ran my own business"){
    $html .=''.dboutput($row4["pre_myNOE"]);

}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Start In';
}

$html.='</td>
				<td style="border:1px solid #000000;margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Datestart"]);
}
elseif($row4["EmPrWork"]=="I previously ran my own business"){
    $html .=''.dboutput($row4["pre_myDatestart"]);

}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Finish On';
}
$html.='</td>
				<td style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Datefinish"]);
}
elseif($row4["EmPrWork"]=="I previously ran my own business"){
    $html .=''.dboutput($row4["pre_myDatefinish"]);

}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($row4["EmPrWork"]=="I was previously employed" || $row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Gross Annual Salary Before Tax';
}

$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I was previously employed"){
    $html .=''.dboutput($row4["Pre_Gross"]);
}
elseif($row4["EmPrWork"]=="I previously ran my own business"){
    $html .=''.dboutput($row4["pre_myGross"]);

}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Date Company Est.';
}
$html.='</td>
				<td style="border:1px solid #000000; color:blue;-webkit-print-color-adjust: exact;">';

if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_myDateEST"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='ACN';
}

$html.='</td>
				<td style="border:1px solid #000000;margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_myACN"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='ABN';
}
$html.='</td>
				<td style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_myABN"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Accountant Name';
}

$html.='</td>				
				<td style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_myAccountant_name"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Phone No';
}

$html.='</td>
					<td style="border:1px solid #000000; margin-left:40%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_A_Phone"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Lawyer Name';
}

$html.='</td>
				<td style="border:1px solid #000000; margin-left:40%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_myLawyer"]);
}
$html.='</td>
			</tr>
			<tr>
				<td>';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .='Phone No';
}

$html.='</td>
				<td style="border:1px solid #000000; margin-left:60%; color:blue;-webkit-print-color-adjust: exact;">';
if($row4["EmPrWork"]=="I previously ran my own business" ){
    $html .=''.dboutput($row4["pre_L_Phone"]);
}
$html.='</td>
			</tr>			
		</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
			<th colspan="2" style="background-color:#bebebe;color:#000">Address</th>
			</tr>
			<tr>			
				<td>I am currently:</td>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["LivingArr"]).'</td>
			</tr>
			<tr>
			 	<td>Address:</td>
			 	<td style="color:blue;border:1px solid #000000">'. dboutput($row4["AddEC"]).'</td>	
			</tr>
			 <tr>
				<td>Monthly Rent:</td>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Monthly_Rent"]).'</td>									
			</tr>	
			<tr>
				<td>Bond fully refunded?: </td>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Refund"]).'</td>											
			</tr>
			<tr>
				<td>Phone: </td>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Phone"]).'</td>								
			</tr>
			<tr>
				<td>Email: </td>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["Email"]).'</td>						
			</tr>

			<tr>
			<td>Reason for leaving: </td>
			   <td style="color:blue;border:1px solid #000000">'. dboutput($row4["ReasonFL"]).'</td>						
			</tr>

			<tr>
				<td>Previous Address: </td>
				<td style="color:blue;border:1px solid #000000">'. dboutput($row4["PrAdd"]).'</td> 		
			</tr>								
		</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');
$html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Personal Question</th>
			</tr>
			<tr>
				<td colspan="2"><strong>Are You a Student?</strong></td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'.$Student.'</td>
			</tr>
			<tr>
				<td colspan="2"><strong>Do You Receive Pension?</strong></td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'.$Pension.'</td>
			</tr>
			<tr>
				<td colspan="2">';
if($Pension=='Yes'){
    $html.='Pension Type';
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue">';
if($Pension=='Yes'){
    $html .=''.dboutput($row4["Pension_Type"]);
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2">';
if($Pension=='Yes'){
    $html.='Pension Number';
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue">';
if($Pension=='Yes'){
    $html .=''.dboutput($row4["Pension_Number"]);
}
$html.='</td>
			</tr>
			<tr>
				<td colspan="2"><strong>Do You Smoke?</strong></td>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">'.$Smoke;
if($row4["Smoke"] == 'Y') { $html .=', Smoke Inside? '.$LSmoke; }
$html .='</td>
			</tr>
			</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');
$photo=mysqli_query($dbh,"Select Upload,Date,Delivery FROM app_documents WHERE Application=".(int)$AppID) or die ('Failed due to:'.mysqli_error ($dbh));
$nikal=mysqli_fetch_array($photo);
if($nikal['Upload'] == 1){
    $html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
			<th colspan="2" style="background-color:#bebebe;color:#000">Inspection Details</th>
			</tr>
			<tr>
				<td colspan="2">Have you inspected this Property? </td>				
			</tr>';}else{
    $photo=mysqli_query($dbh,"Select * FROM tenant_image WHERE UserID=".(int)$TenantID) or die ('Failed due to:'.mysqli_error ($dbh));
    $nikal=mysqli_fetch_array($photo);
    $html = '<table style="border:1px solid #000000"  cellspacing="3" cellpadding="4">
			<tr>
			<th colspan="2" style="background-color:#bebebe;color:#000">Inspection Details</th>
			</tr>
			<tr>
				<td colspan="2">Have you inspected this Property? </td> 
			</tr>';}

if($row3["Inspected"]==4) {
    $html .='<tr>
						<td>Name</td>
						<td style="color:blue;border:1px solid #000000">'.dboutput($row3["Name"]).'</td>
					</tr>';
}
if($row3["Inspected"]==1 || $row3["Inspected"]==2 || $row3["Inspected"]==3 || $row3["Inspected"]==4) {
    $html .='<tr>
						<td>Date</td>
						<td style="color:blue;border:1px solid #000000">'.dboutput($row3["Date"]).'</td>
					</tr>';
}
if($row3["Inspected"]==1 || $row3["Inspected"]==4) {
    $html .='<tr>
						<td>Property Condition</td>
						<td style="color:blue;border:1px solid #000000">';
    if($row3["Clean"]=="Yes") {
        $html .= 'Property was reasonably Clean and Fair';
    }
    else {
        $html .= $row3["Details"];
    }
    $html .='</td>
					</tr>';
}
$html .='</table>';
$pdf->writeHTML($html, true, false, true, false, '');

if($nikal['Upload'] == 1){
    $html = '<table style="border:1px solid #000000"  cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Documents</th>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';
    if($html>0)
    {

    }
    else{
        $html .= 'Will Deliver Documents By: '.$nikal["Delivery"];
        $html.='<br>';
        $html .= 'Will Deliver Documents on: '.$nikal["Date"];
    }
    $html .='</td> 
			</tr>';}else{
    $html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
			<th colspan="2" style="background-color:#bebebe;color:#000">Documents</th>
			</tr>
			<tr>
				<td colspan="2" style="border:1px solid #000000;color:blue;-webkit-print-color-adjust: exact;">';
    $html .= 'Documents have been Uploaded';

    $html .='</td> 
			</tr>';}
$html .='</table>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->addPage();
$pdf->setEqualColumns(2, 89);

$html = '<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr>
				<th colspan="2" style="background-color:#bebebe;color:#000">Income And Expenditure</th>
			</tr>			
			<tr>
				<th colspan="2"><strong>Current Weekly Rent</strong></th>						
			</tr>
			<tr>
				<td colspan="2">Total Amount</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'. dboutput($row4["Tamount"]).'</td>
			</tr>
			<tr>
				<th colspan="2"><strong>Vehicle Information</strong></th>
			</tr>
			<tr>
				<td colspan="2">Total value of car</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'. dboutput($row4["Tvalue"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Owned/Financed</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'. dboutput($row4["car"]).'</td>
			</tr>
			<tr>
				<td colspan="2">All Figures entered below are</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'. dboutput($row4["below"]).'</td>
			</tr>
			<tr>
				<th colspan="2"><strong>Income</strong></th>
			</tr>
			<tr>
				<td colspan="2">Salary/Wages</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["sal"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Self Employed</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000;border:1px solid #000000">'.dboutput($row4["selfemp"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Benefits</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["benefit"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Others</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["other"]).'</td>
			</tr>
			<tr>
				<th colspan="2"><strong>Repayments</strong></th>
			</tr>
			<tr>
				<td colspan="2">Loans</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["loan"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Credit Cards</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["Ccard"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Hire Purchases</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["Hpurchase"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Store Cards</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["Scard"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Others</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["Rother"]).'</td>
			</tr>
			<tr>
				<th colspan="2"><strong>Expenses</strong></th>
			</tr>
			<tr>
				<td colspan="2">Maintenance (Child Support)</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["maint"]).'</td>
			</tr>
			<tr>
				<td colspan="2">Others</td>
			</tr>
			<tr>
				<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row4["Eother"]).'</td>
			</tr>			
		</table></br>';
$pdf->writeHTML($html, true, false, true, false, '');

if($row["Occupants"] > 0) {
    $html ='<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr><th  colspan="2" style="background-color:#bebebe;color:#000">Other Occupants</th></tr>';

    $result6=mysqli_query($dbh,"Select * FROM app_occupants Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
    while($row6=mysqli_fetch_array($result6)){

        $html .='<tr>
					<td>Name </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row6["Name"]).'</td>
				</tr>
				<tr>
					<td>Age </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row6["Age"]).'</td>
				</tr>
				<tr>
					<td>Relationship </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row6["Relationship"]).'</td>
				</tr>';

        if($row6["Age"] > 18){
            $html .='<tr>
						<td>On the lease? </td>
						<td style="color:blue;border:1px solid #000000">'.dboutput($row6["Lease"]).'</td>
					</tr>';
            if($row6["Lease"] == "Yes"){

                $html .='<tr>
								<td>Email </td>
								<td style="color:blue;border:1px solid #000000">'.dboutput($row6["Email"]).'</td>
							</tr>
							<tr>
								<td>Contact No </td>
								<td style="color:blue;border:1px solid #000000">'.dboutput($row6["Mobile"]).'</td>
							</tr>';
            }
        }
    }
    $html .='</table><br/><br/>';
}
if($row["Vehicles"] > 0) {
    $html .='<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr><th colspan="2" style="background-color:#bebebe;color:#000">Vehicles</th></tr>';

    $result7=mysqli_query($dbh,"Select * FROM app_vehicles Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
    while($row7=mysqli_fetch_array($result7)){

        $html .='<tr>
					<td>Type </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row7["Type"]).'</td>
				</tr>
				<tr>
					<td>Model </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row7["Model"]).'</td>
				</tr>
				<tr>
					<td>Registration </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row7["Registration"]).'</td>
				</tr>';
    }
    $html .='</table><br/><br/>';
}

if ($row["Pets"] > 0) {
    $html .='<table style="color:blue;border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr><th colspan="2" style="background-color:#bebebe;color:#000">Pets</th></tr>';
    $result8=mysqli_query($dbh,"Select * FROM app_pets Where Application=".(int)$AppID."") or die('Failed due to:'.mysqli_error($dbh));
    while($row8=mysqli_fetch_array($result8)){
        $html .='<tr>
					<td>Type </td>';
        if($row8["Type"] == "Other"){

            $html .='<td style="color:blue;border:1px solid #000000">'.dboutput($row8["Other"]).'</td>';
        } else{
            $html .='<td style="color:blue;border:1px solid #000000">'.dboutput($row8["Type"]).'</td>';
        }
        $html .='</tr>
				<tr>
					<td>Breed </td>
					<td style="color:blue;border:1px solid #000000">'.dboutput($row8["Breed"]).'</td>
				</tr>';
    }

    $html .='</table></br><br/>';
}

if ($row["ExtraDetails"] !="") {
    $html .='<table style="border:1px solid #000000" cellspacing="3" cellpadding="4">
			<tr><th colspan="2" style="background-color:#bebebe;color:#000">Extra Details</th></tr>';
    '<tr>
					<td colspan="2" style="color:blue;border:1px solid #000000">'.dboutput($row["ExtraDetails"]).'</td></tr>';
    if($row["document1"] != ""){
        $html .='<tr><td colspan="2" style="color:blue;border:1px solid #000000">
							<a href="../../uploads/informations/'.$row["document1"].'" target="_blank" download>
								'.$row["document1"].'
							</a>
							</td>
						</tr>
				</table>';
    }

    $html .='</table>';
}
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$html = '<table cellspacing="3" cellpadding="4">
			<tr><th style="background-color:#bebebe;color:#000;"><span style="text-align:center;">Files Uploaded</span></th></tr>';
if($nikal['Image'] != ""){
    $html .='<tr><td>
					<a href="../../uploads/photos/'.$nikal["Image"].'?image='.$rguid.'" target="_blank" download>
						<img src="../../uploads/photos/'.$nikal["Image"].'" style="width:500px;"/> 
					</a>
					</td>
				</tr>';
}
if($nikal['Image2'] != ""){
    $html .='<tr><td>
					<a href="../../uploads/secondary/'.$nikal["Image2"].'?image='.$rguid.'" target="_blank" download>
						<img src="../../uploads/secondary/'.$nikal["Image2"].'" style="width:500px;"/> 
					</a>
					</td>
				</tr>';
}
if($nikal['Image3'] != ""){
    $html .='<tr><td>
					<a href="../../uploads/history/'.$nikal["Image3"].'?image='.$rguid.'" target="_blank" download>
						<img src="../../uploads/history/'.$nikal["Image3"].'" style="width:500px;"/> 
					</a>
					</td>
				</tr>';
}
if($nikal['Image4'] != ""){
    $html .='<tr><td>
					<a href="../../uploads/employments/'.$nikal["Image4"].'?image='.$rguid.'" target="_blank" download>
						<img src="../../uploads/employments/'.$nikal["Image4"].'" style="width:500px;"/> 
					</a>
					</td>
				</tr>';
}
$html .='</table>';
$pdf->setEqualColumns(2, 89);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
// convert TTF font to TCPDF format and store it on the fonts folder

$html ='<table>
				<tr>
					<td colspan="2">
						<h3>Terms and Conditions</h3>
						<p><img src="../../img/checkbox.png" alt="checkbox" width="15px;"> I acknowledge that I have Read, Understood and Agree with the Tenancy Privacy Statement / Collection Notice & Tenant Declaration </p>
					</td>
				</tr>
				<tr>
					<td style="width:75%;">
						<p>
					<br/><span style="font-family:'.$signature.'; font-size:30px; font-weight:bold">'. $row4["Fname"] .' '. $row4["Mname"].' '.$row4["Lname"].'</span>
						<br/>(Digital representation of tenants signature, approved by tenant)<br/></p>
					</td>
					<td style="width:25%;">
						<p><br/>'.$row["DateAdd"].'<br/>(Date Signed)</p>   
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p class="praone">'. $row4["Fname"] .' '. $row4["Lname"].' Acknowledges that they have Read, Understood and Agree with the following Terms and Conditions </p>
						<p class="pratwo">'. $row4["Fname"] .' '. $row4["Lname"].' also acknowledges that the agent in question cannot confirm that any phone lines to the property are operable or able to be reconnected. '. $row4["Fname"] .' '. $row4["Lname"].' understands that it is his/her responsibility to check with the telephone provider before proceeding with the tenancy to confirm the situation with the telephone line. Ensuring the main switch is in the off position for power connection remains the responsibility of the tenant. 
						Please note that the tenant listed above has submitted their application using our online tenancy application system Tenancyapplication.com.au. All tenants that use our system are made to electronically sign off on a Tenancy Privacy Statement / Collection Notice and a Tenant Declaration and our system is fully compliant with all industry standards and regulations. This act of electronically signing the form does allow third parties to release details of the tenant listed above for the purposes of processing a tenancy application under the Privacy Act. </p>
						<p>We understand that you have accepted the terms & conditions of tenancy application without any modifications.</h3>
						<p>Using tenancy application constitutes your agreement to our terms and conditions.</p>
						<p>This website i.e. tenancyapplication.com.au is for your personal use.</p>
						<p>You are not suppose to use Tenancyapplication.com.au for any purpose that is illegal. You cannot edit, copy, distribute, reproduce, or sell any kind of information, contained within Tenancyapplication.com.au without the agreement in writing by Tenancyapplication.com.au or if the above actions form part of the functionality within the website such as submitting and/or processing tenancy/property applications. Tenancyapplication.com.au takes no responsibilities about the document uploaded on the website as most of the information uploaded are by third parties such as concerned tenants who are using the Tenancyapplication.com.au platform to submit tenancy applications.</p>
						<p>We will try all possible care is taken, Tenancyapplication.com.au cannot assure the accuracy of the information submitted through the Tenancyapplication.com.au website or accept any claims for mistakes or faults..</p>
						<p>Tenancyapplication.com.au can make refinement and/or edits to the Tenancyapplication.com.au website at any time.</p>
						<p>Tenancyapplication.com.au does not portray about the suitability of the information, products, document, and services posted/advertised on tenancyapplication.com.au for any legal or illegal purpose. All such information, products & services are provided "similar" without warranty of any kind. Tenancyapplication.com.au hereby disclaim all contract/bond/guaranty, conditions and fitness for a particular purpose with regard to the information, products, and services, posted on tenancyapplication.com.au.</p>
						<p>In no circumstance shall Tenancyapplication.com.au be responsible for any damages turn out of or in any way linked with the use of Tenancyapplication.com.au.</p>
					</td>
				</tr>
			</table>';
$pdf->resetColumns();
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();


ob_end_clean();
$pdf->Output('tenancy_application.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+