<?php
include("../../admin/functions.php");
include("../../get_templateemp.php");

require_once('tcpdf_include.php');
$msg = "";
$ID = 0;

if ($_REQUEST['action'] && $_REQUEST['action'] == 'confirmation_request') {
    if (isset($_POST['CurrentDate'])) {
        $Econfirmation_Date = trim($_POST['CurrentDate']);
    }
    if (isset($_POST['TenantName'])) {
        $TenantName = trim($_POST['TenantName']);
    }
    if (isset($_POST['ECompany'])) {
        $Econfirmation_Company = trim($_POST['ECompany']);
    }
    if (isset($_POST['WorkStarted'])) {
        $Econfirmation_WorkStarted = trim($_POST['WorkStarted']);
    }
    if (isset($_POST['WorkEnded'])) {
        $Econfirmation_WorkEnded = trim($_POST['WorkEnded']);
    }
    if (isset($_POST['TDesignation'])) {
        $Econfirmation_Tdesignation = trim($_POST['TDesignation']);
    }
    if (isset($_POST['Ecompleted_By'])) {
        $Econfirmation_CompletedBy = trim($_POST['Econfirmation_CompletedBy']);
    }
    if (isset($_POST['Eposition'])) {
        $Econfirmation_Position = trim($_POST['Eposition']);
    }
    if (isset($_POST['EDate'])) {
        $Econfirmation_Edate = trim($_POST['EDate']);
    }
    if (isset($_POST["PEmail"])) {
        $PEmail = trim($_POST["PEmail"]);
    }
    if (isset($_POST["AppID"])) {
        $AppID = trim($_POST["AppID"]);

        $patani = mysqli_fetch_array(mysqli_query($dbh, "SELECT PM_Email FROM application WHERE GUID='" . $AppID . "'"));
        $PM_Email = $patani['PM_Email'];
    }
}


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tenancy Application');
$pdf->SetTitle('employment Confirmation');
$pdf->SetSubject('PDF View');
$pdf->SetKeywords('Tenancy, PDF, Application, Confirmation, employment');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once(dirname(__FILE__) . '/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '
<style>

table {
padding:10px 0;
}

td.label {
width:10%;
color:#000;
}

.data {
width:50%;
color:#000;
}
span.data {
display:block;
}
</style>
    
<br><br>
        <p> Date: '.$Econfirmation_Date.'</p>
        <p>'.$PM_Email.'</p>
       
        <br><br>
        <p></p>Subject: Letter of employment Confirmation for '.$TenantName.'<br><br>
        
        <br><br>
        <p>Dear '.$PM_Email.'</p>
        
        <br>
        <p>
           '.$TenantName.' has been employed as a '.$Econfirmation_Tdesignation.' at '.$Econfirmation_Company.' 
            since '.$Econfirmation_WorkStarted.' to '.$Econfirmation_WorkEnded.' on full time basis. 
        </p>
        <br>
        
        <p>
            '.$Econfirmation_CompletedBy.'<br>
            '.$Econfirmation_Position.'<br>
            '.$Econfirmation_Company.'
        </p>

';

$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

$pdf->Output("/home/tenancyapplicati/public_html/pdf/review/data/employment_confirmation.pdf", "F");


require_once '../../PHPMailer/class.phpmailer.php';

$mail = new PHPMailer(true);
try {
    $mail->CharSet = 'UTF-8';
    $mail->AddAddress($PM_Email);
    $mail->SetFrom($PEmail);
    $mail->FromName = 'TenancyApplication.com.au';
    $mail->WordWrap = 50;
    $mail->IsHTML(true);
    $mail->Subject = 'EMPLOYMENT CONFIRMATION SUBMITTED';
    $mail->Body = 'CONFIRMATION HAS BEEN SUBMITTED';
    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
    $mail->AddAttachment("/home/tenancyapplicati/public_html/pdf/review/data/employment_confirmation.pdf");
    $mail->Send();
} catch (phpmailerException $e) {
    echo $e->errorMessage();
}
redirect("../../thankyou.php");


//============================================================+
// END OF FILE
//============================================================+

