<?php
include("../../admin/functions.php");
include("../../get_templateemp.php");

require_once('tcpdf_include.php');
$msg="";
$ID=0;


	if(isset($_REQUEST["action"]) && $_REQUEST["action"]=="submit_form"){

	
		if(isset($_POST["sel1"]))
			$sel1=trim($_POST["sel1"]);
		if(isset($_POST["propertyto"]))
			$propertyto=trim($_POST["propertyto"]);
		if(isset($_POST["propertyfrom"]))
			$propertyfrom=trim($_POST["propertyfrom"]);
		if(isset($_POST["sel2"]))
			$sel2=trim($_POST["sel2"]);
		if(isset($_POST["sel3"]))
			$sel3=trim($_POST["sel3"]);
		if(isset($_POST["sel4"]))
			$sel4=trim($_POST["sel4"]);
		if(isset($_POST["Yes4"]))
			$Yes4=trim($_POST["Yes4"]);
		if(isset($_POST["sel5"]))
			$sel5=trim($_POST["sel5"]);
		if(isset($_POST["sel6"]))
			$sel6=trim($_POST["sel6"]);	
		if(isset($_POST["sel7"]))
			$sel7=trim($_POST["sel7"]);	
		if(isset($_POST["vacate"]))
			$vacate=trim($_POST["vacate"]);	
		if(isset($_POST["sel8"]))
			$sel8=trim($_POST["sel8"]);
		if(isset($_POST["rate"]))
			$rate=trim($_POST["rate"]);
		if(isset($_POST["complete"]))
			$complete=trim($_POST["complete"]);	
		if(isset($_POST["position"]))
			$position=trim($_POST["position"]);	
		if(isset($_POST["TDate"]))
			$TDate=trim($_POST["TDate"]);	
		if(isset($_POST["ADate"]))
			$ADate=trim($_POST["ADate"]);
		if(isset($_POST["Agency"]))
			$Agency=trim($_POST["Agency"]);
		if(isset($_POST["Name"]))
			$Name=trim($_POST["Name"]);
		if(isset($_POST["Breach"]))
			$Breach=trim($_POST["Breach"]);
		if(isset($_POST["Fax"]))
			$Fax=trim($_POST["Fax"]);
		if(isset($_POST["PAddress"]))
			$PAddress=trim($_POST["PAddress"]);
		if(isset($_POST["bond"]))
			$bond=trim($_POST["bond"]);
	        if(isset($_POST["terminate"]))
			$terminate=trim($_POST["terminate"]);
	        if(isset($_POST["complaints"]))
			$complaints=trim($_POST["complaints"]);
	        if(isset($_POST["comments"]))
			$comments=trim($_POST["comments"]);
	        if(isset($_POST["rent"]))
			$rent=trim($_POST["rent"]);
	        if(isset($_POST["damage"]))
			$damage=trim($_POST["damage"]);
            if(isset($_POST["PEmail"]))
			$PEmail=trim($_POST["PEmail"]);
		    if(isset($_POST["AppID"])){
			$AppID=trim($_POST["AppID"]);
			
			$patani=mysqli_fetch_array(mysqli_query($dbh,"SELECT PM_Email FROM application WHERE GUID='".$AppID."'"));
			$PM_Email=$patani['PM_Email'];
			
			}
		   
		   
		
		
	}	


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Tenancy Application');
$pdf->SetTitle('Review Application');
$pdf->SetSubject('PDF View');
$pdf->SetKeywords('Tenancy, PDF, Application');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '
<style>

table {
padding:10px 0;
}

td.label {
width:10%;
color:#000;
}

.data {
width:50%;
color:#000;
}
span.data {
display:block;
}
</style>

<table  border="0" cellspacing="0" cellspacing="0">
<tr>
<td style="color:#000;margin-top:0;text-decoration:underline;font-size:25px;">REQUEST FOR REFERENCE</td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td class="label">Date:</td>
<td class="data">'.$ADate.'</td>
</tr>

<tr>
<td class="label">Agency:</td>
<td class="data">'.$Agency.'</td>
</tr>

<tr>
<td class="label">Fax:</td>
<td class="data">'.$Fax.'</td>
</tr>

<tr>
<td class="label" style="width:18% !important;">Tenant(s) Name:</td>
<td class="data" style="width:42%;">'.$Name.'</td>
</tr>

<tr>
<td class="label" style="width:18% !important;">Property Address:</td>
<td class="data" style="width:42%;">'.$PAddress.'</td>
</tr>
</table>

<table style="border:1px solid #000; text-align:center;" border="0" cellspacing="0" cellspacing="0">
<tr>
<td>Would you please answer the following questions in relation to the above person(s)</td>
</tr>
<tr>
<td>A signed declaration is following giving authorisation to disclose this information.</td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 1. Was the applicant(s) listed as a lessee? </td>
<td> '.$sel1.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 2. What dates were they in the property? </td>
<td> '.$propertyfrom .' to '.$propertyto .' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 3. Did your office Terminate the Tenancy? </td>
<td> '.$sel2.' </td>
</tr>
<tr>
<td> If yes, please state reason </td>
<td> '.$terminate.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 4. During the tenancy, were the tenant(s) ever in arrears? </td>
<td> '.$sel3.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 5. Did the tenant recieve any Breach Notices other than arrears? </td>
<td> '.$sel4.' </td>
</tr>
<tr>
<td> If yes, what? </td>
<td> '.$Breach.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 6. Was there any damage noted during inspections? </td>
<td> '.$sel5.' </td>
</tr>
<tr>
<td> If yes, what? </td>
<td> '.$damage.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 7. During the tenancy, what were the inspections like? </td>
<td> '.$sel6.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 8. Were pets kept at the Property? </td>
<td> '.$sel7.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 9.Was there any complaints or damage? </td>
<td> '.$complaints.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 10. What date did tenants vacate the property? </td>
<td> '.$vacate.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 11. Was the Bond refunded in full? </td>
<td> '.$sel8.' </td>
</tr>
<tr>
<td> If not, please state reason </td>
<td> '.$bond.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 12. On a scale of 1-10 how would you rate this tenant(s)? </td>
<td> '.$rate.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> 13. Would you rent to them again? </td>
<td> '.$rent.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> Additional Comments: </td>
<td> '.$comments.' </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> Completed by: </td>
<td> '.$complete.' </td>

<td> Position: </td>
<td> '.$position.' </td>

<td> Date: </td>
<td> '.$TDate .' </td>
</tr>
</table>

<table style="text-align:center;" border="0" cellspacing="0" cellspacing="0">
<tr>
<td style="font-size:25px;font-weight: bold;"> Please provide a tenant ledger </td>
</tr>
</table>

<table border="0" cellspacing="0" cellspacing="0">
<tr>
<td> Thank you for your time. </td>
</tr>
</table>';
		
		$pdf->writeHTML($html, true, false, true, false, '');
			  
      

// reset pointer to the last page
$pdf->lastPage();
 
$pdf->Output("/home/tenancyapplicati/public_html/pdf/review/data/tenancy_reference.pdf", "F"); 


require_once '../../PHPMailer/class.phpmailer.php';

	$mail = new PHPMailer(true); 
		try { 
			$mail->CharSet = 'UTF-8';
			$mail->AddAddress($PM_Email); 
			$mail->SetFrom($PEmail); 
			$mail->FromName = 'TenancyApplication.com.au';
			$mail->WordWrap = 50;
			$mail->IsHTML(true);
			$mail->Subject = 'REFERENCE SUBMITTED'; 
			$mail->Body    = 'NEW REFERENCE HAS BEEN SUBMITTED';
			$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->AddAttachment("/home/tenancyapplicati/public_html/pdf/review/data/tenancy_reference.pdf");   
			$mail->Send(); 
		} 
		catch (phpmailerException $e) 
		{ 
			echo $e->errorMessage(); 
}
redirect("../../thankyou.php");


//============================================================+
// END OF FILE
//============================================================+
