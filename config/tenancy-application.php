<?php

return [
    'states' => [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ],
    'title' => [
        'Mr', 'Mrs', 'Miss'
    ],
    'email-application-notify' => [
        'Notify the applicant of a successful application',
        'Notify the applicant of an unsuccessful application',
        'Notify the applicant that applications have closed for this property'
    ],
    'arrears' => ['Frequently', 'Occassionally', 'Rarely', 'Never'],
    'inspection_like' => ['Immaculate', 'Good', 'Average', 'Poor'],
    'setting-keys' => [
        'Preferred Connection Provider Notification Through', 
        'Connection Provider Email'
    ],
    'document_description' => [
        '40 points* Primary/Photo ID. or National card',
        'Secondary Photo',
        '20 points* proof of address bank statement, utility bill, motor vehicle registration.(Eg. Birth Certificate, Student Card, Medicare Card, Health Care Card, Vehicle Registration)',
        '15 points Rental History/tenent ledger or rental reference/older referene',
        '15 points* Employment/Proof of Income. (Eg. Payslips, Letter of Employment, Employment Reference)',
        '10 points for lease Reference',
        ''
    ],
    'titles' => [
        'Mr',
        'Mrs',
        'Miss'
    ],
    'marital' => [
        'single',
        'divorced',
        'engaged',
        'partner living with me',
        'seperated',
        'widow',
        'married'
    ],
    'relation' => [
        'aunty',
        // 'boss',
        'brother',
        'colleague',
        'cousin',
        'daughter',
        'defacto',
        'employer',
        'father',
        'father in law',
        'friend',
        'grand daughter',
        'grand son',
        'grand father',
        'grand mother',
        'husband',
        'lawyer',
        'manager',
        'mother',
        'mother in law',
        'nephew',
        'niece',
        'non immediate relative',
        'partner',
        'previous employer',
        'sister',
        'son',
        'supervisor',
        'uncle',
        'wife',
        'other'
    ],
    'arrangement' => [
        'Owner',
        'Renting Through An Agent',
        'Renting Through Private Landlord',
        'With Parent',
        'Sharing',
        'Other'
    ],
    'employment' => [
        'Not Employed',
        'Own Business',
        'Contractor',
        'FT Employed',
        'PT Employed',
        'Casual',
        // 'Employed',
        'Retired',
        'Pensioner',
        'Student'
    ],
    'year-range' => '1920:2020',
    'moving-service-connection' => [
        'Compare And Connection',
        'Others',
        'Custom Connection'
    ],
    'myconnect-property-type' => [
        'HOUSE',
        'UNIT',
        'TOWNHOUSE',
        'UNIT',
        'FLAT',
        'UNIT'
    ],

];
