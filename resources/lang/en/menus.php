<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Access Management',

            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'users' => [
                'all'             => 'All Users',
                'change-password' => 'Change Password',
                'create'          => 'Create User',
                'deactivated'     => 'Deactivated Users',
                'deleted'         => 'Deleted Users',
                'edit'            => 'Edit User',
                'main'            => 'Users',
                'view'            => 'View User',
            ],
        ],

        'log-viewer' => [
            'main'      => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general'   => 'General',
            'system'    => 'System',
        ],

        'agency' => [
            'create' => 'Create Consultancy',
            'edit'   => 'Edit Consultancy',
            'delete' => 'Delete Consultancy',
            'all'    => 'All Agencies',  
            'users' => [
                'all' => 'All Consultancy Users',
                'create' => 'Create Consultancy User',
            ],
        ],
    ],

    'agency' => [
        'access' => [
            'title' => 'Access Management',

            'roles' => [
                'all'        => 'All Roles',
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',
                'main'       => 'Roles',
            ],

            'users' => [
                'all'             => 'All Users',
                'change-password' => 'Change Password',
                'create'          => 'Add User',
                'deactivated'     => 'Deactivated Users',
                'deleted'         => 'Deleted Users',
                'edit'            => 'Edit User',
                'main'            => 'Users',
                'view'            => 'View User',
            ],

            'student' => [
                'all'             => 'All Students',
                'change-password' => 'Change Password',
                'create'          => 'Add Student',
                'deactivated'     => 'Deactivated Students',
                'deleted'         => 'Deleted Students',
                'edit'            => 'Edit Student',
                'main'            => 'Students',
                'view'            => 'View Student',
            ],

            'student_note' => [
                'all'             => 'All Notes',
                'edit'            => 'Edit Note',
                'main'            => 'Notes',
                'view'            => 'View Note',
                'create'          => 'Create Note',
            ],

            
        ],

        'log-viewer' => [
            'main'      => 'Log Viewer',
            'dashboard' => 'Dashboard',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general'   => 'General',
            'system'    => 'System',
        ],

        'agency' => [
            'create' => 'Create Agency',
            'edit'   => 'Edit Agency',
            'delete' => 'Delete Agency',
        ],
    ],

    'property-manager' => [
        'sidebar' => [
            'general' => 'General',
            'dashboard' => 'Dashboard',
            'tenant' => 'Tenant',
            'application-management' => 'Application Management',
            'application-pdf' => 'Tenancy Application PDF',
            'application' => 'Application',
            'payment-management' => 'Payment Management',
            'request-deposit' => 'Request Deposit',
            'request-ongoing-payment' => 'Request Ongoing Payment',
            'maintainance' => 'Maintainance Requests',
            'service' => 'Services',
            'moving-service-applicants' => 'Moving Service Applicants',
            'maintainance-request' => 'Maintainance Request',
            'office-tenants' => 'Office Tenants'
        ],
        'dashboard' => [
            'application' => 'Applications',
            'tenants' => 'Tenants',
            'maintainance-requests' => 'Maintainance Requests',
            'moving-service' => 'Moving Service Applications',
        ],
    ],

    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar'    => 'Arabic',
            'zh'    => 'Chinese Simplified',
            'zh-TW' => 'Chinese Traditional',
            'da'    => 'Danish',
            'de'    => 'German',
            'el'    => 'Greek',
            'en'    => 'English',
            'es'    => 'Spanish',
            'fr'    => 'French',
            'id'    => 'Indonesian',
            'it'    => 'Italian',
            'ja'    => 'Japanese',
            'nl'    => 'Dutch',
            'pt_BR' => 'Brazilian Portuguese',
            'ru'    => 'Russian',
            'sv'    => 'Swedish',
            'th'    => 'Thai',
            'tr'    => 'Turkish',
        ],
    ],
];
