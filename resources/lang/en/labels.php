<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all'     => 'All',
        'yes'     => 'Yes',
        'no'      => 'No',
        'custom'  => 'Custom',
        'actions' => 'Actions',
        'active'  => 'Active',
        'buttons' => [
            'save'   => 'Save',
            'update' => 'Update',
        ],
        'hide'              => 'Hide',
        'inactive'          => 'Inactive',
        'none'              => 'None',
        'show'              => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],

            'property-manager' => [
                'management' => 'Property Managers Management',
            ],

            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',

                'table' => [
                    'confirmed'      => 'Confirmed',
                    'created'        => 'Created',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Last Updated',
                    'name'           => 'Name',
                    'first_name'     => 'First Name',
                    'last_name'      => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted'     => 'No Deleted Users',
                    'roles'          => 'Roles',
                    'social' => 'Social',
                    'total'          => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                        'consultancy-detail' => 'Consultancy Detail',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmed',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'updated_at'   => 'Last Updated',
                            'email'        => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name'         => 'Name',
                            'first_name'   => 'First Name',
                            'last_name'    => 'Last Name',
                            'status'       => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
        'agency'   =>  [
            'agency'=> 'Agency',
            'management' => 'Agency Management',
            'active'     => 'Active Agencies',
            'create'     => 'Create Agency',
            'edit'       => 'Edit Agency',
            'view'       => 'View Agency',
            'table' => [
                'name'           =>'Name',
                'address'        =>'Address',
                'district'       =>'District',
                'phone'          =>'Phone',
                'website'        =>'Website',
                'registration_no'=>'Registration No',
                'email'          =>'Email',
                'mobile'         =>'Mobile',
                'status'         =>'Status',
                'verified'       =>'Verified',
                'created_at'     =>'Created At',
                'updated_at'     =>'Updated At',
            ],
            'tabs' => [
                'overview' => 'Overview',
                'history'  => 'History',
                'user-list' => 'Associated Users',
            ],
        ],
    ],

    'agency' => [
        'access' => [
            'roles' => [
                'create'     => 'Create Role',
                'edit'       => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions'     => 'Permissions',
                    'role'            => 'Role',
                    'sort'            => 'Sort',
                    'total'           => 'role total|roles total',
                ],
            ],

            'users' => [
                'active'              => 'Active Users',
                'all_permissions'     => 'All Permissions',
                'change_password'     => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create'              => 'Create User',
                'deactivated'         => 'Deactivated Users',
                'deleted'             => 'Deleted Users',
                'edit'                => 'Edit User',
                'management'          => 'User Management',
                'no_permissions'      => 'No Permissions',
                'no_roles'            => 'No Roles to set.',
                'permissions'         => 'Permissions',

                'table' => [
                    'confirmed'      => 'Confirmed',
                    'created'        => 'Created',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'last_updated'   => 'Last Updated',
                    'name'           => 'Name',
                    'first_name'     => 'First Name',
                    'last_name'      => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted'     => 'No Deleted Users',
                    'roles'          => 'Roles',
                    'social' => 'Social',
                    'total'          => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history'  => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmed',
                            'created_at'   => 'Created At',
                            'deleted_at'   => 'Deleted At',
                            'email'        => 'E-mail',
                            'last_updated' => 'Last Updated',
                            'name'         => 'Name',
                            'first_name'   => 'First Name',
                            'last_name'    => 'Last Name',
                            'status'       => 'Status',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
        'agency'   =>  [
            'consultancy'=> 'Consultancy',
            'management' => 'Consultancy Management',
            'active'     => 'Active',
            'create'     => 'Create Consultancy',
            'table' => [
                'name'           =>'Name',
                'address'        =>'Address',
                'district'       =>'District',
                'phone'          =>'Phone',
                'website'        =>'Website',
                'registration_no'=>'Registration No',
                'email'          =>'Email',
                'mobile'         =>'Mobile',
                'status'         =>'Status',
                'verified'       =>'Verified',
            ],
        ],
        'user' => [
            'user' => 'User',
            'management' => 'User Management',
            'property-manager' => 'Property Managers',
        ],
        'student' => [
            'student' => 'Student',
            'management' => 'Student Management',
            'active' => 'Active Students',
            'create' => 'Create Student',
            'table' => [
                    'confirmed'      => 'Confirmed',
                    'created'        => 'Created',
                    'created_at'     => 'Created At',
                    'updated_at'     => 'Updated At',
                    'last_updated'   => 'Last Updated',
                    'email'          => 'E-mail',
                    'id'             => 'ID',
                    'name'           => 'Name',
                    'first_name'     => 'First Name',
                    'middle_name'    => 'Middle Name',
                    'last_name'      => 'Last Name',
                    'mobile'         => 'Mobile',
                    'phone'          => 'Phone',
                    'address'        => 'Address',
                    'district'       => 'District',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted'     => 'No Deleted Users',
                    'roles'          => 'Roles',
                    'social'         => 'Social',
                    'total'          => 'user total|users total',
                    'dob'            => 'Date of Birth',
                    'gender'         => 'Gender',
                    'remarks'        => 'Remarks',
                ],
            'view' => 'View Student',
            'show' => [
                'tabs' =>[
                    'student-detail'    => 'Student Detail',
                    'history'           => 'History',
                    'note'              => 'Note',
                    'created_at'        => 'Created At',
                    'updated_at'        => 'Updated At',
                    'add-info'          => 'Additional Information',
                ],
            ],
        ],
        
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Login',
            'login_button'       => 'Login',
            'login_with'         => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button'    => 'Register',
            'remember_me'        => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Save',
        ],

        'passwords' => [
            'forgot_password'                 => 'Forgot Your Password?',
            'reset_password_box_title'        => 'Reset Password',
            'reset_password_button'           => 'Reset Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'macros' => [
            'country' => [
                'alpha'   => 'Country Alpha Codes',
                'alpha2'  => 'Country Alpha 2 Codes',
                'alpha3'  => 'Country Alpha 3 Codes',
                'numeric' => 'Country Numeric Codes',
            ],

            'macro_examples' => 'Macro Examples',

            'state' => [
                'mexico' => 'Mexico State List',
                'us'     => [
                    'us'       => 'US States',
                    'outlying' => 'US Outlying Territories',
                    'armed'    => 'US Armed Forces',
                ],
            ],

            'territories' => [
                'canada' => 'Canada Province & Territories List',
            ],

            'timezone' => 'Timezone',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar'             => 'Avatar',
                'created_at'         => 'Created At',
                'edit_information'   => 'Edit Information',
                'email'              => 'E-mail',
                'last_updated'       => 'Last Updated',
                'name'               => 'Name',
                'first_name'         => 'First Name',
                'last_name'          => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],

        'agency' => [
            'box_title' => 'Consultancy Details',
            'button'    => 'Send',
        ],

    ],
];
