<!DOCTYPE html>
<html>
<head>
    <title>Tenant Application Form</title>
    <style>
    .page-break {
        page-break-after: always;
    }

    body {
        margin: 0px;
        font-size: 12px;
        font-family: sans-serif;   
    }

    h1 {
        text-align: center;
    }

    h2 {
        text-decoration: underline;
        font-weight: bold;
        font-size: 15px;
    }
    h3{
        font-size: 14px;
    }


    input[type="checkbox"], input[type="radio"] {
        vertical-align: middle;
        margin-top: 8px;
        line-height: 8px;
    }

    input[type="text"]{
        height: 20px;
        vertical-align: middle;
        background-color: rgb(233, 236, 247);
        border-left-color: #fff;
        border-right-color: transparent;
        border-top-color: transparent;
        border-bottom-color: transparent;

    }

    textarea{
         background-color: rgb(233, 236, 247);
         border-color: rgb(233, 236, 247);
         height: 60px;
    }

    label {
        position: relative;
        border-right-color: grey;

        vertical-align: middle;
        font-size: 12px;


    }
    small {
         vertical-align: middle;

    }
    span{
        height: 90px;
        padding-top: 16px;
        padding-bottom: 16px;
    }
    ol li{
        margin-bottom: 10px;
    }
</style>
</head>
<body>


    <div style="width: 100%; position: relative; float: left;">
        <img src="{{-- {{ url('images/logos/ray.png') }} --}}" style="max-width: 250px; width: 100%; float: right; ">
    </div>
    <br>
    <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    <br>
    <h1 style="width: 100%; text-align: left; text-transform: capitalize; font-size: 60px; color: #777;">Tenancy <br>Application Form</h1>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

    <div style="height: 110px; width: 250px; background-color: rgb(241, 244, 255); padding-top: 10px; padding-left: 15px; ">
        Ray White
        <br>
        <br>
        
        <span ">  
            P: </span>
        <br>
        <br>
        <span> E:
        </span>
    </div>

    <div class="page-break"></div>

    <div class="second-page" style="width: 45%; text-align: left;
    padding-right: 30px; position: relative; float: left;">
        <h2>Property Details</h2>
       <span >
        <label>Property Address Applied For? </label>
        <input type="text" style="width: 167px;">
       </span> 
        
        <span>
            <label class="ei-label">State </label>
        <input type="text" style="width: 122px;">
        <label>Suburb </label>
        <input type="text" style="width: 123px;">
        </span>
        
      
        <label>Post Code </label>
        <input type="text" style="width: 72px;">
        <label>Property Type </label>
        <input type="text" style="width: 109px;">
        
        
        
        <label>Number Of Bedrooms? </label>
        <input type="text" style="width: 209px;">
        
        <label>Preferred Commencement Date</label>
        <input type="text" style="width: 168px;">
        
        <label>Length of Lease <small>(months)</small></label>
        <input type="text" style="width: 207px;">
        
        <label>Weekly Rent</label>
        <input type="text" style="width: 92px;">
        <label>Monthly Rent</label>
        <input type="text" style="width: 92px;">
        
        <label>Bond</label>
        <input type="text" style="width: 312px;">

        <br>
        <br>
        <label>Do you require assistance with your bond payment?</label>
        <br>
        <input type="checkbox"> Yes
        <input type="checkbox"> No

        <br>
        <br>
        <label>How did you find out about this property? </label>
        <br>
        

        @foreach (App\Models\Tenant\TenantApplication::PROPERTYFOUNDIN as $propertyfound)
        <input type="checkbox"> {{ $propertyfound }}  <br>
        @endforeach
        <br>

    </div>

    <div style="width:45%; float: left; padding-left: 30px;">

        <h2>Property Manager Details</h2>

        <label>Agency Name</label>
        <input type="text" style="width: 232px;">
        

        <label>Property Manager Name</label>
        <input type="text" style="width: 177px;">
        

        <label>Property Manager Email</label>
        <input type="text" style="width: 178px;">

        <h3>All Childreen Information</h3>
    <textarea rows="3"></textarea>
    {{-- Vehicles Details --}}
    
    <h3>All Vehicles Information</h3>
    <textarea rows="3"></textarea>
    {{-- Pets Details --}}

    <h3>All Pet Information</h3>
    <textarea rows="3"></textarea>
    </div>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

<h2>All Occupancy Details</h2>
    {{-- Occupant Details --}}
    <textarea rows="10"></textarea>
    

    {{-- page-break --}}
    


    {{-- Children Details --}}

    
    {{-- Agent Specific Details --}}

    

    <h3> Agent Specific</h3>

    <div class="second-page" style="width: 45%; padding-right: 30px;  float: left;">

    <label>Has your tenancy ever been terminated by a landlord or agent?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> NO
    <br>
    <br>

    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>

</div>

<div style="width:45%; float: left; padding-left: 10px;">
    <label>Have you ever been refused a property by any landlord or agent?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>
    


    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>
    <br>

</div>
<br>
<br>
<br>
    <br>
<br>
<br>
    <br>
<br>
<br>
<br>
<br>
<br>



<div class="second-page" style="width: 45%; padding-right: 30px; float: left;">
    <label>Are you in debt to another landlord or agent?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>


    <label>If No Please Provide Details </label>
    <br>

    <textarea rows="4"></textarea>
</div>

<div style="width:50%; float: left; padding-left: 10px;">
    <label>Have any deductions ever been made from your rental bond?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>
    

    <label>If NoPlease Provide Details </label>
    <br>

    <textarea rows="4"></textarea>
    <br>
</div>
<div class="page-break"></div>
<div class="second-page" style="width: 45%; padding-right: 30px; float: left;">
  
    <label>Is there any reason known to you that would affect your future rental payments?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>
    

    <label>If No Please Provide Details </label>
    <br>

    <textarea rows="4"></textarea>
    <br>
</div>
<div class="second-page" style="width: 45%; padding-left: 10px; float: left;">

    <label>Do you have any other applications pending on other properties?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>

    <label>Do you currently own a property?</label><br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>

    <label>Please Provide Property Address </label>
    <br>

    <textarea rows="4"></textarea>
    <br>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

    <label>Are you considering buying a property after this tenancy or in the near future?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <br>

    <label>If Yes When do you intend on buying?</label>
    <br>
    <br>
    <input type="checkbox"> Within 12 months
    <input type="checkbox"> Within 24 months

    {{-- Moving service --}}
    <h3> Utility Connections</h3>
    <label>Selecting the right utility provider can make your move easier and may save you money.
    Would you like a call closer to your moving date to help arrange connection to your household services? You'll get a phone call, email and help to compare and select a plan from available suppliers.</label>
    <br>
    Yes {{ Form::radio('need_moving_service', 'yes') }}
    No {{ Form::radio('need_moving_service', 'no') }}
    <br>
    <br>

    <label>If Yes Select Service</label>
    @foreach (App\Models\Tenant\TenantApplication::MOVINGSERVICEOPTION as $option)
    <br>

    <input type="checkbox"> {{ $option }}
    @endforeach

    <br>
    <label>
        <input type="checkbox">
        I agree to the declaration above and use my Digital Signature for the purpose of this Application.
    </label>

<div class="page-break"></div>
    {{-- Rental Rewards --}}
    <h3> Rental Rewards</h3>
     <img src="{{-- {{ url('images/rentalrewards_logo.png') }} --}}" alt="Rental Rewards Logo" width="70%"> 

    <p><strong>Bond &amp; Initial Payments :</strong></p>

    <p><strong>We sk for this Terms to Fallow : </strong></p>

     <ol style="list-style-type: lower-alpha;">
        <li>Bond payments can be made direct to the Bond Board or by Bank Cheque or Money Order.</li>
        <li>The first 2 weeks rent are to be paid by Visa/MasterCard Credit Card or Debit Card, AMEX or Diners.</li>
     </ol>
    <p><strong>Holding Deposit</strong>.</p>

    <p>The holding deposit can only be accepted after the application for tenancy has been approved.</p>

    <p>The holding fee of 1 WEEKS RENT keeps the premises off the market for the prospective tenant for 7 days (or longer by agreement). In consideration of an approved holding fee paid by a prospective tenant, the agent acknowledges that:</p>

    <ol style="list-style-type:lower-roman ">
    <li>The application for tenancy has been approved by the landlord; and</li>

    <li>The premises will not be let during the above period, pending the making of a residential tenancy agreement; and</li>

    <li>If the prospective tenant(s) decide not to enter into such an agreement, the landlord may retain the whole fee; and</li>

    <li>If a residential tenancy agreement is entered into, the holding fee is to be paid towards rent for the residential premises concerned.</li>

    <li>The whole of the fee will be refunded to the prospective tenant if:</li>

        <ol style="list-style-type: lower-alpha;">
            <li>the entering into of the residential tenancy agreement is conditional on the landlord carrying out repairs or other work and the landlord does not carry out the repairs or other work during the specified period</li>
            <li>the landlord/landlord’s agent have failed to disclose a material fact(s) or made misrepresentation(s) before entering into the residential tenancy agreement.</li>
        </ol>
     </ol>   
   
    <p><strong>Ongoing Rent Payments</strong></p>

    <p>Our preferred payment method for rent is Rental Rewards. (Please note that there is a small convenience fee charged for the use of the system). These fees are charged by a third-party payment processor – Rental Rewards</p>

    <p>The fees for the convenience of the use of the services are: (must be able to set-up fee at agent level, as there is variation)</p>
    <ol style = "list-style-type: lower-alpha;">

        <li>Bank Account: $4 Testing.</p>

        <li>BPAY: $4.40 inc GST (internet banking using Rental Rewards Biller and Reference).</p>

        <li>BINt : $45</p>

    </ol>

    <p>Please read above clearly and&nbsp; do as it is asked.</p>

    {{-- Declaration Details --}}
    <h3> Declaration Details</h3>
    <label>Have you inspected this property?</label>
    <br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>
    <h3>If Yes please fill the following fields.</h3>
    <label>Inspected Date </label>
    <input type="text" style="width: 400px;">
    <br>

    <label>Was the property upon your inspection in a reasonably clean and fair condition? </label><br>
    <input type="checkbox"> Yes
    <input type="checkbox"> No
    <br>

    <label>Inspection Code</label>
    <input type="text" style="width: 400px;">
    <br>

    <label>
        <input type="checkbox" style="height: 15px; width: 20px;">
        By ticking this box I acknowledge that I have Read, Understood and Agree with the above Tenancy Privacy Statement / Collection Notice & Tenant Declaration and I authorise the use of my digital signature for the purpose of this application
    </label>
</body>
</html>


<!-- deleted part -->


<div class="second-page" style="width: 50%; text-align: left;
padding-right: 10px; position: relative; float: left;">
<h2>Property Details</h2>
<table style="width:100%">
    <tr>
        <th style="width:50%;">Property Address Applied For?</th><td style="width: 50%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="width: 20%;">State</th><td style="width: 80%;"> &nbsp;</td>
        <th style="width: 22%;">Suburb</th><td style="width: 78%;"> &nbsp;</td>
    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 35%;">Post Code</th><td style="width: 65%;"> &nbsp;</td>
        <th style="width: 50%;">Property Type</th><td style="width: 50%;"> &nbsp;</td>
    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 45%;">Number Of Bedrooms?</th><td style="width: 55%;"> &nbsp;</td>

    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 50%;">Preferred Commencement Date</th><td style="width: 50%;"> &nbsp;</td>

    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 78%;">Length of Lease <small>(months)</small></th><td style="width: 22%;"> &nbsp;</td>
        <th style="width: 50%;">Weekly Rent</th><td style="width: 50%;"> &nbsp;</td>

    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 50%;">Monthly Rent</th><td style="width: 50%;"> &nbsp;</td>
        <th style="width: 30%;">Bond</th><td style="width: 70%;"> &nbsp;</td>

    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 73%;">Do you require assistance with your bond payment?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 100%;">How did you find out about this property?</th>
    </tr>
</table>

<table style="width:100%">

    <tr>

        @foreach (App\Models\Tenant\TenantApplication::PROPERTYFOUNDIN as $propertyfound)


        @if($loop->index < 3)
        <td style="width: 50%;"><input type="checkbox"> {{ $propertyfound }}  </td>
        @endif
        @endforeach

    </tr>
    <tr>
        @foreach (App\Models\Tenant\TenantApplication::PROPERTYFOUNDIN as $propertyfound)

        @if($loop->index > 2)
        <td style="width: 50%;"><input type="checkbox"> {{ $propertyfound }}  </td>
        @endif
        @endforeach

    </tr>

</table>

<h3>All Childreen Information</h3>
<textarea rows="3"></textarea>


</div>

<div style="width:50%; float: left; padding-left: 10px;">

    <h2>Property Manager Details</h2>

    <table style="width:100%">
        <tr>
            <th style="width:30%;">Agency Name</th><td style="width: 70%;"> &nbsp;</td>
        </tr>
    </table>
    <table style="width:100%">
        <tr>
            <th style="width:40%;">Property Manager Name</th><td style="width: 60%;"> &nbsp;</td>
        </tr>
    </table>

    <table style="width:100%">
        <tr>
            <th style="width:40%;">Property Manager Email</th><td style="width: 60%;"> &nbsp;</td>
        </tr>
    </table>

    {{-- Vehicles Details --}}
    
    <h3>All Vehicles Information</h3>
    <textarea rows="2" style="margin-top: -13px;"></textarea>
    {{-- Pets Details --}}

    <h3>All Pet Information</h3>
    <textarea rows="3" style="margin-top: -13px;"></textarea>

    <h3>All Occupants Details</h3>
    <textarea rows="3" style="margin-top: -13px;"></textarea>
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<h2>All Occupants Details</h2>
{{-- Occupant Details --}}
<textarea rows="10"></textarea>


{{-- page-break --}}



{{-- Children Details --}}


{{-- Agent Specific Details --}}



<h3> Agent Specific</h3>

<div class="second-page" style="width: 50%; padding-right: 10px;  float: left;">



    <table style="width:100%">

        <tr>
            <th style="width: 70%;">Has your tenancy ever been terminated by a landlord or agent?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 15%;"><input type="checkbox"> No </td>
        </tr>
    </table>



    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>
    <br>

    <table style="width:100%">

        <tr>
            <th style="width: 70%;">Are you in debt to another landlord or agent?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 15%;"><input type="checkbox"> No </td>
        </tr>
    </table>

    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>


    <br>

    <table style="width:100%">

        <tr>
            <th style="width: 70%;">Is there any reason known to you that would affect your
            future rental payments?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 15%;"><input type="checkbox"> No </td>
        </tr>
    </table>

    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>



</div>

<div style="width:50%; float: left; padding-left: 10px;">


    <table style="width:100%">

        <tr>
            <th style="width: 70%;">Have you ever been refused a property by any landlord or agent?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 15%;"><input type="checkbox"> No </td>
        </tr>
    </table>
    
    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>
    <br>

    <table style="width:100%">

        <tr>
            <th style="width: 70%;">Have any deductions ever been made from your rental bond?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 15%;"><input type="checkbox"> No </td>
        </tr>
    </table>
    <label>If No Please Provide Details </label>
    <br>
    <textarea rows="4"></textarea>
    <br>

    <table style="width:100%">

        <tr>
            <th style="width: 70%;">Do you have any other applications pending on other
                properties?
            </th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 15%;"><input type="checkbox"> No </td>
        </tr>
    </table>

    <table style="width:100%">

        <tr>
            <th style="width: 60%;">Do you currently own a property?
            </th><td style="width: 20%;"><input type="checkbox"> Yes </td><td style="width: 20%;"><input type="checkbox"> No </td>
        </tr>
    </table>
    <label>Please Provide Property Address </label>
    <br>
    <textarea rows="4"></textarea>
    <br>



</div>
<br>


<div class="page-break"></div>



<table style="width:100%">

    <tr>
        <th style="width: 80%;">Are you considering buying a property after this tenancy or in the near future?
        </th><td style="width: 10%;"><input type="checkbox"> Yes </td><td style="width: 10%;"><input type="checkbox"> No </td>
    </tr>

    <tr>
        <th style="width: 80%;">Are you considering buying a property after this tenancy or in the near future?
        </th><td style="width: 10%;"><input type="checkbox"> Yes </td><td style="width: 10%;"><input type="checkbox"> No </td>
    </tr>
    <tr>
        <th style="width: 50%;">If Yes When do you intend on buying?
        </th><td style="width: 25%;"><input type="checkbox"> Within 12 months </td><td style="width: 25%;"><input type="checkbox"> Within 24 months </td>
    </tr>
</table>

<br>

<div class="page-break"></div>

{{-- Moving service --}}
<h3> Utility Connections</h3>

<table style="width:100%">

    <tr>
        <th style="width: 86%;">Selecting the right utility provider can make your move easier and may save you money.
            Would you like a call closer to your moving date to help arrange connection to your household services? You'll get a phone call, email and help to compare and select a plan from available suppliers.
        </th><td style="width: 7%;"><input type="checkbox"> Yes </td><td style="width: 7%;"><input type="checkbox"> No </td>
    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 100%;">If Yes Select Service</th>
    </tr>
</table>

<table style="width:100%">

    <tr>

        @foreach (App\Models\Tenant\TenantApplication::MOVINGSERVICEOPTION as $option)
        
        @if($loop->index < 5)
        <td style="width: 50%;"><input type="checkbox"> {{ $option }}  </td>
        @endif
        @endforeach

    </tr>
    <tr>
        @foreach (App\Models\Tenant\TenantApplication::MOVINGSERVICEOPTION as $option)
        @if($loop->index > 4)
        <td style="width: 50%;"><input type="checkbox"> {{ $option }}  </td>
        @endif
        @endforeach
        
    </tr>
</table>
<br>

<table style="width:100%">

    <tr>

        <th style="width: 5%;"><input type="checkbox">
        </th><td style="width: 95%;"> I agree to the declaration above and use my Digital Signature for the purpose of this Application. </td>
    </tr>
</table>
