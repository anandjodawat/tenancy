@extends('layouts.frontend')

@section ('content')

<div class="container">
    <hr>
    <h1 class="text-center">REQUEST FOR EMPLOYMENT CONFIRMATION</h1>
    <hr>
   
    {{ Form::open(['class' => 'form-horizontal', 'route' => 'employment-confirmation.store', 'method' => 'post']) }}
    <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Date</label>
        <div class="col-md-6">
            {{ Form::text('date', null, ['class' => 'form-control',  'id'=>'employment-confirmation-datepicker']) }}


            @if ($errors->has('date'))
                <strong class="text-danger">{{ $errors->first('date') }}</strong>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Company Name</label>
        <div class="col-md-6">
            {{ Form::text('company_name', null, ['class' => 'form-control']) }}
            @if ($errors->has('company_name'))
                <strong class="text-danger">{{ $errors->first('company_name') }}</strong>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('employee_name') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Employee Name</label>
        <div class="col-md-6">
            {{ Form::text('employee_name', null, ['class' => 'form-control']) }}
            @if ($errors->has('employee_name'))
                <strong class="text-danger">{{ $errors->first('employee_name') }}</strong>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('employee_position') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Employee Position</label>
        <div class="col-md-6">
            {{ Form::text('employee_position', null, ['class' => 'form-control']) }}
            @if ($errors->has('employee_position'))
                <strong class="text-danger">{{ $errors->first('employee_position') }}</strong>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Employee's Work Duration</label>
        <div class="col-md-3">
            {{ Form::text('work_duration_from', null, ['class' => 'form-control', 'id' => 'work-duration-from-datepicker']) }}
        </div>
        <div class="col-md-3">
            {{ Form::text('work_duration_to', null, ['class' => 'form-control', 'id' => 'work-duration-to-datepicker']) }}
        </div>
    </div>

    <hr>
    <h3 class="text-center">Employer Information</h3>
    <div class="form-group {{ $errors->has('completed_by') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Completed By</label>
        <div class="col-md-6">
            {{ Form::text('completed_by', null, ['class' => 'form-control']) }}
            @if ($errors->has('completed_by'))
                <strong class="text-danger">{{ $errors->first('completed_by') }}</strong>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('employer_position') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Position</label>
        <div class="col-md-6">
            {{ Form::text('employer_position', null, ['class' => 'form-control']) }}
            @if ($errors->has('employer_position'))
                <strong class="text-danger">{{ $errors->first('employer_position') }}</strong>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('submission_date') ? 'has-error' : '' }}">
        <label class="col-md-4 control-label">Submission Date</label>
        <div class="col-md-6">
            {{ Form::text('submission_date', null, ['class' => 'form-control',  'id' => 'employment-confirmation-submission-datepicker']) }}
            @if ($errors->has('submission_date'))
                <strong class="text-danger">{{ $errors->first('submission_date') }}</strong>
            @endif
        </div>
    </div>

    {{ Form::hidden('employment_id', request('user')) }}
    {{ Form::hidden('send_to', request('mail_from')) }}
    {{ Form::hidden('manager_email', request('manager_email')) }}
    <hr>
    <h3 class="text-center">Thank You For Your Time.</h3>


    <div class="text-center">
        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
    </div>

    {{ Form::close() }}
    <hr>
</div>
@endsection
@section('after-scripts')

    <script type="text/javascript">
       

          jQuery( "#employment-confirmation-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          
          jQuery( "#employment-confirmation-submission-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#work-duration-to-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#work-duration-from-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
        
      </script>
@endsection 
