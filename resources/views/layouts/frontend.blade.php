<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
    <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
    <meta name="author" content="FREEHTML5.CO" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <!-- <link href="css/font-awesome.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="/frontend/css/frontend-styles.css" rel="stylesheet">

    <link href="/frontend/slick/slick.css" rel="stylesheet">
    <link href="/frontend/slick/slick-theme.css" rel="stylesheet">
    <link href="/frontend/slick/slick-custom.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">



    
    {{-- <link rel="stylesheet" type="text/css" href="url('{{ public_path('fonts/Lato-Bold.ttf') }}')">
    <link rel="stylesheet" type="text/css" href="url('{{ public_path('fonts/Lato-Regular.ttf') }}')"> --}}
<style type="text/css">
@font-face {
    font-family: latobold;
    src: url("/fonts/Lato-Bold.ttf");
}
@font-face {
    font-family: latoregular;
    src: url("/fonts/Lato-Regular.ttf");
}

</style>

    <link rel="shortcut icon" type="" href="/images/favicon.png">

    {{-- <script src="/frontend/js/jquery-1.11.1.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="/frontend/js/bootstrap.min.js"></script>
    <script src="/frontend/js/custom.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
         
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="/js/wow.min.js"></script>

    <script src="/frontend/slick/slick.js"></script>
    <script src="/frontend/slick/slick-custom.js"></script>
</head>
<body>
    @if (!Request::is('employment-confirmation')
        && !Request::is('history-confirmation'))
        @include('frontend.includes.nav')
    @endif
    @if (!Request::is('terms-and-conditions') 
        && !Request::is('privacy-policy')
        && !Request::is('about-us')
        && !Request::is('tenant')
        && !Request::is('propertymanager')
        && !Request::is('contact-us')
        && !Request::is('login')
        && !Request::is('register')
        && !Request::is('employment-confirmation')
        && !Request::is('history-confirmation')
        && !Request::is('getting-started')
        && !Request::is('property-manager-faq')
        && !Request::is('tenant-faq')
        && !Request::is('password/reset')
)

        @include('frontend.includes.header')
    <section class="tanancy-header-home-page" {{-- style="background:  url(images/banner-tenancy.jpg) center center no-repeat; background-size: cover;" --}}>

        <img src="/images/banner-tenancy.jpg" class="img-responsive home-banner-image">
     
        {{-- <p>Apply For Your Next Rental Property</p>
 --}}       
    </section>
    @endif

    @yield('content')

    @if (!Request::is('employment-confirmation')
        && !Request::is('history-confirmation'))
        @include('frontend.includes.footer')
    @endif
        
    <script>
      new WOW().init();
    </script>
    @stack('after-scripts')
</body>
</html>

<!-- linear-gradient(to bottom, rgba(255, 255, 255, 0.99), rgba(149, 88, 151, 0.50)),  -->
