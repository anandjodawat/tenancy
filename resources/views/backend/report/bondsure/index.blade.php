@extends ('backend.layouts.app')

@section ('title', 'Tenant - Applications')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Bond Request Sent
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Bond Request Sent</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="tenant-bondsure-table" class="table table-condensed table-hover">

                <thead>
                    <th>Tenant Code</th>
                    <th>Tenant Name</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Payment</th>
                    <th>Sent To</th>
                    <th>Agency</th>
                    <th>Subject</th>
                    <th>Status</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    @foreach ($applications as $application)
                    @if( !empty($application->email) )
                    <tr>
                        <td>{{ $application->tenant_code }}</td>
                        <td>{{ $application->name }} </td>
                        <td>{{ $application->address }}, {{ $application->suburb }}, {{ $application->state_name }}, {{ $application->postcode }}</td>
                        <td>{{ $application->mobile_number }}</td>
                        <td>{{ $application->payment_amount }}</td>
                        <td>{{ $application->email }}</td>
                        <td>{{ !is_null($application->sentBy)?$application->sentBy->name:'' }}</td>
                        <td>{{ $application->subject }}</td>
                        <td>{{ $application->email_status ? 'SENT' : 'NOT SENT' }}</td>
                        <td>{{ $application->created_at->format('Y/m/d H:i') }}</td>    
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#tenant-bondsure-table').DataTable(

                {
                    "order": [[9, "desc"]],
                    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
                    "dom": 'Blfrtip',
                    "buttons": [
                        { 
                          extend: 'excel',
                          text: 'Download',
                          className: 'downloadButton',
                          title: 'Tenancy Application - Bond Request Sent',
                          filename: "Bond-Request-Sent-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                          exportOptions: {
                            //columns: [ 0, 1, 2, 4, 5 ]
                            //columns: 'th:not(:last-child)'
                            }
                        } 
                    ]
                }

            );
        });
    </script>
@endsection

