@extends ('backend.layouts.app')

@section ('title', 'Tenant - Applications')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Bond Request Sent
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Bond Request Sent</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="tenant-bondsure-table" class="table table-condensed table-hover">

                <thead>
                    <th>State</th>
                    <th>Tenant Name</th>
                    <th>Tenant Email</th>
                    <th>Bondsure Url</th>
                    <th>Status</th>
                    @if (request('status') == 'forwarded')
                    <th>Forwarded To</th>
                    @endif
                    <th>Date</th>
                    <th class="noExl">Action</th>
                </thead>
                <tbody>
                    @foreach ($applications as $application)
                    @if( !empty($application->user) )
                    <tr>
                        <td>{{ $application->state_name }}</td>
                        <td>{{ $application->user->name }}</td>
                        <td>{{ $application->user->email }}</td>
                        <td><a href="{{ $application->bondsure_url }}" class="btn btn-primary btn-xs">BondSure URL</a></td>
                        <td>
                            {{ ucwords($application->status_name) }}
                        </td>
                        @if (request('status') == 'forwarded')
                        <td>
                            {{ optional($application->forwardedTo->sentBy)->name }}
                        </td>
                        @endif
                        <td>{{ $application->created_at->format('Y/m/d H:i') }}</td>
                        <td class="noExl">
                            {{-- <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-sm btn-info"  title="Details">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ route('property-manager.tenancy-pdf-application.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show PDF" target="_blank">
                                <i class="fa fa-file-pdf-o"></i>
                            </a>
                            <a href="{{ route('property-manager.applications.destroy', $application->id) }}" data-method="DELETE" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </a> --}}
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#tenant-bondsure-table').DataTable(

                {
                    "order": [[5, "desc"]],
                    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
                    "dom": 'Blfrtip',
                    "buttons": [
                        { 
                          extend: 'excel',
                          text: 'Download',
                          className: 'downloadButton',
                          title: 'Tenancy Application - Bond Request Sent',
                          filename: "Bond-Request-Sent-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                          exportOptions: {
                            columns: [ 0, 1, 2, 4, 5 ]
                            //columns: 'th:not(:last-child)'
                            }
                        } 
                    ]
                }

            );
        });
    </script>
@endsection

