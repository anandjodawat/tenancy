@extends ('backend.layouts.app')

@section ('title', 'Tenant - Applications')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Connection Services
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Applications</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
           <table class="table" id="connectionServices-applications-table">
            <thead>
                <th>Service Name</th>
                <th>Name</th>
                <th>Email</th>
                <th>Provider</th>
                <th>Agency</th>
                <th>Approved Date</th>
                <th  class="noExl">Action</th>
            </thead>
            <tbody>
                @foreach ($connectionServices as $service)
                @if( !empty($service->application->user) )
                <tr>
                    <td>{{ $service->service_name }}</td>
                    <td>{{ !is_null($service->application)?$service->application->user->first_name:'' }}
                        {{ !is_null($service->application)?$service->application->user->last_name:'' }}
                    </td>
                    <td>{{ !is_null($service->application)?$service->application->user->email:'' }}</td>
                    <td>{{ !is_null($service->application)?$service->application->moving_service_name:'' }}</td>
                    <td>{{ !is_null($service->application)?$service->application->agency_name:'' }}</td><?php //echo '<pre>'; print_r($service->application); ?>
                    <td>{{ $service->created_at->format('Y/m/d H:i') }}</td>
                    <td>
                        {{-- <a href="{{ route('property-manager.applications.show', $service->application_id) }}" class="btn btn-primary btn-sm" title="Show Application" target="_blank">
                            <i class="fa fa-eye"></i>
                        </a> --}}
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection

@section('after-scripts')
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#connectionServices-applications-table').DataTable(

                {
                    "order": [[5, "desc"]],
                    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
                    "dom": 'Blfrtip',
                    "buttons": [
                        { 
                          extend: 'excel',
                          text: 'Download',
                          className: 'downloadButton',
                          title: 'Tenancy Application - Connection Services',
                          filename: "Connection-Services-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                          exportOptions: {
                            //columns: [ 0, 1, 2,3,4,6 ]
                            columns: 'th:not(:last-child)'
                            }
                        } 
                    ]
                }

            );
        });
    </script>
@endsection


