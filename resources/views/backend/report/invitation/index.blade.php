@extends ('backend.layouts.app')

@section ('title', 'Tenant - Applications')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Invitation Sent
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Invitations</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
           <table class="table" id="property-invitation-applications-table">
                <thead>
                    <th>Tenant Email</th>
                    <th>Property Address</th>
                    <th>Sent By</th>
                    <th class="no-sort">Date</th>
                    <th  class="noExl no-sort">Action</th>
                </thead>
                <tbody>
                    @foreach ($invitations as $invitation)
                    <tr>
                        <td>{{ $invitation->tenant_email }}</td>
                        <td>
                            @if( !empty($invitation->property) )
                            {{ $invitation->property->address }}, {{ $invitation->property->suburb }}, {{ $invitation->property->state != null || $invitation->property->state == "0" ? config('tenancy-application.states')[$invitation->property->state] : '' }}, {{ $invitation->property->post_code }}
                            @endif
                        </td>
                        <td>{{ !is_null($invitation->sentBy)?$invitation->sentBy->first_name:'' }}</td>
                        <td>{{ $invitation->created_at->format('Y/m/d H:i') }}</td>
                        <td>
                            <a href="{{ route('property-manager.property-invitation.delete', ['id'=>$invitation->id]) }}" class="btn btn-sm btn-danger"  title="Delete Property Invitation" data-method="DELETE">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#property-invitation-applications-table').DataTable(

                {
                    "order": [[3, "desc"]],
                    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
                    "dom": 'Blfrtip',
                    "buttons": [
                        { 
                          extend: 'excel',
                          text: 'Download',
                          className: 'downloadButton',
                          title: 'Tenancy Application - Invitation Sent',
                          filename: "Invitation-Sent-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                          exportOptions: {
                            //columns: [ 0, 1, 2,3,4,6 ]
                            columns: 'th:not(:last-child)'
                            }
                        } 
                    ]
                }
            );
        });
    </script>
@endsection


