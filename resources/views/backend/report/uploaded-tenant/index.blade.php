@extends ('backend.layouts.app')

@section ('title', 'Tenant - Applications')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Uploaded Tenants
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Applications</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="uploaded-tenants-table" class="table table-condensed table-hover">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th class="no-sort">Mobile Number</th>
                    <th>Email Status</th>
                    <th>Agency Name</th>
                    <th>Agency Email</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    @foreach ($tenants as $tenant)
                    <tr>
                        <td>{{ $tenant->name }}</td>
                        <td>{{ $tenant->email }}</td>
                        <td>{{ $tenant->mobile }}</td>
                        <td>{{ $tenant->email_status == 1 ? 'SENT' : 'NOT SENT' }}</td>
                        <td>{{ !is_null($tenant->agencyData)?$tenant->agencyData->name:'' }}</td>
                        <td>{{ !is_null($tenant->agencyData)?$tenant->agencyData->email:'' }}</td>
                        <td>{{ $tenant->created_at->format('Y/m/d H:i') }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#uploaded-tenants-table').DataTable(

            {

                "order": [[4, "desc"]],
                    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
                    "dom": 'Blfrtip',
                    "buttons": [
                        { 
                          extend: 'excel',
                          text: 'Download',
                          className: 'downloadButton',
                          title: 'Tenancy Application - Uploaded Tenants',
                          filename: "Uploaded-Tenants-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                          exportOptions: {
                            //columns: [ 0, 1, 2,3,4,6 ]
                            //columns: 'th:not(:last-child)'
                            }
                        } 
                    ]

            }

        );
    });
</script>
@endsection


