@extends ('backend.layouts.app')

@section ('title', 'Report - Request Deposit')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Request Deposit
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Request Deposit</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
           <table class="table" id="request-deposits-applications-table">
            <thead>
                <th>Tenant Code</th>
                <th>Name</th>
                <th>Address</th>
                <th>Contact Number</th>
                <th>Payment Amount</th>
                <th>Sent To</th>
                <th>Sent By</th>
                <th>Subject</th>
                <th class="noExl">Status</th>
                <th>Date</th>
            </thead>
            <tbody>
                @foreach ($requests as $request)
                    <tr>
                        <td>{{ $request->tenant_code }}</td>
                        <td>{{ $request->name }}</td>
                        <td>{{ $request->address }}</td>
                        <td>{{ $request->contact_number }}</td>
                        <td>{{ $request->payment_amount }}</td>
                        <td>{{ $request->send_to }}</td>
                        <td>{{ !is_null($request->sentBy)?$request->sentBy->name:'' }}</td>
                        <td>{{ $request->subject }}</td>
                        <td>{{ $request->email_status ? 'SENT' : 'NOT SENT' }}</td>
                        <td>{{ $request->created_at->format('Y/m/d H:i') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#request-deposits-applications-table').DataTable(

                {
                    "order": [[9, "desc"]],
                    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
                    "dom": 'Blfrtip',
                    "buttons": [
                        { 
                          extend: 'excel',
                          text: 'Download',
                          className: 'downloadButton',
                          title: 'Tenancy Application - Request Deposit',
                          filename: "Request-Deposit-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                          exportOptions: {
                            //columns: [ 0, 1, 2,3,4,6 ]
                            //columns: 'th:not(:last-child)'
                            }
                        } 
                    ]
                }

            );
        });
    </script>
@endsection

