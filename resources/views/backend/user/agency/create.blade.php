@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.consultancy.management') . ' | ' . trans('labels.backend.consultancy.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.consultancy.management') }}
        <small>{{ trans('labels.backend.consultancy.create') }}</small>
    </h1>
@endsection

@section('content')
	
	<div class="box box-success">
        <div class="box-body">
            <div class="box-tools pull-left">
                @include('backend.consultancy.includes.partials.consultancy-header-buttons')
            </div>
        </div>
    </div>

	{{ Form::open(['route' => 'admin.consultancy.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

		 @include('backend.consultancy.form')

	{{ Form::close() }}

@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}
@endsection