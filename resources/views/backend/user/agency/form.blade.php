<div class="box box-success">
    <div class="box-header with-border">
    @if ($errors->has('name'))
        <span class="text-danger">{{ $errors->first('name') }}</span>
     @endif
        <h3 class="box-title">{{ trans('labels.backend.consultancy.create') }}</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.name'), ['class' => 'col-lg-2 control-label']) }}<strong>*</strong>

            <div class="col-lg-10">
                {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.name')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.address'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('address', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.address')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.district'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('district', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.district')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.phone'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::number('phone', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.phone')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.website'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('website', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.website')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.registration_no'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::text('registration_no', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.registration_no')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.email'), ['class' => 'col-lg-2 control-label']) }}<strong>*</strong>

            <div class="col-lg-10">
                {{ Form::text('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.consultancy.email')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.mobile'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-10">
                {{ Form::number('mobile', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.mobile')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->

        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.status'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-4">
                {{ Form::select('status',  [
                   'new' => 'New',
                   'active' => 'Active',
                   'inactive' => 'In-Active'], null, ['class' => 'col-lg-3 control-label']) }}
            </div>
        
            {{ Form::label('name', trans('validation.attributes.backend.consultancy.verified'), ['class' => 'col-lg-1 control-label']) }}

            <div class="col-lg-1">
                {{ Form::checkbox('verified', 1, true) }}
            </div>
        </div><!--form control-->

        <div class="pull-left">
            {{ link_to_route('admin.consultancy.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
            {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-md']) }}
            <div class="clearfix"></div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
