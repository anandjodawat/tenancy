
<div class="pull-left mb-10 hidden-sm hidden-md">
    {{ link_to_route('admin.user-management.agencies.index', trans('menus.backend.agency.all'), [], ['class' => 'btn btn-primary btn-md']) }}
    {{ link_to_route('admin.user-management.agencies.index', 'New', ['status' => 'new'], ['class' => 'btn btn-success btn-md']) }}
    {{ link_to_route('admin.user-management.agencies.index', 'Active', ['status' => 'active'], ['class' => 'btn btn-success btn-md']) }}
    {{ link_to_route('admin.user-management.agencies.index', 'In-active', ['status' => 'inactive'], ['class' => 'btn btn-danger btn-md']) }}
</div><!--pull left-->

<div class="pull-left mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-md dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.agencies.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>
                {{ link_to_route('admin.user-management.agencies.index', trans('menus.backend.agency.all')) }}
            </li>
            <li>
                {{ link_to_route('admin.user-management.agencies.index', 'New', ['status' => 'new'], ['class' => 'btn btn-success btn-md']) }}
            </li>
            <li>
                {{ link_to_route('admin.user-management.agencies.index', 'Active', ['status' => 'active'], ['class' => 'btn btn-success btn-md']) }}
            </li>
            <li>
                {{ link_to_route('admin.user-management.agencies.index', 'In-active', ['status' => 'inactive'], ['class' => 'btn btn-success btn-md']) }}
            </li>
        </ul>
    </div><!--btn group-->
</div><!--pull left-->

<div class="clearfix"></div>