@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.consultancy.management') . ' | ' . trans('labels.backend.consultancy.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.consultancy.management') }}
        <small>{{ trans('labels.backend.consultancy.view') }}</small>
    </h1>
@endsection

@section('content')
    
    <div class="box box-success">
        <div class="box-body">
            <div class="box-tools pull-left">
                @include('backend.consultancy.includes.partials.consultancy-header-buttons')
            </div>
        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.consultancy.view') }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">{{ trans('labels.backend.consultancy.tabs.overview') }}</a>
                    </li>

                    <li role="presentation">
                        <a href="#history" aria-controls="history" role="tab" data-toggle="tab">{{ trans('labels.backend.consultancy.tabs.history') }}</a>
                    </li>

                    <li role="presentation">
                        <a href="#user-list" aria-controls="user-list" role="tab" data-toggle="tab">{{ trans('labels.backend.consultancy.tabs.user-list') }}</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane mt-30 active" id="overview">
                        @include('backend.consultancy.show.tabs.overview')
                    </div><!--tab overview profile-->

                    <div role="tabpanel" class="tab-pane mt-30" id="history">
                        @include('backend.consultancy.show.tabs.history')
                    </div><!--tab panel history-->

                    <div role="tabpanel" class="tab-pane mt-30" id="user-list">
                        @include('backend.consultancy.show.tabs.partial-consultancy-user-list')
                    </div>

                </div><!--tab content-->

            </div><!--tab panel-->

        </div><!-- /.box-body -->
    </div><!--box-->
@endsection