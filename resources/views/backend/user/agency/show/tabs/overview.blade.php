<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.consultancy.table.name') }}</th>
        <td>{{ isset($consultancy->name) ? $consultancy->name : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.address') }}</th>
        <td>{{ isset($consultancy->address) ? $consultancy->address : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.district') }}</th>
        <td>{{ isset($consultancy->district) ? $consultancy->district : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.phone') }}</th>
        <td>{{ isset($consultancy->phone) ? $consultancy->phone : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.mobile') }}</th>
        <td>{{ isset($consultancy->mobile) ? $consultancy->mobile : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.website') }}</th>
        <td>{{ isset($consultancy->website) ? $consultancy->website : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.registration_no') }}</th>
        <td>{{ isset($consultancy->registration_no) ? $consultancy->registration_no : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.email') }}</th>
        <td>{{ isset($consultancy->email) ? $consultancy->email : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.status') }}</th>
        <td>{{ isset($consultancy->status) ? $consultancy->status : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.created_at') }}</th>
        <td>{{ isset($consultancy->created_at) ? $consultancy->created_at : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.consultancy.table.updated_at') }}</th>
        <td>{{ isset($consultancy->updated_at) ? $consultancy->updated_at : ' ' }}</td>
    </tr>

    <tr>
        <th>Action Buttons</th>
        <td>
            <a href="{{ route('admin.consultancy.edit', $consultancy->id) }}" class="btn btn-sm btn-primary">
                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
            </a>

            <a href="#" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('delete-consultancy-{{ $consultancy->id }}').submit();">
                <i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
            </a>

            <a href="{{ route('admin.consultancy.activate', ['consultancy_id' => $consultancy->id]) }}" class="btn btn-sm btn-success">
                <i class="fa fa-user" data-toggle="tooltip" data-placement="top" title="Activate Consultancy"></i>
            </a>

            <form action="{{ route('admin.consultancy.destroy', $consultancy->id) }}" id="delete-consultancy-{{ $consultancy->id }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </td>
    </tr>
</table>
