<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.last_name') }}</th>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.first_name') }}</th>
        <th>Role/s</th>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.email') }}</th>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.status') }}</th>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.confirmed') }}</th>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.created_at') }}</th>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.updated_at') }}</th>
        <th>Action</th>
    </tr>
	@foreach($users as $user)
	    <tr>
	        <td>{{ isset($user->last_name) ? $user->last_name : ' ' }}</td>
	        <td>{{ isset($user->first_name) ? $user->first_name : ' ' }}</td>
	        <td>
	        	@foreach($user->roles as $role)
	        		@foreach(explode(' ', $role->name) as $roleName) 
					    {{$roleName}}
					@endforeach
	        	@endforeach
	        </td>
	        <td>{{ isset($user->email) ? $user->email : ' ' }}</td>
	        <td>{{ isset($user->status) ? 'Active' : '-' }}</td>
	        <td>{{ isset($user->confirmed) ? 'Yes' : 'No' }}</td>
	        <td>{{ isset($user->created_at) ? $user->created_at : ' ' }}</td>
	        <td>{{ isset($user->updated_at) ? $user->updated_at : ' ' }}</td>
	        <td>
		        <a href="{{ route('admin.access.user.login-as', $user->id) }}" class="btn btn-xs btn-success">
		        	<i class="fa fa-lock" data-toggle="tooltip" data-placement="top" title="Log In as {{ $user->full_name}}"></i>
		        </a>

		        <a href="{{ route('admin.access.user.show', $user->id) }}" class="btn btn-xs btn-info">
		        	<i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Detail"></i>
		        </a>

		        <a href="{{ route('admin.access.user.edit', $user->id) }}" class="btn btn-xs btn-primary">
		        	<i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
		        </a>

		        <a href="{{ route('admin.access.user.change-password', $user->id) }}" class="btn btn-xs btn-info">
		        	<i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Change Password"></i>
		        </a>

		        <a href="{{ route('admin.access.user.destroy', $user->id) }}" class="btn btn-xs btn-danger">
		        	<i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
		        </a>
		        
		    </td>
	    </tr>
	@endforeach
</table>


