@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.consultancy.management') . ' | ' . trans('labels.backend.consultancy.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.consultancy.management') }}
        <small>{{ trans('labels.backend.consultancy.edit') }}</small>
    </h1>
@endsection

@section('content')
	<div class="box box-success">
        <div class="box-body">
            <div class="box-tools pull-left">
               @include('backend.consultancy.includes.partials.consultancy-header-buttons')
            </div>
        </div>
    </div>

    {{ Form::model($consultancy, ['route' => ['admin.consultancy.update', $consultancy], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.consultancy.edit') }}</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.name'), ['class' => 'col-lg-2 control-label']) }}<strong>*</strong>

		            <div class="col-lg-10">
		                {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.name')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.address'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-10">
		                {{ Form::text('address', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.address')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.district'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-10">
		                {{ Form::text('district', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.district')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.phone'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-10">
		                {{ Form::number('phone', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.phone')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.website'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-10">
		                {{ Form::text('website', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.website')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.registration_no'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-10">
		                {{ Form::text('registration_no', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.registration_no')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.email'), ['class' => 'col-lg-2 control-label']) }}<strong>*</strong>

		            <div class="col-lg-10">
		                {{ Form::text('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.consultancy.email')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.mobile'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-10">
		                {{ Form::number('mobile', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.mobile')]) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="form-group">
		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.status'), ['class' => 'col-lg-2 control-label']) }}

		            <div class="col-lg-4">
		                {{ Form::select('status',  [
		                   'new' => 'New',
		                   'active' => 'Active',
		                   'inactive' => 'In-Active'], null, ['class' => 'col-lg-3 control-label']) }}
		            </div>

		            {{ Form::label('name', trans('validation.attributes.backend.consultancy.verified'), ['class' => 'col-lg-1 control-label']) }}

		            <div class="col-lg-1">
		                {{ Form::checkbox('verified', 1, true) }}
		            </div><!--col-lg-10-->
		        </div><!--form control-->

		        <div class="pull-left">
                    {{ link_to_route('admin.consultancy.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                    {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success btn-md']) }}
                    <div class="clearfix"></div>
                </div>
		    </div><!-- /.box-body -->
        </div><!--box-->
        
    {{ Form::close() }}
@endsection
