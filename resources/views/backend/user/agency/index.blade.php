@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.agency.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.agency.management') }}
        <small>{{ trans('labels.backend.agency.active') }}</small>
    </h1>
@endsection

@section('content')
    
{{--     <div class="box box-success">
        <div class="box-body">
            <div class="box-tools pull-left">
                @include('backend.agency.includes.partials.agency-header-buttons')
            </div>
        </div>
    </div> --}}

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Agencies</h3>
        </div>

        <div class="box-body">
            <div class="table-responsive">
                <table id="agencies-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.backend.agency.table.name') }}</th>
                        <th>{{ trans('labels.backend.agency.table.address') }}</th>
                        <th>{{ trans('labels.backend.agency.table.phone') }}</th>
                        <th>{{ trans('labels.backend.agency.table.status') }}</th>
                        <th>{{ trans('labels.backend.agency.table.website') }}</th>
                        <th>{{ trans('labels.backend.agency.table.registration_no') }}</th>
                        <th>{{ trans('labels.backend.agency.table.email') }}</th>
                        <th>Date</th>
                        <th>Confirmed?</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($agencies as $agency)
                            <tr>
                                <td>{{ $agency->name }}</td>
                                <td>{{ $agency->address }}</td>
                                <td>{{ $agency->phone_number }}</td>
                                <td>
                                    <span class="badge badge-{{ $agency->status == '1' ? 'success' : 'warning' }}">
                                        {{ $agency->status == '1' ? 'ACTIVE' : 'INACTIVE' }}
                                    </span>
                                </td>
                                <td>{{ $agency->website }}</td>
                                <td>{{ $agency->registration_number }}</td>
                                <td>{{ $agency->email }}</td>
                                <td>{{ $agency->created_at->format('Y/m/d H:i') }}</td>
                                <td>
                                        <span class="badge badge-success">
                                        @if ($agency->user->confirmed=='1')
                                                YES
                                        @else        
                                                <a href="{{ route('admin.user-management.property-managers.agencyconfirm', ['id'=>$agency->user->id]) }}" style="color: #fff;" title="Click here to confirm agency">NO</a>
                                        @endif
                                    </span>
                                </td>
                                <td>{{--<a href="{{ route('admin.user-management.agencies.show', $agency->id) }}" class="btn btn-sm btn-info"  title="Details">
                                <i class="fa fa-eye"></i></a>--}}
                                <a href="{{ route('admin.user-management.agencies.destroy', $agency->id) }}" data-method="DELETE" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                                </a> 
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#agencies-table').DataTable({"order": [[7, "desc"]],"lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]]});
        });
    </script>
@endsection

