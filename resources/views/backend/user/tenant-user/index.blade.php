@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.agency.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        User Management
        <small>Tenant Users</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Tenant Users</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="tenant-users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($tenantUsers as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at->format('Y/m/d H:i') }}</td>
                                <td>
                                    {{-- <a href="{{ route('agency.property-managers.edit', $user->id) }}" class="btn btn-info btn-sm" title="Edit Property Manager">
                                        <i class="fa fa-edit"></i>
                                    </a> --}}
                                    <a href="{{ route('admin.user-management.tenant-users.destroy', $user->id) }}" class="btn btn-danger btn-sm" title="Delete Tanent User" data-method="DELETE">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section ('after-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tenant-users-table').DataTable({"order": [[2, "desc"]],"lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]]});
    })
</script>
@endsection
