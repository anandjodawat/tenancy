@extends ('backend.layouts.app')

@section ('title', 'User - Property Managers')

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        User Management
        <small>Active Property Managers</small>
    </h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Property Managers</h3>
        </div>

        <div class="box-body">
            <div class="table-responsive">
                <table id="property-managers-table" class="table table-condensed table-hover">
                    <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($propertyManagers as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <span class="badge badge-{{ $user->confirmed == '1' ? 'success' : 'warning' }}">
                                        {{ $user->confirmed == '1' ? 'ACTIVE' : 'INACTIVE' }}
                                    </span>
                                </td>
                                <td>{{ $user->created_at->format('Y/m/d H:i') }}</td>
                                <td>
                                    {{-- <a href="{{ route('agency.property-managers.edit', $user->id) }}" class="btn btn-info btn-sm" title="Edit Property Manager">
                                        <i class="fa fa-edit"></i>
                                    </a> --}}
                                    <a href="{{ route('agency.property-managers.destroy', $user->id) }}" class="btn btn-danger btn-sm" title="Delete Property Manager" data-method="DELETE">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#property-managers-table').DataTable({"order": [[3, "desc"]],"lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]]});
        });
    </script>
@endsection

