<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->full_name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            @role(1)

            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>
            <li class="header">User Management</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/user-management/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>User Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/user-management/*'), 'menu-open') }}" style="display: none;">
                    <li class="{{ active_class(Active::checkUriPattern('admin/user-management/tenant-users*')) }}">
                        <a href="{{ route('admin.user-management.tenant-users.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Tenant User</span>
                        </a> 
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/user-management/agencies*')) }}">
                        <a href="{{ route('admin.user-management.agencies.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.agency.agency') }}</span>
                        </a>
                    </li>
                    {{-- <li class="{{ active_class(Active::checkUriPattern('admin/users/office-admins*')) }}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>Office Admin</span>
                        </a> 
                    </li> --}}
                    <li class="{{ active_class(Active::checkUriPattern('admin/user-management/property-managers*')) }}">
                        <a href="{{ route('admin.user-management.property-managers.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Property Managers</span>
                        </a> 
                    </li>
                </ul>
            </li>

            

            <li class="header">Application Management</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/applications*')) }}">
                <a href="{{ route('admin.applications.index') }}">
                    <i class="fa fa-newspaper-o"></i>
                    <span>Applications</span>
                </a>
            </li>

            <li class="header">Reports</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/total-applications*')) }}">
                <a href="{{ route('admin.reports.total-applications.index') }}">
                    <i class="menu-icon fa fa-files-o"></i>
                    <span>Total Applications</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/sent-invitations*')) }}">
                <a href="{{ route('admin.reports.sent-invitations.index') }}">
                    <i class="menu-icon fa fa-envelope-o"></i>
                    <span>Invitation Sent</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/request-deposits*')) }}">
                <a href="{{ route('admin.reports.request-deposits.index') }}">
                    <i class="menu-icon fa fa-dollar"></i>
                    <span>Request Deposit Sent</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/ongoing-payments*')) }}">
                <a href="{{ route('admin.reports.ongoing-payments.index') }}">
                    <i class="menu-icon fa fa-credit-card"></i>
                    <span>Request Ongoing Payment Sent</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/connection-services*')) }}">
                <a href="{{ route('admin.reports.connection-services.index') }}">
                    <i class="menu-icon fa fa-flash"></i>
                    <span>Connection Service Requested</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/bondsure*')) }}">
                <a href="{{ route('admin.reports.bondsure.index') }}">
                    <i class="menu-icon fa fa-ticket"></i>
                    <span>Bond Request Sent</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports/uploaded-tenants*')) }}">
                <a href="{{ route('admin.reports.uploaded-tenants.index') }}">
                    <i class="menu-icon fa fa-envelope-o"></i>
                    <span>Emails sent to Uploaded Tenants</span>
                </a>
            </li>

            
            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>

            <!-- <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-key"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>

                    @if ($pending_approval > 0)
                        <span class="label label-danger pull-right">{{ $pending_approval }}</span>
                    @else
                        <i class="fa fa-angle-left pull-right"></i>
                    @endif
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>

                            @if ($pending_approval > 0)
                                <span class="label label-danger pull-right">{{ $pending_approval }}</span>
                            @endif
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/permission*')) }}">
                        <a href="{{ route('admin.access.permission.index') }}">
                            <i class="fa fa-circle-o"></i>
                            Permission Management
                        </a>
                    </li>
                </ul>
            </li> -->


            <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer')) }}">
                        <a href="{{ route('log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs')) }}">
                        <a href="{{ route('log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth
        </ul><!-- /.sidebar-menu -->

    </section><!-- /.sidebar -->
</aside>