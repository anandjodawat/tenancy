<div class="pull-left mb-10 hidden-sm hidden-md">
    {{ link_to_route('admin.access.permission.index', 'All permissions', [], ['class' => 'btn btn-primary btn-md']) }}
    {{ link_to_route('admin.access.permission.create', 'Create Permission', [], ['class' => 'btn btn-success btn-md']) }}
</div>

<div class="pull-left mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-md dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Permissions <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.access.permission.index', 'All Permissions') }}</li>
            <li>{{ link_to_route('admin.access.permission.create', 'Create Permissions') }}</li>
        </ul>
    </div>
</div>