<div class="pull-left mb-10 hidden-sm hidden-md">
    {{ link_to_route('admin.access.role.index', trans('menus.backend.access.roles.all'), [], ['class' => 'btn btn-primary btn-md']) }}
    {{ link_to_route('admin.access.role.create', trans('menus.backend.access.roles.create'), [], ['class' => 'btn btn-success btn-md']) }}
</div><!--pull left-->

<div class="pull-left mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-md dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.access.roles.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('admin.access.role.index', trans('menus.backend.access.roles.all')) }}</li>
            <li>{{ link_to_route('admin.access.role.create', trans('menus.backend.access.roles.create')) }}</li>
        </ul>
    </div><!--btn group-->
</div><!--pull left-->

<div class="clearfix"></div>