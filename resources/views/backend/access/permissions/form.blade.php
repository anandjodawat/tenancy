<div class="form-group">
    {{ Form::label('name', 'Permission Name', ['class' => 'col-lg-2 control-label']) }}

    <div class="col-lg-10">
        {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}

        @if ($errors->has('name'))
        	<span class="help-block">
        		<strong class="text-danger">{{ $errors->first('name') }}</strong>
        	</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('name', 'Display name', ['class' => 'col-lg-2 control-label']) }}

    <div class="col-lg-10">
        {{ Form::text('display_name', null, ['class' => 'form-control', 'required' => 'required']) }}

        @if ($errors->has('display_name'))
        	<span class="help-block">
        		<strong class="text-danger">{{ $errors->first('display_name') }}</strong>
        	</span>
        @endif
    </div>
</div>
