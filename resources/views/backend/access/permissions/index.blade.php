@extends ('backend.layouts.app')

@section ('title', 'Permission Management')

@section('page-header')
    <h1>Permission Management</h1>
@endsection

@section('content')

    <div class="box box-success">
        <div class="box-body">
            <div class="box-tools pull-left">
                @include('backend.access.includes.partials.permission-header-buttons')
            </div>
        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Permission Management</h3>
			<div class="clearfix"></div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="permissions-table" class="table table-condensed table-hover">
                    <thead>
                    	<th>Name</th>
                    	<th>Display Name</th>
                    	<th>Created at</th>
                        <th>Actions</th>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Permission') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $('#permissions-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.access.permission.get") }}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'display_name', name: 'display_name'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', searchable: false, sortable: false}
                ],
            });
        });
    </script>
@endsection
