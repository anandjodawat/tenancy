@extends ('backend.layouts.app')

@section ('title', 'Permission Management | Edit Permission')

@section('page-header')
    <h1>
        Permission Management
        <small>Edit Permission</small>
    </h1>
@endsection

@section('content')
    
    <div class="box box-success">
        <div class="box-body">
            <div class="box-tools pull-left">
                @include('backend.access.includes.partials.permission-header-buttons')
            </div>
        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Create Permission</h3>
        </div>

        <div class="box-body">
            {{ Form::model($permission, ['route' => ['admin.access.permission.update', $permission->id], 'class' => 'form-horizontal', 'method' => 'patch', 'id' => 'create-permission']) }}

                @include('backend.access.permissions.form')

                <div class="pull-left">
                {{ link_to_route('admin.access.permission.index', 'Cancel', [], ['class' => 'btn btn-danger btn-md']) }}
                {{ Form::submit('Update', ['class' => 'btn btn-success btn-md']) }}
                </div>

                <div class="clearfix"></div>
            {{ Form::close() }}
        </div><!-- /.box-body -->
    </div><!--box-->

@endsection