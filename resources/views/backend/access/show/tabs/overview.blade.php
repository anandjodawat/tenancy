<table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.avatar') }}</th>
        <td><img src="{{ $user->picture }}" class="user-profile-image" /></td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.name') }}</th>
        <td>{{ $user->name }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.email') }}</th>
        <td>{{ $user->email }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.status') }}</th>
        <td>{!! $user->status_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.confirmed') }}</th>
        <td>{!! $user->confirmed_label !!}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.created_at') }}</th>
        <td>{{ $user->created_at }} ({{ $user->created_at->diffForHumans() }})</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.access.users.tabs.content.overview.last_updated') }}</th>
        <td>{{ $user->updated_at }} ({{ $user->updated_at->diffForHumans() }})</td>
    </tr>

    @if ($user->trashed())
        <tr>
            <th>{{ trans('labels.backend.access.users.tabs.content.overview.deleted_at') }}</th>
            <td>{{ $user->deleted_at }} ({{ $user->deleted_at->diffForHumans() }})</td>
        </tr>
    @endif

    <tr>
        <th>Action Buttons</th>
        <td>
            <a href="{{ route('admin.access.user.login-as', $user->id) }}" class="btn btn-sm btn-success">
                <i class="fa fa-lock" data-toggle="tooltip" data-placement="top" title="Log In as {{ $user->full_name}}"></i>
            </a>

            <a href="{{ route('admin.access.user.edit', $user->id) }}" class="btn btn-sm btn-primary">
                <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i>
            </a>

            <a href="{{ route('admin.access.user.change-password', $user->id) }}" class="btn btn-sm btn-info">
                <i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title="Change Password"></i>
            </a>

            <a href="#" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('delete-user-{{ $user->id }}').submit();">
                <i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i>
            </a>

            <form action="{{ route('admin.access.user.destroy', $user->id) }}" id="delete-user-{{ $user->id }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </td>
    </tr>
</table>
