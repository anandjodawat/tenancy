
<a href="{{ route('admin.consultancy.edit', $user->id) }}" class="btn btn-md btn-success">Edit</a>

<table class="table table-striped table-hover">
    
    <tr>
        <th>{{ trans('labels.backend.agency.table.name') }}</th>
        <td>{{ isset($consultancy_detail[0]->name) ? $consultancy_detail[0]->name : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.address') }}</th>
        <td>{{ isset($consultancy_detail[0]->address) ? $consultancy_detail[0]->address : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.district') }}</th>
        <td>{{ isset($consultancy_detail[0]->district) ? $consultancy_detail[0]->district : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.phone') }}</th>
        <td>{{ isset($consultancy_detail[0]->phone) ? $consultancy_detail[0]->phone : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.mobile') }}</th>
        <td>{{ isset($consultancy_detail[0]->mobile) ? $consultancy_detail[0]->mobile : ' ' }}</td>
    </tr>

    <tr>
        <th>Status</th>
        <td>{{ isset($consultancy_detail[0]->status) ? $consultancy_detail[0]->status : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.website') }}</th>
        <td>{{ isset($consultancy_detail[0]->website) ? $consultancy_detail[0]->website : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.registration_no') }}</th>
        <td>{{ isset($consultancy_detail[0]->registration_no) ? $consultancy_detail[0]->registration_no : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.email') }}</th>
        <td>{{ isset($consultancy_detail[0]->email) ? $consultancy_detail[0]->email : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.created_at') }}</th>
        <td>{{ isset($consultancy_detail[0]->created_at) ? $consultancy_detail[0]->created_at : ' ' }}</td>
    </tr>

    <tr>
        <th>{{ trans('labels.backend.agency.table.updated_at') }}</th>
        <td>{{ isset($consultancy_detail[0]->updated_at) ? $consultancy_detail[0]->updated_at : ' ' }}</td>
    </tr>
</table>
