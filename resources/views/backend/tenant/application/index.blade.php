@extends ('backend.layouts.app')

@section ('title', 'Tenant - Applications')

@section('after-styles')
{{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
<h1>
    Application Management
</h1>
@endsection

@section('content')

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Applications</h3>
    </div>

    <div class="box-body">
        <div class="table-responsive">
            <table id="tenant-applications-table" class="table table-condensed table-hover">

                <thead>
                    <th>State</th>
                    <th>Tenant Name</th>
                    <th>Tenant Email</th>
                    <th>Bondsure Url</th>
                    <th>Status</th>
                    @if (request('status') == 'forwarded')
                    <th>Forwarded To</th>
                    @endif
                    <th>Date</th>
                    <th class="noExl">Action</th>
                </thead>
                <tbody>
                    @foreach ($applications as $application)
                    <tr>
                        <td>{{ $application->state_name }}</td>
                        <td>{{ optional($application->user)->name }}</td>
                        <td>{{ optional($application->user)->email }}</td>
                        <td>
                            @if($application->bondsure_url)
                            <a href="{{ $application->bondsure_url }}" target="_blank" class="btn btn-primary btn-sm">Bondsure Url</a></td>
                            @endif
                        <td>
                            {{ ucwords($application->status_name) }}
                        </td>
                        @if (request('status') == 'forwarded')
                        <td>
                            {{ optional($application->forwardedTo->sentBy)->name }}
                        </td>
                        @endif
                        <td>{{ $application->created_at->format('Y/m/d H:i') }}</td>
                        <td class="noExl">
                             {{-- <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-sm btn-info"  title="Details" target="_blank">
                                <i class="fa fa-eye"></i> --}}
                            </a>
                            {{--<a href="{{ route('property-manager.tenancy-pdf-application.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show PDF" target="_blank">
                                <i class="fa fa-file-pdf-o"></i>
                            </a>--}}
                            <a href="{{ route('admin.applications.destroy', $application->id) }}" data-method="DELETE" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#tenant-applications-table').DataTable({"order": [[5, "desc"]],"lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]]});
        });
    </script>
@endsection


