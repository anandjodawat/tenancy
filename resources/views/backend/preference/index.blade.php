@extends('backend.layouts.app')

@section('page-header')
    <h1>
        Preference List
        <small>All Preferences</small>
    </h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="{{ route('admin.preference.country.index') }}">
                <span class="info-box-icon bg-aqua"><i class="fa fa-globe"></i></span>
            </a>

            <div class="info-box-content">
                <span class="info-box-text">Countries</span>
                <span class="info-box-number">{{ $country }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="">
                <span class="info-box-icon bg-green"><i class="fa fa-home"></i></span>
            </a>

            <div class="info-box-content">
                <span class="info-box-text">Cities</span>
                <span class="info-box-number">{{ $city }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <a href="{{ route('admin.preference.university.index') }}">
                <span class="info-box-icon bg-yellow"><i class="fa fa-graduation-cap"></i></span>
            </a>

            <div class="info-box-content">
                <span class="info-box-text">Universities</span>
                <span class="info-box-number">{{ $university }}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Likes</span>
                <span class="info-box-number">93,139</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
@endsection
