@extends('backend.layouts.app')

@section('page-header')
    <h1>
        University List
        <small>All Universities</small>
    </h1>
@endsection

@section('content')
<div class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="box-title">
                University Table
            </div>
        </div>
        <div class="box-body">
            <table class="table" id="admin-university-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Country</th>
                        <th>Created By</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection


@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $('#admin-university-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.preference.university.get") }}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'country_id', name: 'country_id'},
                    {data: 'created_by', name: 'created_by'},
                    // {data: 'action', name: 'action', searchable: false, sortable: false}
                ],
            });
        });
    </script>
@endsection