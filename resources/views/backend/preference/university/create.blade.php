@extends('backend.layouts.app')

@section ('title', trans('labels.consultancy.student.management') . ' | ' . trans('labels.consultancy.student.create'))

@section('page-header')
    <h1>
        University Management
        <small>Add University</small>
    </h1>
@endsection

@section('content')
     <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">University</h3>
            </div>
            <div class="box-body">
               {{ Form::open(['route' => 'admin.preference.university.store', 'class' => 'form-horizontal', 'method' => 'post', 'id' => 'add-new-university-list']) }}
                    <div class="form-group">
                        {!! Form::label('forName', 'Name', ['class' => 'col-lg-2 control-label']) !!}
                        <div class="col-lg-10">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    {!! Form::hidden('country_id', request('country')) !!}
                    <div class="pull-left col-md-offset-2">
                        {!! Form::submit('Create', ['class' => 'btn btn-success btn-md']) !!}
                        <div class="clearfix"></div>
                    </div>

                {{ Form::close() }}
            </div>
        </div>

@endsection
