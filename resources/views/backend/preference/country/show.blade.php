@extends('backend.layouts.app')

@section ('title', trans('labels.consultancy.student.management') . ' | ' . trans('labels.consultancy.student.create'))

@section('page-header')
    <h1>
        Country Management
        <small>Country info</small>
    </h1>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Country</h3>
            </div>
            <div class="box-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Name : </strong>
                        {{ $country->name }}
                    </li>
                    <li class="list-group-item">
                        <strong>Code : </strong>
                        {{ $country->code }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Universities</h3>
                <div class="pull-right">
                    <a href="{{ route('admin.preference.university.create', ['country' => $country->id]) }}" class="btn btn-primary btn-xs">
                        Add University
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>University name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($countryUniversities as $university)
                        <tr>
                            <td>
                                {{ $university->name }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection