@extends('layouts.frontend')

@section ('content')
<style type="text/css">
[type="radio"] {
    transform: scale(0.5);


}

.history-radio-btn {
    margin: 10px 0px -10px 0px;
}


.history-radio-btn input[type="radio"] {
    vertical-align: top;
    width: auto !important;
    min-height: 30px;
    min-width: 30px;
    margin: -5px 20px 20px 0px;
}

</style>
<div class="container">
    <hr>
    <h1 class="text-center">Tenancy History CONFIRMATION</h1>
    <hr>
    {{ Form::open(['class' => 'form-horizontal', 'route' => 'history-confirmation.store', 'method' => 'post']) }}
    <div class="form-group">
        <label class="col-md-4 control-label">Date</label>
        <div class="col-md-6">
            {{ Form::text('date', null, ['class' => 'form-control', 'id' => 'history-confirmation-date-datepicker']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('date') }}</strong>
            </span>
        </div>
    </div>

           
    <div class="form-group">
        <label class="col-md-4 control-label">Agency</label>
        <div class="col-md-6">
            {{ Form::text('agency', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('agency') }}</strong>
            </span>
        </div>
    </div>
{{-- 
    <div class="form-group">
        <label class="col-md-4 control-label">Fax</label>
        <div class="col-md-6">
            {{ Form::text('fax', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('fax') }}</strong>
            </span>
        </div>
    </div> --}}

    <div class="form-group">
        <label class="col-md-4 control-label">Tenant's Name</label>
        <div class="col-md-6">
            {{ Form::text('tenant_name', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('tenant_name') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Property Address</label>
        <div class="col-md-6">
            {{ Form::text('property_address', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('property_address') }}</strong>
            </span>
        </div>
    </div>
    <hr>

    <h3 class="text-center">
        Would you please answer the following questions in relation to the above person(s)

        A signed declaration is following giving authorisation to disclose this information.
    </h3>
    <hr>
    <div class="form-group">
        <label class="col-md-4 control-label">1. Was the applicant(s) listed as a lessee?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('listed_as_lease', 'yes') }}
            No {{ Form::radio('listed_as_lease', 'no') }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('listed_as_lease') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">2. What dates were they in the property?</label>
        <div class="col-md-3">
            {{ Form::text('property_duration_from', null, ['class' => 'form-control', 'id' => 'history-confirmation-duration-from-datepicker']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('property_duration_from') }}</strong>
            </span>
        </div>
        <div class="col-md-3">
            {{ Form::text('property_duration_to', null, ['class' => 'form-control', 'id' => 'history-confirmation-duration-to-datepicker']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('property_duration_to') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">3. Did your office Terminate the Tenancy?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('terminate_tenancy', 'yes') }}
            No {{ Form::radio('terminate_tenancy', 'no') }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('terminate_tenancy') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">If yes, Please state reason.</label>
        <div class="col-md-6">
            {{ Form::textarea('reason_for_termination', null, ['class' => 'form-control', 'rows' => '2']) }}
            
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">4. During the tenancy, were the tenant(s) ever in arrears?</label>
        <div class="col-md-6">
            {{ Form::select('arrears', ['Frequently', 'Occassionally', 'Rarely', 'Never'], null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('arrears') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">5. Did the tenant recieve any Breach Notices other than arrears?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('breach_notices', 'yes') }}
            No {{ Form::radio('breach_notices', 'no') }}
              <span class="help-block">
                <strong class="text-danger">{{ $errors->first('breach_notices') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">If yes, what?</label>
        <div class="col-md-6">
            {{ Form::textarea('reason_for_breach_notices', null, ['class' => 'form-control', 'rows' => '2']) }}
            
        </div>
    </div>

    

    <div class="form-group">
        <label class="col-md-4 control-label">6. Was there any damage noted during inspections?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('damage', 'yes') }}
            No {{ Form::radio('damage', 'no') }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('damage') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">If yes, what?</label>
        <div class="col-md-6">
            {{ Form::textarea('damage_reason', null, ['class' => 'form-control', 'rows' => '2']) }}
           
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">7. During the tenancy, what were the inspections like?</label>
        <div class="col-md-6">
            {{ Form::select('inspection_like', ['Immaculate', 'Good', 'Average', 'Poor'], null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('inspection_like') }}</strong>
            </span>
        </div>
    </div>
           

    <div class="form-group">
        <label class="col-md-4 control-label">8. Were pets kept at the Property?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('pets', 'yes') }}
            No {{ Form::radio('pets', 'no') }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('pets') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">9.Was there any complaints or damage?</label>
        <div class="col-md-6">
            {{ Form::textarea('complaints_damage', null, ['class' => 'form-control', 'rows' => '2']) }}
           
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-4 control-label">10. What date did tenants vacate the property?</label>
        <div class="col-md-6">
            {{ Form::text('vacate_date', null, ['class' => 'form-control', 'id' => 'history-confirmation-vacate-datepicker']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('vacate_date') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">11. Was the Bond refunded in full?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('bond_refund_full', 'yes') }}
            No {{ Form::radio('bond_refund_full', 'no') }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('bond_refund_full') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">If no, Please state reason</label>
        <div class="col-md-6">
            {{ Form::textarea('bond_refund_reason', null, ['class' => 'form-control', 'rows' => '2']) }}
           
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">12. On a scale of 1-10 how would you rate this tenant(s)?</label>
        <div class="col-md-6">
            {{ Form::text('rate_tenant', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('rate_tenant') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">13. Would you rent to them again?</label>
        <div class="col-md-6 history-radio-btn">
            Yes {{ Form::radio('rent_again', 'yes') }}
            No {{ Form::radio('rent_again', 'no') }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('rent_again') }}</strong>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Additional Comments</label>
        <div class="col-md-6">
            {{ Form::textarea('additional_comments', null, ['class' => 'form-control', 'rows' => '2']) }}
           
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Completed By</label>
        <div class="col-md-6">
            {{ Form::text('completed_by', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('completed_by') }}</strong>
            </span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Position</label>
        <div class="col-md-6">
            {{ Form::text('employer_position', null, ['class' => 'form-control']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('employer_position') }}</strong>
            </span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Date</label>
        <div class="col-md-6">
            {{ Form::text('submission_date', null, ['class' => 'form-control', 'id' => 'history-confirmation-submission-datepicker']) }}
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('submission_date') }}</strong>
            </span>
        </div>
    </div>

    {{ Form::hidden('send_to', request('mail_from')) }}
    {{ Form::hidden('personal_ref_id', request('user')) }}
    {{ Form::hidden('manager_email', request('manager_email')) }}

    <hr>
    <h3 class="text-center">Thank You For Your Time.</h3>

    <div class="text-center">
        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
    </div>
    {{ Form::close() }}
    <hr>
</div>
@endsection

@section ('after-scripts')

<script type="text/javascript">
    jQuery( "#history-confirmation-date-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
    jQuery( "#history-confirmation-duration-to-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
    jQuery( "#history-confirmation-duration-from-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
    jQuery( "#history-confirmation-submission-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
    jQuery( "#history-confirmation-vacate-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
</script>
@endsection