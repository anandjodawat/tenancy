@extends('layouts.app')

@section('content')
@include('flash::message')

<div class="col-lg-12">
    <h1 class="page-header">Dashboard</h1>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-md-6" style="margin-bottom: 15px;">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if (auth()->user()->getFirstMedia('profile_images'))
                        <img src="{{ auth()->user()->getFirstMedia('profile_images')->getUrl() }}" alt="Profile Image" width="50%" class="ratio img-responsive img-circle col-md-offset-3">
                        @else
                        <img src="/images/default_user.png" alt="Profile Image" width="50%" class="ratio img-responsive img-circle" style="margin-left: auto; margin-right: auto;">
                        @endif
                        <div class="text-center dashboard-edit-btn">
                            <b>{{ Auth::user()->name }}</b>
                            <br>
                            <span>{{ $profile->mobile }}</span>
                            <br>
                            <span>{{ Auth::user()->email }}</span>
                            <br>
                            <br>
                            <span>{{ optional($profile->tenantCurrentAddress)->current_address_unit }}</span>
                            <span> {{ optional($profile->tenantCurrentAddress)->current_address_street }}</span>
                            <br>
                            <span>{{ optional($profile->tenantCurrentAddress)->current_address_street_name }}</span>
                            <span> {{ optional($profile->tenantCurrentAddress)->current_address_suburb }}</span>
                            <br>
                            <span>{{ optional($profile->tenantCurrentAddress)->state_name }}</span>
                            <span> {{ optional($profile->tenantCurrentAddress)->current_address_post_code }}</span>
                            <br>
                            <br>
                            <a href="/profile" class="btn btn-success" title="Edit Profile"><i class="fa fa-edit"{{--  style="margin-top: 6px; margin-left: 1px;" --}}></i> Edit Profile</a>
                        </div>
                    </div><!--panel body-->
                </div><!-- panel -->


                <div class="col-xs-12" style="margin-top: 20px;">
                    <div class="row">
                        <div class="panel panel-default tennacy_application">

                            <div class="panel-body">
                                <a href="https://searchme.datakatch.com.au/request/desktop/requestform.php?report_type=full&src=tennacyapplication">
                                    <div class="col-md-12 text-center">
                                        <i class="fa fa-check-circle"></i><br><br>
                                        Tenancy Database Check
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div><!-- col-xs-12 -->

            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default tennacy_application">
                    <div class="panel-heading">
                        <h3><b>Applications</b></h3>
                    </div>
                    <div class="panel-body">
                        <a href="{{ route('user-dashboard.applications.index', ['type' => 'draft']) }}">
                            <div class="col-md-6 text-center">
                                <i class="fa fa-file"></i>
                                <h2>{{ $draft }}</h2>
                                DRAFT
                            </div>
                        </a>
                        <a href="{{ route('user-dashboard.applications.index', ['type' => 'sent']) }}">
                            <div class="col-md-6 text-center" style="border-left: 2px solid #714273;">
                                <i class="fa fa-paper-plane"></i>
                                <h2>{{ $sent }}</h2>
                                SENT
                            </div>
                        </a>
                        <div class="clearfix"></div>
                        <hr>
                        <a href="{{ route('user-dashboard.applications.index', ['type' => 'accepted']) }}">
                            <div class="col-md-6 text-center">
                                <i class="fa fa-star"></i>
                                <h2>{{ $accepted }}</h2>
                                ACCEPTED
                            </div>
                        </a>
                        <a href="{{ route('user-dashboard.applications.index', ['type' => 'approved']) }}">
                            <div class="col-md-6 text-center" style="border-left: 2px solid #714273;">
                                <i class="fa fa-check-circle"></i>
                                <h2>{{ $approved }}</h2>
                                APPROVED
                            </div>
                        </a>
                    </div>
                </div>
            </div>


{{--             <div class="col-xs-12 col-md-6">
                <div class="panel panel-default tennacy_application">
                    <div class="panel-heading">
                        <h3><b>Maintenance Request</b><span class="label label-success" style="margin-left: 5px; padding-bottom: 1px;">{{ $totalMaintenanceRequest }}</span></h3>
                    </div>
                    <div class="panel-body">
                     <a href="{{ route('user-dashboard.maintenance-requests.index') }}" target="_blank">
                        <div class="col-md-6 text-center">
                            <i class="fa fa-envelope-open"></i>
                            <br><br>
                            View Maintenance Request  
                        </div>
                    </a>
                    <a href="{{ route('user-dashboard.maintenance-requests.create') }}" target="_blank">
                        <div class="col-md-6 text-center" style="border-left: 2px solid #714273;">
                            <i class="fa fa-paper-plane"></i>
                            <br><br>
                            Send Maintenance Request
                        </div>
                    </a>


                    </div>
                </div>
            </div>
 --}}
        </div><!-- row -->
    </div><!-- col-md-10 -->
</div><!-- row -->
@endsection
