@extends('frontend.layouts.app')

@section('title', app_name() . ' | Consultancy Details')

@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('labels.frontend.consultancy.box_title') }}</div>

                <div class="panel-body">
                    @if(isset($consultancy))
                        {{ Form::model($consultancy, ['route' => 'frontend.user.consultancy.update', 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal']) }}
                        <div class="alert alert-info">
                            <strong>Info!</strong> Your information is already submitted.
                        </div>
                    @else
                        {{ Form::open(['route' => 'frontend.user.consultancy.store', 'class' => 'form-horizontal']) }}
                    @endif

                    <div class="form-group">
                        {{ Form::label('name', trans('validation.attributes.backend.consultancy.name'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.name')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('address', trans('validation.attributes.backend.consultancy.address'), ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::text('address', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.address')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('district', trans('validation.attributes.backend.consultancy.district'), ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::text('district', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.district')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('phone', trans('validation.attributes.backend.consultancy.phone'), ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::number('phone', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.phone')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('website', trans('validation.attributes.backend.consultancy.website'), ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::text('website', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.website')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('registration_no', trans('validation.attributes.backend.consultancy.registration_no'), ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::text('registration_no', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.registration_no')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.backend.consultancy.email'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::text('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.consultancy.email')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('mobile', trans('validation.attributes.backend.consultancy.mobile'), ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-6">
                            {{ Form::number('mobile', null, ['class' => 'form-control', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.consultancy.mobile')]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit(trans('labels.frontend.contact.button'), ['class' => 'btn btn-primary']) }}
                        </div>
                    </div>

                    {{ Form::close() }}
                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
@endsection