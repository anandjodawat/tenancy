@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/key.png" class="img-responsive">
            <h2>Tenant</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/tenant-side.png" alt="tenant" class="img-responsive">
            </div>
            <div class="col-sm-8">
                <div class="paraSection property-block">
                 <p>Use our online application form to apply for your next rental property. </p>
                    <p>Once you apply Tenancy Application will forward your application to the Property Manager.</p>
                    <p> Moving Services provides you the option to choose services you require for your new property which includes Electricity, Gas, Phone, Pay TV and much more and this will be arranged prior to occupying your property at no cost to you. </p>
                    <p>Check your Datakatch Tenancy History file.</p>
             </div>
         </div>

     </div>
 </div>
</div>

@endsection

