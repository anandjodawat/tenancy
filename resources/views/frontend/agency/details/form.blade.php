<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('forAgencyName', 'Name', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('forAgencyId', 'Agency ID', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('agency_id', null, ['class' => 'form-control']) }}

        @if ($errors->has('agency_id'))
            <span class="help-block">
                <strong class="text-danger">{{ $errors->first('agency_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('forAgencyEmail', 'Email', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('email', Auth::user()->email ?? null, ['class' => 'form-control']) }}
    </div>
</div>


<div class="form-group">
    {{ Form::label('forAgencyAddress', 'Address', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('address', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('forPhoneNumber', 'Phone Number', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('phone_number', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('forWebsite', 'Website', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('website', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('forRegisterationNumber', 'Registration Number', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('registration_number', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('forAgencyLogo', 'Upload Logo', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::file('logo', ['class' => 'form-control']) }}
    </div>
</div>