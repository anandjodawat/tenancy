@extends('layouts.app')

@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			Edit Agency Details
		</div>
		<div class="panel-body">
			{{ Form::model($agency, ['route' => ['frontend.user.agency-details.update', $agency->id], 'class' => 'form-horizontal', 'method' => 'patch', 'enctype' => 'multipart/form-data']) }}
				@include('frontend.agency.details.form')

				<div class="form-group">
					{{ Form::submit('Update', ['class' => 'btn btn-primary col-md-offset-4']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
@endsection