@extends('layouts.app')

@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">
			Agency Details
		</div>
		<div class="panel-body">
			{{ Form::open(['route' => 'frontend.user.agency-details.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
				@include('frontend.agency.details.form')

				<div class="form-group">
					{{ Form::submit('Submit', ['class' => 'btn btn-primary col-md-offset-4']) }}
				</div>
			{{ Form::close() }}
		</div>
	</div>
@endsection