@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/agents.png" class="img-responsive">
            <h2>Contact Us</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div>
    <div class="container">
        <div class="col-sm-5 contact-page-side wow fadeInDown" data-wow-delay=".3s">
            <div class="col-sm-12" style="margin-bottom: 30px;">
                <h3 style="margin-bottom: 15px; margin-top: 0px;">Get in Touch</h3>
                <p>Tenancy Application allows Property Managers to receive branded online application forms submitted by Prospective Tenants for their rental properties, easing the process by removing the hardcopy system.</p>
                <h3 style="margin-bottom: 15px; margin-top: 20px;">Contact Details</h3>
                <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>  info@tenancyapplication.com.au</a><br>
                <a href="#"></a>
            </div>
        </div>
        <div class="col-sm-7">

            <div class="paraSection property-block">


                @include('flash::message')

                <p>We constantly strive to assure comfort and ease with the use of our website. </p>
                <p> Please feel free to contact us for any needs you find requires addressing, and will be do our best to reply in a timely manner. </p>        

                <div class="contact_right">
                    {{ Form::open(['method' => 'post', 'route' => 'frontend.contact.store']) }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'First Name']) }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Last Name']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required']) }}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {{ Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Your Message', 'row' => '5', 'required' => 'required']) }}
                            </div>
                        </div>
                    </div>
                    @if (config('access.captcha.registration'))
                    <div class="row">
                        <div class="col">
                            {!! Captcha::display() !!}
                            {{Form::hidden('captcha_status', 'true') }}
                            @if ($errors->has('g-recaptcha-response'))
                            <span class="text-danger">reCAPTCHA is mandatory.</span>
                            @endif
                            <br>
                        </div><!--col-->
                    </div><!--row-->
                    @endif
                    <div class="row">
                        <div class="col-sm-12">
                            {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
@push('after-scripts')
<script>console.log('test');</script>
@if (config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif
@endpush
