

@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
	<div class="property-page-heading">
		<div class="container">
			<img src="/images/about-us.png" class="img-responsive">
			<h2>Property Manager FAQ</h2>
			<hr style="margin-bottom: 60px;">
		</div>
	</div> 
	<div class="container">
		<div class="row">
			
			<div class="col-sm-12">
				<div class="paraSection property-block">
					<div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">
						<div class="panel panel-default">
							<div id="headingOne" role="tab" class="panel-heading">
								<h4 class="panel-title">
									<a aria-controls="collapseOne" aria-expanded="false" href="#collapseOne" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW DOES A PROPERTY MANAGER USES TENANCY APPLICATION?</a>
								</h4>
							</div>
							<div aria-labelledby="headingOne" role="tabpanel" class="panel-collapse collapse" id="collapseOne" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">Tenancy Application is an online application system which Property Managers use to obtain Tenancy Application forms submitted by Prospective Tenants.
									Property Managers will receive notifications of any application submitted by a tenant for their rental property.
									Application is checked and Property Manager will advise tenant of their decision on the application.
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div id="headingTwo" role="tab" class="panel-heading">
								<h4 class="panel-title">
									<a aria-controls="collapseTwo" aria-expanded="false" href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">ARE TENANCY APPLICATIONS SENT TO ME?</a>
								</h4>
							</div>
							<div aria-labelledby="headingTwo" role="tabpanel" class="panel-collapse collapse" id="collapseTwo" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">You will receive notifications of each Prospective Tenant from our website that has applied for your rental property to your email address. </div>
							</div>
						</div>

						<div class="panel panel-default">
							<div id="headingTwo" role="tab" class="panel-heading">
								<h4 class="panel-title">
									<a aria-controls="collapseThree" aria-expanded="false" href="#collapseThree" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">WHAT IS THE PROCESS IF I AM NOT REGISTERED ON TENANCY APPLICATION? </a>
								</h4>
							</div>
							<div aria-labelledby="headingTwo" role="tabpanel" class="panel-collapse collapse" id="collapseThree" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">If you are not registered, you will receive a notification email from us that a tenant has applied for a rental property using our online Tenancy application, where you will be required to register to view the application.
								</div>
							</div>
						</div>

<!-- <div class="panel panel-default">
<div id="headingThree" role="tab" class="panel-heading">
<h4 class="panel-title">
<a aria-controls="collapseThree" aria-expanded="false" href="#collapseThree" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">How it work?</a>
</h4>
</div>
<div aria-labelledby="headingThree" role="tabpanel" class="panel-collapse collapse" id="collapseThree" aria-expanded="false">
<div class="panel-body">Tenancy Application is a time saver by instantly connecting you with prospective tenants for your properties. You will no longer need to juggle around interested parties while trying to keep everything professional. Tenancy Application provides you with the means of a secure application system to instantly connect and review an application. The use of our website is free and requires no form of installation.</div>
</div>
</div> -->

<div class="panel panel-default">
	<div id="headingfour" role="tab" class="panel-heading">
		<h4 class="panel-title">
			<a aria-controls="headingfour" aria-expanded="false" href="#collapsefour" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW DO I START?</a>
		</h4>
	</div>
	<div aria-labelledby="collapsefour" role="tabpanel" class="panel-collapse collapse" id="collapsefour" aria-expanded="false">
		<div class="panel-body">Click on <a href="/register?registeras=agency-admin">Property Manager Signup</a> and follow the prompts from there. </div>
	</div>
</div>

<div class="panel panel-default">
	<div id="headingfive" role="tab" class="panel-heading">
		<h4 class="panel-title">
			<a aria-controls="headingfive" aria-expanded="false" href="#collapsefive" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">WHAT ABOUT DOCUMENTS UPLOADED BY TENANTS?</a>
		</h4>
	</div>
	<div aria-labelledby="collapsefour" role="tabpanel" class="panel-collapse collapse" id="collapsefive" aria-expanded="false">
		<div class="panel-body">You the Property Manager will receive all necessary documentation uploaded by the Prospective Tenant together with their Application. 
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div id="headingsix" role="tab" class="panel-heading">
		<h4 class="panel-title">
			<a aria-controls="headingsix" aria-expanded="false" href="#collapsesix" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed"> IS TENANCY APPLICATION A FREE SERVICE?
			</a>
		</h4>
	</div>
	<div aria-labelledby="collapsesix" role="tabpanel" class="panel-collapse collapse" id="collapsesix" aria-expanded="false">
		<div class="panel-body">Tenancy Application is completely free and usable at any time.</div>
	</div>
</div>

<div class="panel panel-default">
	<div id="headingseven" role="tab" class="panel-heading">
		<h4 class="panel-title">
			<a aria-controls="headingseven" aria-expanded="false" href="#collapseseven" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">WHAT IS RENTAL REWARDS?</a>
		</h4>
	</div>
	<div aria-labelledby="collapseseven" role="tabpanel" class="panel-collapse collapse" id="collapseseven" aria-expanded="false">
		<div class="panel-body">Once a Tenant is approved by the Agent, they will pay a holding deposit through Rental Rewards. 
			Rental Rewards is a 100% Australian owned and operated company offering revolutionary Real Estate payment methods.
			With Rental Rewards, Tenants can pay rent to any Real Estate Agent in Australia. This convenient and rewarding service processes rent payments on behalf of tenants and forwards the funds to the tenant’s Real Estate Agents.
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div id="headingseven" role="tab" class="panel-heading">
		<h4 class="panel-title">
			<a aria-controls="headingseven" aria-expanded="false" href="#collapseeight" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW DOES MOVING SERVICES WORK FOR THE PROPERTY MANAGER?</a>
		</h4>
	</div>
	<div aria-labelledby="collapseeight" role="tabpanel" class="panel-collapse collapse" id="collapseeight" aria-expanded="false">
		<div class="panel-body">Should a Tenant choose services to be arranged for their new rental property, the Real Estate Agency will be paid a fee by the Moving Services Company for services the tenant successful connects for the Agents rental property.
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div id="headingseven" role="tab" class="panel-heading">
		<h4 class="panel-title">
			<a aria-controls="headingseven" aria-expanded="false" href="#collapsenine" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW SECURED IS THE SYSTEM?</a>
		</h4>
	</div>
	<div aria-labelledby="collapsenine" role="tabpanel" class="panel-collapse collapse" id="collapsenine" aria-expanded="false">
		<div class="panel-body">Your safety is our main priority. Our system is securely guarded and only a select few authorized individuals of our team are allowed full access to our database. You will always be prompted to supply your username and password to authenticate all changes. You can review our Privacy and Policy section for more details.
		</div>
	</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>

@endsection

