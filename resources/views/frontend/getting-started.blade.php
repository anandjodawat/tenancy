@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/about-us.png" class="img-responsive">
            <h2>Getting Started With Tenancy</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div> 
    <div class="container">
        <div class="row">
             <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/about-us-side.png" alt="tenant" class="img-responsive">
            </div> 
            <div class="col-sm-8">
                <div class="paraSection property-block">
                  <p><strong>Getting Started with Tenancy Application:</strong></p>

    <p>Tenancy Applications is all about easing the process of applying for a rental property. 
To Register, simply press on the <a href="index.php">Home</a> tab and click on <a href="index.php">Register as A Tenant</a>. Follow the prompts from there.</p>

<h3>Tenancy Applications</h3>
<p>Register with Tenancy Application as either a tenant applying for a new property, or as a Property Manager to accept online rental applications from prospective tenants. </p>
<p>Tenancy Application has made itself easy for both the tenant and the property manager, by the tenant completing the online form and uploading required documentation.  Property Manager will then assess the application online, keeping the process paperless.</p>
<p>With Tenancy Application, Tenants will be able to apply for multiple properties without the need of completing several forms. Tenants will simply use all the information they have uploaded and given, to submit to several Property Managers.</p>
<p>For more information <a href="contact.php">contact us</a> now!</p>
              </div>
          </div>

      </div>
  </div>
</div>

@endsection

