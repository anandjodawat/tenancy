<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid" style="max-width:1170px;">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="/"><img src="/images/logo-final.png" class="image-responsive logo-img"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="{{ Request::is('about-us') ? 'active' : '' }}">
                        <a href="/about-us">
                            About Us
                        </a>
                    </li>
                    <li class="{{ Request::is('tenant') ? 'active' : '' }}">
                        <a href="/tenant">Tenant</a>
                    </li>
                    <li class="{{ Request::is('propertymanager') ? 'active' : '' }}">
                        <a href="/propertymanager">Property Manager</a>
                    </li>
                    <li class="{{ Request::is('contact-us') ? 'active' : '' }}">
                        <a href="/contact-us">Contact Us</a>
                    </li>
                    @if (!auth()->user())
                        <li><a href="/login">Login</a></li>
                        <li><a href="/register" class="btn btn-success btn-round btn-xs signup-btn">Signup</a></li>
                    @else
                        <li><a href="{{ route('frontend.auth.logout') }}">Logout</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>