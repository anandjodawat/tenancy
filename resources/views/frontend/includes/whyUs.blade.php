<section class="why-us-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 why-us-block">
                <h1>Why <span class="greenunderline">Us? </span></h1>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="why-us-single-block ten_left">
                            <!-- <i class="fas fa-spinner"></i> -->
                            <div class="col-sm-3">
                                <img src="images/time.png" class="img-responsive">
                            </div>
                            <div class="col-sm-9">
                                <h2>Save Time</h2>
                                <p>Apply, Connect and Pay reducing the time under One Application</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="why-us-single-block ten_right">
                            <!-- <i class="fas fa-truck"></i> -->
                            <div class="col-sm-3">
                                <img src="images/delivery.png" class="img-responsive">
                            </div>
                            <div class="col-sm-9">
                                <h2>Moving Services</h2>
                                <p>Connect your Utility Services</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="why-us-single-block ten_left">
                         <!--  <i class="fas fa-file-alt"></i> -->
                         <div class="col-sm-3">
                             <img src="images/hand fold.png" class="img-responsive">
                         </div>
                         <div class="col-sm-9">
                             <h2>Convenience</h2>
                             <p>Simple and Easy to Use</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-sm-6">
                    <div class="why-us-single-block ten_right">
                        <!-- <i class="fas fa-coins"></i> -->
                        <div class="col-sm-3">
                            <img src="images/note.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>One Application</h2>
                            <p>Create One Account and Apply for Multiple Properties</p>
                        </div>
                    </div>
                </div>
            </div>  

            <div class="row">
                <div class="col-sm-6">
                    <div class="why-us-single-block ten_left">
                        <!-- <i class="fas fa-desktop"></i> -->
                        <div class="col-sm-3">
                            <img src="images/money.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>It's Free</h2>
                            <p>Free Service to Use</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="why-us-single-block ten_right">
                        <!-- <i class="fas fa-desktop"></i> -->
                        <div class="col-sm-3">
                            <img src="images/devices.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>Apply From Anywhere</h2>
                            <p>Apply at Property Inspections or anywhere using our Mobile Site</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="why-us-single-block ten_left">
                        <!-- <i class="fas fa-desktop"></i> -->
                        <div class="col-sm-3">
                            <img src="images/preferreed by real state agents.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>Maintenance</h2>
                            <p>Book your property repairs online and directly to the agent</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="why-us-single-block ten_right">
                        <!-- <i class="fas fa-desktop"></i> -->
                        <div class="col-sm-3">
                            <img src="images/agents.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>Agents</h2>
                            <p>Preferred and Used by Real Estate Agents across Australia</p>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="why-us-single-block ten_left">
                        <!-- <i class="fas fa-desktop"></i> -->
                        <div class="col-sm-3">
                            <img src="images/pay your rent depossits.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>Pay Your Rent/Deposits</h2>
                            <p>Once your application is approved, you can pay a holding deposit and also your ongoing payments</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="why-us-single-block ten_right">
                        <!-- <i class="fas fa-desktop"></i> -->
                        <div class="col-sm-3">
                            <img src="images/bond assistance.png" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2>Bond Assistance</h2>
                            <p>Get help to pay your upfront bond payment</p>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
</section>