
<div class="container-fluid" style="max-width:1170px; margin-top: 40px;">
   <hr>
</div>

<footer>
    <div class="class="container-fluid"">  
        <div class="col-md-12 footer-block">
            <div class="row">
                <div class="col-md-3 col-sm-6 footer-logo-block footer-block-single">
                    <img src="/images/logo-final.png" class="logo-img footer-logo">
                    <p>Tenancy Application is one of the fastest growing online rental application systems in Australia.</p>
                </div>

                <div class="col-md-3 col-sm-6 footer-block-single">
                    <h2>Tenant</h2>
                    <ul>
                        <li>Tenant <a href="/login">Login</a>/<a href="/register">SignUp</a></li>
                        <li><a href="/tenant">What is Tenancy Application?</a></li>
                        <li><a href="/tenant-faq">Tenant FAQ</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 footer-block-single">
                    <h2>Property Manager</h2>
                    <ul>
                        <li>Property Manager <a href="/login">Login</a>/<a href="/register">Sign Up</a></li>
                        <li><a href="/tenant">What is Tenancy Application?</a></li>
                        <li><a href="property-manager-faq">Property Manager FAQ</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 footer-block-single">

                    <h2>About Us</h2>
                    <ul>
                        <li style="text-align: justify;"><a href="/getting-started">Getting Started with The Tenancy Application</a></li>
                        <li><a href="/contact-us">Contact Us</a></li>
                    </ul>
                </div>

        <!-- <div class="pull-right footer-block">
            <a href="privacy-policy">Privacy Policy</a>
            | <a href="terms-and-conditions">Terms and Conditions</a>
        </div> -->
        
        <div class="col-md-12 tanant-copyright text-center">
            <a href="https://www.facebook.com/Tenancy-Application-297344536956843"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
            <a href="https://twitter.com/TenancyA
            "><i class="fab fa-twitter" aria-hidden="true"></i></a>
            <a href="https://www.instagram.com/tenancy_application/
            "><i class="fab fa-instagram" aria-hidden="true"></i></a>
            <p>© 2018 Tenancy Application. All Right Reserved</p>
        </div>

    </div>
</div>
</div>  

</footer>
