<section class="dream-house-section">
  <div class="container">
    <div class="row">

      <div class="col-sm-6 dreamhouse dream-house-image wow fadeInLeft" data-wow-delay=".3s"> 
       <div class="dreamimage">
         <img src="/images/dream.jpg" class="img-responsive your-dream-image">
       </div>

     </div>
     <div class="col-sm-6 why-us-block why-dream-house">
      <h1>Your <span class="greenunderline">New</span> Home</h1>
      <h2>On your fingertips</h2>
      <p>Save time and apply for Multiple Properties today.<br>
      Fast and Easy Service, Lodge your application within no time.</p>
    </div>  
  </div>
</div>
</section>



<section class="dream-house-section start-apply-today">
  <div class="container">
   <div class="row">

    <div class="col-sm-6 why-us-block start-apply" >
      <h1>Start Applying <span class="greenunderline">Today</span></h1>

      {{-- <p>Save time and apply for Multiple Properties today.<br>
      Fast and Easy Service, Lodge your application within no time.</p> --}}

      <div class="login-register">
        <a href="{{ route('frontend.auth.login') }}">
          <button type="submit" class="btn btn-primary submit-botton login-botton">Login</button>
        </a>    
        <a href="{{ route('frontend.auth.register') }}">
          <button type="submit" class="btn btn-primary submit-botton">Sign up</button>
        </a>
      </div>

    </div>  

    <div class="col-sm-6 startapply wow fadeInRight" data-wow-delay=".4s"> 
      <img src="images/apply.jpg" class="img-responsive your-dream-image startapplyimage">
    </div>

  </div>
</div>
</section>

{{-- <section class="alliance-parners alliance-partners">
  <div class="container">
   <div class="row">
    <h1 class="alliance-parners">Integrated <span class="greenunderline"> Partners</span></h1>
    <p>We are working with some of the biggest and most trusted names in finance, property and technology to deliver market leading solutions.</p>
    <section class="regular customer-logo slider " class="img-responsive">
      
      <div>
        <img src="images/logos/bondsure1.png" class="img-responsive">
      </div>
      
      <div>
        <img src="images/logos/pro-logo.png" class="img-responsive">
      </div>
      <div>
        <img src="images/logos/cnc.png" class="img-responsive">
      </div>
      <div>
        <img src="images/logos/rrlogo.png" class="img-responsive">
      </div>
    </section>
  </div>
</div>
</section> --}}


<section class="alliance-parners">
  <div class="container">
   <div class="row">
    <h1 class="alliance-parners">Our <span class="greenunderline"> Agencies</span></h1>
    <p>Agencies that use Tenancy Application include:</p>
    <section class="center regular customer-logo slider ">
      <div>
        <img src="images/logos/ljhooker.jpg">
      </div>
      <div>
        <img src="images/logos/pdrnationwide.png">
      </div>
      <div>
        <img src="images/logos/raywhite.png">
      </div>

      <div>
        <img src="images/logos/barryplant.png">
      </div>
      <div>
        <img src="images/logos/century21.png">
      </div>
      <div>
        <img src="images/logos/national2.png">
      </div>
      <div>
        <img src="images/logos/professionals.jpg">
      </div>
      <div>
        <img src="images/logos/rnw.png">
      </div>
      <div>
        <img src="images/logos/stockdale.png">
      </div>
    </section>
  </div>
</div>
</section>
