

@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
	<div class="property-page-heading">
		<div class="container">
			<img src="/images/about-us.png" class="img-responsive">
			<h2>Tenant FAQ</h2>
			<hr style="margin-bottom: 60px;">
		</div>
	</div> 
	<div class="container">
		<div class="row">
			<div aria-multiselectable="true" role="tablist" id="accordion" class="panel-group">
				<div class="panel panel-default">
					<div id="headingOne" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="collapseOne" aria-expanded="false" href="#collapseOne" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">WHAT IS TENANCY APPLICATION?</a>
						</h4>
					</div>
					<div aria-labelledby="headingOne" role="tabpanel" class="panel-collapse collapse" id="collapseOne" aria-expanded="false" style="height: 0px;">
						<div class="panel-body">An efficient way to apply online for your next rental property.
							Register, complete an online application form and submit it to the property manager and await their decision.
							Set up the services you require by using our Moving Services section.

						Check your Datakatch Tenancy History file.</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div id="headingTwo" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="collapseTwo" aria-expanded="false" href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed" style="text-transform: capitalize">WHY HAS MY TENANCY APPLICATION BEEN REJECTED BY THE PROPERTY MANAGER?</a>
						</h4>
					</div>
					<div aria-labelledby="headingTwo" role="tabpanel" class="panel-collapse collapse" id="collapseTwo" aria-expanded="false" style="height: 0px;">
						<div class="panel-body">In case your application is not accepted by the Property Manager you have applied for, you will be notified by the property manager.</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div id="headingThree" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="collapseThree" aria-expanded="false" href="#collapseThree" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW LONG DOES IT TAKE FOR THE PROPERTY MANAGER TO RESPOND TO MY APPLICATION?</a>
						</h4>
					</div>
					<div aria-labelledby="headingThree" role="tabpanel" class="panel-collapse collapse" id="collapseThree" aria-expanded="false">
						<div class="panel-body">Your application is promptly delivered to the Property Manager through a secure online platform, as soon as you submit it. Property Manager will then take the necessary steps to evaluate your application and provide you with an update through to your registered contact details.</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div id="headingfour" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="headingfour" aria-expanded="false" href="#collapsefour" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW TO GET STARTED WITH REGISTERING AS A TENANT?</a>
						</h4>
					</div>
					<div aria-labelledby="collapsefour" role="tabpanel" class="panel-collapse collapse" id="collapsefour" aria-expanded="false">
						<div class="panel-body">Click on the <a href="/register">Register as A Tenant</a> tab and create your new account. Follow the prompts to finish registration.</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div id="headingfive" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="headingfive" aria-expanded="false" href="#collapsefive" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">WHAT IF I FORGOT MY PASSWORD?</a>
						</h4>
					</div>
					<div aria-labelledby="collapsefour" role="tabpanel" class="panel-collapse collapse" id="collapsefive" aria-expanded="false">
						<div class="panel-body">In case you have forgotten your password, click <a href="/password/reset" >Forgot password</a> on the home page and follow the prompts. After finishing the steps, a link will be sent to your email in order to verify that it is your account and that you wish to change your password.</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div id="headingsix" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="headingsix" aria-expanded="false" href="#collapsesix" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW TO CHANGE YOUR PASSWORD?</a>
						</h4>
					</div>
					<div aria-labelledby="collapsesix" role="tabpanel" class="panel-collapse collapse" id="collapsesix" aria-expanded="false">
						<div class="panel-body">To change your password, go to <a href="/login">My Account</a> and select Edit Email/Password, from here you can edit your password.</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div id="headingseven" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="headingseven" aria-expanded="false" href="#collapseseven" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW TO APPLY FOR ANOTHER RENTAL PROPERTY?</a>
						</h4>
					</div>
					<div aria-labelledby="collapseseven" role="tabpanel" class="panel-collapse collapse" id="collapseseven" aria-expanded="false">
						<div class="panel-body">After registering, you can submit as many applications as you wish to as many properties, simply by clicking “Send a new application”. Once you register, you will be able to create a Profile which will be used for multiple applications. </div>
					</div>
				</div>

				<div class="panel panel-default">
					<div id="headingeight" role="tab" class="panel-heading">
						<h4 class="panel-title">
							<a aria-controls="headingeight" aria-expanded="false" href="#collapseeight" data-parent="#accordion" data-toggle="collapse" role="button" class="collapsed">HOW SECURE IS TENANCY APPLICATIONS?</a>
						</h4>
					</div>
					<div aria-labelledby="collapseeight" role="tabpanel" class="panel-collapse collapse" id="collapseeight" aria-expanded="false">
						<div class="panel-body">Your protection and privacy is one of our highest concerns. Tenancy Applications offers you the comfort of undertaking all your needs within a highly secure environment. We ensure you maintain your security and privacy. For any transaction, you would be required to input your username and password. Check Privacy and Policy for more detailed information.</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

@endsection

