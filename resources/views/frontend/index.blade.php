@extends('layouts.frontend')

@include('flash::message')

@section('content')
    
    @include('frontend.includes.whyUs')

    @include('frontend.includes.dreamHouse')
    
@endsection

