@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/about-us.png" class="img-responsive">
            <h2>About Us</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/about-us-side.png" alt="tenant" class="img-responsive">
            </div>
            <div class="col-sm-8">
                <div class="paraSection property-block">
                  <p>Simple and Free to Use.</p>
                  <p>Tenancy Application is one of the fastest growing online rental application systems in Australia.</p>
                  <p>Simply register and complete your Online Tenancy Application easily.</p>
                  <p>Apply for Multiple Properties at No Cost.</p>
                  <p>Select your Utility and Moving Services while applying for your property and it will be arranged on your behalf upon completion of your Tenancy Application.</p>

                  <a href="/contact-us" class="btn btn-success btn-round btn-xs signup-btn" style="background-color: #719759; border-color: #719759; margin-top: 30px;">Contact Us</a>
              </div>
          </div>

      </div>
  </div>
</div>

@endsection

