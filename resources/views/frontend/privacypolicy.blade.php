@extends('layouts.frontend')

@section('content')
<hr>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<h1>General</h1>
			<p>Tenancy Application Pty Ltd complies with and holds in high regards all applicable laws to protect the privacy of our clients. Tenancy Application Pty Ltd has taken and continues to take all necessary steps to ensure the safety and privacy of our clients, as per the law.</p>
			<h3>Collection and access</h3>
			<p>For the purposes of collecting and accessing information, Tenancy Application Pty Ltd ensures that all information collected is purely for the possible future transactions of its clients. No sensitive personal information is requested nor retained should the client not wish it. Access to the database is carefully restricted to individuals with official protocol to access said database. Any information gathered by Tenancy Application Pty Ltd about any clients or individuals is available to said clients through the online application system or, if they so choose, by submitting and emailing a request to Tenancy Application Pty Ltd at info@tenancyapplication.com.au. If we receive a request from any client listed in our database, and it provides legitimate, then we will provide said client with a copy of the information we hold.</p>
			<h3>Accuracy of information</h3>
			<p>Tenancy Application always strives to ensure all information provided on its database is up to date and accurate for client use. Information regarding clients upon the database will not be changed or edited and will be left as is, unless there is an explicit reason to involve ourselves. The database is available and online at all times for clients to update their information as they so choose. It is requested that all clients attempt to keep their information up to date as best they can. If you believe that any information found on the database is out of date or simply incorrect, please contact us and we will make every effort to correct this inaccuracy without compromising our client’s goals. If it becomes apparent that information on the database is inaccurate or incomplete then we reserve the right to remove the information from the database until it has been adjusted.</p>
			<p>Tenancy Application Pty Ltd takes care to always ensure that the information provided on the database is up to date.</p>
			<h3>What is the Information Held by TENANCY APPLICATION?</h3>
			<p>All information held in Tenancy Application’s database is information provided by our clients. The main criteria of information held is those that help identify an individual, such as date of birth, name and address. We may as well ask require said information for verification purposes. The personal information collected in relation to prospective tenants is more extensive. This is expected, as per the procedure of a successful transaction. All information gathered is contained securely.</p>
			<h3>What is the Purpose of Collecting these Information?</h3>
			<p>The personal Information of our clients is use for several functions and activities pertaining to Tenancy Application, which include:</p>
			<p>The submission and processing of client information, as well as applications to other goods and service providers requested.</p>
			<p>For review by real estate agents, as well as any other goods and service providers.</p>
			<p>Disclosure to solicitors, commercial agents, banks and any other requested organizations that require the client’s details for a transaction.</p>
			<h3>Who does Tenancy Application Disclose Information to?</h3>
			<p>Tenancy Application Pty Ltd will disclose personal information collected to organizations required to ensure the completion of a transaction for the client. </p>
			<p>Due to the changes in the Privacy Laws, all property managers must ensure that you (the applicant) fully understand the National Privacy Principles and the manner in which they must use your personal information in order to carry out their role as professional property managers.</p>
			<p>The personal information collected about you (the applicant) in this application may be disclosed, by use of the internet or otherwise, to other parties, including:</p>
			<ul>
				<li>The Landlord</li>
				<li>Trades People</li>
				<li>Financial Institutions</li>
				<li>Government and Statutory bodies</li>
				<li>Refrees</li>
				<li>Solicitors</li>
				<li>Property Evaluators</li>
				<li>Existing or potential clients of the agent</li>
				<li>Rental Bond Authorities</li>
				<li>Tenant Database</li>
				<li>other Real Estate Agents</li>
				<li>Other Third parties as required by law</li>
				<li>Collection Agents</li>
				<li>Verification Services</li>
				<li>Other Landlords</li>
				<li>Body Corporates</li>
			</ul>
			<p>Information already held on tenancy databases may also be disclosed to the Agent and/or landlord. Unless you advise the Agent to the contrary, the Agent may also disclose such information to The Real Estate Institute of your State and to DataKatch, NTD, TRA or TICA for the purpose of documenting all leasing data in the area for the benefit of its members as part of membership services and for others in the property related industries, and so as to assist them in continuing to provide the best possible service to their clients. In providing this information, you (the applicant) agree to its use, unless you advise the Agent differently.</p>
			<ul>
				<li>The privacy policy of your State's Real Estate Institute can be viewed by logging on www.reia.asn.com.au and selecting your State.</li> 
				<li>The privacy policy of NTD can be viewed by logging on to <a href="https://www.ntd.net.au" target="_blank">https://www.ntd.net.au</a></li>
				<li>The privacy policy of TRA can be viewed by logging on to <a href="http://tradingreference.com" target="_blank">http://tradingreference.com</a></li>
				<li>The privacy policy of TICA can be viewed by logging on to <a href="https://www.tica.com.au/" target="_blank">https://www.tica.com.au/</a></li>
				<li>The privacy policy of BARCLAY MIS can be viewed on <a href="http://barclaymis.com.au " target="_blank">http://barclaymis.com.au </a></li>
				<li>The privacy policy of DATAKATCH can be viewed on <a href="http://datakatch.com.au" target="_blank">http://datakatch.com.au</a></li>
				<li>Contact numbers for the tenancy database agencies are below:</li>
				<li>NTD – 1300 563 826</li>
				<li>TRA – (02) 9363 9244</li>
				<li>TICA – 1902 220 346</li>
				<li>BARLCAY MIS – 1300 883 916</li>
				<li>DATAKATCH – (02) 9086 9388</li>

			</ul>
			<p>The Agent will only disclose information in this way to other parties to achieve the purposes specified above or as allowed under the Privacy Act.</p>
			<p>If you (the applicant) would like to access this information you can do so by contacting the Agent at the address and contact numbers for the property you are interested in renting. You (the applicant) can also correct this information if it is inaccurate, incomplete or out of date.</p>
			<p>If your personal information is not provided to the Agent and you (the applicant) do not consent to the use of this information as specified above, the Agent cannot carry out their duties and may not be able to provide you with the lease/tenancy of the premises.</p>
			<h3>Tenancy Application policies in relation to the management of personal information</h3>
			<p>Tenancy Application Pty Ltd maintains the following policies in relation to the management of personal information:</p>
			<ol>
				<li>High security protocols are maintained to prevent unauthorized access to the Tenancy Application database and records; this includes preventing unauthorized modification or disclosure of the records in that database. We take our client’s privacy very seriously and ensure they remain in a secure environment to undergo their transaction. The security of the database is reviewed periodically, to ensure the safety of our clients. </li>
				<li>Tenancy Application maintains a select group of authorized individuals that have access to the database. Your security is their highest concern. Those with access are listed and always reviewed, and if need be added to, to ensure the safety of our clients.</li>
				<li>Information is collected about individuals only by lawful and fair means, as provided to the company. </li>
				<li>Tenancy Application requires users to agree to the terms and conditions set forth, of which in part are intended to ensure users comply with their own obligations under privacy laws. </li>
				<li>Tenancy Application does not collect any sensitive information about its clients, by means of the Privacy Act 1988 (Cth), such as racial or ethnicity information, or political opinions.</li>
				<li>Tenancy Application will change the information of any person that informs Tenancy Application that his or her information is incorrect, upon being reasonably satisfied of the legitimacy of that claim. </li>
				<li>Tenancy Application will make a copy the collected information available, to any person who asks for it, pertaining to them.</li>
				<li>Tenancy Application always takes appropriate steps to provide its contact details to clients that are listed in its records.</li>
				<li>Tenancy Application will provide all collected information of any client, should the client request it. This is after being reasonably satisfied of the identity of the individual making the request.</li>

			</ol>
		</div>
	</div>
</div>

@endsection

