@extends('layouts.frontend')

@section('content')
<hr>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="paraSection">
                <h1>Terms and Conditions</h1><hr>
                <p><b>Please read this
                legal information carefully </b></p>
                <p>Your continued
                    use of our web sites constitutes your acceptance of the terms in this notice
                    and the other notices on our web sites. Should you object to any of the
                    conditions in the notices on our web sites your sole option is to immediately
                cease your use of the web sites.</p>
                <p>Information on
                    our web sites and in any tenancyapplication.com.au publication should not be
                    regarded as a substitute for professional legal, financial or real estate
                advice.</p>
                <p>Tenancy Application
                    Pty Ltd and its related entities responsible for maintaining our web sites
                    and all Tenancyapplication.com.au publications and its directors, officers
                    and agents believe that all information contained within them is correct.
                    However, no warranty is made as to the accuracy or reliability of the
                    information contained therein and Tenancy Application Pty Ltd and its related
                    entities, directors, officers and agents disclaim all liability and
                    responsibility for any direct or indirect loss or damage which may be
                    suffered by any recipient through relying on anything contained in or omitted
                from our web sites or publications.</p>
                <p><b>Copyright</b></p>
                <p>The subject
                    matter on and accessible from our web sites and publications is copyright.
                    Apart from fair dealing permitted by the Copyright Act 1968, Tenancy
                    Application Pty Ltd grants visitors to the site permission to download
                    copyright material only for private purposes. For reproduction or use of tenancyapplication.com.au
                    copyright material beyond such use, written permission must be obtained
                    directly from Tenancy Application Pty Ltd or the relevant copyright owner. If
                    given, permission will be subject to the requirement that the copyright
                    owner's name and interest in the material be acknowledged when the material
                is reproduced or quoted, in whole or in part.</p>
                <p><b>Third party
                links and advertising</b></p>
                <p>Our web sites
                    often include advertisements, hyperlinks and pointers to web sites operated
                    by third parties. Those third party web sites do not form part of the tenancyapplication.com.au
                    web sites and are not under the control of or the responsibility of Tenancy
                    Application or its related entities. When you link to those web sites you
                    leave our web site and do so entirely at your own risk. A display of
                    advertising does not imply an endorsement or recommendation by Tenancy
                Application Pty Ltd or its related entities.</p>
                <p><b>Policy for
                Linking to Our Websites</b></p>
                <p>Hyperlinks
                    enhance the user’s experience by providing easy access to additional
                    information or original source material, whether on the same site or from an
                    external source. For this reason, we value the use of hyperlinks in our
                content. However, in using hyperlinks, we:</p>
                <ul type="disc">
                    <li>Attribute
                        the original source of the hyperlink where it points to third party
                    content.</li>
                    <li>Do not
                    claim ownership or rights to content belonging to other parties.</li>
                </ul>
                <p>&nbsp;</p>
                <p>We ask that you
                    observe similar practices when linking to our content. You should be aware
                    that if you do not do this, this may be in breach of our rights or the rights
                of our licensors or other third parties.</p>
                <p>Please do:</p>
                <ul type="disc">
                    <li>Link to our
                    content directly. We encourage direct links.</li>
                    <li>Comply with
                    all copyright, trade mark or other applicable laws.</li>
                    <li>Use the
                        title or headline of the content to which you are linking, as long as
                    you link directly to that content.</li>
                </ul>
                <p>&nbsp;</p>
                <p>Please do not:</p>
                <ul type="disc">
                    <li>Attribute a
                        link to Tenancy Application Pty Ltd content as being a link to your own
                        or someone else’s content (for example, use your own logo to link to our
                    content).</li>
                    <li>Attribute a
                    link to our site and then link somewhere else.</li>
                    <li>Frame our
                        content in such a way as to present it as your own or as belonging to
                    anyone other than us or our licensors.</li>
                </ul>
                <p>&nbsp;</p>
                <p><b>Limitation of
                liability</b></p>
                <p>Certain rights
                    and remedies may be available under the Trade Practices Act 1974 or similar
                    legislation of other States or Territories and may not be permitted to be
                    excluded, restricted or modified. Apart from those that cannot be excluded, Tenancy
                    Application Pty Ltd and its related entities exclude all conditions and
                    warranties that may be implied by law. To the extent permitted by law, our liability
                    for breach of any implied warranty or condition that cannot be excluded is
                    restricted, at our option to: the re-supply of services or payment of the
                    cost of re-supply of services; or the replacement or repair of goods or
                payment of the cost of replacement or repair.</p>
                <p>&nbsp;</p>
                <p><b>Jurisdiction</b></p>
                <p>Your use of this
                    web site and all of our legal notices will be governed by and construed in
                    accordance with the Laws of New South Wales , Australia and by using our
                    Internet sites you irrevocably and unconditionally submit to the jurisdiction
                of the courts of that State.</p>


            </div>
        </div>
    </div>
</div>

@endsection

