<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Agency Name', 'required' => 'required']) }}
    <span class="help-block">
        <strong class="text-danger">{{ $errors->first('name') }}</strong>
    </span>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::text('agency_id', null, ['class' => 'form-control', 'placeholder' => 'Agency ID', 'required' => 'required' ]) }}
    <span class="help-block">
        <strong class="text-danger">{{ $errors->first('agency_id') }}</strong>
    </span>
</div>

<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
    {{ Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Agency Address', 'required' => 'required']) }}
</div>

<div class="form-group {{ $errors->has('suburb') ? 'has-error' : '' }}">
    {{ Form::text('suburb', null, ['class' => 'form-control', 'placeholder' => 'Suburb', 'required' => 'required']) }}
</div>

<div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
    {{ Form::select('state', App\Models\Agency::STATES, null, ['class' => 'form-control', 'placeholder' => '--select state--', 'required' => 'required']) }}
</div>

<div class="form-group {{ $errors->has('post_code') ? 'has-error' : '' }}">
    {{ Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code', 'required' => 'required']) }}
</div>

<div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
    {{ Form::text('phone_number', null, ['class' => 'form-control', 'placeholder' => 'Phone Number', 'required' => 'required']) }}
</div>

<div class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
    {{ Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website URL']) }}
</div>

<div class="form-group {{ $errors->has('registration_number') ? 'has-error' : '' }}">
    {{ Form::text('registration_number', null, ['class' => 'form-control', 'placeholder' => 'ABN/ACN Number']) }}
</div>

<div class="form-group">
    {{ Form::label('logo', 'Choose Agency Logo', ['class' => 'control-label col-md-6']) }}
    <div class="col-md-6">
        {{ Form::file('logo', ['class' => 'form-control']) }}
    </div>
</div>