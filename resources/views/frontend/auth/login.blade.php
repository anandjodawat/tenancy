@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/unlock.png" class="img-responsive">
            <h2>Sign In</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/sign-in-side.png" alt="tenant" class="img-responsive">

            </div>
            <div class="col-sm-8">

                @include('flash::message')

                @php if ($applied_for != '') { @endphp
                <div class="apply_address_for">
                  Property: {{$applied_for}} {{$suburb}} {{$state_name}} {{$post_code}}
                </div>
                @php } @endphp

                <div class="paraSection property-block">
                   {{ Form::open(['route' => 'frontend.auth.login.post', 'role' => 'form']) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            </div>
                            <div class="form-group">
                                {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            </div>
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                                </label>
                                <div class="pull-right">
                                    <a href="/password/reset">Forgot your password?</a>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary btn-lg ten_login-btn" value="Login">
                        </fieldset>
                        {{ Form::close() }}
              </div>
          </div>

      </div>
  </div>
</div>

@endsection
