@extends('layouts.app')

@section('content')
    <div style="margin-top: 20px;"></div>
    @if (request('registeras'))
    <div class="col-md-6 col-md-offset-3">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">Register As Agency Admin</div>
            <div class="panel-body">
                {{ Form::open(['route' => 'frontend.auth.register.post']) }}
                <fieldset>
                    <div class="form-group">
                        {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                    </div>

                    @if (config('access.captcha.registration'))
                    <div class="form-group">
                        {!! Form::captcha() !!}
                        {{ Form::hidden('captcha_status', 'true') }}
                    </div>
                    @endif
                    {{ Form::hidden('login_as', 'agency-admin') }}

                    <hr>
                    <h3>Agency Details</h3>
                    <hr>

                    @include('frontend.auth._agency_details')

                    <input type="submit" class="btn btn-primary" value="Register">
                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    @else
        <div class="col-md-6 col-md-offset-3">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">Register as Tenant User</div>
                <div class="panel-body">
                    {{ Form::open(['route' => 'frontend.auth.register.post']) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
                            </div>
                            <div class="form-group">
                            {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
                            </div>
                            <div class="form-group">
                            {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                            </div>
                            <div class="form-group">
                            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                            </div>
                            <div class="form-group">
                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                            </div>

                            @if (config('access.captcha.registration'))
                                <div class="form-group">
                                    {!! Form::captcha() !!}
                                    {{ Form::hidden('captcha_status', 'true') }}
                                </div>
                            @endif
                            <input type="submit" class="btn btn-primary" value="Register">
                        </fieldset>
                    {{ Form::close() }}
                    <hr>
                    <div class="text-center">
                        <a href="{{ route('frontend.auth.register', ['registeras' => 'agency-admin']) }}">Register As Agency Admin</a>
                    </div>
                </div>
            </div>
        </div><!-- /.col-->
    @endif
@endsection

{{-- @section('after-scripts')
    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif
@endsection --}}