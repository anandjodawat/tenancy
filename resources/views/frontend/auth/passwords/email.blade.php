@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            {{-- <img src="/images/unlock.png" class="img-responsive"> --}}
            <h2>Reset Password</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div>
    @include('flash::message')
    <div class="container">
        <div class="row">
            <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/reset-password.png" alt="tenant" class="img-responsive">

            </div>
            <div class="col-sm-8">
                <div class="paraSection property-block">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                        {{ Form::open(['route' => 'frontend.auth.password.email.post', 'class' => 'form-horizontal']) }}

                        <div class="form-group">
                            {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{ Form::submit(trans('labels.frontend.passwords.send_password_reset_link_button'), ['class' => 'btn btn-primary']) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        {{ Form::close() }}
              </div>
          </div>

      </div>
  </div>
</div>

@endsection

