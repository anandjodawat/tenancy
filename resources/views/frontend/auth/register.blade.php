@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap tenant_register-page">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/unlock.png" class="img-responsive">
            <h2>Sign Up</h2>
            <hr style="margin-bottom: 60px;">
        </div>
    </div>
    <div class="container">
        <div class="row">
            @if(!request('registeras'))
            <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/reset-password.png" alt="tenant" class="img-responsive"> 
            </div>
            @endif
            <div class="col-sm-{{ request('registeras') ? '12' : '8' }}">
                @include('flash::message')
                <div class="paraSection property-block">
                  @if (request('registeras'))
                  {{ Form::open(['route' => 'frontend.auth.register.post', 'enctype' => 'multipart/form-data']) }}
                  <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>User Details</h3>
                            <hr>
                            <div class="form-group">
                                {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                                </span>
                            </div>
                            <div class="form-group">
                                {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                                </span>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            </div>
                            <div class="form-group">
                                {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            </div>
                            <div class="form-group">
                                {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            </div>

                            @if (config('access.captcha.registration'))
                            <!--<div class="form-group">
                                {!! Form::captcha() !!}
                                {{ Form::hidden('captcha_status', 'true') }}
                            </div>-->
                            @endif
                            {{ Form::hidden('login_as', 'agency-admin') }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Agency Details</h3>
                            <hr>
                            @include('frontend.auth._agency_details')
                        </div>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-primary btn-lg ten_login-btn" value="Register">
                    </div>
                </div>
                {{ Form::close() }}
                @else 
                <div class="col-sm-12 ">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {{ Form::open(['route' => 'frontend.auth.register.post']) }}
                            <fieldset>
                                <div class="form-group">
                                    {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                                    </span>
                                </div>
                                <div class="form-group">
                                    {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                                    </span>
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                </div>
                                <div class="form-group">
                                    {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                    </span>
                                </div>
                                <div class="form-group">
                                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                                    <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                </div>

                                @if (config('access.captcha.registration'))
                                <!--<div class="form-group">
                                    {!! Form::captcha() !!}
                                    {{ Form::hidden('captcha_status', 'true') }}
                                </div>-->
                                @endif
                                {{ Form::hidden('login_as', 'normal-user') }}
                                <input type="submit" class="btn btn-primary btn-lg ten_login-btn" value="Register As Tenant">
                            </fieldset>
                            {{ Form::close() }}
                            <hr>
                            <div class="text-center">
                                <a href="{{ route('frontend.auth.register', ['registeras' => 'agency-admin']) }}" class="btn btn-primary ten_login-btn">Register As Agency Admin</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif


            </div>
        </div>

    </div>
</div>
</div>

@endsection

