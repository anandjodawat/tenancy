@extends('layouts.app')

@section('content')
    <div style="margin-top: 20px;"></div>

		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">{{ trans('labels.frontend.auth.login_box_title') }}</div>
				<div class="panel-body">
					{{ Form::open(['route' => 'frontend.auth.login.post', 'role' => 'form']) }}
						<fieldset>
							<div class="form-group">
								 {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
							</div>
							<div class="form-group">
								 {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
							</div>
							<div class="checkbox">
								<label>
									{{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
								</label>
							</div>
							<input type="submit" class="btn btn-primary" value="Login">
						</fieldset>
					{{ Form::close() }}
				</div>
			</div>
		</div><!-- /.col-->
@endsection