@extends('layouts.frontend')

@section('content')

<div class="innerPageWrap">
    <div class="property-page-heading">
        <div class="container">
            <img src="/images/pm.png" class="img-responsive">
            <h2>Property Manager</h2>
            <hr>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 property-image wow fadeInDown" data-wow-delay=".3s">
                <img src="/images/property-manager-side.png" alt="tenant" class="img-responsive property-image">
            </div>
            <div class="col-sm-8">
                <div class="paraSection property-block">
                 <h3>Managing Property Made Easier.</h3>
                 <p>Tenancy Application allows Property Managers to receive branded online application forms submitted by Prospective Tenants for their rental properties, easing the process by removing the hardcopy system.</p>
                 <p>Property Managers can review references and identification through the Agent backend system as well as approving/rejecting the application.</p>
                 <p>Once a tenant is approved, there is an option for the tenant to pay a holding deposit through our rental payment gateway alliance.
                 </p>
             </div>
         </div>

     </div>
 </div>
</div>

@endsection

