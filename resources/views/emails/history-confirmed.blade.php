<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
    /* Media Queries */
    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }
</style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #f2f4f6;',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

    /* Masthead ----------------------- */

    'email-masthead' => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
    'email-body_cell' => 'padding: 35px;',

    'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

    /* Body ------------------------------ */

    'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
    'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

    /* Type ------------------------------ */

    'anchor' => 'color: #3869D4;',
    'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;',
    'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center' => 'text-align: center;',

    /* Buttons ------------------------------ */

    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
    background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
    text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

    'button_new' => 'padding: 9px 12px; mso-padding-alt: 9px 12px; min-width:180px; display: block;text-decoration: none;border:0; text-align: center; text-transform:uppercase; font-weight: 600;font-size: 16px; color: #FFFFFF; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px; mso-line-height-rule: exactly; line-height:18px;',

    'button--green' => 'background-color: #22BC66;', 'button--green--border' => 'border: 1px solid #22BC66;',
    'button--red' => 'background-color: #dc4d2f;',
    'button--blue' => 'background-color: #3869D4;',
        'logo-header-style' => 'background: #fff; width: 570px;  padding: 20px 0px 19px 0px; border: 1px solid f2f4f6; margin: -25px auto -26px auto;',
    'mail-table' => 'margin: 0 auto 0 auto; background: #fff; width: 570px;',
];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

<body style="{{ $style['body'] }}">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="{{ $style['email-wrapper'] }}" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <!-- Email Body -->
                    <tr>
                        <td style="text-align:center; background-size: cover; background-position: center;">
                            <img src="{{ $message->embed(url('images/logo-final.png')) }}" width="40%" style="max-width:100px; margin-bottom: -6px;">
                        </td>
                    </tr>

                        <tr>
                            <td style="{{ $style['email-body'] }}" width="100%">
                                <table style="{{ $style['email-body_inner'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                            <!-- Greeting -->
                                            <h1 style="{{ $style['header-1'] }}">
                                                <div align="center">
                                                    TENANCY HISTORY CONFIRMATION
                                                </div>
                                            </h1>
                                            <p style="{{ $style['paragraph'] }}">
                                                Reference Check has been completed from {{ $historyConfirmed->completed_by }}. <br>Please find the following information for verification.
                                            </p>
                                            <p>
                                                <b>Date: </b> {{ $historyConfirmed->date }} <br>
                                                <b>Agency Name: </b> {{ $historyConfirmed->agency }}<br>
                                                <b>Tenant Name: </b> {{ $historyConfirmed->tenant_name }}<br>
                                                <b>Property Address: </b> {{ $historyConfirmed->property_address }}<br>
                                                <b>Was the applicant(s) listed as a lessee? </b> {{ ucfirst($historyConfirmed->listed_as_lease) }}<br>
                                                <b>What dates were they in the property? </b> From {{ $historyConfirmed->property_duration_from }} to {{ $historyConfirmed->property_duration_to }}<br>
                                                <b>Did your office Terminate the Tenancy? </b> {{ ucfirst($historyConfirmed->terminate_tenancy) }}<br>
                                                @if ($historyConfirmed->terminate_tenancy == 'yes')
                                                <b>Reason: </b> {{ $historyConfirmed->reason_for_termination }}<br>
                                                @endif
                                                <b>During the tenancy, were the tenant(s) ever in arrears? </b> {{ $historyConfirmed->arrears }}<br>
                                                <b>Did the tenant recieve any Breach Notices other than arrears? </b> {{ ucfirst($historyConfirmed->breach_notices) }}<br>
                                                @if ($historyConfirmed->breach_notices == 'yes')
                                                <b>Reason: </b> {{ $historyConfirmed->reason_for_breach_notices }}<br>
                                                @endif
                                                <b>Was there any damage noted during inspections? </b> {{ $historyConfirmed->damage }}<br>
                                                @if ($historyConfirmed->damage == 'yes')
                                                <b>What was the damage? </b> {{ $historyConfirmed->damage_reason }}<br>
                                                @endif
                                                <b>During the tenancy, what were the inspections like? </b> {{ $historyConfirmed->inspection_like }}<br>
                                                <b>Were pets kept at the Property? </b> {{ $historyConfirmed->pets }}<br>
                                                <b>Was there any complaints or damage? </b> {{ $historyConfirmed->complaints_damage }}<br>
                                                <b>What date did tenants vacate the property? </b> {{ $historyConfirmed->vacate_date }}<br>
                                                <b>Was the Bond refunded in full? </b> {{ $historyConfirmed->bond_refund_full }}<br>
                                                @if ($historyConfirmed->bond_refund_full == 'yes')
                                                <b>Reason: </b> {{ $historyConfirmed->bond_refund_reason }}<br>
                                                @endif
                                                <b>On a scale of 1-10 how would you rate this tenant(s)? </b> {{ $historyConfirmed->rate_tenant }}<br>
                                                <b>Would you rent to them again? </b> {{ $historyConfirmed->rent_again }}<br>
                                                <b>Additional Comments: </b> {{ $historyConfirmed->additional_comments }}<br>
                                                <b>Completed By: </b> {{ $historyConfirmed->completed_by }}<br>
                                                <b>Submission Date: </b> {{ $historyConfirmed->submission_date }}
                                            </p>
                                            <!-- Salutation -->
                                            <p style="{{ $style['paragraph'] }}">
                                                {{ trans('strings.emails.auth.regards') }}<br>{{ app_name() }}
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- Footer -->
                        <tr>
                            <td>
                                <table style="{{ $style['email-footer'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                                            <p style="{{ $style['paragraph-sub'] }}">
                                                &copy; {{ date('Y') }}
                                                <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">{{ app_name() }}</a>.
                                                {{ trans('strings.backend.general.all_rights_reserved') }}
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    </html>
