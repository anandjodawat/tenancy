<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
    /* Media Queries */
    @media only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }
</style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #f2f4f6;',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

    /* Masthead ----------------------- */

    'email-masthead' => ' text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #f2f4f6;',
    'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; background:#fff;',
    'email-body_cell' => 'padding: 35px; line-height: 33px;',

    'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #AEAEAE; padding: 10px; text-align: center;',

    /* Body ------------------------------ */

    'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
    'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

    /* Type ------------------------------ */

    'anchor' => 'color: #3869D4;',
    'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;',
    'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center' => 'text-align: center;',

    /* Buttons ------------------------------ */

    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
    background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
    text-align: center; text-decoration: none; -webkit-text-size-adjust: none; margin: 20px;',

    'button_new' => 'padding: 9px 12px; mso-padding-alt: 9px 12px; min-width:180px; display: block;text-decoration: none;border:0; text-align: center; text-transform:uppercase; font-weight: 600;font-size: 16px; color: #FFFFFF; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px; mso-line-height-rule: exactly; line-height:18px;',

    'button--green' => 'background-color: #22BC66;', 'button--green--border' => 'border: 1px solid #22BC66;',
    'button--red' => 'background-color: #dc4d2f;',
    'button--blue' => 'background-color: #3869D4;',
    'logo-header-style' => 'background: #fff; width: 570px;  padding: 20px 0px 19px 0px; border: 1px solid f2f4f6; margin: -25px auto -26px auto;',
    'mail-table' => 'margin: 0 auto 0 auto; background: #fff; width: 570px;',
];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

<body style="{{ $style['body'] }}">
  <table width="100%" cellpadding="0" cellspacing="0" {{ $style['mail-table'] }} style="margin: 0 auto 0 auto; background: #fff; width: 570px; ">
    <tr style="margin: 0 auto 0 auto; background: #fff; width: 570px; ">
      <td style="{{ $style['email-wrapper'] }}" align="center">
        <table width="100%" cellpadding="0" cellspacing="0">

          <!-- Email Body -->
          <tr>
            <td style="{{ $style['email-body'] }}" width="100%">
              <table style="{{ $style['email-body_inner'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                <tr style="background: #ffffff;">
                  <td style="text-align:center; background-size: cover; background-position: center;">
                    <img src="{{ $message->embed(url('images/logo-final.png')) }}" width="40%" style="max-width:100px; margin-bottom: -6px;">
                  </td>
                </tr>

                <tr style="background: #ffffff;">
                  <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                    <!-- Greeting -->
                    <center>
                      <h1 style="{{ $style['header-1'] }}">
                        Thank you for registering online, you are steps away from activating your account.
                      </h1>
                    </center>

                    <!-- Intro -->
                    <p style="{{ $style['paragraph'] }}">
                      @if ($user->hasRole('agency-admin'))
                        Please click on CONFIRM ACCOUNT button below to activate your AGENCY account.
                      @else
                        Please click on CONFIRM ACCOUNT button below to activate your TENANT account.
                      @endif
                    </p>

                    <p style="{{ $style['paragraph'] }}">
                      Please do not reply to this mail
                    </p>

                    <!-- Action Button -->
                    {{-- @if (isset($actionText)) --}}
                    <table style="{{ $style['body_action'] }}" align="center" width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center">
                          <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                            <tr>
                              <td align="center" style="{{ $style['button--green'] }} min-width:180px; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px; font-family: 'Neutraface-Demi', Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                <a class="msoAltFont" target="_blank" href="{{ route('frontend.auth.account.confirm', $confirmation_code) }}" style="{{ $style['button_new'] }} {{ $style['button--green'] }} {{ $style['button--green--border'] }}">CONFIRM ACCOUNT</a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    {{-- @endif --}}

                    <!-- Outro -->
                    <p style="{{ $style['paragraph'] }}">
                    Thank you for using our application!
                    </p>

                    <!-- Salutation -->
                    <p style="{{ $style['paragraph'] }}">
                    {{ trans('strings.emails.auth.regards') }}<br>{{ app_name() }}
                    </p>

                    <!-- Sub Copy -->
                    @if (isset($actionText))
                    <table style="{{ $style['body_sub'] }}">
                      <tr>
                        <td style="{{ $fontFamily }}">
                          <p style="{{ $style['paragraph-sub'] }}">
                            {{ trans('strings.emails.auth.trouble_clicking_button', ['action_text' => $actionText]) }}
                          </p>

                          <p style="{{ $style['paragraph-sub'] }}">
                            <a style="{{ $style['anchor'] }}" href="{{ $actionUrl }}" target="_blank">{{ $actionUrl }} {{ route('frontend.auth.account.confirm', $this->confirmation_code) }}</a>
                          </p>
                        </td>
                      </tr>
                    </table>
                    @endif
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <!-- Footer -->
          <tr>
            <td>
              <table style="{{ $style['email-footer'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                <tr>
                  <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                    <p style="{{ $style['paragraph-sub'] }}">
                      &copy; {{ date('Y') }}
                      <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">{{ app_name() }}</a>.
                      {{ trans('strings.backend.general.all_rights_reserved') }}
                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
</table>
</body>
</html>
