<!DOCTYPE html>
<html>
<head>
    <title>Tenant Application Form</title>
    <style>
    .page-break {
        page-break-after: always;
    }

    body {
        margin: 0px;
        font-size: 12px;
        font-family: sans-serif;
    }

    h1 {
        text-align: center;
    }

    h2 {
        font-weight: bold;
        font-size: 15px;
    }
    h3{
        font-size: 14px;
    }


    input[type="checkbox"], input[type="radio"] {
        vertical-align: middle;
        margin-top: 8px;
        line-height: 8px;
    }

    input[type="text"]{
        height: 20px;
        vertical-align: middle;
        background-color: rgb(233, 236, 247);
        border-left-color: #fff;
        border-right-color: transparent;
        border-top-color: transparent;
        border-bottom-color: transparent;
    }

    textarea{
     background-color: rgb(233, 236, 247);
     border-color: rgb(233, 236, 247);
     height: 60px;
 }

 label {
    position: relative;
    border-right-color: grey;

    vertical-align: middle;
    font-size: 12px;


}
small {
 vertical-align: middle;

}
span{
    height: 90px;
    padding-top: 16px;
    padding-bottom: 16px;
}
ol li{
    margin-bottom: 10px;

}

th{
    background: rgb(233, 236, 247);;
    height: 20px;
    border: 1px solid #ddd;
    font-size: 12px;
    font-weight: 400;
}

td{
    background: #ddd;
    height: 20px;
}

table{
    margin-top: -3px;
}
.terms-condition p{
    font-size: 10px;
    line-height: 11px;
    margin-top: 3px;
    margin-bottom: 3px;
}

.terms-condition ul li{
    font-size: 10px;
    line-height: 11px;
    margin-top: 3px;
    margin-bottom: 3px;
}
.terms-condition ul{
    margin-bottom: 2px;
    margin-top: 2px;
}
.rental-rewards p{
    font-size: 10px;
    line-height: 11px;
    margin-top: 3px;
    margin-bottom: 3px;
}
.rental-rewards li{
    font-size: 10px;
    line-height: 11px;
    margin-top: 3px;
    margin-bottom: 3px;
}

.rental-rewards ol{
    margin-bottom: 5px;
    margin-top: 5px;
}

</style>
</head>
<body>
    <div style="width: 100%; position: relative; float: left;">
        <img src=" {{ url('images/logos/ray1111.png') }} " style="max-width: 160px; width: 100%; float: right; ">
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div style="background: #800080;">
        <h1 style="width: 100%; text-align: center; text-transform: capitalize; font-size: 35px; color: #fff;">Residential Tenancy Application</h1>
        <p style="text-align: center; font-size: 20px; color: #fff; margin-top: -25px; padding-bottom: 15px;">www.tenancyapplication.com.au</p>
    </div>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

{{--     <div style="height: 110px; width: 250px; background-color: rgb(241, 244, 255); padding-top: 10px; padding-left: 15px; ">
        <img {{ url('images/logos/ray1111.png') }} " style="max-width: 200px; width: 100%;" alt="Agency Logo" title="Agency Logo">
    </div>
    --}}
    <div style="width: 100%;">
        <img src=" {{ url('images/logos/ray1111.png') }} " style="max-width: 160px; width: 100%; ">
    </div>
    <br>
    <br>
    <br>

    <div class="second-page" style="width: 50%; text-align: left;
    padding-right: 10px; position: relative; float: left;">
        <h2>AGENT DETAILS</h2>
    <br>
    <br>

    <table style="width:100%">
        <tr>
            <th style="width:40%;">Agency Name</th><td style="width: 60%;"> {{ optional(auth()->user()->agency_profile())->name }}</td>
        </tr>
        <tr>
            <th style="width:40%;">Property Manager</th><td style="width: 60%;"> {{ auth()->user()->name }}</td>
        </tr>

        <tr>
            <th style="width: 20%;">Email</th><td style="width: 80%;"> {{ auth()->user()->email }}</td>
        </tr>
        <tr>
            <th style="width: 40%;">Date Received</th><td style="width: 80%;"> &nbsp;</td>
        </tr>
        <tr>
            <th style="width: 40%;">Application ID</th><td style="width: 80%;"> &nbsp;</td>
        </tr>
    </table>
    <table style="width:100%">

        <tr>
            <th style="width: 23%;">Apply Online</th><td style="width: 77%; color: blue;"> http://www.tenancyapplication.com.au/rentalhome</td>
        </tr>

    </table>
</div>


<div style="width:50%; float: left; padding-left: 10px;">

    <h2>Supporting Documents</h2>
    @if ($supporting_document)
    @php

    $segments = explode('<li>', $supporting_document);
    foreach ($segments as $key => $segment) {
        if ($key == 0){

         echo  '<table style="width:100%;" >
         <tr>
         <h3 style="font-size: 14px; margin-top: 0px; margin-bottom: 0px;">'.$segment.'</h3>
         </tr>
         </table>';
     } else {
         echo '<table style="width:100%"> <tr>

         <th style="width: 7%;"><input type="checkbox">
         </th><td style="width: 93%;">'.$segment.  '</td>
         </tr></table>';

     }
 }
 @endphp
 @else



 <table style="width:100%;" >
    <tr>
     <h3 style="font-size: 14px; margin-top: 0px; margin-bottom: 0px;">A minimum of 100 points is required to apply.</h3>
 </tr>
</table>
<table style="width:100%">

    <tr>

        <th style="width: 7%;"><input type="checkbox">
        </th><td style="width: 93%;"> 40 points* Primary/Photo ID. or National card  </td>
    </tr>

    <tr>

        <th style="width: 7%;"><input type="checkbox">
        </th><td style="width: 93%;">20 points* proof of address bank statement, utility bill,
            motor vehicle registration.(Eg. Birth Certificate, Student
        Card, Medicare Card, Health Care Card, Vehicle Registration)</td>
    </tr>

    <tr>

        <th style="width: 7%;"><input type="checkbox">
        </th><td style="width: 93%;"> 15 points Rental History/tenent ledger or rental
        reference/older referene </td>
    </tr>

    <tr>

        <th style="width: 7%;"><input type="checkbox">
        </th><td style="width: 93%;"> 15 points* Employment/Proof of Income. (Eg. Payslips,
        Letter of Employment, Employment Reference) </td>
    </tr>

    <tr>

        <th style="width: 7%;"><input type="checkbox">
        </th><td style="width: 93%;"> 10 points for lease Reference
        </td>
    </tr>


</table>


@endif


</div>



<div class="page-break"></div>

<div class="second-page" style="width: 50%; text-align: left;
padding-right: 10px; position: relative; float: left;">
<h2 style="margin-top: 4px; margin-bottom: 4px;">Property Details</h2>
<table style="width:100%">
    <tr>
        <th style="width:20%;">Full Address</th><td style="width: 70%; height: 40px;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="width: 40%;">Post Code</th><td style="width: 60%;"> &nbsp;</td>
        <th style="width: 50%;">Property Code</th><td style="width: 50%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 50%;">No. of Bedroom</th><td style="width: 50%;"> &nbsp;</td>
        <th style="width: 30%;">Bond</th><td style="width: 70%;"> $</td>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="width: 30%;">Rent</th><td style="width: 70%;"> &nbsp;</td>
        <th style="width:62%;"><input type="checkbox">Weekly $</th><td style="width: 38%;"> </td>
        <th style="width: 62%;"><input type="checkbox">Monthly $</th><td style="width: 38%;"> </td>
    </tr>
</table>
<table style="width:100%">

    <tr>
     <th style="width: 40%;">Commencement Date </th><td style="width: 60%;"> &nbsp; </td>
 </tr>
</table>
<table style="width:100%">

    <tr>
     <th style="width: 40%;">Preferred Lease Term </th><td style="width: 60%;"> &nbsp; </td>
 </tr>
</table>

<table style="width:100%">

    <tr>
     <th style="width: 40%;">Total No. of Occupants  </th><td style="width: 60%;"> &nbsp; </td>
 </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 70%;">Total No. of Children </th><td style="width: 30%;"> &nbsp;</td>
        <th style="width:20%;">Ages</th><td style="width: 80%;"> </td>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="width: 60%;">No. of Vehicles </th><td style="width: 40%;"> &nbsp;</td>
        <th style="width:40%;">No. of Pets</th><td style="width: 60%;"> </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width:50%; height: 40px;">How did you find out about this property? </th><td style="width: 50%;"> </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width:40%;">Date Inspected</th><td style="width: 60%;"> </td>
    </tr>
    <tr>
        <th style="width:40%;">Inspection code</th><td style="width: 60%;"> </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width:28%; height: 50px;">Other Occupants details </th><td style="width: 72%;"> </td>
    </tr>
</table>
<h2 style="margin-top: 4px;">Moving Services</h2>
<p style="margin-top: -10px;">Ozaybhatta@gmail.com</p>

<table style="width:100%; margin-top: -15px;">
    <tr>
        <td>Selecting the right utility provider can make your move
            easier and may save you money.
            Would you like a call closer to your moving date to help
            arrange connection to your household services? You'll get a
            phone call, email and help to compare and select a plan
            from available suppliers.
        </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width:62%;"><input type="checkbox">Electricity </th>
        <th style="width:62%;"><input type="checkbox">Gas</th>
        <th style="width: 62%;"><input type="checkbox">Telephone</th>
    </tr>
    <tr>
        <th style="width:62%;"><input type="checkbox">Internet </th>
        <th style="width:62%;"><input type="checkbox">Bottled gas</th>
        <th style="width: 62%;"><input type="checkbox">Pay TV</th>
    </tr>
    <tr>
        <th style="width:62%;"><input type="checkbox">Disconnections </th>
        <th style="width:62%;"><input type="checkbox">Removalist</th>
        <th style="width: 62%;"><input type="checkbox">Vehicle hire</th>
    </tr>
    <tr>
        <th style="width:62%;"><input type="checkbox">Cleaning services </th>
    </tr>

</table>

<table style="width:100%; margin-top: 6px;">
    <tr>
        <td style="font-size: 10.7482px;">By submitting this application, you are providing your consent and
            authorisation to the moving services provider to arrange for the connection
            and/or disconnection of the nominated utility products or services as well as
            the purchase of the nominated product. In order to operate their business
            activities efficiently and to facilitate your request they may need to collect
            your personal information. By submitting this application you are providing
            your consent and authorisation for the moving service provider to disclose
            and use your personal information in accordance with their privacy policy.
            By submitting this application, you declare that all of the information
            contained or disclosed in this application is true and correct. You further are
            confirming your understanding and agreement to the Privacy Policy and
            Terms and Conditions which is being provided to you. <br><br><br><br>
            <p style="margin-bottom: -30px;">&nbsp;&nbsp;........................................ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;........................................ </p>

            <p>&nbsp;&nbsp;(Signature) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Date)</p>
        </td>
    </tr>
</table>
</div>

<div class="second-page" style="width: 50%; text-align: left;
padding-right: 10px; position: relative; float: left;">
<h2 style="margin-top: 4px; margin-bottom: 4px;">Personal Details</h2>
<table style="width:100%">
    <tr>
        <th style="width:20%;">Name</th><td style="width: 80%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:20%;">Gender</th><td style="width: 25%;"> &nbsp;</td>
        <th style="width:35%;">Date of birth</th><td style="width: 80%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:45%;">Home Phone</th><td style="width: 55%;"> &nbsp;</td>
        <th style="width:45%;">Work Phone </th><td style="width: 55%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:30%;">Mobile</th><td style="width: 70%;"> &nbsp;</td>
        <!-- <th style="width:25%;">Fax</th><td style="width: 75%;"> &nbsp;</td> -->
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:15%;">Email</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
</table>
<h2 style="margin-top: 4px; margin-bottom: 4px;">Emergency Contact</h2>
<table style="width:100%">
    <tr>
        <th style="width:15%;">Name</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width:15%;">Relationship</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:30%;">Contact Address</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:25%;">Home Phone</th><td style="width: 75%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width:25%;">Work Phone</th><td style="width: 75%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:15%;">Mobile</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width:15%;">Email</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
</table>

<h2 style="margin-top: 4px; margin-bottom: 4px;">Current Address Details</h2>
<table style="width:100%">
    <tr>
        <td>What are your current living arrangements? <br><br><br></td>
    </tr>
</table>
<br>
<table style="width:100%; margin-top: -16.5px;">
    <tr>
        <th style="width:10%;">Unit</th><td style="width: 90%;"> &nbsp;</td>
        <th style="width:20%;">Street</th><td style="width: 80%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:45%;">Street Name</th><td style="width: 55%;"> &nbsp;</td>
        <th style="width:40%;">Street Type</th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:40%;">Suburb</th><td style="width: 60%;"> &nbsp;</td>
        <th style="width:35%;">State</th><td style="width: 65%;"> &nbsp;</td>
        <th style="width:55%;">Post Code</th><td style="width: 45%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:40%;">When did you move in?</th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%; border-top: .01px solid #fff;">
    <tr>
        <td>Reason for leaving: <br><br><br></td>
    </tr>
</table>

<br>
<table style="width:100%; margin-top: -17px;">
    <tr>
        <th style="width:30%;">Landlord's Name</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>


<table style="width:100%">

    <tr>
        <th style="width: 73%;">Is your postal address different?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%;">
    <tr>
        <th style="width:30%;">Post Address</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%;">
    <tr>
        <th style="width:30%;">Post Subrub</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%;">
    <tr>
        <th style="width:40%;">Post State</th><td style="width: 60%;"> &nbsp;</td>
        <th style="width:40%;">Post Code</th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<h2 style="margin-top: 4px; margin-bottom: 4px;">Previous address details</h2>
<table style="width:100%">
    <tr>
        <td>What was your previous living arrangements? <br><br><br></td>
    </tr>
</table>
<br>
<table style="width:100%; margin-top: -16.8px;">
    <tr>
        <th style="width:10%;">Unit</th><td style="width: 90%;"> &nbsp;</td>
        <th style="width:20%;">Street</th><td style="width: 80%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:45%;">Street Name</th><td style="width: 55%;"> &nbsp;</td>
        <th style="width:40%;">Street Type</th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%; border-top: .01px solid #fff;">
    <tr>
        <th style="width:40%;">Suburb</th><td style="width: 60%;"> &nbsp;</td>
        <th style="width:35%;">State</th><td style="width: 65%;"> &nbsp;</td>
        <th style="width:55%;">Post Code</th><td style="width: 45%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:40%;">From Date</th><td style="width: 60%;"> &nbsp;</td>
        <th style="width:35%;">To Date</th><td style="width: 65%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <td>Reason for leaving <br><br><br></td>
    </tr>
</table>

</div>





<div class="page-break"></div>

<div class="second-page" style="width: 50%; text-align: left;
padding-right: 10px; position: relative; float: left;">
<h2 style="margin-top: 4px; margin-bottom: 4px;">Current Emplyment Details</h2>
<table style="width:100%">
    <tr>
        <td>What is your current work situation? <br><br></td>
    </tr>
</table>
<br>
<table style="width:100%; margin-top: -16.8px;">
    <tr>
        <td style="width:62%;"><input type="checkbox">I am currently employed </td>
        <td style="width: 62%;"><input type="checkbox">Self employed</td>
    </tr>
</table>
<table style="width:100%;">

    <tr>
        <td style="width: 62%;"><input type="checkbox">Unemployed</td>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="width: 40%;">Company Name</th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 50%;">Manager/Contact Name</th><td style="width: 50%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 50%;">Phone</th><td style="width: 50%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 50%;">Email</th><td style="width: 50%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">
    <tr>
        <td>Current Address: <br><br></td>
    </tr>
</table>
<br>
<table style="width:100%; margin-top: -16.8px;">
    <tr>
        <th style="width: 30%;">Industry</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 30%;">Occupation/Position <br><br></th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%;">

    <tr>
        <th style="width: 40%;">Nature of Employment <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 40%;">When did you start? </th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%;">

    <tr>
        <th style="width: 55%;">Gross Annual Salary Before Tax </th><td style="width: 45%;"> &nbsp;</td>
    </tr>
</table>


<h2 style="margin-top: 4px; margin-bottom: 4px;">Previous Emplyment Details</h2>
<table style="width:100%">
    <tr>
        <td>What was your previous work? <br><br></td>
    </tr>
</table>
<br>
<table style="width:100%; margin-top: -16.8px;">
    <tr>
        <td style="width:62%;"><input type="checkbox">I was previously employed </td>
        <td style="width: 62%;"><input type="checkbox">Self employed</td>
    </tr>
</table>
<table style="width:100%;">

    <tr>
        <td style="width: 62%;"><input type="checkbox">Unemployed</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width: 40%;">Company Name <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%; border-top: .01px solid #fff;">
    <tr>
        <th style="width: 50%;">Manager/Contact Name <br><br></th><td style="width: 50%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%; border-top: .01px solid #fff;">
    <tr>
        <th style="width: 20%;">Phone</th><td style="width: 80%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%; ">
    <tr>
        <th style="width: 20%;">Email</th><td style="width: 80%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%;  border-top: .01px solid #fff; ">
    <tr>
        <td>Address: <br><br></td>
    </tr>
</table>
<br>
<table style="width:100%; margin-top: -16.8px; border-top: .01px solid #fff;"">
    <tr>
        <th style="width: 40%;">Industry <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 40%;">Occupation/Position <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%; border-top: .01px solid #fff;">

    <tr>
        <th style="width: 40%;">Nature of Employment <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 40%;">When did you start? <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width: 40%;">When did you finish? <br><br></th><td style="width: 60%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%;  border-top: .01px solid #fff;">

    <tr>
        <th style="width: 55%;">Gross Annual Salary Before Tax <br><br></th><td style="width: 45%;"> &nbsp;</td>
    </tr>
</table>
</div>

<div class="second-page" style="width: 50%; text-align: left;
padding-right: 10px; position: relative; float: left;">
<h2 style="margin-top: 4px; margin-bottom: 4px;">Personal References</h2>

<table style="width:100%">
    <tr>
        <td>Someone who knows you well who will not be living with you <br><br></td>
    </tr>
</table>
<br>

<table style="width:100%; margin-top: -16.8px;">

    <tr>
        <th style="width:35%;">Personal Reference</th><td style="width: 65%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:20%;">Occupation</th><td style="width: 25%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width:20%;">Relationship</th><td style="width: 25%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:45%;">Phone</th><td style="width: 55%;"> &nbsp;</td>
        <th style="width:45%;">Mobile </th><td style="width: 55%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">
    <tr>
        <th style="width:15%;">Email</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width:15%;">Address</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
</table>
<h2 style="margin-top: 4px; margin-bottom: 4px;">Professional References</h2>
<table style="width:100%">
    <tr>
        <th style="width:55%;">Work colleagues, associates, etc</th><td style="width: 45%;"> &nbsp;</td>
    </tr>
    <tr>
        <th style="width:55%;">Name of Professional Reference 1</th><td style="width: 45%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:30%;">Company Name</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:25%;">Relationship</th><td style="width: 75%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:25%;">Phone</th><td style="width: 75%;"> &nbsp;</td>
        <th style="width:25%;">Mobile</th><td style="width: 75%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:15%;">Email</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">
    <tr>
        <th style="width:30%;">Company Address</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">
    <tr>
        <th style="width:55%;">Name of Professional Reference 2</th><td style="width: 45%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:30%;">Company Name</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:25%;">Relationship</th><td style="width: 75%;"> &nbsp;</td>
    </tr>
</table>
<table style="width:100%; border-top: .1px solid #fff;">
    <tr>
        <th style="width:20%;">Phone</th><td style="width: 80%;"> </td>
        <th style="width:20%;">Mobile</th><td style="width: 80%;"></td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th style="width:15%;">Email</th><td style="width: 85%;"> &nbsp;</td>
    </tr>
</table>

<table style="width:100%">
    <tr>
        <th style="width:30%;">Company Address</th><td style="width: 70%;"> &nbsp;</td>
    </tr>
</table>


<h2 style="margin-top: 4px; margin-bottom: 4px;">Further Questions</h2>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Has your tenancy ever been terminated by a landlord or agent?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="width: 73%;">Have you ever been refused a property by any landlord or agent?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Are you in debt to another landlord or agent?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Have any deductions ever been made from your rental bond?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Is there any reason known to you that would affect your future rental payments?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Do you have any other applications pending on other properties?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Do you currently own a property?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

<table style="width:100%">

    <tr>
        <th style="width: 73%;">Are you considering buying a property after this tenancy or in the near future?</th><td style="width: 15%;"><input type="checkbox"> Yes </td><td style="width: 12%;"><input type="checkbox"> No </td>
    </tr>
</table>

</div>



<div class="page-break"></div>
{{-- Rental Rewards --}}
<div class="rental-rewards">

    @if($lease_terms)

    @php

    echo $lease_terms;

    @endphp

    @else

    <div style="width: 30%; float: left">
        <h3> Rental Rewards</h3>
        <img src="{{-- {{ url('images/rentalrewards_logo.png') }} --}}" alt="Rental Rewards Logo" width="70%">

        <p><strong>Bond &amp; Initial Payments :</strong></p>

        <p><strong>We sk for this Terms to Fallow : </strong></p>
    </div>

    <div style="width: 70%; position: relative; float: left;">
        <img src=" {{ url('images/logos/ray1111.png') }} " style="width: auto; min-width: 300px; height: 75px; float: left; ">
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <ol style="list-style-type: lower-alpha;">
        <li>Bond payments can be made direct to the Bond Board or by Bank Cheque or Money Order.</li>
        <li>The first 2 weeks rent are to be paid by Visa/MasterCard Credit Card or Debit Card, AMEX or Diners.</li>
    </ol>
    <p><strong>Holding Deposit</strong>.</p>

    <p>The holding deposit can only be accepted after the application for tenancy has been approved.</p>

    <p>The holding fee of 1 WEEKS RENT keeps the premises off the market for the prospective tenant for 7 days (or longer by agreement). In consideration of an approved holding fee paid by a prospective tenant, the agent acknowledges that:</p>

    <ol style="list-style-type:lower-roman ">
        <li>The application for tenancy has been approved by the landlord; and</li>

        <li>The premises will not be let during the above period, pending the making of a residential tenancy agreement; and</li>

        <li>If the prospective tenant(s) decide not to enter into such an agreement, the landlord may retain the whole fee; and</li>

        <li>If a residential tenancy agreement is entered into, the holding fee is to be paid towards rent for the residential premises concerned.</li>

        <li>The whole of the fee will be refunded to the prospective tenant if:</li>

        <ol style="list-style-type: lower-alpha;">
            <li>the entering into of the residential tenancy agreement is conditional on the landlord carrying out repairs or other work and the landlord does not carry out the repairs or other work during the specified period</li>
            <li>the landlord/landlord’s agent have failed to disclose a material fact(s) or made misrepresentation(s) before entering into the residential tenancy agreement.</li>
        </ol>
    </ol>

    <p><strong>Ongoing Rent Payments</strong></p>

    <p>Our preferred payment method for rent is Rental Rewards. (Please note that there is a small convenience fee charged for the use of the system). These fees are charged by a third-party payment processor – Rental Rewards</p>

    <p>The fees for the convenience of the use of the services are: (must be able to set-up fee at agent level, as there is variation)</p>
    <ol style = "list-style-type: lower-alpha;">

        <li>Bank Account: $4 Testing.</p>

            <li>BPAY: $4.40 inc GST (internet banking using Rental Rewards Biller and Reference).</p>

                <li>BINt : $45</p>

                </ol>

                <p>Please read above clearly and&nbsp; do as it is asked.</p>

                {{-- Declaration Details --}}
                <h3 style="margin-bottom: 5px; margin-top: 5px;"> Declaration Details</h3>

                <table style="width:100%">

                    <tr>
                        <th style="width: 80%;">Have you inspected this property?</th><td style="width: 10%;"><input type="checkbox"> Yes </td><td style="width: 10%;"><input type="checkbox"> No </td>
                    </tr>
                </table>


                <h3 style="margin-top: 5px; margin-bottom: 5px;">If Yes please fill the following fields.</h3>

                <table style="width:100%">

                    <tr>
                        <th style="width: 30%;">Inspected Date</th><td style="width: 70%;">&nbsp; </td>
                    </tr>
                </table>


                <table style="width:100%">

                    <tr>
                        <th style="width: 80%;">Was the property upon your inspection in a reasonably clean and fair condition?</th><td style="width: 10%;"><input type="checkbox"> Yes </td><td style="width: 10%;"><input type="checkbox"> No </td>
                    </tr>
                </table>

                <table style="width:100%">

                    <tr>
                        <th style="width: 30%;">Inspection Code</th><td style="width: 70%;">&nbsp; </td>
                    </tr>
                </table>
                <br>
                <table style="width:100%">

                    <tr>

                        <td style="width: 4%;"><input type="checkbox">
                        </td><th style="width: 96%;"> By ticking this box I acknowledge that I have Read, Understood and Agree with the above Tenancy Privacy Statement / Collection Notice & Tenant Declaration and I authorise the use of my digital signature for the purpose of this application </th>
                    </tr>
                </table>

            </div>

            @endif
            <div class="page-break"></div>




            <div class="terms-condition">
                <h3>Terms and Conditions</h3>

                <p>1. Tenancy Privacy Statement / Collection Notice</p>

                <p>Due to the changes in the Privacy Laws, all property managers must ensure that you (the applicant) fully understand the National Privacy Principles and the way they must use your personal information to carry out their role as professional property managers.</p>
                <p>The information, personal or otherwise, provided by the prospective tenant in this application or that which is collected from other sources is necessary for the agent to assess the risk in providing you with the tenancy, to identify the applicant’s identity and to process, evaluate and manage the tenancy.</p>

                <p>The personal information collected about you (the applicant) in this application may be disclosed, by use of the internet or otherwise, to other parties, including:</p>
                <ul>
                    <li>Trades People</li>
                    <li>Financial Institutions</li>
                    <li>Government and Statutory bodies</li>
                    <li>Referees</li>
                    <li>Solicitors</li>
                    <li>Property Evaluators</li>
                    <li>Existing or potential clients of the agent</li>
                    <li>Rental Bond Authorities</li>
                    <li>Tenant Databases</li>
                    <li>Other Real Estate Agents</li>
                    <li>Other Third Parties as required by law</li>
                    <li>Collection Agents</li>
                    <li>Verification Services</li>
                    <li>Other Landlords</li>
                    <li>Body Corporates</li>
                </ul>

                <p>Information already held on tenancy databases may also be disclosed to the Agent and/or landlord. Unless you advise the Agent to the contrary, the Agent may also disclose such information to The Real Estate Institute of your State and to NTD, TRA, TICA, RP DATA, BARCLAY MIS or DATAKATCH for the purpose of documenting all leasing data in the area for the benefit of its members as part of membership services and for others in the property related industries, and so as to assist them in continuing to provide the best possible service to their clients. In providing this information, you (the applicant) agree to its use, unless you advise the Agent differently.</p>

                <p>The privacy policy of your State's Real Estate Institute can be viewed by logging on to www.reia.asn.com.au and selecting your State.</p>
                <p>The privacy policy of NTD can be viewed by logging on to https://www.ntd.net.au or contact them on NTD – 1300 563 826</p>
                <p>
                The privacy policy of TRA can be viewed by logging on to http://tradingreference.com or contact them on TRA – (02) 9363 9244</p>
                <p>The privacy policy of TICA can be viewed by logging on to https://www.tica.com.au/ or contact them on TICA – 1902 220 346
                </p>
                <p>The privacy policy of BARCLAY MIS can be viewed on http://barclaymis.com.au or contact them on BARLCAY MIS – 1300 883 916
                </p>

                <p>The privacy policy of DATAKATCH can be viewed on http://datakatch.com.au or contact them on DATAKATCH – (02) 9086 9388
                </p>
                <p>The Agent will only disclose information in this way to other parties to achieve the purposes specified above or as allowed under the Privacy Act.</p>

                <p>If you (the applicant) would like to access this information you can do so by contacting the Agent at the address and contact numbers for the property you are interested in renting. You (the applicant) can also correct this information if it is inaccurate, incomplete or out of date.
                </p>
                <p>
                If your personal information is not provided to the Agent and you (the applicant) do not consent to the use of this information as specified above, the Agent cannot carry out their duties and may not be able to provide you with the lease/tenancy of the premises.</p>

                <p>2. Tenant Declaration</p>
                <ul>
                    <li>
                    I acknowledge that this is an application to lease the property for which I am applying and that my application is subject to the owner's approval and the availability of the premises on the due date. No action will be taken against the landlord or agent should the premises not be ready for occupation on the due date or if my application is unsuccessful.</li>

                    <li>I acknowledge that the processing period for my application could be up to 2 working days and in some circumstances longer. Unless contacted earlier by staff from the real estate agent in question - I will expect this time frame.
                    </li>
                    <li>
                    I acknowledge that the landlord and landlord's agent will rely on the truth of my answers in assessing the application for tenancy</li>

                    <li> I hereby offer to rent the property from the owner under a lease to be prepared by the Agent pursuant to the Residential Tenancies Act.
                    </li>
                    <li>
                        I acknowledge that I will be required to pay rent and a rental bond subject to the conditions of the Agent
                    </li>
                    <li>
                       I acknowledge that an inquiry, independent or otherwise, may be made on all applicants applying for this property, to verify the validity of the personal details that have been supplied and to check my credit worthiness. If I default under a rental agreement, the Agent may disclose details of any such default to any person whom the Agent reasonably considers has an interest receiving such information.
                   </li>

                   <li>
                    I/we have been given the opportunity to view a copy of the standard terms and conditions that would be included in a lease, should my application be successful
                </li>
                <li>I declare that all information contained in this application is true and correct and given of my own free will and can be based as fact.
                </li>
                <li>
                    I acknowledge that the agent in question cannot confirm that any phone lines to the property are operable or able to be reconnected. I understand that’s it’s the tenants responsibility to check with the telephone provider before proceeding with the tenancy to confirm the situation with the telephone line. Ensuring the main switch is in the off position for power connection remains the responsibly of the tenant .
                </li>
            </ul>

            <table style="width:100%">

                <tr>

                    <td style="width: 4%;"><input type="checkbox">
                    </td><th style="width: 96%;">
                    I acknowledge that I have Read, Understood and Agree with the General Lease Terms, Tenancy Privacy Statement / Collection Notice & Tenant Declaration </th>
                </tr>
            </table>

            <br>
            <br>
            <br>
            <p>......................................................................... &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                ...........................................<br>
                &nbsp;&nbsp;(Signature)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (Date)
            </p>

        </div>
    </body>
    </html>
