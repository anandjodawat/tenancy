<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        {{ env('APP_NAME') }}
    </title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="/property-manager/assets/css/normalize.css">
    <link rel="stylesheet" href="/property-manager/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/property-manager/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/property-manager/assets/css/themify-icons.css">
    <link rel="stylesheet" href="/property-manager/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="/property-manager/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/property-manager/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/css/backend/backend.css">
    <!-- <link rel="stylesheet" href="/property-manager/assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="/property-manager/assets/scss/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link href="/property-manager/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <style type="text/css">
    aside.left-panel{
        padding: 0;
    }
    #main-menu.main-menu.navbar-collapse{
        padding: 0 20px;
    }
    .navbar .navbar-nav li > a .menu-icon{
        width: 25px;
    }
</style>
</head>
<body>

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            @include('property-manager.includes.sidebar-header')
            @include('property-manager.includes.sidebar')

        </nav>
    </aside>
    <div id="right-panel" class="right-panel">
        @include('property-manager.includes.header')
        @yield('content')
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="/js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="/property-manager/assets/js/plugins.js"></script>
    <script src="/property-manager/assets/js/main.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="/frontend/js/jquery.validate.min.js"></script>
    
    @yield('after-scripts')       
    <script src="/js/tableexcel.min.js"></script>
</body>
</html>