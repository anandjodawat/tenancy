@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Apply Now Link</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><b>Apply Now Link</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Personalized Apply Now Link
                        </strong>
                    </div>
                    <div class="card-body">
                        @php
                            $base_url = URL::to('/');
                        @endphp
                        <strong>
                            <a href="{{ $base_url }}/my-agency/{{ auth()->user()->agency_profile() ? auth()->user()->agency_profile()->name : '' }}">{{ $base_url }}/my-agency/{{ auth()->user()->agency_profile() ? auth()->user()->agency_profile()->name : '' }}</a>
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

