<?php use \App\Http\Controllers\PropertyManager\ApplicationController; ?>
@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
           {{--  <div class="page-title">
                <h1>Applications</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><b>Applications</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Applications
                            <button class="btn btn-primary btn-sm" id="download-total-application-report" style="display: none;">Download</button>
                        </strong>
                        
                    </div>
                    <div class="card-body">
                       {{--  <div class="col-md-4 offset-md-8" style="margin-bottom: 10px;">
                         {{ Form::open(['route' => 'property-manager.total-applications.index', 'method' => 'get']) }}
                         <div class="input-group">
                            {{ Form::text('search', request('search'), ['class' => 'form-control', 'placeholder' => 'Search']) }}
                            <div class="input-group-btn">
                                {{ Form::submit('Search', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="total-tenant-application-table">
                            <thead>
                                <th>Address</th>
                                <th>Sent By</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th class="noExl no-sort">Joint Application</th>
                                <th style="display: none;">Joint Application</th>
                                <th class="noExl no-sort">Action</th>
                            </thead>
                            <tbody>
                                @foreach ($applications as $application)
                                @php
                                $app = App\Models\Tenant\TenantApplication::find($application->id);
                                @endphp
                                <tr>
                                    <td>
                                       {{ $app->applied_for }}, {{ $app->suburb }} {{ $app->state_name }} {{ $app->post_code }}
                                    </td>
                                    <td>{{ $application->user->name }}</td>
                                    <td>{{ $application->user->email }}</td>
                                    <td>{{ strtoupper($application->status_name) }}</td>
                                    <td>{{ $application->created_at->format('Y/m/d H:i') }}</td>
                                    <td class="noExl">
                                        @if($application->joint_app_id!=0)
                                            <a href="{{ route('property-manager.applications.show', $application->joint_app_id) }}" class="btn btn-sm btn-info"  title="View Primary Application" target="_blank">
                                            <i class="fa fa-eye"></i>
                                            </a>
                                        @endif
                                        
                                        @php
                                            $ja_app = ApplicationController::get_joint_appid($application->id);
                                        @endphp
                                        @if(!empty($ja_app[0]->id))
                                        <a href="{{ route('property-manager.applications.show', $ja_app[0]->id) }}" class="btn btn-sm btn-info"  title="View Joint Application" target="_blank">
                                        <i class="fa fa-eye"></i>
                                        </a>
                                        @endif
                                    </td>
                                    <td style="display: none;">@if($application->joint_app_id!=0)
                                            Yes
                                        @else 
                                            No   
                                        @endif
                                        </td>
                                    <td class="noExl">
                                        <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-sm btn-info"  title="Details">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('property-manager.tenancy-pdf-application.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show PDF" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </a>
                                        <a href="{{ route('property-manager.applications.delete', ['id' => $application->id]) }}" data-method="DELETE" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('after-scripts')
<script type="text/javascript">
    jQuery(document).ready(function () {
        /*jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "totalapplication" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
                // exclude_inputs: true
            });
            console.clear();
        });*/
    })
</script>

<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#total-tenant-application-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[4, "desc"]],
            "dom": 'Blfrtip',
            "buttons": [
                { 
                  extend: 'excel',
                  text: 'Download',
                  className: 'downloadButton',
                  title: 'Tenancy Applications',
                  filename: "applications-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  exportOptions: {
                    columns: [ 0, 1, 2,3,4,6 ]
                    }
                } 
            ]
        });
    })
</script>
@endsection

