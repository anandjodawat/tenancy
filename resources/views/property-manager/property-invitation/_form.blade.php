<div class="row form-group">
    {{ Form::label('tenant_emails', 'Tenant Emails', ['class' => 'col-md-4 form-control-label'] ) }}
    <div class="col-md-6">
        {{ Form::text('tenant_emails', null, ['class' => 'form-control', 'placeholder' => 'Seperate emails with comma']) }}
        @if ($errors->has('tenant_emails'))
        <span class="text-danger">{{ $errors->first('tenant_emails') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('property_id') ? 'has-error' : '' }}">
    {{ Form::label('property_id', 'Select a property address for', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        <select class="form-control" name="property_id">
            @foreach ($properties as $property)
            <option value="{{ $property->id }}">{{ $property->address }}, {{ $property->suburb }} {{ config('tenancy-application.states')[$property->state] }} {{ $property->post_code }}</option>
            @endforeach
            <option>others</option>
        </select>
        @if ($errors->has('property_id'))
        <span class="text-danger">{{ $errors->first('property_id') }}</span>
        @endif
    </div>
</div>

<div class="hide-when-address-for-is-others">
    <div class="row form-group {{ $errors->has('address_for_invite') ? 'has-error' : '' }}">
        {{ Form::label('address_for_invite', 'Property address for invite', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('address_for_invite', null, ['class' => 'form-control']) }}

            @if ($errors->has('address_for_invite'))
            <span class="text-danger">{{ $errors->first('address_for_invite') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('suburb') ? 'has-error' : '' }}">
        {{ Form::label('suburb', 'Suburb', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('suburb', null, ['class' => 'form-control']) }}
            @if ($errors->has('suburb'))
            <span class="text-danger">{{ $errors->first('suburb') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('state') ? 'has-error' : '' }}">
        {{ Form::label('state', 'State', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::select('state', config('tenancy-application.states'), null, ['class' => 'form-control', 'placeholder' => 'Select State']) }}
            @if ($errors->has('state'))
            <span class="text-danger">{{ $errors->first('state') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('post_code') ? 'has-error' : '' }}">
        {{ Form::label('post_code', 'Post Code', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('post_code', null, ['class' => 'form-control']) }}
            @if ($errors->has('post_code'))
            <span class="text-danger">{{ $errors->first('post_code') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('rent_amount') ? 'has-error' : '' }}">
        {{ Form::label('rent_amount', 'Rent Amount', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('rent_amount', null, ['class' => 'form-control']) }}
            @if ($errors->has('rent_amount'))
            <span class="text-danger">{{ $errors->first('rent_amount') }}</span>
            @endif
        </div>
    </div>
</div>
<!-- Testing -->
<div class="row form-group {{ $errors->has('inspection_date') ? 'has-error' : '' }}">
    {{ Form::label('inspection_date', 'Inspection Date', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('inspection_date', null, ['class' => 'form-control', 'id' => 'property-invitation-datepicker']) }}
        @if ($errors->has('inspection_date'))
        <span class="text-danger">{{ $errors->first('inspection_date') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forBody', 'Body', ['class' => 'col-md-4 control-label']) }}
    @if ($errors->has('body'))
    <span class="text-danger">{{ $errors->first('body') }}</span>
    @endif
    <div class="col-md-6">
        <textarea id="invite-tenant-body" class="form-control" name="body">
            @if (auth()->user()->agency_profile()->mail_templates && auth()->user()->agency_profile()->mail_templates->where('type', '3')->count() > 0)
                {{ auth()->user()->agency_profile()->mail_templates->where('type', '3')->first()->body }}
            @else
            @include('property-manager.ongoing-payment.default-template')
            @endif
        </textarea>
    </div>
</div>
