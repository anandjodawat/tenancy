@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>
               		Property Invitation
               	</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li><a href="{{ route('property-manager.property-invitation.index') }} "><b>Property Invitation</b></a></li>
                    <li class="active"><b>Create</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Invite Tenant To Apply</strong>
        {{-- <a href="{{ route('property-manager.property-invitation-tenants.create') }}" class="btn btn-primary btn-sm pull-right">Add Tenant To Property Invite</a> --}}
    </div>
    {{ Form::open(['route' => 'property-manager.property-invitation.store', 'method' => 'post', 'id' => 'apply-prop-form']) }}
    <div class="card-body card-block">
    	@include('property-manager.property-invitation._form')

        {{ Form::hidden('others', 'no') }}
    </div>
    <div class="card-footer">
    	<a href="{{ route('property-manager.property-invitation.index') }}" class="btn btn-danger">Cancel</a>
    	{{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
    </div>
    {{ Form::close() }}
</div>
</div>

@endsection

@section ('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('invite-tenant-body');

        jQuery(document).ready( function () {
            hide_show_address_property_invitation_detail();
            jQuery( "#property-invitation-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
            jQuery('select[name=property_id]').on('change', function () {
                hide_show_address_property_invitation_detail();
            });
            jQuery('#tenants').select2();
        });

        function hide_show_address_property_invitation_detail()
        {
            var lastOption = jQuery('select[name=property_id] option').last().val();
            var currOption = jQuery('select[name=property_id]').val();
            if (lastOption == currOption) {
                jQuery('.hide-when-address-for-is-others').show();
                jQuery('input[name=others]').val('yes');
            } else {
                jQuery('.hide-when-address-for-is-others').hide();
                jQuery('input[name=others]').val('no');
            }
        }

        // form validation
        jQuery('#apply-prop-form').validate({
            errorClass: 'error-message',
            rules : {
                // Property Details
                tenant_emails : 'required',
                property_id : 'required',
                inspection_date : 'required',
                //invite-tenant-body: 'required',
                address_for_invite: 'required',
                suburb: 'required',
                state : 'required',
                post_code: 'required',
                rent_amount: 'required',
            }
        });
    
    </script>

@endsection
