
@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>
                 Property Invitation
             </h1>
         </div> --}}
     </div>
 </div>
 <div class="col-sm-8">
    <div class="page-header float-right">
        <div class="page-title">
            <ol class="breadcrumb text-right">
                <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                <li class="active"><b>Property Invitation</b></li>
            </ol>
        </div>
    </div>
</div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Property Invitation
                <button class="btn btn-primary btn-sm" id="download-total-application-report" style="display: none;">Download</button>
            </strong>

            <a href="{{ route('property-manager.property-invitation.create') }}" class="btn btn-primary btn-sm pull-right">Invite</a>
        </div>
        <div class="card-body card-block">
            {{-- <div class="col-md-4 offset-md-8" style="margin-bottom: 10px;">
                {{ Form::open(['route' => 'property-manager.property-invitation.index', 'method' => 'get']) }}
                <div class="input-group">
                    {{ Form::text('search', request('search'), ['class' => 'form-control', 'placeholder' => 'Search']) }}
                    <div class="input-group-btn">
                        {{ Form::submit('Search', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div> --}}
            <div class="table-responsive">
             <table class="table table-bordered" id="property-invitation-applications-table">
                <thead>
                    <th>Tenant Email</th>
                    <th>Property Address</th>
                    <th>Suburb</th>
                    <th>Sent By</th>
                    <th>Date</th>
                    <th class="noExl no-sort">Action</th>
                </thead>
                <tbody>
                    @foreach ($invitations as $invitation)
                    <tr>
                        <td>{{ $invitation->tenant_email }}</td>
                        <td>{{ $invitation->property->address ?? '-' }}</td>
                        <td>{{ $invitation->property->suburb ?? '-' }}</td>
                        <td>{{ !is_null($invitation->sentBy)?$invitation->sentBy->first_name:'' }}</td>
                        <td>{{ $invitation->created_at->format('Y/m/d H:i') }}</td>
                        <td>
                            <a href="{{ route('property-manager.property-invitation.delete', ['id'=>$invitation->id]) }}" class="btn btn-sm btn-danger"  title="Delete Property Invitation" data-method="DELETE">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

      {{--   <div class="card-footer">
            {{ $invitations->appends(request()->input())->links() }}
        </div> --}}
    </div>
</div>

@endsection

@section('after-scripts')
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "property-invitation" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
                // exclude_inputs: true
            });
            console.clear();
        });
    })
</script>
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#property-invitation-applications-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[4, "desc"]],
            "dom": 'Blfrtip',
            "buttons": [
                { 
                  extend: 'excel',
                  text: 'Download',
                  className: 'downloadButton',
                  title: 'Tenancy Application - Property Invitation',
                  filename: "property-invitation-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  exportOptions: {
                    //columns: [ 0, 1, 2,3,4,6 ]
                    columns: 'th:not(:last-child)'
                    }
                } 
            ]
        });
    });
</script>
@endsection
