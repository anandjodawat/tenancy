<div class="row form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('first_name', null, ['class' => 'form-control']) }}

        @if ($errors->has('first_name'))
        <span class="text-danger">{{ $errors->first('first_name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
    {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('last_name', null, ['class' => 'form-control']) }}

        @if ($errors->has('last_name'))
        <span class="text-danger">{{ $errors->first('last_name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    {{ Form::label('email', 'Email', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::email('email', null, ['class' => 'form-control']) }}

        @if ($errors->has('email'))
        <span class="text-danger">{{ $errors->first('email') }}</span>
        @endif
    </div>
</div>
