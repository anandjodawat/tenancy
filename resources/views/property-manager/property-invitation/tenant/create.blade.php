@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
           {{--  <div class="page-title">
                <h1>
               		Property Invitation
               	</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} ">Dashboard</a></li>
                    <li><a href="{{ route('property-manager.property-invitation.index') }} ">Property Invitation</a></li>
                    <li class="active">Create</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Property Invitation</strong>
        <a href="{{ route('property-manager.property-invitation.create') }}" class="btn btn-primary pull-right">Back</a>
    </div>
    {{ Form::open(['route' => 'property-manager.property-invitation-tenants.store', 'method' => 'post']) }}
    <div class="card-body card-block">
    	@include('property-manager.property-invitation.tenant._form')

        {{ Form::hidden('others', 'no') }}
    </div>
    <div class="card-footer">
    	<a href="{{ route('property-manager.property-invitation.index') }}" class="btn btn-danger">Cancel</a>
    	{{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
    </div>
    {{ Form::close() }}
</div>
</div>

@endsection
