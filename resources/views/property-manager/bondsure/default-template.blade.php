<p>Dear Applicant,  &nbsp;</p>

<p>
	Your Tenancy Application has been Approved for address: &nbsp;.<br />
	&nbsp;
</p>
<p>Please Click PAY NOW button below to pay your bond deposit.</p>

<p>If you need bond assistance, then we can help you with EasyBondPay</p>

[[EasyBondPayLogo]]

<p>Please click on the EasyBondPay button below to assist you with Bond Payment. </p>

[[EasyBondPayButton]]

<p><br />
	{{ auth()->user()->name }}<br />
	{{ auth()->user()->email }}<br />
	@if (auth()->user()->agency_profile())
	<a href="{{ auth()->user()->agency_profile()->website }}">{{ auth()->user()->agency_profile()->website }}</a>
	@endif
</p>