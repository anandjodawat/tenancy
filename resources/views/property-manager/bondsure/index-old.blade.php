@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>BondSure Application Requests</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                     <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li class="active"><b>Bond Application Requests</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@include('flash::message')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Bond Application Requests
                             <button class="btn btn-primary btn-sm" id="download-total-application-report" style="display: none;">Download</button>
                        </strong>
                        
  
                    </div>
                    <div class="card-body">
                        {{-- <div class="row">
                        <div class="col-md-3">
                                   <div class="dropdown show">
                                    Filter by: <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       {{ request('status') ? ucwords(request('status')) : 'Inbox' }}
                                      </a>
                                       <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="{{ route('property-manager.applications.index', ['status' => 'inbox']) }}">Inbox</a>
                                     
                                        @foreach (App\Models\Tenant\TenantApplication::APPLICATIONSTATUS as $status)
                                              <a class="dropdown-item" href="{{ route('property-manager.applications.index', ['status' => $status]) }}">{{ ucwords($status) }}</a>
                                         @endforeach
                                      </div>
                                    </div>              
                        </div>
                        <div class="col-md-4 offset-md-5" style="margin-bottom: 10px;">
                         {{ Form::open(['route' => 'property-manager.applications.index', 'method' => 'get']) }}
                         <div class="input-group">
                            {{ Form::text('search', request('search'), ['class' => 'form-control', 'placeholder' => 'Search']) }}
                            <div class="input-group-btn">
                                {{ Form::submit('Search', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div> --}}
                <div class="table-responsive">
                    <table class="table table-bordered" id="bondsure-tenant-applications-table">
                        <thead>
                            <th>State</th>
                            <th>Tenant Name</th>
                            <th>Tenant Email</th>
                            <th>Status</th>
                            @if (request('status') == 'forwarded')
                                <th>Forwarded To</th>
                            @endif
                            <th>Date</th>
                            <th class="noExl no-sort">Action</th>
                        </thead>
                        <tbody>
                            @foreach ($applications as $application)
                            <tr>
                                <td>{{ $application->state_name }}</td>
                                <td>{{ $application->user->name }}</td>
                                <td>{{ $application->user->email }}</td>
                                <td>{{ ucwords($application->status_name) }}</td>
                                @if (request('status') == 'forwarded')
                                    <td>
                                        {{ optional($application->forwardedTo->sentBy)->name }}
                                    </td>
                                @endif
                                <td class="noExl">{{ $application->created_at->format('Y/m/d H:i') }}</td>
                                <td class="noExl">
                                    <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-sm btn-info"  title="Details">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route('property-manager.tenancy-pdf-application.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show PDF" target="_blank">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </a>
                                    <a href="{{ route('property-manager.applications.destroy', $application->id) }}" data-method="DELETE" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
              {{--   <div class="card-footer">
                    {{ $applications->appends(request()->input())->links() }}
                </div> --}}
            </div>
        </div>
    </div>
</div>
</div>
@endsection
  
@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        /*jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "BondSure" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
            });
            console.clear();
        });*/
        jQuery('#bondsure-tenant-applications-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[4, "desc"]],
            "dom": 'Blfrtip',
            "buttons": [
                { 
                  extend: 'excel',
                  text: 'Download',
                  className: 'downloadButton',
                  title: 'Tenancy Application - Bond Application Requests',
                  filename: "Bond-Application-Requests-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  exportOptions: {
                    //columns: [ 0, 1, 2,3,4,6 ]
                    columns: 'th:not(:last-child)'
                    }
                } 
            ]
        });
    });
</script>

@endsection
