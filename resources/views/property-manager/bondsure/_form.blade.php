<div class="row form-group {{ $errors->has('tenant_code') ? 'has-error' : '' }}">
    {{ Form::label('forTenantCode', 'Tenant Code', ['class' => 'col-md-4 form-control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('tenant_code', null, ['class' => 'form-control']) }}
        @if ($errors->has('tenant_code'))
            <span class="text-danger">{{ $errors->first('tenant_code') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('title', 'Title', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::select('title', config('tenancy-application.title'), null, ['class' => 'form-control', 'placeholder' => 'Select Title']) }}
        @if ($errors->has('title'))
        <span class="text-danger">{{ $errors->first('title') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('first_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('first_name'))
        <span class="text-danger">{{ $errors->first('first_name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('last_name'))
        <span class="text-danger">{{ $errors->first('last_name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forDateOfBirth', 'Date Of Birth', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('dob', null, ['class' => 'form-control', 'id' => 'ongoing-payment-datepicker']) }}
        @if ($errors->has('dob'))
        <span class="text-danger">{{ $errors->first('dob') }}</span>
        @endif

    </div>
</div>

<div class="row form-group">
    {{ Form::label('forAddress', 'Address', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('address', null, ['class' => 'form-control']) }}
        @if ($errors->has('address'))
        <span class="text-danger">{{ $errors->first('address') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forSururb', 'Suburb', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('suburb', null, ['class' => 'form-control']) }}
        @if ($errors->has('suburb'))
        <span class="text-danger">{{ $errors->first('suburb') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forState', 'State', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::select('state', config('tenancy-application.states'), null, ['class' => 'form-control']) }}

    </div>
</div>

<div class="row form-group">
    {{ Form::label('forPostCode', 'Postcode', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('postcode', null, ['class' => 'form-control']) }}
        @if ($errors->has('postcode'))
        <span class="text-danger">{{ $errors->first('postcode') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('payment_amount') ? 'has-error' : '' }}">
    {{ Form::label('forPaymentAmount', 'Payment Amount ($)', ['class' => 'col-md-4 form-control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('payment_amount', null, ['class' => 'form-control']) }}
        @if ($errors->has('payment_amount'))
            <span class="text-danger">{{ $errors->first('payment_amount') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('finance_period') ? 'has-error' : '' }}">
    {{ Form::label('forFinancePeriod', 'Finance Period', ['class' => 'col-md-4 form-control-label requiredcls']) }}
    <div class="col-md-6">
        @php $finaceperiod = array('6'=>'6','3'=>'3'); @endphp
        {{ Form::select('finance_period', $finaceperiod, null, ['class' => 'form-control'] ) }}
        @if ($errors->has('finance_period'))
            <span class="text-danger">{{ $errors->first('finance_period') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forMobileNumber', 'Mobile Number', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('mobile_number', null, ['class' => 'form-control']) }}
        @if ($errors->has('mobile_number'))
        <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forPhoneNumber', 'Phone Number', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('phone_number', null, ['class' => 'form-control']) }}
        @if ($errors->has('phone_number'))
        <span class="text-danger">{{ $errors->first('phone_number') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forEmail', 'Email', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::email('email', null, ['class' => 'form-control']) }}
        @if ($errors->has('email'))
        <span class="text-danger">{{ $errors->first('email') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forSubject', 'Subject', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('subject', null, ['class' => 'form-control']) }}
        @if ($errors->has('subject'))
        <span class="text-danger">{{ $errors->first('subject') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('attachment', 'Attachment', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::file('attachment', ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forBody', 'Body', ['class' => 'col-md-4 control-label requiredcls']) }}
    @if ($errors->has('body'))
    <span class="text-danger">{{ $errors->first('body') }}</span>
    @endif
    <div class="col-md-6">
        <textarea id="bond-requests-body" class="form-control" name="body">
            @if (auth()->user()->agency_profile()->mail_templates && auth()->user()->agency_profile()->mail_templates->where('type', '2')->count() > 0)
                {{ auth()->user()->agency_profile()->mail_templates->where('type', '2')->first()->body }}
            @else
            @include('property-manager.bondsure.default-template') 
            @endif      
        </textarea>
    </div>
</div>
