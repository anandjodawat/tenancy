@extends('property-manager.layouts.app')

@section('page-header')
    <h1 class="page-title">
        {{ app_name() }}
        <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-body box-profile">

            <h3 class="profile-username text-center">Nina Mcintire</h3>

            <p class="text-muted text-center">Software Engineer</p>

            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                    <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                    <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
            </ul>
        </div>
        <!-- /.box-body -->
    </div>
@endsection