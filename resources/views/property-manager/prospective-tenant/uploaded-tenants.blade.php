<div class="table-responsive">
    <table class="table" id="tenant-applications-table">
        <thead>
            <th>Name</th>
            <th>Email</th>
            <th>Created</th>
        </thead>
        <tbody>
            @foreach ($tenants as $tenant)
            <tr>
                <td>{{ $tenant->name }}</td>
                <td>{{ $tenant->email }}</td>
                <td>{{ $tenant->created_at->format('Y/m/d H:i') }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>