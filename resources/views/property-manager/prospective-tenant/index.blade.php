@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Prospective Tenants</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                   <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                   <li class="active"><b>Prospective Tenants</b></li>
               </ol>
           </div>
       </div>
   </div>
</div>
@include('flash::message')
@include('property-manager.prospective-tenant.includes.header')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Prospective Tenants
                        </strong>
                    </div>
                    <div class="card-body">
                        {{-- <div class="col-md-4 offset-md-8" style="margin-bottom: 10px;">
                         {{ Form::open(['route' => 'property-manager.applications.index', 'method' => 'get']) }}
                         <div class="input-group">
                            {{ Form::text('search', request('search'), ['class' => 'form-control', 'placeholder' => 'Search']) }}
                            <div class="input-group-btn">
                                {{ Form::submit('Search', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div> --}}
                    @if (!request('tenants') || request('tenants') == 'system-tenants')
                    @include('property-manager.prospective-tenant.system-tenants', ['tenants' => $tenants])
                    @endif

                    @if (request('tenants') == 'uploaded-tenants')
                    @include('property-manager.prospective-tenant.uploaded-tenants', ['tenants' => $tenants])
                    @endif

                    @if (request('tenants') == 'invited-tenants')
                    @include('property-manager.prospective-tenant.invited-tenants', ['tenants' => $tenants])
                    @endif
                </div>
                {{-- <div class="card-footer">
                    {{ $tenants->appends(request()->input())->links() }}
                </div> --}}
            </div>
        </div>
    </div>

</div>
</div>
@endsection

@section ('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#tenant-applications-table').DataTable(

            {"order": [[2, "desc"]]}

            );
        
        jQuery('#tenant-applications-table2').DataTable(

            {"order": [[1, "desc"]]}

            );
        });
</script>
@endsection