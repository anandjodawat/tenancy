<div class="table-responsive">
      <table class="table" id="tenant-applications-table2">
    <thead>
        <th>Email</th>
        <th>Last Invitation Date</th>
    </thead>
    <tbody>
        @foreach ($tenants as $tenant)
        <tr>
            <td>{{ $tenant->tenant_email }}</td>
            {{-- <td>{{ App\Models\PropertyInvitation::whereTenantEmail($tenant->tenant_email)->latest()->first()->standard_date }}</td> --}}
            <td>{{ App\Models\PropertyInvitation::whereTenantEmail($tenant->tenant_email)->latest()->first()->created_at->format('Y/m/d H:i') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>