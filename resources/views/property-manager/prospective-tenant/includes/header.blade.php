<div class="col-md-12">
    <div class="card">
        <div class="card-body">
                <a href="{{ route('property-manager.prospective-tenants.index', ['tenants' => 'system-tenants']) }}" class="btn btn-primary col-md-2">
                    {{-- <i class="fa fa-home"></i> --}}
                    {{-- <br> --}}
                    Approved Tenants
                </a>
                <a href="{{ route('property-manager.prospective-tenants.index', ['tenants' => 'uploaded-tenants']) }}" class="btn btn-danger col-md-2">
                    {{-- <i class="fa fa-envelope-o"></i> --}}
                    {{-- <br> --}}
                    Uploaded Tenants
                </a>

                <a href="{{ route('property-manager.prospective-tenants.index', ['tenants' => 'invited-tenants']) }}" class="btn btn-info col-md-2">
                    Invited Tenants
                </a>
        </div>
    </div>
</div>