<div class="table-responsive">
    <table class="table" id="tenant-applications-table">
        <thead>
            <th>Name</th>
            <th>Email</th>
            <th>Created</th>
        </thead>
        <tbody>
            @foreach ($tenants as $tenant)
            @if ( !empty($tenant->user) )
            <tr>
                <td>{{ $tenant->user->name }}</td>
                <td>{{ $tenant->user->email }}</td>
                <td>{{ $tenant->user->created_at->format('Y/m/d H:i') }}</td>
            </tr>
            @endif
            @endforeach
        </tbody>
    </table>
</div>