@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Applications</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li>Tenants</li>
                    <li class="active">Applications</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-12 col-lg-12">
    <div class="card">
        <div class="card-body">
            This tenant applied for <b>{{ $applications->count() }}</b> property.
        </div>
    </div>
</div>

@foreach ($applications as $application)
<div class="col-xl-3 col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <b>Property Address</b>
                <p>{{ $application->applied_for }}</p>
                <br>
                sent on : {{ $application->created_at }}
                <hr>
                <a href="{{ route('property-manager.tenancy-pdf-application.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show PDF" target="_blank">
                    <i class="fa fa-file-pdf-o"></i>
                </a>
                <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-primary btn-sm" target="_blank">
                    <i class="fa fa-search"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection