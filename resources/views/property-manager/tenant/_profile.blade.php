{{-- tabs --}}
<div class="col-lg-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h4>
                Profile Details
            </h4>
        </div>
        <div class="card-body">
            <div class="default-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show" data-toggle="tab" href="#nav-personal-information"  aria-selected="true">
                            Personal Information
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-emergency-contact"  aria-selected="false">
                            Emergency Contact
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-current-address"  aria-selected="false">
                            Current Address
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-previous-address"  aria-selected="false">
                            Previous Address
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-current_employment"  aria-selected="false">
                            Current Employment
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-previous_employment"  aria-selected="false">
                            Previous Employment
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-personal_reference"  aria-selected="false">
                            Presonal Reference
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-professional_reference"  aria-selected="false">
                            Professional Reference
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-income_expenditure"  aria-selected="false">
                            Income and Expenditure
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-repayments"  aria-selected="false">
                            Repayments
                        </a>
                        <!--<a class="nav-item nav-link" data-toggle="tab" href="#nav-employment-confirmation" aria-selected="false">
                            Employment Confirmation
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-history-confirmation" aria-selected="false">
                            History Confirmation
                        </a>-->
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    <?php /*<div class="tab-pane fade" id="nav-employment-confirmation" role="tabpanel">
                      @if ($employmentConfirmation)
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Date: </strong><span class="pull-right">{{ $employmentConfirmation->date }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Company Name: </strong><span class="pull-right">{{ $employmentConfirmation->company_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Employee Name: </strong><span class="pull-right">{{ $employmentConfirmation->employee_position }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Work Duration: </strong><span class="pull-right">{{ $employmentConfirmation->work_duration_from }} to {{ $employmentConfirmation->work_duration_to }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Completed By: </strong><span class="pull-right">{{ $employmentConfirmation->completed_by }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Submission Date: </strong><span class="pull-right">{{ $employmentConfirmation->submission_date }}</span>
                                </li>
                            </ul>
                            @else
                            No data available.
                            @endif
                    </div>*/?>

                    <?php /*<div class="tab-pane fade" id="nav-history-confirmation" role="tabpanel">
                  
                      @if ($historyConfirmation)
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Date: </strong><span class="pull-right">{{ $historyConfirmation->date }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Agency Name: </strong><span class="pull-right">{{ $historyConfirmation->agency }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Tenant Name: </strong><span class="pull-right">{{ $historyConfirmation->tenant_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Property Address: </strong><span class="pull-right">{{ $historyConfirmation->property_address }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Was the applicant(s) listed as a lessee?: </strong><span class="pull-right">{{ ucfirst($historyConfirmation->listed_as_lease) }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Was the applicant(s) listed as a lessee? </strong> <span class="pull-right">{{ ucfirst($historyConfirmation->listed_as_lease) }}</span>
                                </li>
                                <li class="list-group-item">

                                    <strong>What dates were they in the property? </strong> <span class="pull-right">From {{ $historyConfirmation->property_duration_from }} to {{ $historyConfirmation->property_duration_to }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Did your office Terminate the Tenancy? </strong><span class="pull-right">{{ ucfirst($historyConfirmation->terminate_tenancy) }}</span>
                                </li>
                                    @if ($historyConfirmation->terminate_tenancy == 'yes')
                                <li class="list-group-item">
                                    <strong>Reason: </strong> <span class="pull-right">{{ $historyConfirmation->reason_for_termination }}</span>
                                </li>
                                    @endif
                                <li class="list-group-item">
                                <strong>During the tenancy, were the tenant(s) ever in arrears?</strong> <span class="pull-right">{{ config('tenancy-application.arrears')[$historyConfirmation->arrears] }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Did the tenant recieve any Breach Notices other than arrears? </strong><span class="pull-right"> {{ ucfirst($historyConfirmation->breach_notices) }}</span>
                                </li>
                                @if ($historyConfirmation->breach_notices == 'yes')
                                <li class="list-group-item">
                                    <strong>Reason: </strong> <span class="pull-right">{{ $historyConfirmation->reason_for_breach_notices }}</span>
                                </li>
                                @endif
                                <li class="list-group-item">
                                    <strong>Was there any damage noted during inspections? </strong><span class="pull-right"> {{ ucfirst($historyConfirmation->damage) }}</span>
                                </li>
                                @if ($historyConfirmation->damage == 'yes')
                                <li class="list-group-item">
                                <strong>What was the damage? </strong><span class="pull-right">{{ $historyConfirmation->damage_reason }}</span>
                                </li>
                                @endif
                                <li class="list-group-item">
                                    <strong>During the tenancy, what were the inspections like? </strong><span class="pull-right">{{ config('tenancy-application.inspection_like')[$historyConfirmation->inspection_like] }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Were pets kept at the Property? </strong><span class="pull-right"> {{ ucfirst($historyConfirmation->pets) }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Was there any complaints or damage? </strong><span class="pull-right">{{ ucfirst($historyConfirmation->complaints_damage) }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>What date did tenants vacate the property? </strong><span class="pull-right">{{ $historyConfirmation->vacate_date }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Was the Bond refunded in full? </strong><span class="pull-right">{{ ucfirst($historyConfirmation->bond_refund_full) }}</span>
                                </li>
                                @if ($historyConfirmation->bond_refund_full == 'yes')
                                <li class="list-group-item">
                                <strong>Reason: </strong><span class="pull-right">{{ $historyConfirmation->bond_refund_reason }}</span>
                                </li>
                                @endif
                                <li class="list-group-item">
                                <strong>On a scale of 1-10 how would you rate this tenant(s)? </strong><span class="pull-right">{{ $historyConfirmation->rate_tenant }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>On a scale of 1-10 how would you rate this tenant(s)? </strong><span class="pull-right">{{ $historyConfirmation->rent_again }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Additional Comments: </strong><span class="pull-right">{{ $historyConfirmation->additional_comments }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Completed By: </strong><span class="pull-right">{{ $historyConfirmation->completed_by }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Submission Date: </strong><span class="pull-right">{{ $historyConfirmation->submission_date }}</span>
                                </li>
                            </ul>
                            @else
                                No data available
                            @endif
                  
                    </div>*/ ?>
                    <div class="tab-pane fade active show" id="nav-personal-information" role="tabpanel">
                    <ul class="list-group">
                        <li class="list-group-item">
                            Name : <b>{{ config('tenancy-application.titles')[$profile->title] }}. {{ $profile->first_name }} {{ $profile->last_name }}</b>
                        </li>
                        <li class="list-group-item">
                            DOB : <b>{{ $profile->dob }}</b>
                        </li>
                        
                        <li class="list-group-item">
                            Gender : <b>{{ $profile->gender }}</b>
                        </li>
                        <li class="list-group-item">
                            Marital Status : 
                                @if ($profile->marital_status!="")
                                <b>{{ config('tenancy-application.marital')[$profile->marital_status] }}</b>
                                @endif  
                        </li>
                        <li class="list-group-item">
                            Home Phone : <b>{{ $profile->home_phone }}</b>
                        </li>
                        <li class="list-group-item">
                            Work Phone : <b>{{ $profile->work_phone }}</b>
                        </li>
                        <li class="list-group-item">
                            Mobile : <b>{{ $profile->mobile }}</b>
                        </li>
                    {{--     <li class="list-group-item">
                            Fax : <b>{{ $profile->fax }}</b>
                        </li> --}}
                        <li class="list-group-item">
                            Email Address : <b>{{ $profile->email }}</b>
                        </li>
                        <li class="list-group-item">
                            Preffered Time To Call : <b>{{ $profile->dob }}</b>
                        </li>
                        <li class="list-group-item">
                            Do You Smoke : <b>{{ $profile->smoke }}</b> 
                        </li>
                        @if ($profile->smoke=="yes")
                                
                                    <li class="list-group-item">
                          Smoke Inside <b>
                           {{  $profile->smoke_inside }}
                       </b>
                        </li>
                       
                            @endif  
                        <li class="list-group-item">
                            Are You Studying : <b>{{ $profile->studying }}</b>
                        </li>
                        @if ($profile->studying=='yes')
                             <li class="list-group-item">
                            Institute Name : <b>{{ $profile->institution }}</b>
                        </li>
                         <li class="list-group-item">
                            Enrollment number : <b>{{ $profile->enrollment_number }}</b>
                        </li>
                         <li class="list-group-item">
                            Course Contact Number : <b>{{ $profile->course_contact_number }}</b>
                        </li>
                         <li class="list-group-item">
                            Institution Phone : <b>{{ $profile->institution_phone }}</b>
                        </li>
                         <li class="list-group-item">
                            Source Of Income : <b>{{ $profile->source_of_income }}</b>
                        </li>
                         <li class="list-group-item">
                            Net Weekly Amount : <b>{{ $profile->net_weekly_amount }}</b>
                        </li>
                         <li class="list-group-item">
                            Guardian Name : <b>{{ $profile->guardian_name }}</b>
                        </li>
                        <?php //echo '<pre>'; print_r($profile);exit; ?>
                         <li class="list-group-item">
                            Guardian Relationship : <?php /*<b> {{   $profile->tenantRelationName($profile->guardian_relationship) }} </b>*/?> 
                            @if ($profile->guardian_relationship!="")
                            <b> {{ config('tenancy-application.relation')[$profile->guardian_relationship] }}
                                </b>
                            @endif
                        </li>
                         <li class="list-group-item">
                            Guardian Address : <b>{{ $profile->guardian_address }}</b>
                        </li>
                         <li class="list-group-item">
                            Guardian Phone Number : <b>{{ $profile->guardian_phone_number }}</b>
                        </li>
                         <li class="list-group-item">
                            Do you get pension : <b>{{ $profile->pension }}</b>
                        </li>
                            @if ($profile->pension!='' && $profile->pension=='yes')
                                     <li class="list-group-item">
                                        Pension Type : <b>{{ $profile->pention_type }}</b>
                                    </li>
                                     <li class="list-group-item">
                                        Pension Number : <b>{{ $profile->pension_number }}</b>
                                    </li>
                            @endif
                        @endif
                    </ul>
                    </div>


                    <div class="tab-pane fade" id="nav-emergency-contact" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Name : <b>{{ $profile->emergency_contact_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Relation : 
                             @if ($profile->emergency_contact_relation!="")
                            <b> {{ config('tenancy-application.relation')[$profile->emergency_contact_relation] }}
                                </b>
                            @endif
                        </li>
                        
                        <li class="list-group-item">
                            Contact Number : <b>{{ $profile->emergency_contact_number }}</b>
                        </li>
                        <li class="list-group-item">
                            State :
                            @if ($profile->emergency_contact_state!="") 
                            <b> {{ config('tenancy-application.states')[$profile->emergency_contact_state] }}
                            </b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Suburb : <b>{{ $profile->emergency_contact_suburb }}</b>
                        </li>
                        <li class="list-group-item">
                            Street : <b>{{ $profile->emergency_contact_street }}</b>
                        </li>
                        <li class="list-group-item">
                            Home Phone : <b>{{ $profile->emergency_contact_home_phone }}</b>
                        </li>
                        <li class="list-group-item">
                            Work Phone : <b>{{ $profile->emergency_contact_work_phone }}</b>
                        </li>
                      
                        <li class="list-group-item">
                            Email Address : <b>{{ $profile->emergency_contact_email }}</b>
                        </li>
                    </ul>
                  
                    </div>

                     <div class="tab-pane fade" id="nav-current-address" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Living Arrangement : 
                            @if ($profile->current_living_arrangement!="")
                            <b>{{ config('tenancy-application.arrangement')[$profile->current_living_arrangement] }}
                               </b>
                            @endif
                        </li>
                         <li class="list-group-item">
                            Address Unit : <b>{{ $profile->current_address_unit }}</b>
                        </li>
                         <li class="list-group-item">
                            Address Street : <b>{{ $profile->current_address_street }}</b>
                        </li>
                         <li class="list-group-item">
                            Address Street Name : <b>{{ $profile->current_address_street_name }}</b>
                        </li>
                         <li class="list-group-item">
                            Address State : <b>{{ config('tenancy-application.states')[$profile->current_address_state] }}</b>
                        </li>
                         <li class="list-group-item">
                            Address Suburb : <b>{{ $profile->current_address_suburb }}</b>
                        </li>
                         <li class="list-group-item">
                            Post Code : <b>{{ $profile->current_address_post_code }}</b>
                        </li>
                         <li class="list-group-item">
                            Move In : <b>{{ $profile->current_address_move_in }}</b>
                        </li>
                    </ul>
                  
                    </div>

                    <div class="tab-pane fade" id="nav-previous-address" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Living Arrangement : 
                            @if ($profile->previous_living_arrangement!="")
                            <b>{{ config('tenancy-application.arrangement')[$profile->previous_living_arrangement] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Address Unit : <b>{{ $profile->previous_address_unit }}</b>
                        </li>
                        <li class="list-group-item">
                            Address Street : <b>{{ $profile->previous_address_street }}</b>
                        </li>
                        <li class="list-group-item">
                            Street Name : <b>{{ $profile->previous_address_street_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Street Type : <b>{{ $profile->previous_address_street_type }}</b>
                        </li>
                        <li class="list-group-item">
                            Address State : <b>{{ config('tenancy-application.states')[$profile->previous_address_state] }}</b>
                        </li>
                        <li class="list-group-item">
                            Address Suburb : <b>{{ $profile->previous_address_suburb }}</b>
                        </li>
                        <li class="list-group-item">
                            Address Postcode : <b>{{ $profile->previous_address_postcode }}</b>
                        </li>
                        <li class="list-group-item">
                            Address From : <b>{{ $profile->previous_address_from }}</b>
                        </li>
                        <li class="list-group-item">
                            Address To : <b>{{ $profile->previous_address_to }}</b>
                        </li>
                        <li class="list-group-item">
                            Leave Reason : <b>{{ $profile->previous_address_leave_reason }}</b>
                        </li>
                        <li class="list-group-item">
                            Agent Name : <b>{{ $profile->previous_address_agent_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Address Rent : <b>{{ $profile->previous_address_rent }}</b>
                        </li>
                        <li class="list-group-item">
                            Address Email : <b>{{ $profile->previous_address_email }}</b>
                        </li>
                        <li class="list-group-item">
                            Bond Refund : <b>{{ $profile->previous_address_bond_refund }}</b>
                        </li>
                        <li class="list-group-item">
                            Additional Information : <b>{{ $profile->previous_address_additional_information }}</b>
                        </li>
                        <li class="list-group-item">
                            Information Detail : <b>{{ $profile->previous_address_additional_information_detail }}</b>
                        </li>
                    </ul>
                  
                    </div>


                    <div class="tab-pane fade" id="nav-current_employment" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Work Situation : 
                            @if ($profile->current_employment_work_situation!="")
                            <b>{{ config('tenancy-application.employment')[$profile->current_employment_work_situation] }}</b>
                            @endif
                        </li>
                         <li class="list-group-item">
                            Company Name : <b>{{ $profile->current_company_name }}</b>
                        </li>
                         <li class="list-group-item">
                            Manager Name : <b>{{ $profile->current_manager_name }}</b>
                        </li>
                         <li class="list-group-item">
                            Manager Phone : <b>{{ $profile->current_manager_phone }}</b>
                        </li>
                         <li class="list-group-item">
                            Manager Email : <b>{{ $profile->current_manager_email }}</b>
                        </li>
                         <li class="list-group-item">
                            Position : <b>{{ $profile->current_work_position }}</b>
                        </li>
                         <li class="list-group-item">
                            State : 
                            @if ($profile->current_company_state!="")
                            <b>{{ config('tenancy-application.states')[$profile->current_company_state] }}</b>
                            @endif
                        </li>
                         <li class="list-group-item">
                            Street : <b>{{ $profile->current_company_street }}</b>
                        </li>
                         <li class="list-group-item">
                            Suburb : <b>{{ $profile->current_company_suburb }}</b>
                        </li>
                         <li class="list-group-item">
                            Industry type : <b>{{ $profile->current_industry_type }}</b>
                        </li>
                         <li class="list-group-item">
                            Nature : <b>{{ $profile->current_employment_nature }}</b>
                        </li>
                         <li class="list-group-item">
                            Start Date : <b>{{ $profile->current_employment_start_date }}</b>
                        </li>
                         <li class="list-group-item">
                            Job Status : <b>{{ $profile->current_job_status }}</b>
                        </li>
                         <li class="list-group-item">
                            Gross Annual Salary : <b>{{ $profile->current_gross_annual_salary }}</b>
                        </li>
                    </ul>
                  
                    </div>

                    <div class="tab-pane fade" id="nav-previous_employment" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Work Situation : 
                            @if ($profile->previous_employment_work_situtation!="")
                            <b>{{ config('tenancy-application.employment')[$profile->previous_employment_work_situtation] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Company Name : <b>{{ $profile->previous_company_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Manager Name : <b>{{ $profile->previous_manager_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Manager Phone : <b>{{ $profile->previous_manager_phone }}</b>
                        </li>
                        <li class="list-group-item">
                            Manager Email : <b>{{ $profile->previous_manager_email }}</b>
                        </li>
                        <li class="list-group-item">
                            Work Position : <b>{{ $profile->previous_work_position }}</b>
                        </li>
                        <li class="list-group-item">
                            Company State : 
                            @if ($profile->previous_company_state!="")
                            <b>{{ config('tenancy-application.states')[$profile->previous_company_state] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Company Street : <b>{{ $profile->previous_company_street }}</b>
                        </li>
                        <li class="list-group-item">
                            Company Suburb : <b>{{ $profile->previous_company_suburb }}</b>
                        </li>
                        <li class="list-group-item">
                            Industry Type : <b>{{ $profile->previous_industry_type }}</b>
                        </li>
                        <li class="list-group-item">
                            Employment Nature : <b>{{ $profile->previous_employment_nature }}</b>
                        </li>
                        <li class="list-group-item">
                            Start Date : <b>{{ $profile->previous_employment_start_date }}</b>
                        </li>
                        <li class="list-group-item">
                            End Date : <b>{{ $profile->previous_employment_end_date }}</b>
                        </li>
                        <li class="list-group-item">
                            Gross Annual Salary : <b>{{ $profile->previous_gross_annual_salary }}</b>
                        </li>
                    </ul>
                  
                    </div>

                     <div class="tab-pane fade" id="nav-personal_reference" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Reference Name : <b>{{ $profile->personal_ref_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Occupation : <b>{{ $profile->personal_ref_occupation }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Relationship : 
                            @if ($profile->personal_ref_relationship!="")
                            <b>{{ config('tenancy-application.relation')[$profile->personal_ref_relationship] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Reference Phone : <b>{{ $profile->personal_ref_phone }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Mobile : <b>{{ $profile->personal_ref_mobile }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Email : <b>{{ $profile->personal_ref_email }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference State : 
                            @if ($profile->personal_ref_state!="")
                            <b>{{ config('tenancy-application.states')[$profile->personal_ref_state] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Reference Suburb : <b>{{ $profile->personal_ref_suburb }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Street : <b>{{ $profile->personal_ref_street }}</b>
                        </li>
                    </ul>
                  
                    </div>

                    <div class="tab-pane fade" id="nav-professional_reference" role="tabpanel">
                  
                    <ul class="list-group">
                       <li class="list-group-item">
                            Reference Name : <b>{{ $profile->professional_ref_name }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Company : <b>{{ $profile->professional_ref_company }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Occupation : <b>{{ $profile->professional_ref_occupation }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Relationship :  
                            @if ($profile->professional_ref_relationship!="")
                            <b>{{ config('tenancy-application.relation')[$profile->professional_ref_relationship] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Reference Phone : <b>{{ $profile->professional_ref_phone }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Mobile : <b>{{ $profile->professional_ref_mobile }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Email : <b>{{ $profile->professional_ref_email }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference State : 
                            @if ($profile->professional_ref_company_state!="")
                            <b>{{ config('tenancy-application.states')[$profile->professional_ref_company_state] }}</b>
                            @endif
                        </li>
                        <li class="list-group-item">
                            Reference Suburb : <b>{{ $profile->professional_ref_company_suburb }}</b>
                        </li>
                        <li class="list-group-item">
                            Reference Street : <b>{{ $profile->professional_ref_company_street }}</b>
                        </li>
                        
                        
                        
                    </ul>
                  
                    </div>



                    <div class="tab-pane fade" id="nav-income_expenditure" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Current Weekly Rent : <b>{{ $profile->current_weekly_rent }}</b>
                        </li>
                        <li class="list-group-item">
                            Vehicle Value : <b>{{ $profile->vehicle_value }}</b>
                        </li>
                        <li class="list-group-item">
                            Vehicle Type : <b>{{ $profile->vehicle_type }}</b>
                        </li>
                        <li class="list-group-item">
                            Income Type : <b>{{ $profile->income_type }}</b>
                        </li>
                        <li class="list-group-item">
                            Salary/Wages Income : <b>{{ $profile->salary_wages_income }}</b>
                        </li>
                        <li class="list-group-item">
                            Self Employed Income : <b>{{ $profile->self_employed_income }}</b>
                        </li>
                        <li class="list-group-item">
                            Benefit Income : <b>{{ $profile->benefit_income }}</b>
                        </li>
                        <li class="list-group-item">
                            Other Income : <b>{{ $profile->other_income }}</b>
                        </li>
                        
                        

                              
                        
                    </ul>
                  
                    </div>

                    <div class="tab-pane fade" id="nav-repayments" role="tabpanel">
                  
                    <ul class="list-group">
                        <li class="list-group-item">
                            Loans : <b>{{ $profile->repayment_loans }}</b>
                        </li>
                        <li class="list-group-item">
                            Credit Cards : <b>{{ $profile->repayment_credit_cards }}</b>
                        </li>
                        <li class="list-group-item">
                            Hire Purchase : <b>{{ $profile->repayment_hire_purchase }}</b>
                        </li>
                        <li class="list-group-item">
                            Store Card : <b>{{ $profile->repayment_store_card }}</b>
                        </li>
                        <li class="list-group-item">
                            Others : <b>{{ $profile->repayment_others }}</b>
                        </li>
                        <li class="list-group-item">
                            Expenses : <b>{{ $profile->repayment_expenses }}</b>
                        </li>
                        
                        

                              
                        
                    </ul>
                  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>