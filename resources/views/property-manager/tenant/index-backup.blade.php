@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Tenants</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li><b>Tenants</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title mb-3">Tenants</strong>
        </div>
        <table class="table table-bordered" id="tenant-applications-table">
        <div class="card-body">
            @if ($tenants->count() > 0)
                @foreach ($tenants as $tenant)
                <div class="col-md-3">
                    <div class="card" style="min-height: 400px;">
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                @if ($tenant->user->getFirstMedia('profile_images'))
                                <img class="rounded-circle mx-auto d-block" src="{{ $tenant->user->getFirstMedia('profile_images')->getUrl() }}" alt="Profile Image" style="max-width:200px; ">
                                @else
                                <img class="rounded-circle mx-auto d-block" src="http://www.gravatar.com/avatar/dfd6b10215e2e61b5b56789294d068f5.jpg?s=80&d=mm&r=g" alt="Profile Image">
                                @endif
                                <h5 class="text-sm-center mt-2 mb-1">{{ $tenant->user->name }}</h5>
                                <div class="text-center dashboard-edit-btn">
                                    <span>{{ optional($tenant->user->tenantProfile)->mobile }}</span>
                                    <br>
                                    <span>{{ $tenant->user->email }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{ route('property-manager.tenants.show', $tenant->user->id) }}" class="btn btn-info" title="View Profile">
                                <i class="fa fa-eye"></i>
                            </a>
                           {{--  <a href="#" class="btn btn-danger" title="Delete Tenant">
                                <i class="fa fa-trash"></i>
                            </a> --}}
                        </div>
                    </div>
                </div>
                @endforeach
            @else
            No Entries.
            @endif
        </div>
    </table>
    </div>
</div>
@endsection
@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery('#tenant-applications-table').DataTable({
            "columnDefs": [ {
              "targets": 'no-sort',
              "orderable": false,
          } ]
      });
    });
</script>
@endsection