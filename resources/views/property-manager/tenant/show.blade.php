@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1> 
                    <a href="{{ route('property-manager.tenant-applications.index', ['tenant_id' => $user->id, 'tenant_name' => $user->name]) }}" class="btn btn-primary btn-sm"><b>View Applications</b></a>
                </h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li>Tenants</li>
                    <li class="active">Show</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

@include('property-manager.tenant._profile')

{{-- @include('property-manager.tenant.confirmation') --}}

@include('property-manager.application._documents')

@endsection