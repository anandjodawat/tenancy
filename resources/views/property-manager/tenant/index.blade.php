@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Tenants</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li><b>Tenants</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Tenants
                            <!--<button class="btn btn-primary btn-sm" id="download-total-application-report">Download</button>-->
                        </strong>
                        
                    </div>
                    <div class="card-body">
                       {{--  <div class="col-md-4 offset-md-8" style="margin-bottom: 10px;">
                         {{ Form::open(['route' => 'property-manager.tenant.index', 'method' => 'get']) }}
                         <div class="input-group">
                            {{ Form::text('search', request('search'), ['class' => 'form-control', 'placeholder' => 'Search']) }}
                            <div class="input-group-btn">
                                {{ Form::submit('Search', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tenant-list-table">
                            <thead>
                                <th class="noExl no-sort">Image</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Created</th>
                                <th class="noExl no-sort">Action</th>
                            </thead>
                            <tbody>
                                @foreach ($tenants as $tenant)
                                @if ( !empty($tenant->user) )
                                <tr>
                                    <td class="noExl no-sort">
                                        @if ( $tenant->user->getFirstMedia('profile_images') )
                                        <img class="rounded-circle mx-auto d-block" src="{{ $tenant->user->getFirstMedia('profile_images')->getUrl() }}" alt="Profile Image" style="max-width:200px; ">
                                        @else
                                        <img class="rounded-circle mx-auto d-block" src="http://www.gravatar.com/avatar/dfd6b10215e2e61b5b56789294d068f5.jpg?s=80&d=mm&r=g" alt="Profile Image">
                                        @endif
                                    </td>
                                    <td>{{ $tenant->user->name }}</td>
                                    <td>{{ optional($tenant->user->tenantProfile)->mobile }}</td>
                                    <td>{{ $tenant->user->email }}</td>
                                    <td>{{ $tenant->user->created_at->format('Y/m/d H:i') }}</td>
                                    <td class="noExl">
                                        <a href="{{ route('property-manager.tenants.show', $tenant->user->id) }}" class="btn btn-sm btn-info"  title="Details">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ route('property-manager.tenants.delete', ['id'=>$tenant->user->id]) }}" class="btn btn-danger btn-sm" title="Delete Tenant User" data-method="DELETE">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('after-scripts')
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "totalapplication" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
                // exclude_inputs: true
            });
            console.clear();
        });
    })
</script>

<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#tenant-list-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": true,
                //"type": 'de_datetime',
            } ],
            "order": [[4, "desc"]]
        });
    })
</script>
@endsection