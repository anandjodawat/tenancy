    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Employment Confirmation
                            </strong>
                        </div>
                        <div class="card-body">
                            @if ($employmentConfirmation)
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Date: </strong><span class="pull-right">{{ $employmentConfirmation->date }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Company Name: </strong><span class="pull-right">{{ $employmentConfirmation->company_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Employee Name: </strong><span class="pull-right">{{ $employmentConfirmation->employee_position }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Work Duration: </strong><span class="pull-right">{{ $employmentConfirmation->work_duration_from }} to {{ $employmentConfirmation->work_duration_to }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Completed By: </strong><span class="pull-right">{{ $employmentConfirmation->completed_by }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Submission Date: </strong><span class="pull-right">{{ $employmentConfirmation->submission_date }}</span>
                                </li>
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                History Confirmation
                            </strong>
                        </div>
                        <div class="card-body">

                            @if ($historyConfirmation)
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <strong>Date: </strong><span class="pull-right">{{ $historyConfirmation->date }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Agency Name: </strong><span class="pull-right">{{ $historyConfirmation->agency }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Tenant Name: </strong><span class="pull-right">{{ $historyConfirmation->tenant_name }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Property Address: </strong><span class="pull-right">{{ $historyConfirmation->property_address }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Was the applicant(s) listed as a lessee?: </strong><span class="pull-right">{{ ucfirst($historyConfirmation->listed_as_lease) }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Was the applicant(s) listed as a lessee? </strong> <span class="pull-right">{{ ucfirst($historyConfirmation->listed_as_lease) }}</span>
                                </li>
                                <li class="list-group-item">

                                    <strong>What dates were they in the property? </strong> <span class="pull-right">From {{ $historyConfirmation->property_duration_from }} to {{ $historyConfirmation->property_duration_to }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Did your office Terminate the Tenancy? </strong><span class="pull-right">{{ ucfirst($historyConfirmation->terminate_tenancy) }}</span>
                                </li>
                                    @if ($historyConfirmation->terminate_tenancy == 'yes')
                                <li class="list-group-item">
                                    <strong>Reason: </strong> <span class="pull-right">{{ $historyConfirmation->reason_for_termination }}</span>
                                </li>
                                    @endif
                                <li class="list-group-item">
                                <strong>During the tenancy, were the tenant(s) ever in arrears?</strong> <span class="pull-right">{{ config('tenancy-application.arrears')[$historyConfirmation->arrears] }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Did the tenant recieve any Breach Notices other than arrears? </strong><span class="pull-right"> {{ ucfirst($historyConfirmation->breach_notices) }}</span>
                                </li>
                                @if ($historyConfirmation->breach_notices == 'yes')
                                <li class="list-group-item">
                                    <strong>Reason: </strong> <span class="pull-right">{{ $historyConfirmation->reason_for_breach_notices }}</span>
                                </li>
                                @endif
                                <li class="list-group-item">
                                    <strong>Was there any damage noted during inspections? </strong><span class="pull-right"> {{ ucfirst($historyConfirmation->damage) }}</span>
                                </li>
                                @if ($historyConfirmation->damage == 'yes')
                                <li class="list-group-item">
                                <strong>What was the damage? </strong><span class="pull-right">{{ $historyConfirmation->damage_reason }}</span>
                                </li>
                                @endif
                                <li class="list-group-item">
                                    <strong>During the tenancy, what were the inspections like? </strong><span class="pull-right">{{ config('tenancy-application.inspection_like')[$historyConfirmation->inspection_like] }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Were pets kept at the Property? </strong><span class="pull-right"> {{ ucfirst($historyConfirmation->pets) }}</span>
                                </li>
                                <li class="list-group-item">
                                    <strong>Was there any complaints or damage? </strong><span class="pull-right">{{ ucfirst($historyConfirmation->complaints_damage) }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>What date did tenants vacate the property? </strong><span class="pull-right">{{ $historyConfirmation->vacate_date }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Was the Bond refunded in full? </strong><span class="pull-right">{{ ucfirst($historyConfirmation->bond_refund_full) }}</span>
                                </li>
                                @if ($historyConfirmation->bond_refund_full == 'yes')
                                <li class="list-group-item">
                                <strong>Reason: </strong><span class="pull-right">{{ $bond_refund_reason }}</span>
                                </li>
                                @endif
                                <li class="list-group-item">
                                <strong>On a scale of 1-10 how would you rate this tenant(s)? </strong><span class="pull-right">{{ $historyConfirmation->rate_tenant }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>On a scale of 1-10 how would you rate this tenant(s)? </strong><span class="pull-right">{{ $historyConfirmation->rent_again }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Additional Comments: </strong><span class="pull-right">{{ $historyConfirmation->additional_comments }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Completed By: </strong><span class="pull-right">{{ $historyConfirmation->completed_by }}</span>
                                </li>
                                <li class="list-group-item">
                                <strong>Submission Date: </strong><span class="pull-right">{{ $historyConfirmation->submission_date }}</span>
                                </li>
                            </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>