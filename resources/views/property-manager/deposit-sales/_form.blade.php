<div class="row form-group {{ $errors->has('tenant_code') ? 'has-error' : '' }}">
    {{ Form::label('forTenantCode', 'Payment Reference', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('tenant_code', null, ['class' => 'form-control']) }}
        @if ($errors->has('tenant_code'))
            <span class="text-danger">{{ $errors->first('tenant_code') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {{ Form::label('forName', 'Name', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
        @if ($errors->has('name'))
            <span class="text-danger">{{ $errors->first('name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('address') ? 'has-error' : '' }}">
    {{ Form::label('forAddress', 'Address', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('address', null, ['class' => 'form-control']) }}
        @if ($errors->has('address'))
            <span class="text-danger">{{ $errors->first('address') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forContactNumber', 'Contact Number', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('contact_number', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group {{ $errors->has('payment_amount') ? 'has-error' : '' }}">
    {{ Form::label('forPaymentAmount', 'Payment Amount', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('payment_amount', null, ['class' => 'form-control']) }}
        @if ($errors->has('payment_amount'))
            <span class="text-danger">{{ $errors->first('payment_amount') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('send_to') ? 'has-error' : '' }}">
    {{ Form::label('forSendTo', 'Send To / Email', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::email('send_to', null, ['class' => 'form-control']) }}
        @if ($errors->has('send_to'))
            <span class="text-danger">{{ $errors->first('send_to') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
    {{ Form::label('forSubject', 'Subject', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::text('subject', null, ['class' => 'form-control']) }}
        @if ($errors->has('subject'))
            <span class="text-danger">{{ $errors->first('subject') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('attachment', 'Attachment', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::file('attachment', ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group {{ $errors->has('body') ? 'has-error' : '' }}">
    {{ Form::label('forBody', 'Body', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        <textarea name="body" class="form-control" id="request-deposit-body">
            @if (auth()->user()->agency_profile()->mail_templates && auth()->user()->agency_profile()->mail_templates->where('type', '5')->count() > 0)
                {{ auth()->user()->agency_profile()->mail_templates->where('type', '5')->first()->body }}
            @else
                @include ('property-manager.request-deposit.default-template')
            @endif
        </textarea>
        @if ($errors->has('body'))
            <span class="text-danger">{{ $errors->first('body') }}</span>
        @endif
    </div>
</div>
