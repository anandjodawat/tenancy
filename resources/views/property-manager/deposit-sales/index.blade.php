@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Request Sales</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li class="active"><b>Request Sales</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Request Sales
            <button class="btn btn-primary btn-sm" id="download-total-application-report">Download</button>
        </strong>
        
    </div>
    <div class="card-body card-block">
        <div class="table-responsive">
            <table class="table" id="deposit-sales-applications-table">
                <thead>
                    <th>Tenant Code</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact Number</th>
                    <th>Payment Amount</th>
                    <th>Sent To</th>
                    <th>Sent By</th>
                    <th>Subject</th>
                    <th class="noExl">Status</th>
                </thead>
                <tbody>
                    @foreach ($requests as $request)
                    <tr>
                        <td>{{ $request->tenant_code }}</td>
                        <td>{{ $request->name }}</td>
                        <td>{{ $request->address }}</td>
                        <td>{{ $request->contact_number }}</td>
                        <td>{{ $request->payment_amount }}</td>
                        <td>{{ $request->send_to }}</td>
                        <td>{{ $request->sentBy->name }}</td>
                        <td>{{ $request->subject }}</td>
                        <td>{{ $request->email_status ? 'SENT' : 'NOT SENT' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

@endsection

@section('after-scripts')
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "sales_report" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
                // exclude_inputs: true
            });
            console.clear();
        });
    })
</script>
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#deposit-sales-applications-table').DataTable();
    })
</script>
@endsection