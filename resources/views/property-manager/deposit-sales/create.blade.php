@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Request Sales</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                     <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                      <li><a href="{{ route('property-manager.request-deposits.index') }} "><b>Request Sales</b></a></li>
                    <li class="active">Create</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Request Sales</strong>
    </div>
    {{ Form::open(['class' => 'form-horizontal', 'method' => 'post', 'route' => 'property-manager.request-deposits.store']) }}
    <div class="card-body card-block">
        @include('property-manager.deposit-sales._form')
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm pull-right">
            Submit
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            Reset
        </button>
    </div>
    {{ Form::close() }}
</div>
</div>

@endsection

@section ('after-scripts')
<script type="text/javascript">
    CKEDITOR.replace('request-deposit-body');
</script>
@endsection