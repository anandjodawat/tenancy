@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
           {{--  <div class="page-title">
                <h1>Settings</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                   <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                   <li class="active"><b>Settings</b></li>
               </ol>
           </div>
       </div>
   </div>
</div>

@include('flash::message')

@if (auth()->user()->hasRole('property-manager'))
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Settings
                        </strong>
                    </div>
                    <div class="card-body">
                        <h5>Please Contact Agency Admin ({{ auth()->user()->agency_profile()->name }}, {{ auth()->user()->agency_profile()->email }}) to Edit Agency Name, Change Logo, Moving Service Setting or to Add a New Property Manager.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if (auth()->user()->hasRole('agency-admin'))
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title" style="font-size: 1.5em;">
                            Moving Service Setting
                        </strong>
                    </div>
                    {{ Form::open(['class' => 'form-horizontal', 'route' => 'property-manager.settings.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
                    <div class="card-body">
                        <!--<div class="row form-group">
                            {{ Form::label('key', "Preferred Connection Provider Notification Through", ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::select('key', ['Compare And Connect', 'Add New'], $setting? $setting->key:0, ['class' => 'form-control'] ) }}
                            </div>
                        </div>-->

                        <div class="row form-group">
                            {{ Form::label('key', "Preferred Connection Provider Notification Through", ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-6">
                                <select name="key" id="key" class="form-control input-sm">
                                @if($setting_dropdown->count() > 0)
                                @foreach($setting_dropdown as $set) 
                                    <option value = "{{ $set->key }}" 
                                        {{ $set->key==$setting->key ? 'selected=selected' : '' }}  >
                                        {{ $set->key=='0'?'Compare And Connect':$set->connection_name }}
                                    </option>
                                @endforeach
                                @else
                                <option value="0">Compare And Connect</option>
                                @endif
                                @if( $setting_dropdown->count()==1 && $setting->key==1 )
                                <option value="0">Compare And Connect</option>
                                @endif
                                <option value="{{ $setting ? $setting->id+1 : '1' }}">Add New</option>
                               </select>

                            </div>
                        </div>

                        <div class="row form-group hide-on-compare-and-connect">
                            {{ Form::label('setting', "Connection Provider Email", ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::email('value', $setting ? $setting->value : null, ['class' => 'form-control', 'id'=>'contact_email'] ) }}
                            </div>
                        </div>
                        <div class="row form-group show-on-others-selection hide-on-compare-and-connect">
                            {{ Form::label('connection_name', 'Connection Provider Name', ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::text('connection_name', $setting && $setting->connection_name ? $setting->connection_name : null, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row form-group hide-on-compare-and-connect">
                            {{ Form::label('file', 'Upload Utility Connection Image', ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-3">
                                {{ Form::file('file', ['class' => 'form-control', 'accept'=>'image/*']) }}
                            </div>
                            <div class="col-md-3 imgprw">
                                @if (!is_null($setting) && auth()->user()->agency_profile()->getFirstMedia(auth()->user()->agency_id.'-'.$setting->key))
                                <img src="{{ auth()->user()->agency_profile()->getFirstMedia(auth()->user()->agency_id.'-'.$setting->key)->getUrl() }}" alt="Utility Connection Logo" class="img img-responsive" style="max-height: 40px;">
                                @endif
                            </div>
                        </div>
                        <div class="row form-group hide-on-compare-and-connect">
                            {{ Form::label('contact_number', "Contact Number", ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-6">
                                {{ Form::text('contact_number', $setting ? $setting->contact_number : null, ['class' => 'form-control'] ) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            {{ Form::label('body', 'Terms and Conditions', ['class' => 'col-md-6 control-label']) }}
                            <div class="col-md-6">
                                <textarea class="form-group" id="settings-body" name="body">
                                    @if ($setting && $setting->body != null) 
                                    @php
                                    echo $setting->body;
                                    @endphp
                                    @else 
                                    @include('property-manager.settings._default_terms')
                                    @endif
                                </textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                           <div class="col-md-12" style="text-align: right; margin-top: 20px;"> 
                               {{ Form::submit('submit', ['class' => 'btn btn-success']) }}
                           </div>
                       </div>
                   </div>
                   {{ Form::close() }}
               </div>
               <div style="display: none;" id="default_settings_terms">@include('property-manager.settings._default_terms')</div>
           </div>
       </div>
   </div>
</div>
@endif
@endsection

@section ('after-scripts')
<script type="text/javascript">
    CKEDITOR.replace('settings-body');
    jQuery(document).ready( function () {
        show_connection_name_field();
        /*jQuery('select[name=key]').on('change', function () {
            show_connection_name_field();
        });*/
    });

    jQuery('select[name=key]').on('change', function () {
        //CKEDITOR.instances.rightcolumn.destroy();   
        var value = jQuery('select[name=key]').val();
        var optionVal = jQuery(this).find("option:selected").text();
        
        if (value == '0') {
            jQuery('.show-on-others-selection').hide();
            jQuery('.hide-on-compare-and-connect').hide();
            CKEDITOR.replace('settings-body');
        } else {

            jQuery('.imgprw').html('');
            if(optionVal=='Add New'){

                jQuery('.show-on-others-selection').show();
                jQuery('.hide-on-compare-and-connect').show();
                
                jQuery('#connection_name').val('');
                jQuery('#contact_email').val('');
                jQuery('#contact_number').val('');
                var default_settings_terms = jQuery('#default_settings_terms').html();
                CKEDITOR.instances['settings-body'].setData( default_settings_terms );
            } else {
                
                jQuery.ajax({
                    url: "{{route('property-manager.settings.getservices')}}",
                    data: {
                            action : "getSettings",
                            term : value
                     },
                    dataType: "json",
                    success: function(obj){
                       
                        if(obj!=''){
                            jQuery('.show-on-others-selection').show();
                            jQuery('.hide-on-compare-and-connect').show();
                            jQuery('#connection_name').val(obj.connection_name);
                            jQuery('#contact_email').val(obj.value);
                            jQuery('#contact_number').val(obj.contact_number);
                            if(obj.provider_logo!=''){
                                jQuery('.imgprw').html('<img src="'+obj.provider_logo+'" alt="Utility Connection Logo" class="img img-responsive" style="max-height: 40px;">');
                            }
                            CKEDITOR.instances['settings-body'].setData( obj.body );
                        } 
                    }
                });
            }
        }
    
    });

    function show_connection_name_field()
    {
        var value = jQuery('select[name=key]').val();
        if (value == '0') {
            jQuery('.show-on-others-selection').hide();
            jQuery('.hide-on-compare-and-connect').hide();
            CKEDITOR.replace('settings-body');
        } else {
            jQuery('.show-on-others-selection').show();
            jQuery('.hide-on-compare-and-connect').show();
        }
    }
</script>
@endsection