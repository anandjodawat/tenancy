@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
           {{--  <div class="page-title">
                <h1>Connection Service</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                   <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                   <li class="active"><b>Connection Service</b></li>
               </ol>
           </div>
       </div>
   </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Connection Service
                            <button class="btn btn-primary btn-sm" id="download-total-application-report" style="display: none;">Download</button>
                        </strong>
                        
                        
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="connectionServices-applications-table">
                                <thead>
                                    <th>Service Name</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Approved Date</th>
                                    <th class="noExl no-sort">Action</th>
                                </thead>
                                <tbody>
                                    @foreach ($applications as $application)
                                    @foreach ($application->moving_service as $service)
                                    <tr>
                                        <td>{{ $service->service_name }}</td>
                                        <td>{{ $service->application->user->first_name }} {{ $service->application->user->last_name }}</td>
                                        <td>{{ $service->application->user->email }}</td>
                                        <td>{{ $service->created_at->format('Y/m/d H:i') }}</td>
                                        <td>
                                            <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show Application" target="_blank">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        /*jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "connectionServices" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
            });
            console.clear();
        });*/
        jQuery('#connectionServices-applications-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[3, "desc"]],
            "dom": 'Blfrtip',
            "buttons": [
                { 
                  extend: 'excel',
                  text: 'Download',
                  className: 'downloadButton',
                  title: 'Tenancy Application - Connection Service',
                  filename: "Connection-Service-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  exportOptions: {
                    //columns: [ 0, 1, 2,3,4,6 ]
                    columns: 'th:not(:last-child)'
                    }
                } 
            ]
        });
    })
</script>
@endsection