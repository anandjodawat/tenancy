@extends('property-manager.layouts.app')

@section('title')
    {{ app_name() }}
@endsection

@section('content')

        <div class="breadcrumbs">
            <!--<div class="col-sm-4">
                <div class="page-header float-left">
                    {{-- <div class="page-title">
                        <h1>Dashboard</h1>
                    </div> --}}
                </div>
            </div>-->
            <div class="col-sm-12">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active"><b>Dashboard</b></li>
                        </ol>
                    </div>
                </div>
                <div class="page-header float-left">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active"><b>Welcome, {{ access()->user()->full_name }}</b></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-4 manager-dashboard-block">

        <a href="{{ route('property-manager.applications.index', ['status' => 'inbox']) }}">
            <div class="col-xl-5 col-lg-6 single-block">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib" style="background: #28a745!important; padding: 5px 10px 5px 10px; border: 1px solid #28a745; border-radius: 5px;">
                               <i class="ti-email text-success border-success" style="color: #fff !important; font-size: 36px;"></i>
                            </div>
                            <div class="stat-content dib">
                                <div class="stat-text">Inbox Application</div>
                                <div class="stat-digit">{{ $inbox }}</div>
                            </div>
                            <div style="padding: 33px 0px 5px 0px; float: right;">
                                <i class="fa fa-caret-right" style="color: #000 !important; font-size: 24px;">
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href="{{ route('property-manager.applications.index', ['status' => 'accepted']) }}">
            <div class="col-xl-5 col-lg-6 single-block">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib" style="padding: 5px 10px 5px 10px;  border: 1px solid #955897; border-radius: 5px;  background: #955897;">
                                <i class="ti-thumb-up text-primary border-primary" style="border-color: #955897 !important; color: #fff !important; font-size: 36px;">
                                </i>
                            </div>
                            <div class="stat-content dib">
                                <div class="stat-text">Accepted Application</div>
                                <div class="stat-digit">{{ $accepted }}</div>
                            </div>
                            <div style="padding: 33px 0px 5px 0px; float: right;">
                                <i class="fa fa-caret-right" style="color: #000 !important; font-size: 24px;">
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href="{{ route('property-manager.applications.index', ['status' => 'approved']) }}">
            <div class="col-xl-5 col-lg-6 single-block">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-one">
                            <div class="stat-icon dib" style="background: #ffc107!important; padding: 5px 10px 5px 10px;  border: 1px solid #ffc107; border-radius: 5px; ">
                                <i class="ti-check-box text-warning border-warning"style="color: #fff !important; font-size: 36px;"></i>
                            </div>
                            <div class="stat-content dib">
                                <div class="stat-text">Approved Application</div>
                                <div class="stat-digit">{{ $approved }}</div>
                            </div>
                            <div style="padding: 33px 0px 5px 0px; float: right;">
                                <i class="fa fa-caret-right" style="color: #000 !important; font-size: 24px;">
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        </div> <!-- .content -->
@endsection
