@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Request Deposits</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                     <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                      <li><a href="{{ route('property-manager.request-deposits.index') }} "><b>Request Deposit</b></a></li>
                    <li class="active"><b>Create</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Request Deposits</strong>
    </div>
    {{ Form::open(['class' => 'form-horizontal', 'method' => 'post', 'route' => 'property-manager.request-deposits.store', 'enctype' => 'multipart/form-data', 'id' => 'apply-prop-form']) }}
    <div class="card-body card-block">
        @include('property-manager.request-deposit._form')
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm pull-right">
            Submit
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            Reset
        </button>
    </div>
    {{ Form::close() }}
</div>
</div>

@endsection

@section ('after-scripts')
<script type="text/javascript">
    CKEDITOR.replace('request-deposit-body');

    jQuery(document).ready(function() {

        jQuery( "#reqName" ).autocomplete({

            source: function(request, response) {
                jQuery.ajax({
                url: "{{route('property-manager.request-deposits.autocomplete')}}",
                data: {
                        action : "getName",
                        term : request.term
                 },
                dataType: "json",
                success: function(data){
                   var resp = jQuery.map(data,function(obj){
                        //console.log(obj);
                        return obj.name;
                   }); 

                   response(resp);
                }
                });
            },
            minLength: 3
        });

        jQuery( "#reqAddress" ).autocomplete({

            source: function(request, response) {
                jQuery.ajax({
                url: "{{route('property-manager.request-deposits.autocomplete')}}",
                data: {
                        action : "getAddress",
                        term : request.term
                 },
                dataType: "json",
                success: function(data){
                   var resp = jQuery.map(data,function(obj){
                        console.log(obj);
                        return obj.fulladdress;
                   }); 

                   response(resp);
                }
                });
            },
            minLength: 3
        });

        jQuery("#attachment").on("change", function(e) {

          jQuery("#clearPreview").show();  
          var files = e.target.files,
            filesLength = files.length;

          for (var i = 0; i < filesLength; i++) {
            var f = files[i];

            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
              var file = e.target;
              var fileName = e.target.fileName;
              //console.log('file: '+i+files[i]);
              //var extension = fileName.match(/\.[0-9a-z]+$/i);
              //var extension = f.name.split('.').pop().toLowerCase();
              var filePath = e.target.result;
              var res = filePath.split(';base64');
              var extension = res[0].split('/');
              //console.log('file: '+extension[0]);

              if(extension[0]=='data:image'){

                jQuery( "#filePreview" ).append(jQuery("<span class=\"pelem pip\">" +
                "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + fileName + "\"/>" + "</span>"));

              } else {

                jQuery( "#filePreview" ).append(jQuery("<span class=\"pelem pip\">" +
                "<i class=\"fa fa-file-text\" aria-hidden=\"true\" style=\"font-size: 60px;vertical-align:middle\" title=\"" + fileName + "\"></i>" + "</span>"));

              }
              

            });
            fileReader.fileName = files[i].name;
            fileReader.readAsDataURL(f);
            //fileReader.readAsText(f);
          }


        });

        jQuery("#clearPreview").on("click", function(e) {

            jQuery(".pelem").remove();
            jQuery('#attachment').val("");
            
            jQuery("#clearPreview").hide();
        });

        // form validation
        jQuery('#apply-prop-form').validate({
            errorClass: 'error-message',
            rules : {
                // Property Details
                tenant_code : 'required',
                name : 'required',
                address: 'required',
                payment_amount: { required: true, number: true},
                send_to : 'required',
                subject: 'required',
                body: 'required',
            }
        });
        
    });

</script>
@endsection