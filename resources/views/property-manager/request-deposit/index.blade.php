@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
          {{--   <div class="page-title">
                <h1>Request Deposits</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li class="active"><b>Request Deposits</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Request Deposits
          <button class="btn btn-primary btn-sm" id="download-total-application-report" style="display: none;">Download</button>
        </strong>
    </div>
    <div class="card-body card-block">
        <div class="table-responsive">
            <table class="table table-bordered" id="request-deposits-applications-table">
                <thead>
                    <th>Tenant Code</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact Number</th>
                    <th>Payment Amount</th>
                    <th>Sent To</th>
                    <th>Sent By</th>
                    <th>Subject</th>
                    <th>Status</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    @foreach ($requests as $request)
                    <tr>
                        <td>{{ $request->tenant_code }}</td>
                        <td>{{ $request->name }}</td>
                        <td>{{ $request->address }}</td>
                        <td>{{ $request->contact_number }}</td>
                        <td>{{ $request->payment_amount }}</td>
                        <td>{{ $request->send_to }}</td>
                        <td>{{ $request->sentBy->name }}</td>
                        <td>{{ $request->subject }}</td>
                        <td>{{ $request->email_status ? 'SENT' : 'NOT SENT' }}</td>
                        <td>{{ $request->created_at->format('Y/m/d H:i') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

@endsection

@section('after-scripts')
<script type="text/javascript">
    /*jQuery(document).ready(function () {
        jQuery('#download-total-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Total Application",
                filename: "request_deposits" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
            });
            console.clear();
        });
    })*/
</script>
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#request-deposits-applications-table').DataTable(
            {
                "order": [[9, "desc"]],
                "dom": 'Blfrtip',
                "buttons": [
                    { 
                      extend: 'excel',
                      text: 'Download',
                      className: 'downloadButton',
                      title: 'Tenancy Application - Request Deposits',
                      filename: "Request-Deposits-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                      exportOptions: {
                        //columns: [ 0, 1, 2,3,4,6 ]
                        }
                    } 
                ]

        });
    })
</script>
@endsection