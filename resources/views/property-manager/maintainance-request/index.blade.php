@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
              {{--   <div class="page-title">
                    <h1>maintenance Request</h1>
                </div> --}}
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                        <li class="active"><b>Maintenance Request</b></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Maintenance Applications
                                <button class="btn btn-primary btn-sm pull-right" style="float: none;" id="download-maintenance-request-report">Download</button>
                            </strong>
                            
                        </div>
                        <div class="card-body">
                         <div class="table-responsive">
                             <table class="table table-bordered" id="maintenance-requests-table">
                                <thead>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Suburb</th>
                                    <th>PostCode</th>
                                    <th>Status</th>
                                    <th class="noExl no-sort">Date Added</th>
                                    <th class="noExl no-sort">Action</th>
                                </thead>
                                <tbody>
                                    @foreach ($maintenances as $maintenance)
                                    <tr>
                                        <td>{{ $maintenance->name }}</td>
                                        <td>{{ $maintenance->street_address }}</td>
                                        <td>{{ $maintenance->suburb }}</td>
                                        <td>{{ $maintenance->post_code }}</td>
                                        <td>{{ $maintenance->status_name }}</td>
                                        <td class="noExl">{{ $maintenance->created_at->diffForHumans() }}</td>
                                        <td class="noExl">
                                            <a href="{{ route('property-manager.maintenance-requests.show', $maintenance->id) }}" title="Show" class="btn btn-info btn-sm">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#download-maintenance-request-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "maintenance Request",
                filename: "maintenance_request" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
            });
            console.clear();
        });
        jQuery('#maintenance-requests-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ]
        });
    });
</script>
@endsection