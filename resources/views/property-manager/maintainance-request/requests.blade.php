<div class="table-responsive">
    <table class="table" id="maintenance-requests-table">
        <thead>
            <th>Name</th>
            <th>Address</th>
            <th>PostCode</th>
            <th>Status</th>
            <th>Date Added</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($maintenances as $maintenance)
            <tr>
                <td>{{ $maintenance->name }}</td>
                <td>{{ $maintenance->street_address }}</td>
                <td>{{ $maintenance->post_code }}</td>
                <td>{{ $maintenance->status_name }}</td>
                <td>{{ $maintenance->created_at->diffForHumans() }}</td>
                <td>
                    <a href="{{ route('property-manager.maintenance-requests.show', $maintenance->id) }}" title="Show" class="btn btn-info btn-sm">
                        <i class="fa fa-eye"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>