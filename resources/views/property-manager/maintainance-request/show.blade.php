@extends('property-manager.layouts.app')

@section('page-header')
    <h1 class="page-title">
        {{ app_name() }}
        <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
 <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                {{-- <div class="page-title">
                    <h1>maintenance Request</h1>
                </div> --}}
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"><b>Maintenance Request</b></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Maintenance Application
                            </strong>
                        </div>
                        <div class="card-body">
                            <div class="col-md-6">
                                <h4 class="text-center"><strong>Maintenance / Repairs Required?</strong></h4>
                                <hr>
                                <ul class="list-group">
                                    <li class="list-group-item">Street Address<span class="pull-right">{{ $maintenance->street_address }}</span></li>
                                    <li class="list-group-item">Suburb<span class="pull-right">{{ $maintenance->suburb }}</span></li>
                                    <li class="list-group-item">State<span class="pull-right">{{ config('tenancy-application.states')[$maintenance->state] }}</span></li>
                                    <li class="list-group-item">Post Code<span class="pull-right">{{ $maintenance->post_code }}</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h4 class="text-center"><strong>Contact Details</strong></h4>
                                <hr>
                                <ul class="list-group">
                                    <li class="list-group-item">Are You The Tenant?<span class="pull-right">{{ ucfirst($maintenance->tenant) }}</span></li>
                                    <li class="list-group-item">Name<span class="pull-right">{{ $maintenance->name }}</span></li>
                                    <li class="list-group-item">Mobile Phone<span class="pull-right">{{ $maintenance->mobile_phone }}</span></li>
                                    <li class="list-group-item">Work Phone<span class="pull-right">{{ $maintenance->work_phone }}</span></li>
                                    <li class="list-group-item">Home Phone<span class="pull-right">{{ $maintenance->home_phone }}</span></li>
                                    <li class="list-group-item">Email Address<span class="pull-right">{{ $maintenance->email }}</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <hr>
                                <h4 class="text-center"><strong>Details of Maintenance Request</strong></h4>
                                <hr>
                                <ul class="list-group">
                                    <li class="list-group-item">{{ $maintenance->details }}</li>
                                    <li class="list-group-item">Preferred Date<span class="pull-right">{{ $maintenance->preferred_date }}</span></li>
                                    <li class="list-group-item">Preferred Time<span class="pull-right">{{ $maintenance->preferred_time }}</span></li>
                                    <li class="list-group-item">How Long has this maintenance Existed?<span class="pull-right">{{ $maintenance->maintenance_existed }}</span></li>
                                    <li class="list-group-item">Home Phone<span class="pull-right">{{ $maintenance->home_phone }}</span></li>
                                    <li class="list-group-item">Property Manager Email<span class="pull-right">{{ $maintenance->property_manager_email }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection