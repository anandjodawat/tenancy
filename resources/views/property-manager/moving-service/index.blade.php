@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Moving Service</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                   <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                   <li class="active">Moving Service</li>
               </ol>
           </div>
       </div>
   </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Moving Service
                        </strong>
                        <button class="btn btn-primary btn-sm" id="download-moving-service-report" style="background: #719759; border: 1px solid #719759;">Download</button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="moving-service-application-table">
                                <thead>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Services</th>
                                    <th class="no-sort">Approved Date</th>
                                    <th class="no-sort noExl">Action</th>
                                </thead>
                                <tbody>
                                    @foreach ($movingServicesApplication as $application)
                                    <tr>
                                        <td>{{ $application->user->first_name }} {{ $application->user->last_name }}</td>
                                        <td>{{ $application->user->email }}</td>
                                        <td>
                                            @foreach ($application->moving_service as $moving_service)
                                            <span class="badge badge-pill badge-primary">
                                                {{ $moving_service->service_name }}
                                            </span>
                                            @endforeach
                                        </td>
                                        <td>{{ $application->created_at }}</td>
                                        <td class="noExl">
                                            <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show Application" target="_blank">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#download-moving-service-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Moving Service",
                filename: "moving_service" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
            });
            console.clear();
        });
        jQuery('#moving-service-application-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ]
        });
    })
</script>
@endsection