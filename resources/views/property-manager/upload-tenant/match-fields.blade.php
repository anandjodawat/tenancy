@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Match Fields</strong>
        </div>
        {{ Form::open(['route' => 'property-manager.upload-tenants.store', 'method' => 'post']) }}
        {{ Form::hidden('type', 'excel') }}
        <div class="card-body card-block">
            <div class="col-md-8 offset-md-2">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="col-md-6"><b>Name</b></div>
                        <div class="col-md-6">
                         {{ Form::select('name', $excel_fields, array_search('name', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                     </div>
                 </li>
                 <li class="list-group-item">
                     <div class="col-md-6"><b>Lease Name</b></div>
                     <div class="col-md-6">
                        {{ Form::select('lease_name', $excel_fields, array_search('lease_name', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                    </div>
                </li>
                <li class="list-group-item">
                 <div class="col-md-6"><b>Post Line 1</b></div>
                 <div class="col-md-6">
                    {{ Form::select('post_line1', $excel_fields, array_search('post_line1', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Post Line 2</b></div>
                <div class="col-md-6">
                    {{ Form::select('post_line2', $excel_fields, array_search('post_line2', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Post Line 3</b></div>
                <div class="col-md-6">
                    {{ Form::select('post_line3', $excel_fields, array_search('post_line3', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                    
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Suburb</b></div>
                <div class="col-md-6">
                    {{ Form::select('suburb', $excel_fields, array_search('suburb', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                    
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>State</b></div>
                <div class="col-md-6">
                    {{ Form::select('state', $excel_fields, array_search('state', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                    
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Post Code</b></div>
                <div class="col-md-6">
                    {{ Form::select('post_code', $excel_fields, array_search('post_code', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Email Address</b></div>
                <div class="col-md-6">
                    {{ Form::select('email', $excel_fields, array_search('email', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--', 'required']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Mobile</b></div>
                <div class="col-md-6">
                    {{ Form::select('mobile', $excel_fields, array_search('mobile', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Address 1</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_address1', $excel_fields, array_search('property_address1', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Address 2</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_address2', $excel_fields, array_search('property_address2', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Address 3</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_address3', $excel_fields, array_search('property_address3', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Suburb</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_suburb', $excel_fields, array_search('property_suburb', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property State</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_state', $excel_fields, array_search('property_state', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Post Code</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_post_code', $excel_fields, array_search('property_post_code', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Alpha</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_alpha', $excel_fields, array_search('property_alpha', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Manager</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_manager', $excel_fields, array_search('property_manager', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Manager Email</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_manager_email', $excel_fields, array_search('property_manager_email', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Manager Mobile</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_manager_mobile', $excel_fields, array_search('property_manager_mobile', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Manager FName</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_manager_first_name', $excel_fields, array_search('property_manager_first_name', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Property Manager LName</b></div>
                <div class="col-md-6">
                    {{ Form::select('property_manager_last_name', $excel_fields, array_search('property_manager_last_name', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Rent Amount</b></div>
                <div class="col-md-6">
                    {{ Form::select('rent_amount1', $excel_fields, array_search('rent_amount1', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Rent Per</b></div>
                <div class="col-md-6">
                    {{ Form::select('rent_per1', $excel_fields, array_search('rent_per1', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
            <li class="list-group-item">
                <div class="col-md-6"><b>Tenant Code</b></div>
                <div class="col-md-6">
                    {{ Form::select('tenant_code', $excel_fields, array_search('tenant_code', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--']) }}
                </div>
            </li>
        </ul>
    </div>
</div>
</div>
<div class="card-footer">
    <a href="{{ route('property-manager.properties.index') }}" class="btn btn-danger">Cancel</a>
    {{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
</div>
{{ Form::close() }}
</div>
</div>
{{-- 
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Your Excel Sample Data</strong>
        </div>
        <div class="card-body card-block">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        @foreach (session('tenant_excel_fields') as $field)
                        <th>{{ $field }}</th>
                        @endforeach
                    </thead>
                    <tbody>
                        @foreach (session('tenant_data') as $key => $data)
                        @if ($loop->index > 4)
                        @break
                        @endif
                        <tr>
                            @foreach (session('tenant_excel_fields') as $fields)
                            <td>
                                {{ $data->$fields }}
                            </td>
                            @endforeach
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div> --}}


@endsection