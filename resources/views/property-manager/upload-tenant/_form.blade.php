<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Tenant Name</label>
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Lease Name </label>
        {{ Form::text('lease_name', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Email</label>
        {{ Form::email('email', null, ['class' => 'form-control']) }}
        @if ($errors->has('email'))
            <strong class="text-danger">{{ $errors->first('email') }}</strong>
        @endif
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Postline 1</label>
        {{ Form::text('post_line1', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Postline 2</label>
        {{ Form::text('post_line2', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">postline 3</label>
        {{ Form::text('post_line3', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Tenant Suburb</label>
        {{ Form::text('suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Tenant State</label>
        {{ Form::text('state', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">Tenant Pcode</label>
        {{ Form::text('post_code', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">mobile</label>
        {{ Form::text('mobile', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Address 1</label>
        {{ Form::text('property_address1', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Address 2</label>
        {{ Form::text('property_address2', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Address 3</label>
        {{ Form::text('property_address3', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Suburb</label>
        {{ Form::text('property_suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property State</label>
        {{ Form::text('property_state', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Pcode</label>
        {{ Form::text('property_post_code', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Alpha</label>
        {{ Form::text('property_alpha', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Manager</label>
        {{ Form::text('property_manager', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Manager Email</label>
        {{ Form::text('property_manager_email', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Manager Mobile</label>
        {{ Form::text('property_manager_mobile', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Manager First Name</label>
        {{ Form::text('property_manager_first_name', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Property Manager Last Name</label>
        {{ Form::text('property_manager_last_name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Rent Amount</label>
        {{ Form::text('rent_amount1', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Rent Per</label>
        {{ Form::text('rent_per1', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="control-label">Tenant Code</label>
        {{ Form::text('tenant_code', null, ['class' => 'form-control']) }}
    </div>
</div>



