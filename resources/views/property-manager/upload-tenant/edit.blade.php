@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Add Tenants</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li><a href="{{ route('property-manager.tenants.index') }}" ><b>Tenants</b></a></li>
                    <li><a href="{{ route('property-manager.upload-tenants.index') }}" ><b>Upload</b></a></li>
                    <li class="active"><b>Edit</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title mb-3">Edit Uploaded Tenant</strong>
        </div>
        {{ Form::model($tenant, ['route' => ['property-manager.upload-tenants.update', $tenant->id], 'method' => 'patch']) }}

        <div class="card-body">
            @include('property-manager.upload-tenant._form')
        </div>
        <div class="clearfix"></div>
        <div class="card-footer">
            <a href="{{ route('property-manager.upload-tenants.index') }}" class="btn btn-danger">Cancel</a>
            {{ Form::submit('Update', ['class' => 'btn btn-primary pull-right']) }}
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection