@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Upload Tenants</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li><b>Mails Out</b></li>
                    <li class="active"><b>Upload</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-md-12 upload-excel-tenant" style="display: none;">
    <div class="card">
        <div class="card-body">
            {{ Form::open(['enctype' => 'multipart/form-data', 'method' => 'post', 'route' => 'property-manager.import-tenant']) }}
            <div class="form-group">
                <div class="col-md-4">
                    {{ Form::file('file', ['class' => 'form-control', 'required']) }}
                    @if ($errors->has('file'))
                    <strong class="text-danger">{{ $errors->first('file') }}</strong>
                    @endif
                </div>
                <div class="col-md-8">
                    {{ Form::submit('Upload', ['class' => 'btn btn-success']) }}
                    <a href="#" class="btn btn-danger" onclick="toggle_upload_tenant();">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <a href="#" class="btn btn-primary col-md-2" onclick="toggle_upload_tenant();">
                <i class="fa fa-file-excel-o"></i>
                <br>
                Upload Office tenants
            </a>
            <a href="{{ route('property-manager.download-excel') }}" class="btn btn-success col-md-2" style=" color: #fff;background-color: #17a2b8; border-color: #17a2b8;">
                <i class="fa fa-download"></i>
                <br>
                Download Excel
            </a>
            <a href="{{ route('property-manager.upload-tenants.create') }}" class="btn btn-warning col-md-2" style="color: #ffffff; background-color: #d9534f; border-color: #d9534f;">
                <i class="fa fa-plus"></i>
                <br>
                Add Tenants
            </a>
            <div class="pull-right">
             
            </div>
        </div>
    </div>
</div>


{{ Form::open(['route' => 'property-manager.send-mail', 'method' => 'post', 'id' => 'upload-tenants-form']) }}
<div class="col-md-12 send-mail-to-tenant" style="display: none;">
    <div class="card">
        <div class="card-body">
            <div class="form-group">
                {{ Form::text('subject', "NOTICE: Setup Rental Payment", ['class' => 'form-control', 'placeholder' => 'Subject']) }}
                @if ($errors->has('subject'))
                <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                @endif
            </div>
            <div class="form-group">
                <textarea name="template" id="upload-tenant-default-template">
                    @include('property-manager.upload-tenant.default-template')
                </textarea>
                @if ($errors->has('template'))
                <strong class="text-danger">{{ $errors->first('template') }}</strong>
                @endif
            </div>
        </div>
        <div class="card-footer">
            {{ Form::submit('Send', ['class' => 'btn btn-primary pull-right']) }}
            <a href="#" class="btn btn-danger" onclick="toggle_send_mail_to_tenant();">Cancel</a>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <a href="{{ route('property-manager.upload-tenants.index') }}" class="btn btn-success" style=" color: #fff;background-color: #775b3e; border-color: #775b3e;">View All Tenants</a>                    
                <a href="{{ route('property-manager.upload-tenants.index', ['action' => 'filter', 'mail' => 'sent_mail']) }}" class="btn btn-primary">Email Sent</a>                    
                <a href="{{ route('property-manager.upload-tenants.index', ['action' => 'filter', 'mail' => 'failed_mail']) }}" class="btn btn-warning" style="color: #ffffff; background-color: #d9534f; border-color: #d9534f;">Email Not Sent</a>
                <div class="pull-right">
                    <a href="#" class="btn btn-secondary" onclick="toggle_send_mail_to_tenant();">Send Email</a>
                    <a href="#" class="btn btn-danger" onclick="delete_uploaded_tenant();">Delete</a>
                </div>                 
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="upload-tenant-application-table">
                    <thead>
                        <th><input type="checkbox" id="select-all-checkbox"></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>Email Status</th>
                        <th>Created</th>
                        <th class="no-sort">Action</th>
                    </thead>
                    <tbody>
                        @foreach ($tenants as $tenant)
                        <tr>
                            <td><input type="checkbox" name="users[]" value="{{ $tenant->id }}"></td>
                            <td>{{ $tenant->lease_name }}</td>
                            <td>{{ $tenant->email }}</td>
                            <td>{{ $tenant->mobile }}</td>
                            <td>{{ $tenant->email_status == 1 ? 'SENT' : 'NOT SENT' }}</td>
                            <td>{{ $tenant->created_at->format('Y/m/d H:i') }}</td>
                            <td>
                                <a href="{{ route('property-manager.upload-tenants.show', $tenant->id) }}" class="btn btn-info btn-sm" title="View Uploaded Tenant">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a href="{{ route('property-manager.upload-tenants.edit', $tenant->id) }}" class="btn btn-primary btn-sm" title="Edit Uploaded Tenant">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ route('property-manager.upload-tenants.delete_uploaded_tenant', ['id' => $tenant->id]) }}" class="btn btn-danger btn-sm" title="Delete Uploaded Tenant" data-method="DELETE">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection

@section('after-scripts')
<script type="text/javascript">
    function toggle_upload_tenant()
    {
        jQuery( ".upload-excel-tenant" ).toggle();
    }

    function toggle_send_mail_to_tenant()
    {
        jQuery( ".send-mail-to-tenant" ).toggle();
    }

    jQuery(document).ready(function () {
        CKEDITOR.replace('upload-tenant-default-template');
    });

    function delete_uploaded_tenant()
    {
        var confirmation = confirm("Are you sure you want to delete?");
        if(confirmation) {
            var url = '/property-manager/delete-uploaded-tenants';
            var data = jQuery('#upload-tenants-form').serialize();
            jQuery.ajax({
                url: url,
                data: data,
                type: "POST",
                success: function (response) {
                    location.reload();
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        else{
            return;
        }
    }
</script>
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#upload-tenant-application-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[5, "desc"]]
        });

        jQuery('#select-all-checkbox').on('change', function () {
            if ($(this).prop('checked') == true) {
                $('input:checkbox').not(this).prop('checked', this.checked);
            } else {
                $('input:checkbox').removeAttr('checked');
            }
        });
    });
</script>
@endsection