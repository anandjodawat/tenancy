<table style="width:100%">
    <tbody>
        <tr>
            <td>
                <p><strong>Changes to your rent payment method</strong></p>
            </td>
        </tr>
    </tbody>
</table>

<p>Dear Tenant,</p>

<p>At {{ auth()->user()->agency_profile()->name }} we constantly review our systems and services to our tenants. With many recent complications around internet banking, we will not be accepting EFT into our trust account. Our office has introduced Rental Rewards, a secure, rewarding, convenient and easy way to pay your rent so that we can continue to offer an electronic payment option. As of Oct 09, 2018, there will be changes to the way you pay rent to us. We will no longer be accepting payments viaEFT. As a part of this change we are also looking at changing our financial institution and thereby need to ensure that the rent collection process is streamlined prior to the change, to ensure that your rent is paid on time to the correct account. What this means is that there will be changes to the way you pay rent to us. We shall be providing the following options to pay your rent:</p>

<p><strong>Ongoing Rent Payments</strong></p>

<p>Our preferred payment method for rent is Rental Rewards. (Please note that there is a small convenience fee charged for the use of the system). These fees are charged by a third-party payment processor &ndash; Rental Rewards</p>

<p>The fees for the convenience of the use of the services are: (must be able to set-up fee at agent level, as there is variation)</p>

<p>a. Bank Account: $2.</p>

<p>b. BPAY: $2.20 inc GST (internet banking using Rental Rewards Biller and Reference).</p>

<p>Please note that any convenience fee charged is for the use of the system. These fees are charged by a third-party payment processor &ndash; Rental Rewards.<br />
    Using Rental Rewards means you turn your rent into rewards. The benefits of Rental Rewards include using the 55 days Interest Free period of your credit card &ndash; and in many instances, earn credit card points from your card provider. If you are a Qantas Frequent Flyer customer you&rsquo;ll also benefit from 1000 instant points from Qantas.<br />
Using Rental Rewards as your method of rent payment ensures that your rent never gets forgotten or allocated incorrectly and is always paid on time.</p>

<p>Other accepted payment methods that do not incur a charge to pay rental to us is by</p>

<p>Cheque</p>

<p>Deduction from Pay (organised through your employer)</p>

<p>Centrepay(organised through Centrelink)</p>

<p>We have provided several options for your convenience and ask that you consider what is most suitable to your circumstances.</p>

<p>If you choose Rental Rewards then kindly complete the link below to setup your ongoing payment.</p>

<p>It is important that you complete your online form no later than Oct 09, 2018 avoid tenants that do the right thing falling into arrears.</p>

<p>As a part of this change we will also be contacting you via phone to assist you with the changeover to a method that best suits your convenience.</p>

<p>If you have any questions or if you need assistance with completing the online form, please contact the office.</p>

<p>Regards,</p>

<p><br />
    {{ auth()->user()->name }}<br />
    <br />
    {{ auth()->user()->email }}<br />
    @if (auth()->user()->agency_profile())
        <a href="{{ auth()->user()->agency_profile()->website }}">{{ auth()->user()->agency_profile()->website }}</a>
    @endif
</p>

@if (auth()->user()->agency_logo())
<!--<table style="width:100px">
    <tbody>
        <tr>
            <td><img src="{{ url('/').auth()->user()->agency_logo() }}" width="100" /></td>
        </tr>
    </tbody>
</table>-->
@endif
