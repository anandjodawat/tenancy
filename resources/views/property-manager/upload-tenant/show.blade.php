@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Upload Tenants</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><b>Tenants</b></li>
                    <li><b>Upload</b></li>
                    <li class="active"><b>Show</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <strong class="card-title mb-3">Uploaded Tenants Details</strong>
        </div>
        <div class="card-body">
            <div class="mx-auto d-block">
                <h5 class="text-sm-center mt-2 mb-1">{{ $tenant->name }}</h5>
                <div class="location text-sm-center"><i class="fa fa-map-marker"></i> {{ $tenant->suburb }} {{ $tenant->state }}, {{ $tenant->post_code }}</div>
            </div>
            <hr>
            <div class="col-md-6 offset-md-3">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <b>Lease Name</b> <span class="pull-right">{{ $tenant->lease_name }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Post Line Code 1</b> <span class="pull-right">{{ $tenant->post_line1 }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Post Line Code 2</b> <span class="pull-right">{{ $tenant->post_line2 }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Post Line Code 3</b> <span class="pull-right">{{ $tenant->post_line3 }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Suburb</b> <span class="pull-right">{{ $tenant->suburb }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>State</b> <span class="pull-right">{{ $tenant->state }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Post Code</b> <span class="pull-right">{{ $tenant->post_code }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Email Address</b> <span class="pull-right">{{ $tenant->email }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Mobile Number</b> <span class="pull-right">{{ $tenant->mobile }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Property Address 1</b> <span class="pull-right">{{ $tenant->property_address1 }}</span>
                    </li> 
                    <li class="list-group-item">
                        <b>Property Address 2</b> <span class="pull-right">{{ $tenant->property_address2 }}</span>
                    </li> 
                    <li class="list-group-item">
                        <b>Property Address 3</b> <span class="pull-right">{{ $tenant->property_address3 }}</span>
                    </li> 
                    <li class="list-group-item">
                        <b>Tenant Code</b> <span class="pull-right">{{ $tenant->tenant_code }}</span>
                    </li>
                </ul>
                <br><br>
                <h3>Property Manager Details</h3>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <b>Property Manager</b> <span class="pull-right">{{ $tenant->property_manager }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Email</b> <span class="pull-right">{{ $tenant->property_manager_email }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Mobile Number</b> <span class="pull-right">{{ $tenant->property_manager_mobile }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>First Name</b> <span class="pull-right">{{ $tenant->property_manager_first_name }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Last Name</b> <span class="pull-right">{{ $tenant->property_manager_last_name }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Rent Amount</b> <span class="pull-right">{{ $tenant->rent_amount1 }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Rent Per</b> <span class="pull-right">{{ $tenant->rent_per1 }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection