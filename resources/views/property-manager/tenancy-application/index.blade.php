@extends('property-manager.layouts.app')

@section('page-header')
    <h1 class="page-title">
        {{ app_name() }}
        <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
 <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                {{-- <div class="page-title">
                    <h1>Tenant Applications</h1>
                </div> --}}
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active"><b>Tenant Applications</b></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                PDF Applications
                            </strong>
                        </div>
                        <div class="card-body">
                           No data
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection