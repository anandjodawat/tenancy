<div class="row form-group {{ $errors->has('tenant_code') ? 'has-error' : '' }}">
    {{ Form::label('forTenantCode', 'Tenant Code', ['class' => 'col-md-4 form-control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('tenant_code', null, ['class' => 'form-control']) }}
        @if ($errors->has('tenant_code'))
            <span class="text-danger">{{ $errors->first('tenant_code') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('title', 'Title', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::select('title', config('tenancy-application.title'), null, ['class' => 'form-control', 'placeholder' => 'Select Title']) }}
        @if ($errors->has('title'))
        <span class="text-danger">{{ $errors->first('title') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('first_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('first_name'))
        <span class="text-danger">{{ $errors->first('first_name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('last_name'))
        <span class="text-danger">{{ $errors->first('last_name') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forDateOfBirth', 'Date Of Birth', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('dob', null, ['class' => 'form-control', 'id' => 'ongoing-payment-datepicker']) }}
        @if ($errors->has('dob'))
        <span class="text-danger">{{ $errors->first('dob') }}</span>
        @endif

    </div>
</div>

<div class="row form-group">
    {{ Form::label('forAddress', 'Address', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('address', null, ['class' => 'form-control']) }}
        @if ($errors->has('address'))
        <span class="text-danger">{{ $errors->first('address') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forSururb', 'Suburb', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('suburb', null, ['class' => 'form-control']) }}
        @if ($errors->has('suburb'))
        <span class="text-danger">{{ $errors->first('suburb') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forState', 'State', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('state', config('tenancy-application.states'), null, ['class' => 'form-control']) }}

    </div>
</div>

<div class="row form-group">
    {{ Form::label('forPostCode', 'Postcode', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('postcode', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forMobileNumber', 'Mobile Number', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('mobile_number', null, ['class' => 'form-control']) }}
        @if ($errors->has('mobile_number'))
        <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forPhoneNumber', 'Phone Number', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('phone_number', null, ['class' => 'form-control']) }}
        @if ($errors->has('phone_number'))
        <span class="text-danger">{{ $errors->first('phone_number') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forEmail', 'Email', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::email('email', null, ['class' => 'form-control']) }}
        @if ($errors->has('email'))
        <span class="text-danger">{{ $errors->first('email') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forSubject', 'Subject', ['class' => 'col-md-4 control-label requiredcls']) }}
    <div class="col-md-6">
        {{ Form::text('subject', null, ['class' => 'form-control']) }}
        @if ($errors->has('subject'))
        <span class="text-danger">{{ $errors->first('subject') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    {{ Form::label('attachment', 'Attachment (Allowed multiple)', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{ Form::file('attachment[]', array('multiple'=>true), ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group">
    {{ Form::label('forBody', 'Body', ['class' => 'col-md-4 control-label']) }}
    @if ($errors->has('body'))
    <span class="text-danger">{{ $errors->first('body') }}</span>
    @endif
    <div class="col-md-6">
        <textarea id="ongoing-payment-body" class="form-control" name="body">
            @if (auth()->user()->agency_profile()->mail_templates && auth()->user()->agency_profile()->mail_templates->where('type', '1')->count() > 0)
                {{ auth()->user()->agency_profile()->mail_templates->where('type', '1')->first()->body }}
            @else
            @include('property-manager.ongoing-payment.default-template')
            @endif         
        </textarea>
    </div>
</div>
