@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Request Ongoing Payments</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }}"><b>Dashboard</b></a></li>
                    <li><a href="{{ route('property-manager.ongoing-payments.index') }}"><b>Request Ongoing Payments</b></a></li>
                    <li class="active">Create</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Request Ongoing Payments</strong>
        </div>
        {{ Form::open(['class' => 'form-horizontal', 'method' => 'post', 'route' => 'property-manager.ongoing-payments.store', 'enctype' => 'multipart/form-data', 'id' => 'apply-prop-form']) }}
        <div class="card-body card-block">
            @include('property-manager.ongoing-payment._form')
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-sm pull-right">
                Submit
            </button>
            <button type="reset" class="btn btn-danger btn-sm">
                Reset
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>

@endsection

@section ('after-scripts')
<script type="text/javascript">
    CKEDITOR.replace('ongoing-payment-body');
    
    jQuery( "#ongoing-payment-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();

    // form validation
    jQuery('#apply-prop-form').validate({
        errorClass: 'error-message',
        rules : {
            // Property Details
            tenant_code : 'required',
            title : 'required',
            first_name : 'required',
            last_name: 'required',
            address: 'required',
            suburb: 'required',
            state : 'required',
            postcode: 'required',
            mobile_number: { required: true, number: true},
            phone_number: { number: true},
            email : 'required',
            subject: 'required',
            body: 'required',
        }
    });

</script>
@endsection
