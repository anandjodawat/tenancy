<div class="card-body">
    <a href="" class="btn btn-primary">
        <i class="fa fa-users"></i>
        View All Tenants
    </a>

    <a href="" class="btn btn-primary">
        <i class="fa fa-users"></i>
        Email Sent
    </a>

    <a href="" class="btn btn-primary">
        <i class="fa fa-users"></i>
        Email Not Sent
    </a>

    <div class="pull-right">
        <a href="" class="btn btn-primary">
            <i class="fa fa-file"></i>
            Upload Office Tenants
        </a>

        <a href="" class="btn btn-primary">
            <i class="fa fa-download"></i>
            Upload Excel
        </a>

        <a href="" class="btn btn-primary">
            <i class="fa fa-user"></i>
            Add Tenant
        </a>
    </div>
</div>

