@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
           {{--  <div class="page-title">
                <h1>Mail Outs</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><b>Mail Outs</b></li>
                    <li class="active"><b>Create</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><strong>Mail Outs</strong><small> Form</small></div>
                    <div class="card-body card-block">

                        <div class="row form-group">
                            <div class="col-8">
                                <div class="form-group">
                                    <label for="city" class=" form-control-label">Subject</label>
                                    {{ Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Enter Subject']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea id="mail-outs-form" class="form-control" rows="10">
                                @include('property-manager.mail-outs.default-template')
                            </textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm pull-right">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section ('after-scripts')

<script type="text/javascript">
    CKEDITOR.replace( 'mail-outs-form' );
</script>
@endsection