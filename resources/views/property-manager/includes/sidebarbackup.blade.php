<div id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">
        @if (auth()->user()->hasRole('agency-admin'))
        <li class="{{ active_class(Active::checkUriPattern('agency/dashboard')) }}">
            <a href="{{ route('agency.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                {{ trans('menus.property-manager.sidebar.dashboard') }}
            </a>
        </li>
        @else
        <li class="{{ active_class(Active::checkUriPattern('property-manager/dashboard')) }}">
            <a href="{{ route('property-manager.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                {{ trans('menus.property-manager.sidebar.dashboard') }}
            </a>
        </li>
        @endif
        <li class="menu-item-has-children dropdown show">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>General</a>
            <ul class="sub-menu children dropdown-menu show">
                <li>
                    <i class="menu-icon fa fa-files-o"></i>
                    <a href="{{ route('property-manager.applications.index') }}">Applications</a>
                </li>
                <li>
                <i class="menu-icon fa fa-users"></i>
                    <a href="{{ route('property-manager.tenants.index') }}">Tenants</a>
                </li>
            </ul>
        </li>
    </ul>
</div>