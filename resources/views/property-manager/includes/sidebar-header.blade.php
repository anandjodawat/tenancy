<div class="navbar-header" style="background-color: white;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
    </button>
    <a class="navbar-brand text-center" href="/"><img src="/images/logo-final.png" alt="Logo" height="39"></a>
    <a class="navbar-brand hidden" href="./"><img src="/images/logo-final.png" alt="Logo"></a>
</div>
