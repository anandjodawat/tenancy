<div id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">
        @if (auth()->user()->hasRole('agency-admin'))
        <li class="{{ active_class(Active::checkUriPattern('agency/dashboard')) }}">
            <a href="{{ route('agency.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                {{ trans('menus.property-manager.sidebar.dashboard') }}
            </a>
        </li>
        @else
        <li class="{{ active_class(Active::checkUriPattern('property-manager/properties')) }}">
            <a href="{{ route('property-manager.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                {{ trans('menus.property-manager.sidebar.dashboard') }}
            </a>
        </li>
        @endif

        <li class="menu-item-has-children dropdown show">
            <a href="#" class="dropdown-toggle menu-title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>{{ trans('menus.property-manager.sidebar.general') }}</a>
            <ul class="sub-menu children dropdown-menu show">
                <li class="{{ active_class(Active::checkUriPattern('property-manager/applications*')) }}">
                    <i class="menu-icon fa fa-files-o"></i>
                    <a href="{{ route('property-manager.applications.index') }}">Applications</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/tenants*')) }}">
                <i class="menu-icon fa fa-users"></i>
                    <a href="{{ route('property-manager.tenants.index') }}">Tenants</a>
                </li>
            </ul>
        </li>

        

{{--         <li class="{{ active_class(Active::checkUriPattern('property-manager/maintenance-requests*')) }}">
            <a href="{{ route('property-manager.maintenance-requests.index') }}">
                <i class="menu-icon fa fa-users"></i>
                Maintenance Requests
            </a>
        </li> --}}


        <li class="menu-item-has-children dropdown {{ active_class(Active::checkUriPattern('property-manager/properties*'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/prospective*'), 'show') }}">
            <a href="#" class="dropdown-toggle menu-title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>Manage</a>
            <ul class="sub-menu children dropdown-menu {{ active_class(Active::checkUriPattern('property-manager/properties*'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/prospective*'), 'show')}}">
                <li class="{{ active_class(Active::checkUriPattern('property-manager/properties*')) }}">
                    <i class="menu-icon fa fa-building-o"></i>
                    <a href="{{ route('property-manager.properties.index') }}">Properties</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/prospective-tenants')) }}">
                <i class="menu-icon fa fa-users"></i>
                    <a href="{{ route('property-manager.prospective-tenants.index') }}">{{-- Contacts / Tenants --}}
                Approved / Applied / Invited Tenants</a>
                </li>
            </ul>
        </li>

        <li class="menu-item-has-children dropdown {{ active_class(Active::checkUriPattern('property-manager/property-invitation/create'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/request-deposits/create'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/ongoing-payments/create'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/upload-tenants*'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/bond-requests*'), 'show') }}">
            <a href="#" class="dropdown-toggle menu-title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>Actions</a>
            <ul class="sub-menu children dropdown-menu {{ active_class(Active::checkUriPattern('property-manager/property-invitation/create'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/request-deposits/create'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/ongoing-payments/create'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/upload-tenants*'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/bond-requests*'), 'show') }}">
                <li class="{{ active_class(Active::checkUriPattern('property-manager/property-invitation/create')) }}">
                    <i class="menu-icon fa fa-envelope-o"></i>
                    <a href="{{ route('property-manager.property-invitation.create') }}">Invitation to Apply for Property</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/request-deposits/create')) }}">
                <i class="menu-icon fa fa-dollar"></i>
                    <a href="{{ route('property-manager.request-deposits.create') }}">{{ trans('menus.property-manager.sidebar.request-deposit') }}</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/ongoing-payments/create')) }}">
                    <i class="menu-icon fa fa-credit-card"></i>
                    <a href="{{ route('property-manager.ongoing-payments.create') }}">{{ trans('menus.property-manager.sidebar.request-ongoing-payment') }}</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/bond-requests/create')) }}">
                    <i class="menu-icon fa fa-ticket"></i>
                    <a href="{{ route('property-manager.bond-requests.create') }}">Bond Request</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/upload-tenants*')) }}">
                <i class="menu-icon fa fa-cloud-upload"></i>
                    <a href="{{ route('property-manager.upload-tenants.index') }}">Mail Outs</a>
                </li>
            </ul>
        </li>

        
{{--         <li class="{{ active_class(Active::checkUriPattern('property-manager/deposit-sales/create')) }}">
            <a href="{{ route('property-manager.deposit-sales.create') }}">
                <i class="menu-icon fa fa-dollar"></i>
                <span>Request Any Deposit</span>
            </a>
        </li> --}}
       {{--  <li class="{{ active_class(Active::checkUriPattern('property-manager/moving-service-applications')) }}">
            <a href="{{ route('property-manager.moving-service-applications.index') }}">
                <i class="menu-icon fa fa-files-o"></i>
                <span>Moving Service Applications</span>
            </a>
        </li> --}}
{{-- 
        <li class="{{ active_class(Active::checkUriPattern('property-manager/personalized-apply-now-link')) }}">
            <a href="{{ route('property-manager.personalized-apply-now-link') }}">
                <i class="menu-icon fa fa-files-o"></i>
                <span>Personalized Apply Now Link</span>
            </a>
        </li> --}}


        <li class="menu-item-has-children dropdown {{ active_class(Active::checkUriPattern('property-manager/total-applications'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/property-invitation'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/request-deposits'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/ongoing-payments'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/connection-services*'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/bond-requests'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/tenancy-applications'), 'show') }}
        ">
            <a href="#" class="dropdown-toggle menu-title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>Reports</a>
            <ul class="sub-menu children dropdown-menu {{ active_class(Active::checkUriPattern('property-manager/total-applications'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/property-invitation'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/request-deposits'), 'show') }} {{ active_class(Active::checkUriPattern('property-manager/ongoing-payments'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/connection-services*'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/bond-requests'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/tenancy-applications'), 'show') }}
            ">
                <li class="{{ active_class(Active::checkUriPattern('property-manager/total-applications')) }}">
                    <i class="menu-icon fa fa-files-o"></i>
                    <a href="{{ route('property-manager.total-applications.index') }}">Total Applications</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/property-invitation')) }}">
                <i class="menu-icon fa fa-envelope-o"></i>
                    <a href="{{ route('property-manager.property-invitation.index') }}">Invitation Sent</a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/request-deposits')) }}">
                    <i class="menu-icon fa fa-dollar"></i>
                    <a href="{{ route('property-manager.request-deposits.index') }}">
                        <span>Request Deposit Sent</span>
                    </a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/ongoing-payments')) }}">
                    <i class="menu-icon fa fa-credit-card"></i>
                    <a href="{{ route('property-manager.ongoing-payments.index') }}">
                        <span>Request Ongoing Payment Sent</span>
                    </a>
                </li>
                {{-- <li class="{{ active_class(Active::checkUriPattern('property-manager/deposit-sales')) }}">
                    <a href="{{ route('property-manager.deposit-sales.index') }}">
                        <i class="menu-icon fa fa-dollar"></i>
                        <span>Request Any Deposit Sent</span>
                    </a>
                </li> --}}
                <li class="{{ active_class(Active::checkUriPattern('property-manager/connection-services*')) }}">
                    <i class="menu-icon fa fa-flash"></i>
                 <a href="{{ route('property-manager.connection-services.index') }}">
                    <span>Connection Service Requested</span>
                </a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('property-manager/bond-requests')) }}">
                    <i class="menu-icon fa fa-ticket"></i>
                    <a href="{{ route('property-manager.bond-requests.index') }}">
                        <span>Bond Request Sent</span>
                    </a>
                </li>
              {{--   <li class="{{ active_class(Active::checkUriPattern('property-manager/upload-tenants')) }}">
                    <a href="{{ route('property-manager.upload-tenants.index') }}">
                        <i class="menu-icon fa fa-envelope-o"></i>
                        <span>Emails sent to Uploaded Tenants</span>
                    </a>
                </li> --}}
                <li class="{{ active_class(Active::checkUriPattern('property-manager/tenancy-applications')) }}">
                    <i class="menu-icon fa fa-download"></i>
                     <a href="{{ route('property-manager.tenancy-pdf-application.index') }}" target="_blank">
                        <span>{{ trans('menus.property-manager.sidebar.application-pdf') }}</span>
                    </a>
                </li>

            </ul>
        </li>

        <li class="menu-item-has-children dropdown 
            {{ active_class(Active::checkUriPattern('agency/property-managers*'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/agency/profile*'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/agency/mail-templates*'), 'show') }}
            {{ active_class(Active::checkUriPattern('settings*'), 'show') }}
        ">
            <a href="#" class="dropdown-toggle menu-title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>Settings</a>
            <ul class="sub-menu children dropdown-menu
            {{ active_class(Active::checkUriPattern('agency/property-managers*'), 'show') }}
            {{ active_class(Active::checkUriPattern('agency/profile*'), 'show') }}
            {{ active_class(Active::checkUriPattern('agency/mail-templates*'), 'show') }}
            {{ active_class(Active::checkUriPattern('property-manager/settings*'), 'show') }}
            ">
                @if (auth()->user()->hasRole('agency-admin'))
                <li class="{{ active_class(Active::checkUriPattern('agency/property-managers*')) }}">
                    <i class="menu-icon fa fa-users"></i>
                    <a href="{{ route('agency.property-managers.index') }}">
                        Property Managers
                    </a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('agency/profile*')) }}">
                    <i class="menu-icon fa fa-odnoklassniki"></i>
                    <a href="{{ route('agency.profile.show', auth()->user()->id) }}">
                        Agency Profile
                    </a>
                </li>
                <li class="{{ active_class(Active::checkUriPattern('agency/mail-templates*')) }}">
                    <i class="menu-icon fa fa-bookmark-o"></i>
                    <a href="{{ route('agency.mail-templates.index') }}">
                        Mail Templates
                    </a>
                </li>
                @endif
                <li class="{{ active_class(Active::checkUriPattern('property-manager/settings*')) }}">
                    <i class="menu-icon fa fa-cog"></i>
                    <a href="{{ route('property-manager.settings.index') }}">
                         Moving Service Setting
                    </a>
                </li>
            </ul>
        </li>

        @if (auth()->user()->hasRole('administrator'))
        <li class="menu-item-has-children dropdown {{ active_class(Active::checkUriPattern('agency/contact-us'), 'show') }}">
            <a href="#" class="dropdown-toggle menu-title" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>Contact Us</a>
            <ul class="sub-menu children dropdown-menu {{ active_class(Active::checkUriPattern('agency/contact-us'), 'show') }}">
                <li class="{{ active_class(Active::checkUriPattern('agency/contact-us')) }}">
                    <i class="menu-icon fa fa-envelope-o"></i>
                    <a href="{{ route('agency.contact-us.index') }}">
                        Messages
                    </a>
                </li>
            </ul>
        </li>
        @endif

    </ul>
</div>