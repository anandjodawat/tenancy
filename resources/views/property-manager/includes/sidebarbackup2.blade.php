<div id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">
        @if (auth()->user()->hasRole('agency-admin'))
        <li class="{{ active_class(Active::checkUriPattern('agency/dashboard')) }}">
            <a href="{{ route('agency.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                {{ trans('menus.property-manager.sidebar.dashboard') }}
            </a>
        </li>
        @else
        <li class="{{ active_class(Active::checkUriPattern('property-manager/dashboard')) }}">
            <a href="{{ route('property-manager.dashboard') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                {{ trans('menus.property-manager.sidebar.dashboard') }}
            </a>
        </li>
        @endif
        <h3 class="menu-title">{{ trans('menus.property-manager.sidebar.general') }}</h3>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/applications*')) }}">
            <a href="{{ route('property-manager.applications.index') }}">
                <i class="menu-icon fa fa-files-o"></i>
                Applications
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/tenants*')) }}">
            <a href="{{ route('property-manager.tenants.index') }}">
                <i class="menu-icon fa fa-users"></i>
                Tenants
            </a>
        </li>

{{--         <li class="{{ active_class(Active::checkUriPattern('property-manager/maintenance-requests*')) }}">
            <a href="{{ route('property-manager.maintenance-requests.index') }}">
                <i class="menu-icon fa fa-users"></i>
                Maintenance Requests
            </a>
        </li> --}}

        <h3 class="menu-title">Manage</h3>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/properties*')) }}">
            <a href="{{ route('property-manager.properties.index') }}">
                <i class="menu-icon fa fa-building-o"></i>
                Properties
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('contacts')) }}">
            <a href="{{ route('property-manager.prospective-tenants.index') }}">
                <i class="menu-icon fa fa-users"></i>
                {{-- Contacts / Tenants --}}
                Approved / Applied / Invited Tenants
            </a>
        </li>
        <h3 class="menu-title">Actions</h3>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/property-invitation/create')) }}">
            <a href="{{ route('property-manager.property-invitation.create') }}">
                <i class="menu-icon fa fa-envelope-o"></i>
                <span>Invitation to Apply for Property</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/request-deposits/create')) }}">
            <a href="{{ route('property-manager.request-deposits.create') }}">
                <i class="menu-icon fa fa-dollar"></i>
                <span>{{ trans('menus.property-manager.sidebar.request-deposit') }}</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/ongoing-payments/create')) }}">
            <a href="{{ route('property-manager.ongoing-payments.create') }}">
                <i class="menu-icon fa  fa-credit-card"></i>
                <span>{{ trans('menus.property-manager.sidebar.request-ongoing-payment') }}</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/upload-tenants*')) }}">
            <a href="{{ route('property-manager.upload-tenants.index') }}">
                <i class="menu-icon fa fa-cloud-upload"></i>
                <span>Mail Outs</span>
            </a>
        </li>
        
{{--         <li class="{{ active_class(Active::checkUriPattern('property-manager/deposit-sales/create')) }}">
            <a href="{{ route('property-manager.deposit-sales.create') }}">
                <i class="menu-icon fa fa-dollar"></i>
                <span>Request Any Deposit</span>
            </a>
        </li> --}}
       {{--  <li class="{{ active_class(Active::checkUriPattern('property-manager/moving-service-applications')) }}">
            <a href="{{ route('property-manager.moving-service-applications.index') }}">
                <i class="menu-icon fa fa-files-o"></i>
                <span>Moving Service Applications</span>
            </a>
        </li> --}}
{{-- 
        <li class="{{ active_class(Active::checkUriPattern('property-manager/personalized-apply-now-link')) }}">
            <a href="{{ route('property-manager.personalized-apply-now-link') }}">
                <i class="menu-icon fa fa-files-o"></i>
                <span>Personalized Apply Now Link</span>
            </a>
        </li> --}}

        <li class="menu-title">Reports</li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/total-applications')) }}">
            <a href="{{ route('property-manager.total-applications.index') }}">
                <i class="menu-icon fa fa-files-o"></i>
                <span>Total Applications</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/property-invitation')) }}">
            <a href="{{ route('property-manager.property-invitation.index') }}">
                <i class="menu-icon fa fa-envelope-o"></i>
                <span>Invitation Sent</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/request-deposits')) }}">
            <a href="{{ route('property-manager.request-deposits.index') }}">
                <i class="menu-icon fa fa-dollar"></i>
                <span>Request Deposit Sent</span>
            </a>
        </li>
        {{-- <li class="{{ active_class(Active::checkUriPattern('property-manager/deposit-sales')) }}">
            <a href="{{ route('property-manager.deposit-sales.index') }}">
                <i class="menu-icon fa fa-dollar"></i>
                <span>Request Any Deposit Sent</span>
            </a>
        </li> --}}
        <li class="{{ active_class(Active::checkUriPattern('property-manager/ongoing-payments')) }}">
            <a href="{{ route('property-manager.ongoing-payments.index') }}">
                <i class="menu-icon fa fa-credit-card"></i>
                <span>Request Ongoing Payment Sent</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/connection-services*')) }}">
            <a href="{{ route('property-manager.connection-services.index') }}">
                <i class="menu-icon fa fa-flash"></i>
                <span>Connection Service Requested</span>
            </a>
        </li>
        <li class="{{ active_class(Active::checkUriPattern('property-manager/bond-requests')) }}">
            <a href="{{ route('property-manager.bond-requests.index') }}">
                <i class="menu-icon fa fa-ticket"></i>
                <span>Bond Request Sent</span>
            </a>
        </li>
          {{--   <li class="{{ active_class(Active::checkUriPattern('property-manager/upload-tenants')) }}">
                <a href="{{ route('property-manager.upload-tenants.index') }}">
                    <i class="menu-icon fa fa-envelope-o"></i>
                    <span>Emails sent to Uploaded Tenants</span>
                </a>
            </li> --}}
            <li class="{{ active_class(Active::checkUriPattern('property-manager/tenancy-applications')) }}">
                <a href="{{ route('property-manager.tenancy-pdf-application.index') }}" target="_blank">
                    <i class="menu-icon fa fa-download"></i>
                    <span>{{ trans('menus.property-manager.sidebar.application-pdf') }}</span>
                </a>
            </li>
            <li class="menu-title">Settings</li>
            @if (auth()->user()->hasRole('agency-admin'))
            <li class="{{ active_class(Active::checkUriPattern('agency/property-managers*')) }}">
                <a href="{{ route('agency.property-managers.index') }}">
                    <i class="menu-icon fa fa-users"></i>
                    Property Managers
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('agency/profile*')) }}">
                <a href="{{ route('agency.profile.show', auth()->user()->id) }}">
                    <i class="menu-icon fa fa-odnoklassniki"></i>
                    Agency Profile
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('agency/mail-templates*')) }}">
                <a href="{{ route('agency.mail-templates.index') }}">
                    <i class="menu-icon fa fa-bookmark-o"></i>
                    Mail Templates
                </a>
            </li>

            @endif

            @if (auth()->user()->hasRole('administrator'))
            <li class="menu-title">Contact Us</li>
            <li class="{{ active_class(Active::checkUriPattern('agency/contact-us')) }}">
                <a href="{{ route('agency.contact-us.index') }}">
                    <i class="menu-icon fa fa-envelope-o"></i>
                    Messages
                </a>
            </li>
            @endif

            <li class="{{ active_class(Active::checkUriPattern('property-manager/settings*')) }}">
                <a href="{{ route('property-manager.settings.index') }}">
                    <i class="menu-icon fa fa-cog"></i>
                    Moving Service Setting
                </a>
            </li>
        </ul>
    </div>

