@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
          {{--   <div class="page-title">
                <h1>Property</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li class="active"><b>Property</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <a href="#" class="btn btn-primary col-md-2" onclick="toggle_upload_properties();">
                <i class="fa fa-file-excel-o"></i>
                <br>
                Upload Properties
            </a>
            <a href="{{ route('property-manager.properties.create') }}" class="btn btn-success col-md-2" style="color: #ffffff; background-color: #d9534f; border-color: #d9534f;">
                <i class="fa fa-plus"></i>
                <br>
                Add New Property
            </a>
            <div class="pull-right">
             
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 upload-excel-properties" style="display: none;">
    <div class="card">
        <div class="card-body">
            {{ Form::open(['enctype' => 'multipart/form-data', 'method' => 'post', 'route' => 'property-manager.properties.parse-properties']) }}
            <div class="form-group">
                <div class="col-md-4">
                    {{ Form::file('file', ['class' => 'form-control']) }}
                    @if ($errors->has('file'))
                    <strong class="text-danger">{{ $errors->first('file') }}</strong>
                    @endif
                </div>
                <div class="col-md-8">
                    {{ Form::submit('Upload', ['class' => 'btn btn-success']) }}
                    <a href="#" class="btn btn-danger" onclick="toggle_upload_tenant();">Cancel</a>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Properties
                        </strong>
                        <button class="btn btn-primary btn-sm" id="download-property-report" 
                        style="background: #719759; border: 1px solid #719759;display: none;">Download</button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="property-application-table">
                                <thead>
                                    <th>Address</th>
                                    <th>Created By</th>
                                    <th>Date</th>
                                    <th class="noExl no-sort">Action</th>
                                </thead>
                                <tbody>
                                    @foreach ($properties as $property)
                                    <tr>
                                        <td>{{ $property->address }}, {{ $property->suburb }}
{{ $property->state != null || $property->state == "0" ? config('tenancy-application.states')[$property->state] : '' }} {{ $property->post_code }}
                                        </td>
                                        <td>{{ $property->createdBy->name }}</td>
                                        <td>{{ $property->created_at->format('Y/m/d H:i') }}</td>
                                        <td class="noExl">
                                    {{-- <a href="{{ route('property-manager.properties.show', $property->id) }}" class="btn btn-sm btn-info"  title="Details">
                                        <i class="fa fa-eye"></i>
                                    </a> --}}
                                    <a href="{{ route('property-manager.properties.edit', $property->id) }}" class="btn btn-sm btn-primary"  title="Edit Property">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ route('property-manager.properties.delete-properties', ['id' => $property->id]) }}" class="btn btn-sm btn-danger"  title="Delete Property" data-method="DELETE">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>

</div>
</div>
@endsection

@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>

<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        /*jQuery('#download-property-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Property",
                filename: "property" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
                title: 'Data export'
            });
            console.clear();
        });*/
        jQuery('#property-application-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[2, "desc"]],
            "dom": 'Bfrtip',
            "buttons": [
                { 
                  extend: 'excel',
                  text: 'Download',
                  className: 'downloadButton',
                  title: 'Tenancy Application Properties',
                  filename: "properties-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  exportOptions: {
                    columns: [ 0, 1, 2 ]
                    },
                    /*customize: function( xlsx ) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];
         
                        jQuery('row c[r=A1]', sheet).attr( 's', '2' ); //bold
                        //jQuery('row:eq(0) c', sheet).attr( 's', '51' );  //center
                    }*/
                } 
            ]
        });
        //jQuery('#download-property-report').hide();
    });

    function toggle_upload_properties()
    {
        jQuery( ".upload-excel-properties" ).toggle();
    }
</script>
@endsection