<div class="row form-group {{ $errors->has('address') ? 'has-error' : '' }}">
        {{ Form::label('address', 'Property address for invite', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('address', null, ['class' => 'form-control']) }}

            @if ($errors->has('address'))
            <span class="text-danger">{{ $errors->first('address') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('suburb') ? 'has-error' : '' }}">
        {{ Form::label('suburb', 'Suburb', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('suburb', null, ['class' => 'form-control']) }}
            @if ($errors->has('suburb'))
            <span class="text-danger">{{ $errors->first('suburb') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('state') ? 'has-error' : '' }}">
        {{ Form::label('state', 'State', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::select('state', config('tenancy-application.states'), null, ['class' => 'form-control', 'placeholder' => 'Select State']) }}
            @if ($errors->has('state'))
            <span class="text-danger">{{ $errors->first('state') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('post_code') ? 'has-error' : '' }}">
        {{ Form::label('post_code', 'Post Code', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::text('post_code', null, ['class' => 'form-control']) }}
            @if ($errors->has('post_code'))
            <span class="text-danger">{{ $errors->first('post_code') }}</span>
            @endif
        </div>
    </div>

    <div class="row form-group {{ $errors->has('rent_amount') ? 'has-error' : '' }}">
        {{ Form::label('rent_amount', 'Rent Amount ($)', ['class' => 'col-md-4 form-control-label']) }}
        <div class="col-md-6">
            {{ Form::number('rent_amount', null, ['class' => 'form-control']) }}
            @if ($errors->has('rent_amount'))
            <span class="text-danger">{{ $errors->first('rent_amount') }}</span>
            @endif
        </div>
    </div>