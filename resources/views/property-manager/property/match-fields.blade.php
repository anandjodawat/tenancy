@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Match Fields</strong>
        </div>
        {{ Form::open(['route' => 'property-manager.properties.store', 'method' => 'post']) }}
        {{ Form::hidden('type', 'excel') }}
        <div class="card-body card-block">
            <table class="table table-bordered">
                <thead>
                    <th>address</th>
                    <th>suburb</th>
                    <th>state</th>
                    <th>post_code</th>
                    <th>rent_amount</th>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {{ Form::select('address', $excel_fields, array_search('address', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--', 'required']) }}
                        </td>
                        <td>
                            {{ Form::select('suburb', $excel_fields, array_search('suburb', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--', 'required']) }}
                        </td>
                        <td>
                            {{ Form::select('state', $excel_fields, array_search('state', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--', 'required']) }}
                        </td>
                        <td>
                            {{ Form::select('post_code', $excel_fields, array_search('post_code', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--','required']) }}
                        </td>
                        <td>
                            {{ Form::select('rent_amount', $excel_fields, array_search('rent_amount', $excel_fields), ['class' => 'form-control', 'placeholder' => '--select matching field--', 'required']) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('property-manager.properties.index') }}" class="btn btn-danger">Cancel</a>
            {{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
        </div>
        {{ Form::close() }}
    </div>
</div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Your Excel Sample Data</strong>
        </div>
        <div class="card-body card-block">
            <table class="table table-bordered">
                <thead>
                    @foreach (session('excel_fields') as $field)
                    <th>{{ $field }}</th>
                    @endforeach
                </thead>
                <tbody>
                    @foreach (session('data') as $key => $data)
                    @if ($loop->index > 4)
                    @break
                    @endif
                    <tr>
                        @foreach (session('excel_fields') as $fields)
                        <td>
                            {{ $data->$fields }}
                        </td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
</div>


@endsection