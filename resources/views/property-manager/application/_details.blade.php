{{-- tabs --}}
<div class="col-lg-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h4>Application Details</h4>
        </div>
        <div class="card-body">
            <div class="default-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show" data-toggle="tab" href="#nav-property-details"  aria-selected="true">
                            Property
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-agent-specific-details"  aria-selected="false">
                            Agent Specific
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-property-manager-details" aria-selected="false">
                            Property Manager
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-occupancy-details" aria-selected="false">
                            Occupancy Details
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-moving-services" aria-selected="false">
                            Moving Services
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-declaration-details" aria-selected="false">
                            Declaration
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#nav-supporting-documents" aria-selected="false">
                            Supporting Documents
                        </a>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    @include('property-manager.application.includes.tabs.property-details')
                    @include('property-manager.application.includes.tabs.agent-specific-details')
                    @include('property-manager.application.includes.tabs.property-manager-details')
                    @include('property-manager.application.includes.tabs.occupancy-details')
                    @include('property-manager.application.includes.tabs.moving-services')
                    @include('property-manager.application.includes.tabs.declaration-details')
                    @include('property-manager.application.includes.tabs.document-details')
                </div>
            </div>
        </div>
    </div>
</div>