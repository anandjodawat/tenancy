<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            {{-- Email Application --}}
                <a href="{{ route('property-manager.email-application.index', ['application_id' => $application->id]) }}" class="btn btn-primary col-md-2">
                    <i class="fa fa-envelope-o"></i>
                    <br>
                    Email Application
                </a>

            {{-- Forward Application --}}
                <a href="#" class="btn btn-primary col-md-2" onclick="forward_application();">
                    <i class="fa fa-random"></i>
                    <br>
                    Forward Application
                </a>

            {{-- Tenancy Reference --}}
                <a href="#" class="btn btn-primary col-md-2" onclick="reference_application();">
                    <i class="fa fa-home"></i>
                    <br>
                    Tenancy Reference
                </a>

            {{-- Employment Confirmation --}}
                <a href="#" class="btn btn-primary col-md-3" onclick="employment_application();">
                    <i class="fa fa-book"></i>
                    <br>
                    Employment Confirmation
                </a>

            {{-- Download PDF --}}
                <a href="{{ route('property-manager.tenancy-pdf-application.show', [$application->id, 'action' => 'download']) }}" class="btn btn-primary col-md-2" target="_blank">
                    <i class="fa fa-file-pdf-o"></i>
                    <br>
                    Download PDF
                </a>
        </div>
    </div>
</div>


<div class="col-md-12 hide-forward-application-form" id="hide-forward-application-form">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">
                Forward Application
            </strong>
            <a onclick="forward_application();" class="pull-right"><i class="fa fa-window-close"></i></a>
        </div>
            {{ Form::open(['route' => 'property-manager.forward-application', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="card-body">
            <strong>Name of Applicant(s): </strong>{{ strtoupper($application->user->name) }}<br>
            <strong>Application ID: </strong> {{ $application->id }}
            <hr>
            <div class="row form-group">
                <label class="col-md-4 control-label">Email</label>
                <div class="col-md-6">
                    {{ Form::email('send_to', null, ['class' => 'form-control']) }}
                </div>
            </div>
            {{ Form::hidden('application_id', $application->id) }}
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Subject
                </label>
                <div class="col-md-6">
                    {{ Form::text('subject', "Tenancy Application for Consideration", ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Attachment
                </label>
                <div class="col-md-6">
                    {{ Form::file('attachment', ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
        <div class="card-footer">
            {{ Form::submit('submit', ['class' => 'btn btn-primary']) }}
        </div>
            {{ Form::close() }}
    </div>
</div>

<div class="col-md-12 hide-reference-application-form" id="hide-reference-application-form">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">
                History Confirmation Form
            </strong>
            <a onclick="reference_application();" class="pull-right"><i class="fa fa-window-close"></i></a>
        </div>
            {{ Form::open(['route' => 'property-manager.reference-application', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="card-body">
            <strong>Name of Applicant(s): </strong>{{ strtoupper($application->user->name) }}<br>
            <strong>Application ID: </strong> {{ $application->id }}
            <hr>
            <div class="row form-group">
                <label class="col-md-4 control-label">Email</label>
                <div class="col-md-6">
                    {{ Form::email('send_to', null, ['class' => 'form-control']) }}
                </div>
            </div>
            {{ Form::hidden('user_id', $application->user->id) }}
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Subject
                </label>
                <div class="col-md-6">
                    {{ Form::text('subject', "History Confirmation Request", ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Attachment
                </label>
                <div class="col-md-6">
                    {{ Form::file('attachment', ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Body
                </label>
                <div class="col-md-6">
                    <textarea name="body" class="form-control" id="application-reference-textarea">
                        @include('property-manager.application.includes._default_reference_body')
                    </textarea>
                </div>
            </div>
        </div>
        <div class="card-footer">
            {{ Form::submit('submit', ['class' => 'btn btn-primary']) }}
        </div>
            {{ Form::close() }}
    </div>
</div>

<div class="col-md-12 hide-employment-application-form" id="hide-employment-application-form">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">
                Employment Confirmation Form
            </strong>
            <a onclick="employment_application();" class="pull-right"><i class="fa fa-window-close"></i></a>
        </div>
            {{ Form::open(['route' => 'property-manager.employment-application', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="card-body">
            <strong>Name of Applicant(s): </strong>{{ strtoupper($application->user->name) }}<br>
            <strong>Application ID: </strong> {{ $application->id }}
            <hr>
            <div class="row form-group">
                <label class="col-md-4 control-label">Employer's Email</label>
                <div class="col-md-6">
                    {{ Form::email('send_to', null, ['class' => 'form-control']) }}
                </div>
            </div>
            {{ Form::hidden('user_id', $application->user->id) }}
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Subject
                </label>
                <div class="col-md-6">
                    {{ Form::text('subject', "Employment Confirmation Request", ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Attachment
                </label>
                <div class="col-md-6">
                    {{ Form::file('attachment', ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-4 control-label">
                    Body
                </label>
                <div class="col-md-6">
                    <textarea name="body" class="form-control" id="application-employment-textarea">
                        @include('property-manager.application.includes._default_employment_body')
                    </textarea>
                </div>
            </div>
        </div>
        <div class="card-footer">
            {{ Form::submit('submit', ['class' => 'btn btn-primary']) }}
        </div>
            {{ Form::close() }}
    </div>
</div>