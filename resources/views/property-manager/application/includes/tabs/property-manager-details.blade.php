<div class="tab-pane fade" id="nav-property-manager-details" role="tabpanel">
    <ul class="list-group">
        <li class="list-group-item">
            <label class="control-label">Agency Name</label>
            <b> : {{ $application->agency_name }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Property Manager Name</label>
            <b> : {{ $application->property_manager_name }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Property Manager Email</label>
            <b> : {{ $application->property_manager_email }}</b>
        </li>
    </ul>
</div>
