<div class="tab-pane fade active show" id="nav-property-details" role="tabpanel">
    <ul class="list-group">
        <li class="list-group-item">
            <label class="control-label">Property Address Applied For?</label>
            <b> : {{ $application->applied_for }}</b>
        </li> 
        <li class="list-group-item">
            <label class="control-label">State</label>
            <b> : {{ $application->state_name }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Suburb</label>
            <b> : {{ $application->suburb }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Post Code</label>
            <b> : {{ $application->post_code }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Property Type</label>
            <b> : {{ ucwords($application->propertyType()) }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Number of Bedrooms</label>
            <b> : {{ $application->number_of_bedrooms }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Preferred Commencement Date</label>
            <b> : {{ $application->commencement_date }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Length of Lease (months)</label>
            <b> : {{ $application->length_of_lease }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Weekly Rent</label>
            <b> : {{ $application->weekly_rent }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Monthly Rent</label>
            <b> : {{ $application->monthly_rent }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Bond</label>
            <b> : {{ $application->bond }}</b>
        </li> 
        <li class="list-group-item">
            <label class="control-label">Do you require assistance with your bond payment?</label>
            <b> : {{ $application->bond_assistance }}</b>
        </li>
    </ul>
</div>
