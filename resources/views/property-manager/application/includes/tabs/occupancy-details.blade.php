<style type="text/css">
    h4 {
        padding-top: 20px;
    }
</style>
<div class="tab-pane fade" id="nav-occupancy-details" role="tabpanel">
    
    @if(!empty($application->joint_app_id))
    
        <div class="col-md-8">
        <h4>This is a joint application, please <a href="{{ route('property-manager.applications.show', $application->joint_app_id) }}" class="" title="View Primary Application" target="_blank">click here</a> to get Primary application details</h4>
        </div>
    @else
        @foreach ($application->occupants as $occupant)
        <div class="col-md-4">
        <h4>Occupant #{{ $loop->index + 1 }}</h4>
        <ul class="list-group">
            <li class="list-group-item">
                <label class="control-label">Name</label>
                <span class="pull-right">{{ $occupant->occupant_name }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Age</label>
                <span class="pull-right">{{ $occupant->occupant_age }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Relationship</label>
                <span class="pull-right">{{ $occupant->occupantRelationship() }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Will this occupant be on the lease?</label>
                <span class="pull-right">{{ $occupant->occupant_on_lease }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Will this occupant be as a Joint Tenant?</label>
                <span class="pull-right">{{ $occupant->occupant_as_joint_tenant }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Person #{{ $loop->index + 1 }} Email Address</label>
                <span class="pull-right">{{ $occupant->occupant_email }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Person #{{ $loop->index + 1 }} Mobile/Phone</label>
                <span class="pull-right">{{ $occupant->occupant_mobile_number }}</span>
            </li>
        </ul>
        </div>
        @endforeach
    @endif

    <div class="col-md-4">
        @foreach ($application->children as $child)
        <h4>Child #{{ $loop->index + 1 }}</h4>
        <ul class="list-group">
            <li class="list-group-item">
                <label class="control-label">Age</label>
                <span class="pull-right">{{ $child->children_age }}</span>
            </li>
        </ul>
        @endforeach
    </div>

    <div class="col-md-4">
        @foreach ($application->vehicles as $vehicle)
        <h4>Vehicle #{{ $loop->index + 1 }}</h4>
        <ul class="list-group">
            <li class="list-group-item">
                <label class="control-label">Type</label>
                <span class="pull-right">{{ $vehicle->vehicle_type }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Registration Number</label>
                <span class="pull-right">{{ $vehicle->vehicle_registration }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Make/Model</label>
                <span class="pull-right">{{ $vehicle->make_model }}</span>
            </li>
        </ul>
        @endforeach
    </div>

    <div class="col-md-4">
        @foreach ($application->pets as $pet)
        <h4>Pet #{{ $loop->index + 1 }}</h4>
        <ul class="list-group">
            <li class="list-group-item">
                <label class="control-label">Type</label>
                <span class="pull-right">{{ $pet->pet_type }}</span>
            </li>
            <li class="list-group-item">
                <label class="control-label">Breed</label>
                <span class="pull-right">{{ $pet->pet_breed }}</span>
            </li>
        </ul>
        @endforeach
    </div>

</div>
