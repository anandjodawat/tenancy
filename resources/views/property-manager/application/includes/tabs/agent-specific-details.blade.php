<div class="tab-pane fade" id="nav-agent-specific-details" role="tabpanel">
	<ul class="list-group">
		<li class="list-group-item">
			<label class="control-label">Has your tenancy ever been terminated by a landlord or agent?</label>
			<br>
			<span>- {{ ucwords($application->is_tenant_terminated) }}</span>
			@if ($application->is_tenant_terminated == 'yes')
			<br>
			<b>Reason</b> - {{ $application->tenant_terminated_details }}
			@endif
		</li>
		<li class="list-group-item">
			<label class="control-label">Have you ever been refused a property by any landlord or agent?</label>
			<br>
			<span>- {{ ucwords($application->is_tenant_refused) }}</span>
			@if ($application->is_tenant_refused == 'yes')
			<br>
			<b>Reason</b> - {{ $application->tenant_refused_details }}
			@endif
		</li>
		<li class="list-group-item">
			<label class="control-label">Are you in debt to another landlord or agent?</label>
			<br>
			<span>- {{ ucwords($application->is_on_debt) }}</span>
			@if ($application->is_on_debt == 'yes')
			<br>
			<b>Reason</b> - {{ $application->on_debt_details }}
			@endif
		</li>
		<li class="list-group-item">
			<label class="control-label">Have any deductions ever been made from your rental bond?</label>
			<br>
			<span>- {{ ucwords($application->is_deduction_rental_bond) }}</span>
			@if ($application->is_deduction_rental_bond == 'yes')
			<br>
			<b>Reason</b> - {{ $application->deduction_rental_bond_details }}
			@endif
		</li>
		<li class="list-group-item">
			<label class="control-label">Is there any <b>reason</b> known to you that would affect your future rental payments?</label>
			<br>
			<span>- {{ ucwords($application->affect_future_rental_payment) }}</span>
			@if ($application->affect_future_rental_payment == 'yes')
			<br>
			<b>Reason</b> - {{ $application->affect_future_rental_payment_details }}
			@endif
		</li>
		<li class="list-group-item">
			<label class="control-label">Do you have any other applications pending on other properties?</label>
			<br>
			<span>- {{ ucwords($application->is_another_pending_application) }}</span>
		</li>
		<li class="list-group-item">
			<label class="control-label">Do you currently own a property?</label>
			<br>
			<span>- {{ ucwords($application->own_a_property) }}</span>
			@if ($application->own_a_property == 'yes')
			<br>
			<b>Reason</b> - {{ $application->own_a_property_details }}
			@endif
		</li>
		<li class="list-group-item">
			<label class="control-label">Are you considering buying a property after this tenancy or in the near future?</label>
			<br>
			<span>- {{ ucwords($application->buy_another_property) }}</span>
			@if ($application->buy_another_property == 'yes')
			<br>
			Buy - {{ $application->intend_to_buy_after }}
			@endif
		</li>
	</ul>
</div>