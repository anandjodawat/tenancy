<div class="tab-pane fade" id="nav-moving-services" role="tabpanel">
    <ul class="list-group">
        <li class="list-group-item">
            <label class="control-label">Need Moving Service?</label>
            <b> : {{ $application->need_moving_service }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Moving Service Name</label>
            <b> : {{ $application->moving_service_name }}</b>
        </li>
        <li class="list-group-item">
            <label class="control-label">Requested Services List</label>
            <b> : {{ $allservices }}</b>
        </li>
        @if ($application->moving_service_ref_no)
        <li class="list-group-item">
            <label class="control-label">API Reference number</label>
            <b> : {{ $application->moving_service_ref_no }}</b>
        </li>
        @endif
    </ul>
</div>