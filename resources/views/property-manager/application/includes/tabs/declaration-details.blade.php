<div class="tab-pane fade" id="nav-declaration-details" role="tabpanel">
	<ul class="list-group">
		<li class="list-group-item">
			<label class="control-label">Have you inspected this property?</label>
			<br><span>{{ ucwords($application->inspected_this_property) }}</span>
		</li>
		@if ($application->inspected_this_property == 'yes')
		<li class="list-group-item">
			<label class="control-label">Inspected Date</label> 
			<span class="pull-right">{{ $application->inspected_date }}</span>
		</li>
		<li class="list-group-item">
			<label class="control-label">Was the property upon your inspection in a reasonably clean and fair condition?</label>
			<span class="pull-right">{{ ucwords($application->good_condition) }}</span>
		</li>
		<li class="list-group-item">
			<label class="control-label">Inspection Code</label>
			<span class="pull-right">{{ $application->inspection_code }}</span>
		</li>
		@endif
	</ul>
</div>
