<p>Dear Employer,<br />
In order to check whether {{ $application->user->name }} is an employee with you, we require proof of employment.<br />
Please complete the Request for Employment Confirmation by clicking the CONFIRM TENANCY EMPLOYMENT button below.<br />
We attached Privacy Statement Declaration executed by the {{ $application->user->name }}.<br />
Appreciate your cooperation in the matter.</p>
