<p>Dear Agent,<br />
RE: {{ $application->user->name }}<br />
We refer to the above mentioned party who has applied for a new property with {{ auth()->user()->agency_profile()->name }}.<br />
Please complete the Tenancy History Confirmation by clicking on CONFIRM TENANCY HISTORY button below </a>.<br />
We attach Privacy Statement Declaration executed by the {{ $application->user->name }}.<br />
Kindly provide a Rental History Ledger for {{ $application->user->name }} via email to {{ auth()->user()->email }} with the Subject &ldquo;Rental History Ledger - {{ $application->user->name }} &rdquo;.<br />
Appreciate your cooperation in the matter.</p>
