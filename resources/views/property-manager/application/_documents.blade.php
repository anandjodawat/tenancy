<div class="col-md-12">
    <div class="card">
        <div class="card-header">Applicant Documents</div>
        <div class="card-body">
            @foreach ($documents as $document)
            @if ($user->getFirstMedia($document))
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4>{{ strtoupper(str_replace('_', ' ', $document)) }}</h4>
                        <img src="{{ $user->getFirstMedia($document)->getUrl() }}" width="100%">
                        <hr>
                        <!-- Large modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#application_modal_{{ $loop->index }}">View</button>

                        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="application_modal_{{ $loop->index }}" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <img src="{{ $user->getFirstMedia($document)->getUrl() }}" width="100%">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>