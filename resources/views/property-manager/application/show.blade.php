@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Applications</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><b>Applications</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Applicant Status : <span class="{{ App\Models\Tenant\TenantApplication::STATUSBUTTONS[$application->status] }}"> 
                      @php
                          $status=strtoupper($application->status_name);
                      if( $status=='ACCEPTED')
                     {   
                          echo strtoupper($application->status_name).'</span> (Request Deposit)';
                     }
                     elseif ($status=='APPROVED') {
                            echo strtoupper($application->status_name).'</span> (Request Ongoing Payment)';   
                     }
                     else
                     {
                          echo strtoupper($application->status_name);
                     }
                     @endphp
        </span>
        {{ $application->status_name == 'forwarded' && optional($application->forwardedTo)->sent_to ? 'to '.optional($application->forwardedTo)->sent_to : '' }}
        </div>
        <div class="card-body">
            @foreach (App\Models\Tenant\TenantApplication::APPLICATIONSTATUS as $status)
            <a href="{{ route('property-manager.change-application-status', ['application_id' => $application->id, 'status' => $loop->index]) }}" class="{{ App\Models\Tenant\TenantApplication::STATUSBUTTONS[$loop->index] }}">{{ strtoupper($status) }}</a>
            @endforeach
        </div>
    </div>
</div>

@include('property-manager.application.includes.header-buttons')

{{-- @include('property-manager.application._documents') --}}

@include('property-manager.application._details')


@include('property-manager.tenant._profile')

@endsection

@section ('after-scripts')
    <script type="text/javascript">
        CKEDITOR.replace('application-reference-textarea');
        CKEDITOR.replace('application-employment-textarea');
    
        jQuery( "#ongoing-payment-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
        jQuery(document).ready( function () {
            jQuery('.hide-forward-application-form').hide();
            jQuery('.hide-reference-application-form').hide();
            jQuery('.hide-employment-application-form').hide();
        });
        
        function forward_application() {
            jQuery('.hide-forward-application-form').toggle();
            jQuery('.hide-reference-application-form').hide();
            jQuery('.hide-employment-application-form').hide();
        }

        function reference_application()
        {
            jQuery('.hide-reference-application-form').toggle();
            jQuery('.hide-forward-application-form').hide();
            jQuery('.hide-employment-application-form').hide();
        }

        function employment_application()
        {
            jQuery('.hide-employment-application-form').toggle();
            jQuery('.hide-forward-application-form').hide();
            jQuery('.hide-reference-application-form').hide();
        }
    </script>
@endsection
