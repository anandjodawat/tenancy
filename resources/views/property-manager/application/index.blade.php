<?php use \App\Http\Controllers\PropertyManager\ApplicationController; ?>
@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('property-manager.dashboard') }} "><b>Dashboard</b></a></li>
                    <li class="active"><b>Applications</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@include('flash::message')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Applications
                        </strong>
                        <button class="btn btn-primary btn-sm" id="download-application-report" style="display: none;">Download</button>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="dropdown show">
                                    Filter by: <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ request('status') ? ucwords(request('status')) : 'Inbox' }}
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="{{ route('property-manager.applications.index', ['status' => 'inbox']) }}">Inbox</a>

                                        @foreach (App\Models\Tenant\TenantApplication::APPLICATIONSTATUS as $status)
                                        <a class="dropdown-item" href="{{ route('property-manager.applications.index', ['status' => $status]) }}">{{ ucwords($status) }}</a>
                                        @endforeach
                                    </div>
                                </div>              
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tenant-applications-table">
                                <thead>
                                    <th>Address</th>
                                    <th>Tenant Name</th>
                                    <th>Tenant Email</th>
                                    {{-- <th class="noExl">Bondsure Url</th> --}}
                                    <th style="display: none;">Status</th>
                                    <th class="noExl no-sort">Change Status</th>
                                    @if (request('status') == 'forwarded')
                                    <th>Forwarded To</th>
                                    @endif
                                    <th>Date</th>
                                    <th class="noExl no-sort">Joint Application</th>
                                    <th style="display: none;">Joint Application</th>
                                    <th class="noExl no-sort">Action</th>
                                </thead>
                                <tbody>
                                    @foreach ($applications as $application)
                                    @php
                                        $app = App\Models\Tenant\TenantApplication::find($application->id);
                                    @endphp
                                    <tr>
                                        <td>
                                           {{ $app->applied_for }}, {{ $app->suburb }} {{ $app->state_name }} {{ $app->post_code }}
                                        </td>
                                        <td>{{ $application->user->name }}</td>
                                        <td>{{ $application->user->email }}</td>
                                        {{-- <td class="noExl">{{ $application->bondsure_url }}</td> --}}
                                        <td style="display: none;">{{ ucwords($application->status_name) }}</td>
                                        <td class="noExl">
                                            <div class="dropdown show" >
                                                <a class="btn btn-default dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{ ucwords($application->status_name) }}
                                                </a>

                                                <div class="dropdown-menu dropdowninbox" style="top: 35px !important;" aria-labelledby="dropdownMenuLink">
                                                    @foreach (App\Models\Tenant\TenantApplication::APPLICATIONSTATUS as $status)
                                                    <a class="dropdown-item" href="{{ route('property-manager.change-application-status', ['application_id' => $application->id, 'status' => $loop->index]) }}">{{ ucwords($status) }}</a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                        @if (request('status') == 'forwarded')
                                        <td>
                                            {{ optional($application->forwardedTo->sentBy)->name }}
                                        </td>
                                        @endif
                                        <td>{{ $application->created_at->format('Y/m/d H:i') }}</td>
                                        <td class="noExl">
                                            @if($application->joint_app_id!=0)
                                                <a href="{{ route('property-manager.applications.show', $application->joint_app_id) }}" class="btn btn-sm btn-info"  title="View Primary Application" target="_blank">
                                                <i class="fa fa-eye"></i>
                                                </a>
                                            @endif
                                            
                                            @php
                                                $ja_app = ApplicationController::get_joint_appid($application->id);
                                            @endphp
                                            @if(!empty($ja_app[0]->id))
                                            <a href="{{ route('property-manager.applications.show', $ja_app[0]->id) }}" class="btn btn-sm btn-info"  title="View Joint Application" target="_blank">
                                            <i class="fa fa-eye"></i>
                                            </a>
                                            @endif

                                        </td>
                                        <td style="display: none;">@if($application->joint_app_id!=0)
                                            Yes
                                        @else 
                                            No   
                                        @endif
                                        </td>
                                        
                                        <td class="noExl">
                                            <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-sm btn-info"  title="Details">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('property-manager.tenancy-pdf-application.show', $application->id) }}" class="btn btn-primary btn-sm" title="Show PDF" target="_blank">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            <a href="{{ route('property-manager.applications.delete', ['id' => $application->id]) }}" data-method="DELETE" class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        /*jQuery('#download-application-report').on('click', function (){
            jQuery(".table").table2excel({
                exclude: ".noExl",
                name: "Tenancy Application",
                filename: "application" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                fileext: ".xlsx",
                exclude_img: true,
                exclude_links: true,
            });
            console.clear();
        });*/
        jQuery('#tenant-applications-table').DataTable({
            "columnDefs": [ {
              "targets": 'no-sort',
              "orderable": false,
          } ],
          "order": [[5, "desc"]],
          "dom": 'Bfrtip',
            "buttons": [
                { 
                  extend: 'excel',
                  text: 'Download',
                  className: 'downloadButton',
                  title: 'Tenancy Applications',
                  filename: "applications-" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
                  exportOptions: {
                    columns: [ 0, 1, 2,3,5,7 ]
                    }
                } 
            ]
        });
    });
</script>
<style type="text/css">
.dropdown-menu.show {
    margin-top: 38px;
}

@media screen and (max-width: 768px) {

    .manager_search_block{
        margin-top: 10px;
        max-width: 300px;
    }
}
</style>
@endsection

