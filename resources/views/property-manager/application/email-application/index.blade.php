@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Email - Applications</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Email - Applications</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Email Application
        </div>
        {{ Form::open(['class' => 'form-horizontal', 'method' => 'post', 'route' => 'property-manager.email-application.store']) }}
        <div class="card-body">
            @include('property-manager.application.email-application._form')
        </div>
        <div class="card-footer">
            <a href="{{ route('property-manager.applications.show', $application->id) }}" class="btn btn-danger" title="Cancel">Cancel</a>
            {{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
        </div>
        {{ Form::close() }}
    </div>
</div>

@endsection
