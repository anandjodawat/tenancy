<div class="row form-group">
    {{ Form::label('applicant_name', 'Name Of Applicant', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('applicant_name', $application->user->name, ['class' => 'form-control', 'readonly' => true]) }}
    </div>
</div>

<div class="row form-group">
    {{ Form::label('application_id', 'Property Applying for : (App ID. )', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('application_id', $application->id, ['class' => 'form-control', 'readonly' => true]) }}
    </div>
</div>

<div class="row form-group">
    {{ Form::label('send_to', 'Send to', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::email('sent_to', $application->user->email, ['class' => 'form-control']) }}
        @if ($errors->has('sent_to'))
            <span class="text-danger">{{ $errors->first('sent_to') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col-md-10">
        {{ Form::select('notify_for', config('tenancy-application.email-application-notify'), null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group">
    {{ Form::label('subject', 'Subject', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('subject', null, ['class' => 'form-control']) }}
         @if ($errors->has('subject'))
            <span class="text-danger">{{ $errors->first('subject') }}</span>
        @endif
    </div>
</div>
{{-- 
<div class="row form-group">
    {{ Form::label('attachment', 'Attachment', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::file('attachment', ['class' => 'form-control']) }}
    </div>
</div>

 --}}