<h4 class="text-center">
    <b>Maintenance / Repairs Required</b>
</h4>
<hr>
<div class="form-group">
    {{ Form::label('street_address', 'Street Address', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('street_address', null, ['class' => 'form-control']) }}
        @if ($errors->has('street_address'))
            <span class="text-danger">{{ $errors->first('street_address') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('suburb', 'Suburb', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('suburb', null, ['class' => 'form-control']) }}
        @if ($errors->has('suburb'))
            <span class="text-danger">{{ $errors->first('suburb') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('state', 'State', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('state', config('tenancy-application.states'), null, ['class' => 'form-control', 'placeholder' => '--select state--']) }}
        @if ($errors->has('state'))
            <span class="text-danger">{{ $errors->first('state') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('post_code', 'Post Code', ['class' => 'col-md-4 control-label']) }}
    <div class="col-md-6">
        {{ Form::text('post_code', null, ['class' => 'form-control']) }}
        @if ($errors->has('post_code'))
            <span class="text-danger">{{ $errors->first('post_code') }}</span>
        @endif
    </div>
</div>