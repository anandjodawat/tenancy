<hr>
<h4 class="text-center">
	<b>Details of Maintenance Request</b>
</h4>
<hr>

<div class="form-group">
	{{ Form::label('details', 'Maintenance Details', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::textarea('details', null, ['class' => 'form-control', 'rows' => '4']) }}
		@if ($errors->has('details'))
			<span class="text-danger">{{ $errors->first('details') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('preferred_date', 'Preferred Date', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('preferred_date', null, ['class' => 'form-control', 'id' => 'preferred-date-datepicker' ]) }}
		@if ($errors->has('preferred_date'))
			<span class="text-danger">{{ $errors->first('preferred_date') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('preferred_time', 'Preferred Time', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('preferred_time', null, ['class' => 'form-control', 'id' => 'preferred-timepicker']) }}
		@if ($errors->has('preferred_time'))
			<span class="text-danger">{{ $errors->first('preferred_time') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('maintenance_existed', 'How Long has this Maintenance Existed?', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('maintenance_existed', null, ['class' => 'form-control']) }}
		@if ($errors->has('maintenance_existed'))
			<span class="text-danger">{{ $errors->first('maintenance_existed') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('file1', 'Photo Attachment 1', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::file('file1', ['class' => 'form-control']) }}
		@if ($errors->has('file1'))
			<span class="text-danger">{{ $errors->first('file1') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('file2', 'Photo Attachment 2', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::file('file2', ['class' => 'form-control']) }}
		@if ($errors->has('file2'))
			<span class="text-danger">{{ $errors->first('file2') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('property_manager_email', 'Property Manager Email', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::email('property_manager_email', null, ['class' => 'form-control']) }}
		@if ($errors->has('property_manager_email'))
			<span class="text-danger">{{ $errors->first('property_manager_email') }}</span>
		@endif
	</div>
</div>
