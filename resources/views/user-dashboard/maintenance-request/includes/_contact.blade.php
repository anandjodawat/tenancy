<hr>
<h4 class="text-center">
	<b>Contact Details</b>
</h4>
<hr>
<div class="form-group">
	{{ Form::label('tenant', 'Are You The Tenant', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		Yes {{ Form::radio('tenant', 'yes') }}
		No {{ Form::radio('tenant', 'no') }}
		@if ($errors->has('tenant'))
			<span class="text-danger">{{ $errors->first('tenant') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('name', null, ['class' => 'form-control']) }}
		@if ($errors->has('name'))
			<span class="text-danger">{{ $errors->first('name') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('mobile_phone', 'Mobile Phone', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('mobile_phone', null, ['class' => 'form-control']) }}
		@if ($errors->has('mobile_phone'))
			<span class="text-danger">{{ $errors->first('mobile_phone') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('work_phone', 'Work Phone', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('work_phone', null, ['class' => 'form-control']) }}
		@if ($errors->has('work_phone'))
			<span class="text-danger">{{ $errors->first('work_phone') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('home_phone', 'Home Phone', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('home_phone', null, ['class' => 'form-control']) }}
		@if ($errors->has('home_phone'))
			<span class="text-danger">{{ $errors->first('home_phone') }}</span>
		@endif
	</div>
</div>

<div class="form-group">
	{{ Form::label('email', 'Email Address', ['class' => 'col-md-4 control-label']) }}
	<div class="col-md-6">
		{{ Form::text('email', null, ['class' => 'form-control']) }}
		@if ($errors->has('email'))
			<span class="text-danger">{{ $errors->first('email') }}</span>
		@endif
	</div>
</div>



