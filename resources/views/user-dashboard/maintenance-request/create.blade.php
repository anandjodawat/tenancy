@extends('layouts.app')

@section('content')
@include('flash::message')

<div class="col-lg-12">
    <h1 class="page-header">Maintenance Request</h1>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-md-12" style="margin-bottom: 15px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Maintenance Request
                    </div>
                    {{ Form::open(['route' => 'user-dashboard.maintenance-requests.store', 'method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
                    <div class="panel-body">
                        @include('user-dashboard.maintenance-request.includes._address')
                        @include('user-dashboard.maintenance-request.includes._contact')
                        @include('user-dashboard.maintenance-request.includes._details')
                    </div><!--panel body-->
                    <div class="panel-footer">
                        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
                    </div>
                    {{ Form::close() }}
                </div><!-- panel -->
            </div><!-- col-xs-12 -->
        </div><!-- row -->
    </div><!-- col-md-10 -->
</div><!-- row -->
@endsection
@section('after-scripts')

<script type="text/javascript">
            
          jQuery( "#preferred-date-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}"}).val();
          jQuery( "#preferred-timepicker" ).datetimepicker(  {format: 'HH:mm:ss'});
</script>
@endsection