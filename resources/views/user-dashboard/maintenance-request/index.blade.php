@extends('layouts.app')

@section('content')

@include('flash::message')

<div class="col-lg-12">
    <h2 class="page-header new-application-title">View Maintenance Request</h2>
</div>

@foreach ($maintenanceRequest as $request)
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-body tenancyapplication-status">
            <strong>
                {{ $request->street_address }}, {{ $request->suburb }} {{ config('tenancy-application.states')[$request->state] }} {{ $request->post_code }}
            </strong>
            <hr>
            Date: {{ $request->created_at->diffForHumans() }}
            <span class="pull-right badge badge-secondary">{{ $request->status_name }}</span>
            <hr>
            <div class="clearfix"></div>
            <div class="pull-right">
                <a href="{{ route('user-dashboard.maintenance-requests.edit', $request->id) }}" title="Edit" class="btn btn-primary btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{ route('user-dashboard.maintenance-requests.destroy', $request->id) }}" title="Edit" class="btn btn-danger btn-xs" disabled="">
                    <i class="fa fa-trash"></i>
                </a>
                {{-- <a href="{{ route('user-dashboard.maintenance-requests.show', $request->id) }}" title="Show" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a> --}}
            </div>
        </div>
    </div>
</div>
</div>
@endforeach

@endsection

<script type="text/javascript">
       
          jQuery( "#prefered-date-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#personal-information-datepicker" ).timepicker().val();
</script>
