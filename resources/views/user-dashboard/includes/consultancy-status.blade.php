@if (!$valid_consultancy)
    <div class="alert alert-warning">
      <strong>Warning!</strong> {{ $message }}
    </div>
@endif