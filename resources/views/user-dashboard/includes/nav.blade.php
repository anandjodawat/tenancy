<nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="background: #FFF;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <a class="navbar-brand" href="/">
                    <img src="/images/logo-final.png" alt="Logo" height="39" style="margin-top: -10px;">
                    {{-- <span>Tenancy</span>
                    Application --}}
                </a>
                <ul class="nav navbar-top-links navbar-right tenancy-navbar">
                    @if (! $logged_in_user)
                    <li class="dropdown">
                        <a href="{{ route('frontend.auth.login') }}">Login</a>
                    </li>
                    <li class="dropdown">
                        <a href="{{ route('frontend.auth.register') }}">Register</a>
                    </li>
                    @else
                    <li class="dropdown">
                        <a class="ten-nav-btn" href="{{ route('frontend.user.dashboard') }}">
                            {{ trans('navs.frontend.dashboard') }}
                        </a>
                    </li>
                    <li class="dropdown" style="background: #fff;">
                        <a class="ten-welcome-user" href="#" data-toggle="dropdown" href="#">
                            Welcome <strong>{{ auth()->user()->name }}</strong>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            @if (Auth::user()->hasRole('property-manager'))
                                <li>{{ link_to_route('property-manager.dashboard', trans('navs.frontend.user.administration')) }}</li>
                            @endif

                            @if (Auth::user()->hasRole('administrator'))
                                <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                            @endif
                            
                            @if (Auth::user()->hasRole('agency-admin'))
                                <li>{{ link_to_route('agency.dashboard', trans('navs.frontend.user.administration')) }}</li>
                            @endif

                            {{-- <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li> --}}

                            <li>
                                <a href="/account">My Account</a>
                            </li>
                            
                            @if (Auth::user()->agency)
                                <li>{{ link_to_route('frontend.user.agency-details.create', trans('navs.frontend.user.agency'), [], ['class' => active_class(Active::checkRoute('frontend.user.agency-details.create')) ]) }}</li>
                            @endif
                            
                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                        </ul>

                    </li>
                    @endif
                </ul>
            </div>
        </div><!-- /.container-fluid -->
    </nav>