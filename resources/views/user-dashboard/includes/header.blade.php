<div style="margin-top: 20px;"></div>
<style type="text/css">
    .vcenter{
        display: inline-block;
        vertical-align: middle;
        float: none;
    }
</style>
<div class="row text-center">
    <a href="{{ route('user-dashboard.applications.create') }}">
        <div class="col-md-4">
            <div class="panel panel-default top-panel {{ Request::is('applications/create') ? 'active' : '' }}">
                <div class="panel-body">
                    <div class="row">
                        <div class="vcenter">
                            <i class="fa fa-file"></i>
                            <h3>New Application</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ route('user-dashboard.applications.index') }}">
        <div class="col-md-4">
            <div class="panel panel-default top-panel {{ Request::is('applications/*') && !Request::is('applications/create') || Request::is('applications') ? 'active' : '' }}">
                <div class="panel-body">
                    <div class="row" >
                        <div class="vcenter">
                            <i class="fa fa-eye"></i>
                            <h3>View Application</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ route('user-dashboard.profile.index') }}">
        <div class="col-md-4">
            <div class="panel panel-default top-panel {{ Request::is('profile') ? 'active' : '' }}">
                <div class="panel-body">
                    <div class="row">
                        <div class="vcenter">
                            <i class="fa fa-user"></i>
                            <h3>Profile</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>