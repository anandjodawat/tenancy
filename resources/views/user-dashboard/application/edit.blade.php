@extends('layouts.app')

@section('content')
<div class="col-lg-12">
    <h2 class="page-header new-application-title">Edit Application</h2>
</div>

<div class="col-lg-12 col-md-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <b>Note: </b>Profile must be completed in order to apply for the properties.
        </div>
    </div>

    @if ($errors->count())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <i class="fa fa-close"></i>
        &nbsp{{ $error }}
        <br>
        @endforeach
    </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-body tenent-application-form">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                   <div class="stepwizard-step">
                        <a href="#tenant-property-details" type="button" class="btn btn-primary btn-circle"><i class="fa fa-home" aria-hidden="true"></i></a>
                        <p>Property Details</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#tenant-occupancy-details" type="button" class="btn btn-default btn-circle" {{-- disabled="disabled" --}}><i class="fa fa-users" aria-hidden="true"></i></a>
                        <p>Occupancy Details</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#tenant-agent-specific-details" type="button" class="btn btn-default btn-circle" {{-- disabled="disabled" --}}><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                        <p>Agent Specific</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#tenant-moving-service-details" type="button" class="btn btn-default btn-circle" {{-- disabled="disabled" --}}><i class="fa fa-truck" aria-hidden="true"></i></a>
                        <p>Moving Service</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#tenant-identification-details" type="button" class="btn btn-default btn-circle" {{-- disabled="disabled" --}}>
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </a>
                        <p>Identification</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#tenant-rental-rewards-details" type="button" class="btn btn-default btn-circle" {{-- disabled="disabled" --}}>
                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        </a>
                        <p>Rental Rewards</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#tenant-declaration-details" type="button" class="btn btn-default btn-circle" {{-- disabled="disabled" --}}>
                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        </a>
                        <p>Declaration</p>
                    </div>
                </div>
            </div>

            {{ Form::model($application, ['method' => 'patch', 'route' => ['user-dashboard.applications.update', $application->id], 'id' => 'tenant-application-form-edit', 'enctype' => 'multipart/form-data']) }}
            @include('user-dashboard.application.includes._property_details')
            @include('user-dashboard.application.includes._edit_occupancy_details')
            @include('user-dashboard.application.includes._agent_specific')
            @include('user-dashboard.application.includes._moving_service')
            @include('user-dashboard.application.includes._identification')
            @include('user-dashboard.application.includes._rental_rewards')
            @include('user-dashboard.application.includes._declaration')
            {{ Form::close() }}

        </div>
    </div>
</div>

@endsection

@section('after-scripts')

<script src="http://benalman.com/code/projects/jquery-throttle-debounce/jquery.ba-throttle-debounce.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        jQuery( "#commencement-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
        jQuery( "#inspection-date-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
       agent_specific_hide_show();
        hide_and_show_inspected_this_property();
        auto_bond_and_rent();
        agency_logo();

       var relationships = ['aunty','brother','colleague','cousin','daughter','defacto','employer','father','father in law','friend','grand daughter','grand son','grand father','grand mother','husband','lawyer','manager','mother','mother in law','nephew','niece','non immediate relative','partner','previous employer','sister','son','supervisor','uncle','wife','other'
       ];

       var navListItems = $('div.setup-panel div a'),
       allWells = $('.setup-content'),
       allNextBtn = $('.nextBtn');

       allWells.hide();

       navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
        $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

       allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input[type='text'],input[type='url']"),
        isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
            isValid = true;
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });
        $('input[name=weekly_rent]').on('change', function () {
            auto_bond_and_rent();
       });

       $('div.setup-panel div a.btn-primary').trigger('click');

        // occupancy persons details
        var number_of_occupant = $('input[name=number_of_occupant]').val().length === 0 ? "0" : $('input[name=number_of_occupant]').val();
        $('input[name=number_of_occupant]').on('change', function () {
            var value = $(this).val();
            if (value > number_of_occupant) {
                for (var i = number_of_occupant; i < value; i++) {
                    var html = '<div class="occupancy-number-of-person col-md-6">\
                    <h3>Person #'+ (parseInt(i) + 1) +'</h3>\
                    <div class="form-group"><br>\
                    <label for="name" class="control-label">Name</label>\
                    <input type="text" name="occupant_name[]" class="form-control">\
                    </div>\
                    <div class="form-group"><br>\
                    <label for="age" class="control-label">Age</label>\
                    <input type="text" name="occupant_age[]" class="form-control">\
                    </div>\
                    <div class="form-group"><br>\
                    <label for="relationship" class="control-label">Relationship</label>\
                    <select name="occupant_relationship[]" class="form-control occupant-relationship"><option value="">--select relationship--</option></select>\
                    </div>\
                    <div class="form-group"><br>\
                    <label for="occupant_on_lease" class="control-label">Will this occupant be on the lease?</label>\
                    <select name="occupant_on_lease[]" class="form-control">\
                    <option value="yes">YES</option>\
                    <option value="no">NO</option>\
                    </select>\
                    </div>\
                    </div>';
                    $('.tenant-occupancy-details-person').append(html);

                    $.each(relationships , function( index, value ) {
                        // console.log(value);
                        var html = '<option value="'+index+'">'+value+'</option>';
                        $('.occupant-relationship').append(html);
                    });
                }
            }

            if (value < number_of_occupant) {
                $('.occupancy-number-of-person:gt('+(parseInt(value) - 1)+')').remove();
            }

            if (value == "0") {
                $('.occupancy-number-of-person').remove();
            }
            number_of_occupant = value;
        });

        $.each(relationships , function( index, value ) {
            var html = '<option value="'+index+'">'+value+'</option>';
            $('.occupant-relationship').append(html);
        });

        // occupancy children details
        var number_of_children = $('input[name=number_of_children]').val().length === 0 ? "0" : $('input[name=number_of_children]').val();
        $('input[name=number_of_children]').on('change', function () {
            var value = $(this).val();
            if (value > number_of_children) {
                for (var i = number_of_children; i < value; i++) {
                    var html = '<div class="occupancy-number-of-children col-md-6">\
                    <h3>Children #'+ (parseInt(i) + 1) +'</h3>\
                    <div class="form-group"><br>\
                    <label for="age" class="control-label">Age</label>\
                    <input type="text" name="children_age[]" class="form-control min="0" max="15">\
                    </div>\
                    </div>';

                    $('.tenant-occupancy-details-children').append(html);
                }
            }
            if (value < number_of_children) {
                $('.occupancy-number-of-children:gt('+(parseInt(value) - 1)+')').remove();
            }

            if (value == "0") {
                $('.occupancy-number-of-children').remove();
            }
            number_of_children = value;
        });

        // Occupancy vehicle details
        var number_of_vehicles = $('input[name=number_of_vehicles]').val().length === 0 ? "0" : $('input[name=number_of_vehicles]').val();
        $('input[name=number_of_vehicles]').on('change', function () {
            var value = $(this).val();
            if (value > number_of_vehicles) {
                for (var i = number_of_vehicles; i < value; i++) {
                    var html = '<div class="occupancy-number-of-vehicles col-md-6">\
                    <h3>Vehicle #'+ (parseInt(i) + 1) +'</h3>\
                    <div class="form-group"><br>\
                    <label for="type" class="control-label">Vehicle Type</label>\
                    <input type="text" name="vehicle_type[]" class="form-control">\
                    </div>\
                    <div class="form-group"><br>\
                    <label for="registration" class="control-label">Registration Number</label>\
                    <input type="text" name="vehicle_registration[]" class="form-control">\
                    </div>\
                    <div class="form-group"><br>\
                    <label for="make_model" class="control-label">Make/Model</label>\
                    <input type="text" name="make_model[]" class="form-control">\
                    </div>\
                    </div>';
                    $('.tenant-occupancy-details-vehicles').append(html);
                }
            }
            if (value < number_of_vehicles) {
                $('.occupancy-number-of-vehicles:gt('+(parseInt(value) - 1)+')').remove();
            }

            if (value == '0') {
                $('.occupancy-number-of-vehicles').remove();
            }
            number_of_vehicles = value;
        });

        //Occupancy pet details
        var number_of_pets = $('input[name=number_of_pets]').val().length === 0 ? "0" : $('input[name=number_of_pets]').val();
        $('input[name=number_of_pets]').on('change', function () {
            var value = $(this).val();
            if (value > number_of_pets) {
                for (var i = number_of_pets; i < value; i++) {
                    var html = '<div class="occupancy-number-of-pets col-md-6">\
                    <h3>Pet #'+ (parseInt(i) + 1) +'</h3>\
                    <div class="form-group"><br>\
                    <label for="type" class="control-label">Pet Type</label>\
                    <input type="text" name="pet_type[]" class="form-control">\
                    </div>\
                    <div class="form-group"><br>\
                    <label for="breed" class="control-label">Breed</label>\
                    <input type="text" name="pet_breed[]" class="form-control">\
                    </div>\
                    </div>';

                    $('.tenant-occupancy-details-pets').append(html);
                }
            }
            if (value < number_of_pets) {
                $('.occupancy-number-of-pets:gt('+(parseInt(value) - 1)+')').remove();
            }
            if (value == '0') {
                $('.occupancy-number-of-pets').remove();
            }
            number_of_pets = value;
        });

        // Agent specific
        $('.agent-specific-radio-button').on('click', function () {
            hide_and_show_agent_details_field($(this));
        });

        function agent_specific_hide_show()
        {
            $( ".agent-specific-radio-button" ).each(function() {
                hide_and_show_agent_details_field($(this));
            });
        }

        function hide_and_show_agent_details_field(selected_radio_button)
        {
            var value = selected_radio_button.val();
            var currDiv = selected_radio_button.parents('.form-group');
            if (value == 'yes') {
                currDiv.next('.form-group').removeClass('hide');
            } else {
                currDiv.next('.form-group').addClass('hide');
            }

        }

        // Declaration
        $('input[name=inspected_this_property]').on('change', function () {
            hide_and_show_inspected_this_property();
            // if ($(this).val() == 'yes') {
            //     $('.hide-if-inspected-property-no').removeClass('hide');
            // } else {
            //     $('.hide-if-inspected-property-no').addClass('hide');
            // }
        });

        function hide_and_show_inspected_this_property()
        {
            var value = $('input[name=inspected_this_property]:checked').val();
            if (value == 'yes') {
                $('.hide-if-inspected-property-no').removeClass('hide');
            } else {
                $('.hide-if-inspected-property-no').addClass('hide');
            }
        }

        // form validation
        $('#tenant-application-form').validate({
            errorClass: 'error-message',
            rules : {
                // Property Details
                applied_for : 'required',
                state : 'required',
                suburb : 'required',
                post_code : 'required',
                property_type : 'required',
                number_of_bedrooms : 'required',
                commencement_date : 'required',
                length_of_lease : 'required',
                weekly_rent : 'required',
                monthly_rent : 'required',
                bond : 'required',
            // bond_assistance : 'required', 
            property_found_in : 'required',

            // Property Manager Details
            agency_name : 'required',
            property_manager_name : 'required',
            property_manager_email : 'required',

            // Occupancy Details
            number_of_occupant : 'required',
            number_of_children : 'required',
            number_of_vehicles : 'required',
            number_of_pets : 'required',

            // Agent Specific
            is_tenant_terminated : 'required', 
            tenant_terminated_details : 'required',
            is_tenant_refused : 'required', 
            tenant_refused_details : 'required',
            is_on_debt : 'required', 
            on_debt_details : 'required',
            is_deduction_rental_bond : 'required', 
            deduction_rental_bond_details : 'required',
            affect_future_rental_payment : 'required', 
            affect_future_rental_payment_details : 'required',
            is_another_pending_application : 'required', 
            own_a_property : 'required', 
            own_a_property_details : 'required',
            buy_another_property : 'required', 
            intend_to_buy_after : 'required',

            // Moving Service
            need_moving_service : 'required', 
            moving_service_agreement : 'required', 

            // Property Declaration
            inspected_this_property : 'required', 
            inspection_date : 'required',
            good_condition : 'required', 

            // Licence and aggreement
            licence_and_agreement : 'required'
        }
    });
    });

    function submit_identification(value)
    {   
        event.preventDefault();
        var data = new FormData();
        data.append('file', $('input[name='+value+']')[0].files[0]);
        data.append('name', value);

        $.ajax({
        url: "{{ route('user-dashboard.save-application-media') }}",
        processData: false,
        contentType: false,
        type: 'POST',
        data: data,
        success: function ( data ) {
            $('input[name='+value+']').siblings('.col-md-12').remove();
            $('input[name='+value+']').parent('.panel-body').append('<div class="col-md-12"><span class="text-success">'+data+'</span></div>');
        }
        });
    }

    function save_as_draft()
    {
        event.preventDefault();

        $.ajax({
            method: 'PATCH',
            url: "{{ route('user-dashboard.save-as-draft') }}",
            data: $('#tenant-application-form-edit').serialize(),
            success:function (response) {
                window.location.replace("{{ route('user-dashboard.applications.index') }}");
            }
        });
    }

    function auto_bond_and_rent()
    {
        
        var value = $('input[name=weekly_rent]').val();
        $('input[name=monthly_rent]').val((value * 4) + (value * 0.4));
        $('input[name=bond]').val(value * 4);
    }

    $('#input-agency-name-list').on('keyup',  $.debounce(1000, function (e) {
        e.preventDefault();
        searchAgency();
    }));

    function searchAgency () {
        // e.preventDefault();
        var value = $('#input-agency-name-list').val();
        if (value.length > 2) {
            $.ajax({
                url:'../../agency-ajax-list?value='+value,
                method: 'get',
                success:function (response) {
                    $('#input-agency-list').empty();
                    $.each(response, function (index) {
                        var html = "<option value='"+response[index].name+"' data-id='"+response[index].id+"'></option>";
                        $('#input-agency-list').append(html);
                    });
                }
            });
        }
    }

    $('input[name=agency_name]').on('change', function (e) {
        e.preventDefault();
        var agencyName = $('#input-agency-name-list').val();
        $.ajax({
            url: '../../agency-property-manager-list?agency_name='+agencyName,
            method: 'get',
            success:function (response) {
                $('#input-property-manager-list').empty();
                    $.each(response, function (index) {
                        var html = "<option value='"+response[index].name+"' data-id='"+response[index].id+"'></option>";
                        $('#input-property-manager-list').append(html);
                    });
            }
        });
        agency_logo();
    });

    $('input[name=property_manager_name]').on('change', function (e) {
        e.preventDefault();
        var propertyManager = $(this).val();
        var userId = $('option[value="'+propertyManager+'"]').data('id');
        if (userId > 0) {
            $.ajax({
                url: '../../agency-property-manager-email?user_id='+userId,
                method: 'get',
                success:function (response) {
                    $('input[name=property_manager_email]').val(response);
                }
            });
        }
    });

    function agency_logo()
    {
        if ("{{ request('agency_name') }}") {
            var agencyName = "{{ request('agency_name') }}";
        } else {
            var agencyName = $('input[name=agency_name]').val();
        }
        if (agencyName.length > 0) {
            $.ajax({
                url: '../agency-application-details?agency_name='+agencyName,
                method: 'get',
                success:function (response) {
                    // console.log(response);
                    $('.remove-application-agency-logo').remove();
                    $('.remove-utility-connection-logo').remove();

                    if (response.logo.length) {
                        var html = '<div class="remove-application-agency-logo"><br><img src="'+response.logo+'" width="30%"></div>';
                       $('input[name=property_manager_email]').parents('.form-group').append(html);
                    }

                    if (response.supporting_documents.length) {
                        $('#default-agency-application-supporting-document').addClass('hide');
                        $('.agency-application-supporting-document').append('<div class="agency-supporting-document">'+response.supporting_documents+'</div>');
                    } else {
                        $('#default-agency-application-supporting-document').removeClass('hide');
                        $('.agency-supporting-document').remove();
                    }

                    if (response.lease_terms != null && response.lease_terms.length) {
                        $('#default-agency-application-lease-terms').addClass('hide');
                        $('.agency-application-lease-terms').append('<div class="agency-lease-terms">'+response.lease_terms+'</div>');
                    } else {
                        $('#default-agency-application-lease-terms').removeClass('hide');
                        $('.agency-lease-terms').remove();
                    }

                    if (response.utility_connection != null && response.utility_connection.length) {
                        $('#utility-connection-logo').append('<div class="remove-utility-connection-logo img img-responsive"><br><img src="'+response.utility_connection+'" style="max-height: 100px;"></div>');
                    } else {
                        $('.remove-utility-connection-logo').remove();
                    }
                }
            });
        }
    }
</script>
@endsection
