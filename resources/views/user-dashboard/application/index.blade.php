@extends('layouts.app')

@section('content')

@include('flash::message')

<div class="col-lg-12">
    <h2 class="page-header new-application-title">
        View Application
        @if (request('type') == 'draft')
            <small>(Draft)</small>
        @elseif (request('type') == 'sent')
            <small>(Sent)</small>
        @elseif (request('type') == 'accepted')
            <small>(Accepted)</small>
        @elseif (request('type') == 'approved')
            <small>(Approved)</small>
        @else
        @endif
    </h2>
</div>
@if ($applications->count() < 1)
    <div class="panel panel-default">
        <div class="panel-body">
            No Applications Submitted
        </div>
    </div>
@endif
@foreach ($applications as $application)
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-body tenancyapplication-status">
            <strong>
                {{ $application->number_of_bedrooms == 0 ? '' : $application->number_of_bedrooms }} {{ $application->number_of_bedrooms ? "Bedroom flat" : "" }}
            </strong>
            <p>{{ $application->applied_for }}, {{ $application->suburb }}, {{ $application->state_name }} {{ $application->post_code }}</p>
            <hr>
            <p>Added: {{ $application->created_at->format('Y/m/d H:i') }}</p>
            
            <div class="pull-left">
                <span class="badge bg-{{ $application->draft == '1' ? 'warning' : 'success' }}">
                    {{ $application->draft == '1' ? 'Draft' : 'Complete' }}
                </span>
            </div>
            <div class="pull-right applic-edit-btn">
                <a href="{{ route('user-dashboard.applications.show', $application->id) }}" class="btn btn-primary btn-circle" title="View Application"> 
                    <i class="fa fa-eye"></i>
                </a>
                @if ($application->draft == "1")
                <a href="{{ route('user-dashboard.applications.edit', $application->id) }}" class="btn btn-info btn-circle" title="Edit Application">
                    <i class="fa fa-edit"></i>
                </a>
                @else
                <a href="{{ route('user-dashboard.applications.show', [$application->id, 'view' => 'pdf']) }}" class="btn btn-info btn-circle" title="View Application PDF" target="_blank">
                    <i class="fa fa-file-pdf-o"></i>
                </a>
                @endif
                <a href="{{ route('user-dashboard.applications.delete', ['id' => $application->id]) }}" class="btn btn-danger btn-circle" title="Delete Application">
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection

