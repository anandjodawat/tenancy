<div class="setup-content" id="tenant-identification-details">
    <br>
    <div class="col-md-12">
        <div class="row" style="padding: 15px;">
            <label class="control-label"> Identification and supporting documentation</label>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i> <strong>Note!</strong> Please click on upload after choosing your document.
        </div>
    </div>
            
    <div class="col-md-9 ten-identification thirty-pixel">

        <div class="agency-application-supporting-document">
            
            <div id="default-agency-application-supporting-document">
                <p>We require the following information to be supplied to process an application123:</p>
                <ul>
                    <li>40 points* Primary/Photo ID. or National card</li>
                    <li>20 points* proof of address bank statement, utility bill, motor vehicle registration.(Eg. Birth Certificate, Student Card, Medicare Card, Health Care Card, Vehicle Registration)</li>
                    <li>15 points Rental History/tenent ledger or rental reference/older referene</li>
                    <li>15 points* Employment/Proof of Income. (Eg. Payslips, Letter of Employment, Employment Reference)</li>
                    <li>10 points for lease Reference</li>
                </ul>
            </div>
        </div>

        <div class="col-md-6 supporting-document-0 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Primary/Photo ID</div>
                <div class="panel-body">
                    {{ Form::file('primary_photo', ['class' => 'form-control', 'accept' => 'image/*' ]) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('primary_photo')">Upload</button>
                    @if (auth()->user()->getFirstMedia('primary_photo'))
                    <span id="sec_primary_photo">
                    <img src="{{ auth()->user()->getFirstMedia('primary_photo')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('primary_photo')"></i>
                    </span>
                    @endif 
                </div>
            </div>
        </div>

        <div class="col-md-6 supporting-document-1 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Secondary ID</div>
                <div class="panel-body">
                    {{ Form::file('secondary_photo', ['class' => 'form-control', 'accept' => 'image/*']) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('secondary_photo')">Upload</button>
                    @if (auth()->user()->getFirstMedia('secondary_photo'))
                    <span id="sec_secondary_photo">
                    <img src="{{ auth()->user()->getFirstMedia('secondary_photo')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('secondary_photo')"></i>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6 supporting-document-2 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Rental History/Proof of Address</div>
                <div class="panel-body">
                    {{ Form::file('proof_of_address', ['class' => 'form-control', 'accept' => 'image/*']) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('proof_of_address')">Upload</button>
                    @if (auth()->user()->getFirstMedia('proof_of_address'))
                    <span id="sec_proof_of_address">
                    <img src="{{ auth()->user()->getFirstMedia('proof_of_address')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('proof_of_address')"></i>
                    </span>
                    @endif 
                </div>
            </div>
        </div>

        <div class="col-md-6 supporting-document-3 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Employment/Proof of Income</div>
                <div class="panel-body">
                    {{ Form::file('proof_of_income', ['class' => 'form-control', 'accept' => 'image/*']) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('proof_of_income')">Upload</button>
                    @if (auth()->user()->getFirstMedia('proof_of_income'))
                    <span id="sec_proof_of_income">
                    <img src="{{ auth()->user()->getFirstMedia('proof_of_income')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('proof_of_income')"></i>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6 supporting-document-4 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Copy of Previous Rental Receipts</div>
                <div class="panel-body">
                    {{ Form::file('previous_rental_receipt', ['class' => 'form-control', 'accept' => 'image/*']) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('previous_rental_receipt')">Upload</button>
                    @if (auth()->user()->getFirstMedia('previous_rental_receipt'))
                    <span id="sec_previous_rental_receipt">
                    <img src="{{ auth()->user()->getFirstMedia('previous_rental_receipt')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('previous_rental_receipt')"></i>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6 supporting-document-5 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Copy of Previous Lease Agreement</div>
                <div class="panel-body">
                    {{ Form::file('previous_lease_agreement', ['class' => 'form-control', 'accept' => 'image/*']) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('previous_lease_agreement')">Upload</button>
                    @if (auth()->user()->getFirstMedia('previous_lease_agreement'))
                    <span id="sec_previous_lease_agreement">
                    <img src="{{ auth()->user()->getFirstMedia('previous_lease_agreement')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('previous_lease_agreement')"></i>
                    </span>
                    @endif 
                </div>
            </div>
        </div>

        <div class="col-md-6 supporting-document-6 supporting-document-default">
            <div class="panel panel-default">
                <div class="panel-heading">Others</div>
                <div class="panel-body">
                    {{ Form::file('others', ['class' => 'form-control', 'accept' => 'image/*']) }}
                    <br>
                    <button class="btn btn-primary btn-xs upload-btn" onclick="submit_identification('others')">Upload</button>
                    @if (auth()->user()->getFirstMedia('others'))
                    <span id="sec_others">
                    <img src="{{ auth()->user()->getFirstMedia('others')->getUrl() }}" style="width:70px;  margin-top: -23px;"> <i class="fa fa-check" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#008000;"></i>
                    <i class="fa fa-remove" aria-hidden="true" style="font-size:22px; margin-top: -10px;color:#ff0000;cursor: pointer;" onclick="remove_identification('others')"></i>
                    </span>
                    @endif 
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <button class="btn btn-primary prev-button pull-left">Previous</button>
        <button class="btn btn-primary nextBtn next-button btn-lg pull-right" type="button" >Next</button>
    </div>
</div>