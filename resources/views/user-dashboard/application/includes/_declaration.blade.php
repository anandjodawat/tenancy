<div class="setup-content" id="tenant-declaration-details">
    <div class="col-md-9 thirty-pixel">
        
        
<h3>Terms and Conditions</h3>
<div class="pre-scrollable">
    <p><span>1. Tenancy Privacy Statement / Collection Notice</span></p>

    <p><span>Due to the changes in the Privacy Laws, all property managers must ensure that you (the applicant) fully understand the National Privacy Principles and the way they must use your personal information to carry out their role as professional property managers. </span></p>

    <p><span>The information, personal or otherwise, provided by the prospective tenant in this application or that which is collected from other sources is necessary for the agent to assess the risk in providing you with the tenancy, to identify the applicant’s identity and to process, evaluate and manage the tenancy. </span></p>

    <p><span>The personal information collected about you (the applicant) in this application may be disclosed, by use of the internet or otherwise, to other parties, including: </span></p> <ul type="disc">  <li><span>The      Landlord</span></li>
        <li><span>Trades      People</span></li>
        <li><span>Financial      Institutions</span></li>
        <li><span>Government      and Statutory bodies</span></li>
        <li><span>Referees</span></li>
        <li><span>Solicitors</span></li>
        <li><span>Property      Evaluators</span></li>
        <li><span>Existing or      potential clients of the agent</span></li>
        <li><span>Rental Bond      Authorities</span></li>
        <li><span>Tenant      Databases</span></li>
        <li><span>Other Real      Estate Agents</span></li>
        <li><span>Other Third      Parties as required by law</span></li>
        <li><span>Collection      Agents</span></li>
        <li><span>Verification Services</span></li>
        <li><span>Other      Landlords</span></li>
        <li><span>Body      Corporates</span></li> </ul> <p><span>Information already held on tenancy databases may also be disclosed to the Agent and/or landlord. Unless you advise the Agent to the contrary, the Agent may also disclose such information to The Real Estate Institute of your State and to NTD, TRA, TICA, RP DATA, BARCLAY MIS or DATAKATCH for the purpose of documenting all leasing data in the area for the benefit of its members as part of membership services and for others in the property related industries, and so as to assist them in continuing to provide the best possible service to their clients. In providing this information, you (the applicant) agree to its use, unless you advise the Agent differently. </span></p>

        <p><span>The privacy policy of your State's Real Estate Institute can be viewed by logging on to www.reia.asn.com.au and selecting your State. </span></p>

        <p><span>The privacy policy of NTD can be viewed by logging on to </span><span><a target="_blank" href="https://www.ntd.net.au"><span>https://www.ntd.net.au</span></a></span> or contact them on <span>NTD – 1300 563 826</span></p>

        <p><span>The privacy policy of TRA can be viewed by logging on to </span><span><a target="_blank" href="http://tradingreference.com/"><span>http://tradingreference.com</span></a></span> or contact them on <span>TRA – (02) 9363 9244</span> </p>

        <p><span>The privacy policy of TICA can be viewed by logging on to </span><span><a target="_blank" href="https://www.tica.com.au/"><span>https://www.tica.com.au/</span></a></span> or contact them on <span>TICA – 1902 220 346</span> </p>

        <p><span>The privacy policy of BARCLAY MIS can be viewed on </span><span><a target="_blank" href="http://barclaymis.com.au"><span>http://barclaymis.com.au</span></a></span> or contact them on <span>BARLCAY MIS – 1300 883 916</span> </p>

        <p><span>The privacy policy of DATAKATCH can be viewed on </span><span><a target="_blank" href="http://datakatch.com.au/"><span>http://datakatch.com.au</span></a></span> or contact them on <span>DATAKATCH – (02) 9086 9388</span></p>



        <p><span>The Agent will only disclose information in this way to other parties to achieve the purposes specified above or as allowed under the Privacy Act. </span></p>

        <p><span>If you (the applicant) would like to access this information you can do so by contacting the Agent at the address and contact numbers for the property you are interested in renting. You (the applicant) can also correct this information if it is inaccurate, incomplete or out of date. </span></p>

        <p><span>If your personal information is not provided to the Agent and you (the applicant) do not consent to the use of this information as specified above, the Agent cannot carry out their duties and may not be able to provide you with the lease/tenancy of the premises. </span></p><p><span>2. Tenant Declaration</span></p>
        <ul type="disc" style="width:100% !important;">
            <li><span>I acknowledge      that this is an application to lease the property for which I am applying      and that my application is subject to the owner's approval and the      availability of the premises on the due date. No action will be taken      against the landlord or agent should the premises not be ready for      occupation on the due date or if my application is unsuccessful. </span></li>
            <li><span>I acknowledge      that the processing period for my application could be up to 2 working      days and in some circumstances longer. Unless contacted earlier by staff      from the real estate agent in question - I will expect this time frame. </span></li>
            <li><span>I acknowledge      that the landlord and landlord's agent will rely on the truth of my      answers in assessing the application for tenancy </span></li>
            <li><span>I hereby      offer to rent the property from the owner under a lease to be prepared by      the Agent pursuant to the Residential Tenancies Act. </span></li>
            <li><span>I acknowledge      that I will be required to pay rent and a rental bond subject to the      conditions of the Agent </span></li>
            <li><span>I acknowledge      that an inquiry, independent or otherwise, may be made on all applicants      applying for this property, to verify the validity of the personal details      that have been supplied and to check my credit worthiness. If I default      under a rental agreement, the Agent may disclose details of any such      default to any person whom the Agent reasonably considers has an interest      receiving such information. </span></li>
            <li><span>I/we have      been given the opportunity to view a copy of the standard terms and      conditions that would be included in a lease, should my application be      successful </span></li>
            <li><span>I declare      that all information contained in this application is true and correct and      given of my own free will and can be based as fact. </span></li>
            <li><span>I acknowledge that the agent in question cannot confirm that any phone lines to the property are operable or able to be reconnected. I understand that’s it’s the tenants responsibility to check with the telephone provider before proceeding with the tenancy to confirm the situation with the telephone line. Ensuring the main switch is in the off position for power connection remains the responsibly of the tenant .</span></li>
        </ul>                                              
    </div>

        <div class="form-group">
            <label class="control-label requiredcls">
            {{ Form::checkbox('licence_and_agreement', 'yes') }}
                By ticking this box I acknowledge that I have Read, Understood and Agree with the above Tenancy Privacy Statement / Collection Notice & Tenant Declaration and I authorise the use of my digital signature for the purpose of this application
            </label>
            <br>
            @if ($errors->has('licence_and_agreement'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>
        <button class="btn prev-button  btn-primary pull-left" onclick="save_as_draft()">Save As Draft</button>
        <button class="btn next-button btn-success pull-right" type="submit">Submit</button>
        {{-- <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button> --}}
    </div>
</div>