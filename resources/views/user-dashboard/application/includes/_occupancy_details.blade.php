<div class="row setup-content" id="tenant-occupancy-details">
    <div class="col-md-9 thirty-pixel">

        {{-- Occupancy Details --}}
        <div class="tenant-occupancy-details-person row" style="margin-right: 10px; margin-left: 10px;">
            <div class="form-group">
                <label class="control-label">Primary Applicant: {{ !empty($primaryApplicantion->user->name)?$primaryApplicantion->user->name:auth()->user()->name }}</label>
            </div>
            @php $disable = ''; @endphp
            @if( !empty($primaryApplicantion->user->name) )
                @php $disable = 'readonly'; @endphp 
            @endif
            <div class="form-group {{ $errors->has('number_of_occupant') ? 'has-error' : '' }}">
                <label class="control-label requiredcls">Number of Other Occupants</label>
                {{ Form::number('number_of_occupant', $latestOccupants == null ? null : $latestOccupants->count(), ['class' => 'form-control', 'min' => '0', 'max' => '10', $disable ]) }}
                @if ($errors->has('number_of_occupant'))
            <span class="text-danger">{{ $errors->first('number_of_occupant') }}</span>
            @endif
            </div>

            {{-- fill occupancy details --}}
            @if ($latestOccupants != null)
                @foreach ($latestOccupants as $occupant)
                <div class="occupancy-number-of-person col-md-6">
                    <h3>Person #{{ $loop->index + 1 }}</h3>

                    <div class="form-group"><br>
                        <label for="name" class="control-label">Name</label>
                        <input type="text" name="occupant_name[]" class="form-control" value="{{ $occupant->occupant_name }}">
                    </div>

                    <div class="form-group"><br>
                        <label for="age" class="control-label">Age</label>
                        <input type="text" name="occupant_age[]" class="form-control" value="{{ $occupant->occupant_age }}">
                    </div>

                    <div class="form-group"><br>
                        <label for="relationship" class="control-label">Relationship</label>
                        <select name="occupant_relationship[]" class="form-control occupant-relationship">
                            <option>--select relationship--</option>
                        </select>
                    </div>

                    <div class="form-group"><br>
                        <label for="occupant_on_lease" class="control-label">Will this occupant be on the lease?</label>
                            <select name="occupant_on_lease[]" class="form-control">
                                <option value="yes">YES</option>
                                <option value="no">NO</option>
                            </select>
                    </div>

                    <div class="form-group"><br>
                        <label for="occupant_as_joint_tenant" class="control-label">Will this occupant be as a Joint Tenant?</label>
                            <select name="occupant_as_joint_tenant[]" class="form-control">
                                <option value="no">NO</option>
                                <option value="yes">YES</option>
                            </select>
                    </div>

                    <div class="form-group"><br>
                        <label for="email" class="control-label">Person #{{ $loop->index + 1 }} Email Address</label>
                        <input type="text" name="joint_occupant_email[]" class="form-control" value="{{ $occupant->joint_occupant_email }}">
                    </div>

                    <div class="form-group"><br>
                        <label for="mobile" class="control-label">Person #{{ $loop->index + 1 }} Mobile/Phone</label>
                        <input type="text" name="joint_occupant_mobile[]" class="form-control" value="{{ $occupant->joint_occupant_mobile }}">
                    </div>

                </div>
                @endforeach
            @endif
        </div>

        <div class="tenant-occupancy-details-children row" style="margin-right: 10px; margin-left: 10px;">
            <div class="form-group {{ $errors->has('number_of_children') ? 'has-error' : '' }}" >
                <label class="control-label requiredcls">Number of Childrens</label>
                {{ Form::number('number_of_children', $latestChildren == null ? null : $latestChildren->count(), ['class' => 'form-control', 'min' => '0', 'max' => '5']) }}
                @if ($errors->has('number_of_children'))
            <span class="text-danger">{{ $errors->first('number_of_children') }}</span>
            @endif
            </div>
            {{-- fill application occupancy children --}}
            @if ($latestChildren != null)
                @foreach ($latestChildren as $children)
                <div class="occupancy-number-of-children col-md-6">
                    <h3>Children #{{ $loop->index + 1 }}</h3>
                    <div class="form-group"><br>
                        <label for="age" class="control-label">Age</label>
                        <input type="text" name="children_age[]" class="form-control min="0" max="15" value="{{ $children->children_age }}">
                    </div>
                </div>
                @endforeach
            @endif
        </div>

        <div class="tenant-occupancy-details-vehicles row" style="margin-right: 10px; margin-left: 10px;">
            <div class="form-group {{ $errors->has('number_of_vehicles') ? 'has-error' : '' }}">
                <label class="control-label requiredcls">Number of Vehicles</label>
                {{ Form::number('number_of_vehicles', $latestVehicle == null ? null : $latestVehicle->count(), ['class' => 'form-control', 'min' => '0', 'max' => '10']) }}
                @if ($errors->has('number_of_vehicles'))
            <span class="text-danger">{{ $errors->first('number_of_vehicles') }}</span>
            @endif
            </div>
            @if ($latestVehicle != null)
                @foreach ($latestVehicle as $vehicle)
                    <div class="occupancy-number-of-vehicles col-md-6">
                        <h3>Vehicle #{{ $loop->index + 1 }}</h3>
                        <div class="form-group"><br>
                            <label for="type" class="control-label">Vehicle Type</label>
                            <input type="text" name="vehicle_type[]" class="form-control" value="{{ $vehicle->vehicle_type }}">
                        </div>
                        <div class="form-group"><br>
                            <label for="registration" class="control-label">Registration Number</label>
                            <input type="text" name="vehicle_registration[]" class="form-control" value="{{ $vehicle->vehicle_registration }}">
                        </div>
                        <div class="form-group"><br>
                            <label for="make_model" class="control-label">Make/Model</label>
                            <input type="text" name="make_model[]" class="form-control" value="{{ $vehicle->make_model }}">
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="tenant-occupancy-details-pets row" style="margin-right: 10px; margin-left: 10px;">
            <div class="form-group {{ $errors->has('number_of_pets') ? 'has-error' : '' }}">
                <label class="control-label requiredcls">Number of Pets</label>
                {{ Form::number('number_of_pets', $latestPets == null ? null : $latestPets->count(), ['class' => 'form-control', 'min' => '0', 'max' => '10']) }}
                @if ($errors->has('number_of_pets'))
            <span class="text-danger">{{ $errors->first('number_of_pets') }}</span>
            @endif
            </div>
            @if ($latestPets != null)
                 @foreach ($latestPets as $pet)
                <div class="occupancy-number-of-pets col-md-6">
                    <h3>Pet #{{ $loop->index + 1 }}</h3>
                    <div class="form-group"><br>
                        <label for="type" class="control-label">Pet Type</label>
                        <input type="text" name="pet_type[]" class="form-control" value="{{ $pet->pet_type }}">
                    </div>
                    <div class="form-group"><br>
                        <label for="breed" class="control-label">Breed</label>
                        <input type="text" name="pet_breed[]" class="form-control" value="{{ $pet->pet_breed }}">
                    </div>
                </div>
                @endforeach
            @endif
            {{ Form::hidden('joint_app_id', !empty($primaryApplicantion->id)?$primaryApplicantion->id:'0', array('id' => 'joint_app_id')) }}
        </div>
        <button class="btn btn-primary prev-button pull-left">Previous</button>
        <button class="btn btn-primary nextBtn next-button btn-lg pull-right" type="button" >Next</button>
    </div>
</div>