<div class="row setup-content" id="tenant-occupancy-details">
    <div class="col-md-12 thirty-pixel">

        {{-- Occupancy Details --}}
        <div class="tenant-occupancy-details-person row" style="margin-right: 10px; margin-left: 10px;">
            <div class="form-group">
                <label class="control-label">Number of Occupants</label>
                {{ Form::number('number_of_occupant', null, ['class' => 'form-control', 'min' => '0', 'max' => '10']) }}
            </div>

            {{-- fill occupancy details --}}
            @foreach ($application->occupants as $occupant)
            <div class="occupancy-number-of-person col-md-6">
                <h3>Person #{{ $loop->index + 1 }}</h3>

                <div class="form-group"><br>
                    <label for="name" class="control-label">Name</label>
                    <input type="text" name="occupant_name[]" class="form-control" value="{{ $occupant->occupant_name }}">
                </div>

                <div class="form-group"><br>
                    <label for="age" class="control-label">Age</label>
                    <input type="text" name="occupant_age[]" class="form-control" value="{{ $occupant->occupant_age }}">
                </div>

                <div class="form-group"><br>
                    <label for="relationship" class="control-label">Relationship</label>
                    <select name="occupant_relationship[]" class="form-control occupant-relationship">
                        <option>--select relationship--</option>
                    </select>
                </div>

                <div class="form-group"><br>
                    <label for="occupant_on_lease" class="control-label">Will this occupant be on the lease?</label>
                        <select name="occupant_on_lease[]" class="form-control">
                            <option value="yes">YES</option>
                            <option value="no">NO</option>
                        </select>
                </div>
            </div>
            @endforeach
        </div>
            <div class="clearfix"></div>

            <div class="tenant-occupancy-details-children row" style="margin-right: 10px; margin-left: 10px;">
                <div class="form-group" >
                    <label class="control-label">Number of Childrens</label>
                    {{ Form::number('number_of_children', null, ['class' => 'form-control', 'min' => '0', 'max' => '5']) }}
                </div>
                {{-- fill application occupancy children --}}
                @foreach ($application->children as $children)
                <div class="occupancy-number-of-children col-md-6">
                    <h3>Children #{{ $loop->index + 1 }}</h3>
                    <div class="form-group"><br>
                        <label for="age" class="control-label">Age</label>
                        <input type="text" name="children_age[]" class="form-control min="0" max="15" value="{{ $children->children_age }}">
                    </div>
                </div>
                @endforeach
            </div>


            <div class="clearfix"></div>

            <div class="tenant-occupancy-details-vehicles row" style="margin-right: 10px; margin-left: 10px;">
                <div class="form-group">
                    <label class="control-label">Number of Vehicles</label>
                    {{ Form::number('number_of_vehicles', null, ['class' => 'form-control', 'min' => '0', 'max' => '10']) }}
                </div>
                @foreach ($application->vehicles as $vehicle)
                <div class="occupancy-number-of-vehicles col-md-6">
                    <h3>Vehicle #{{ $loop->index + 1 }}</h3>
                    <div class="form-group"><br>
                        <label for="type" class="control-label">Vehicle Type</label>
                        <input type="text" name="vehicle_type[]" class="form-control" value="{{ $vehicle->vehicle_type }}">
                    </div>
                    <div class="form-group"><br>
                        <label for="registration" class="control-label">Registration Number</label>
                        <input type="text" name="vehicle_registration[]" class="form-control" value="{{ $vehicle->vehicle_registration }}">
                    </div>
                    <div class="form-group"><br>
                        <label for="make_model" class="control-label">Make/Model</label>
                        <input type="text" name="make_model[]" class="form-control" value="{{ $vehicle->make_model }}">
                    </div>
                </div>
                @endforeach
            </div>

            <div class="tenant-occupancy-details-pets row" style="margin-right: 10px; margin-left: 10px;">
                <div class="form-group">
                    <label class="control-label">Number of Pets</label>
                    {{ Form::number('number_of_pets', null, ['class' => 'form-control', 'min' => '0', 'max' => '10']) }}
                </div>

                @foreach ($application->pets as $pet)
                <div class="occupancy-number-of-pets col-md-6">
                    <h3>Pet #{{ $loop->index + 1 }}</h3>
                    <div class="form-group"><br>
                        <label for="type" class="control-label">Pet Type</label>
                        <input type="text" name="pet_type[]" class="form-control" value="{{ $pet->pet_type }}">
                    </div>
                    <div class="form-group"><br>
                        <label for="breed" class="control-label">Breed</label>
                        <input type="text" name="pet_breed[]" class="form-control" value="{{ $pet->pet_breed }}">
                    </div>
                </div>
                @endforeach
            </div>

            <button class="btn btn-primary next-button nextBtn btn-lg pull-right" type="button" >Next</button>
    </div>
</div>

{{ Form::hidden('application_id', $application->id) }}