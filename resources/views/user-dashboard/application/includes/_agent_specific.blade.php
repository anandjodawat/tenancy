<div class="setup-content" id="tenant-agent-specific-details">
    <div class="col-md-9 thirty-pixel">
        
        <div class="form-group {{ $errors->has('is_tenant_terminated') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Has your tenancy ever been terminated by a landlord or agent? </label>
            <br>
            Yes {{ Form::radio('is_tenant_terminated', 'yes', $latestApplication && $latestApplication->is_tenant_terminated == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('is_tenant_terminated', 'no', $latestApplication && $latestApplication->is_tenant_terminated == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}
            @if ($errors->has('is_tenant_terminated'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('tenant_terminated_details') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Please Provide Details </label>
            {{ Form::textarea('tenant_terminated_details', $latestApplication && $latestApplication->tenant_terminated_details, ['class' => 'form-control', 'rows' => '3']) }}
            @if ($errors->has('tenant_terminated_details'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('is_tenant_refused') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Have you ever been refused a property by any landlord or agent?</label>
            <br>
            Yes {{ Form::radio('is_tenant_refused', 'yes', $latestApplication && $latestApplication->is_tenant_refused == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('is_tenant_refused', 'no', $latestApplication && $latestApplication->is_tenant_refused == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}
              @if ($errors->has('is_tenant_refused'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('tenant_refused_details') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Please Provide Details </label>
            {{ Form::textarea('tenant_refused_details', $latestApplication && $latestApplication->tenant_refused_details ?? null, ['class' => 'form-control', 'rows' => '3']) }}
            @if ($errors->has('tenant_refused_details'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('is_on_debt') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Are you in debt to another landlord or agent?</label>
            <br>
            Yes {{ Form::radio('is_on_debt', 'yes', $latestApplication && $latestApplication->is_on_debt == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('is_on_debt', 'no', $latestApplication && $latestApplication->is_on_debt == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}
            @if ($errors->has('is_on_debt'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('on_debt_details') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Please Provide Details </label>
            {{ Form::textarea('on_debt_details', $latestApplication && $latestApplication->on_debt_details ?? null, ['class' => 'form-control', 'rows' => '3']) }}
            @if ($errors->has('on_debt_details'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('is_deduction_rental_bond') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Have any deductions ever been made from your rental bond?</label>
            <br>
            Yes {{ Form::radio('is_deduction_rental_bond', 'yes', $latestApplication && $latestApplication->is_deduction_rental_bond == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('is_deduction_rental_bond', 'no', $latestApplication && $latestApplication->is_deduction_rental_bond == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}
            @if ($errors->has('is_deduction_rental_bond'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('deduction_rental_bond_details') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Please Provide Details </label>
            {{ Form::textarea('deduction_rental_bond_details', $latestApplication && $latestApplication->deduction_rental_bond_details ?? null, ['class' => 'form-control', 'rows' => '3']) }}
            @if ($errors->has('deduction_rental_bond_details'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('affect_future_rental_payment') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Is there any reason known to you that would affect your future rental payments?</label>
            <br>
            Yes {{ Form::radio('affect_future_rental_payment', 'yes', $latestApplication && $latestApplication->affect_future_rental_payment == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('affect_future_rental_payment', 'no', $latestApplication && $latestApplication->affect_future_rental_payment == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}
            @if ($errors->has('affect_future_rental_payment'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('affect_future_rental_payment_details') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Please Provide Details </label>
            {{ Form::textarea('affect_future_rental_payment_details', $latestApplication && $latestApplication->affect_future_rental_payment_details ?? null, ['class' => 'form-control', 'rows' => '3']) }}
            @if ($errors->has('affect_future_rental_payment_details'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('is_another_pending_application') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Do you have any other applications pending on other properties?</label>
            <br>
            Yes {{ Form::radio('is_another_pending_application', 'yes', $latestApplication && $latestApplication->is_another_pending_application == 'yes' ?? null) }}
            No {{ Form::radio('is_another_pending_application', 'no', $latestApplication && $latestApplication->is_another_pending_application == 'no' ?? null) }}
            @if ($errors->has('is_another_pending_application'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('own_a_property') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Do you currently own a property?</label>
            <br>
            Yes {{ Form::radio('own_a_property', 'yes', $latestApplication && $latestApplication->own_a_property == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('own_a_property', 'no', $latestApplication && $latestApplication->own_a_property == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}
            @if ($errors->has('own_a_property'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('own_a_property_details') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Please Provide Property Address </label>
            {{ Form::textarea('own_a_property_details', $latestApplication && $latestApplication->own_a_property_details ?? null, ['class' => 'form-control', 'rows' => '3']) }}

            @if ($errors->has('own_a_property_details'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('buy_another_property') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Are you considering buying a property after this tenancy or in the near future?</label>
            <br>
            Yes {{ Form::radio('buy_another_property', 'yes', $latestApplication && $latestApplication->buy_another_property == 'yes' ?? null, ['class' => 'agent-specific-radio-button']) }}
            No {{ Form::radio('buy_another_property', 'no', $latestApplication && $latestApplication->buy_another_property == 'no' ?? null, ['class' => 'agent-specific-radio-button']) }}

            @if ($errors->has('buy_another_property'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('intend_to_buy_after') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">When do you intend on buying?</label>
            <br>
            {{ Form::select('intend_to_buy_after', ['Within 12 months', 'Within 24 months'], $latestApplication ? $latestApplication->is_tenant_terminated : null, ['class' => 'form-control']) }}
            @if ($errors->has('intend_to_buy_after'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <hr>
        {{-- <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button> --}}
        <button class="btn btn-primary prev-button pull-left">Previous</button>
        <button class="btn btn-primary nextBtn next-button btn-lg pull-right" type="button" >Next</button>
    </div>
</div>