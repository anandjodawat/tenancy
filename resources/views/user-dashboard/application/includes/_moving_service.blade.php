<div class="setup-content" id="tenant-moving-service-details">
    <div class="thirty-pixel">
        <div class="form-group {{ $errors->has('need_moving_service') ? 'has-error' : '' }}">
            <div class="col-md-9">
                <div class="row">
                    <label class="control-label requiredcls">Selecting the right utility provider can make your move easier and may save you money.
                    Would you like a call closer to your moving date to help arrange connection to your household services? You'll get a phone call, email and help to compare and select a plan from available suppliers.</label>
                    
                </div>
            </div>
            <div class="col-md-2" id="utility-connection-logo">
            </div>
            <div class="col-md-12">
                <div class="row">
                    Yes {{ Form::radio('need_moving_service', 'yes') }}
                    No {{ Form::radio('need_moving_service', 'no') }}
                </div>
                @if ($errors->has('need_moving_service'))
            <span class="text-danger">This field is required.</span>
            @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-3">
                <div class="row">
                    <label class="control-label">Select Service</label>
                    @foreach (App\Models\Tenant\TenantApplication::MOVINGSERVICEOPTION as $option)
                    <br>
                    {{ Form::checkbox('moving_service_id[]', $loop->index) }} {{ $option }}
                    @endforeach
                </div>
            </div>
            <div class="col-md-8" id="moving-service-setting-body">
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group {{ $errors->has('moving_service_agreement') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">
                {{ Form::checkbox('moving_service_agreement', 'yes') }}
                I agree to the declaration above and use my Digital Signature for the purpose of this Application.
            </label>
            <br>
             @if ($errors->has('moving_service_agreement'))
            <span class="text-danger">{{ $errors->first('moving_service_agreement') }}</span>
            @endif
        </div>
        <hr>
        <button class="btn btn-primary prev-button pull-left">Previous</button>
        <button class="btn btn-primary next-button nextBtn btn-lg pull-right" type="button" >Next</button>
    </div>
</div>