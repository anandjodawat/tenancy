<div class="setup-content active" id="tenant-property-details">
    <div class="col-md-9 thirty-pixel">
        <div class="form-group {{ $errors->has('applied_for') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Application address</label>
            {{ Form::text('applied_for', !empty($primaryApplicantion->applied_for)?$primaryApplicantion->applied_for:request('applied_for'), ['maxlength' => '100', 'class' => 'form-control', 'id' => 'reqAddress']) }}
            @if ($errors->has('applied_for'))
            <span class="text-danger">{{ $errors->first('applied_for') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('street_number') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Street Number</label>
            {{ Form::text('street_number', !empty($primaryApplicantion->street_number)?$primaryApplicantion->street_number:request('street_number'), ['class' => 'form-control', 'id' => 'street_number']) }}
            @if ($errors->has('street_number'))
            <span class="text-danger">{{ $errors->first('street_number') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('street_name') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Street Name</label>
            {{ Form::text('street_name', !empty($primaryApplicantion->street_name)?$primaryApplicantion->street_name:request('street_name'), ['class' => 'form-control', 'id' => 'street_name']) }}
            @if ($errors->has('street_name'))
            <span class="text-danger">{{ $errors->first('street_name') }}</span>
            @endif
        </div>
        {{-- <div class="form-group custom-select {{ $errors->has('street_type') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Street Type</label>
            {{ Form::select('street_type', array_map('ucwords', App\Models\Tenant\TenantApplication::STREETTYPE), !empty($primaryApplicantion->street_type)?$primaryApplicantion->street_type:null, ['class' => 'form-control', 'id' => 'street_type']) }}
            @if ($errors->has('street_type'))
            <span class="text-danger">{{ $errors->first('street_type') }}</span>
            @endif
        </div> --}}
        <div class="form-group {{ $errors->has('street_type') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Street Type</label>
            {{ Form::text('street_type', !empty($primaryApplicantion->street_type)?$primaryApplicantion->street_type:request('street_type'), ['class' => 'form-control', 'id' => 'street_type']) }}
            @if ($errors->has('street_type'))
            <span class="text-danger">{{ $errors->first('street_type') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('suburb') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Suburb</label>
            {{ Form::text('suburb', !empty($primaryApplicantion->suburb)?$primaryApplicantion->suburb:request('suburb'), ['class' => 'form-control', 'id' => 'suburb']) }}
            @if ($errors->has('suburb'))
            <span class="text-danger">{{ $errors->first('suburb') }}</span>
            @endif
        </div>
        <div class="form-groupn  {{ $errors->has('state') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">State</label>
            {{ Form::select('state', App\Models\Tenant\TenantApplication::STATES, !empty($primaryApplicantion->state_name)?$primaryApplicantion->state_name:request('state'), ['class' => 'form-control', 'id' => 'state' ]) }}
            @if ($errors->has('state'))
            <span class="text-danger">{{ $errors->first('state') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('post_code') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Post Code</label>
            {{ Form::text('post_code', !empty($primaryApplicantion->post_code)?$primaryApplicantion->post_code:request('post_code'), ['class' => 'form-control', 'id' => 'post_code']) }}
            @if ($errors->has('post_code'))
            <span class="text-danger">{{ $errors->first('post_code') }}</span>
            @endif
        </div>
        <div class="form-group custom-select {{ $errors->has('property_type') ? 'has-error' : '' }}">
            <label class="control-label">Property Type</label>
            {{ Form::select('property_type', array_map('ucwords', App\Models\Tenant\TenantApplication::PROPERTYTYPE), !empty($primaryApplicantion->property_type)?$primaryApplicantion->property_type:null, ['class' => 'form-control']) }}
            @if ($errors->has('property_type'))
            <span class="text-danger">{{ $errors->first('property_type') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('number_of_bedrooms') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Number Of Bedrooms?</label>
            {{ Form::number('number_of_bedrooms', !empty($primaryApplicantion->number_of_bedrooms)?$primaryApplicantion->number_of_bedrooms:null, ['class' => 'form-control']) }}
            @if ($errors->has('number_of_bedrooms'))
            <span class="text-danger">{{ $errors->first('number_of_bedrooms') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('commencement_date') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Preferred Commencement Date</label>
            {{ Form::text('commencement_date', null, ['class' => 'form-control', 'id' => 'commencement-datepicker']) }}
            @if ($errors->has('commencement_date'))
            <span class="text-danger">{{ $errors->first('commencement_date') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('length_of_lease') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Length of Lease <small>(months)</small></label>
            {{ Form::text('length_of_lease', null, ['class' => 'form-control']) }}
            @if ($errors->has('length_of_lease'))
            <span class="text-danger">{{ $errors->first('length_of_lease') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('weekly_rent') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Weekly Rent</label>
            {{ Form::text('weekly_rent', !empty($primaryApplicantion->weekly_rent)?$primaryApplicantion->weekly_rent:request('weekly_rent'), ['class' => 'form-control']) }}
            @if ($errors->has('weekly_rent'))
            <span class="text-danger">{{ $errors->first('weekly_rent') }}</span>
            @endif
        </div>
        <div class="form-group">
            <label class="control-label requiredcls">Monthly Rent</label>
            {{ Form::text('monthly_rent', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group {{ $errors->has('bond') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Bond</label>
            {{ Form::text('bond', !empty($primaryApplicantion->bond)?$primaryApplicantion->bond:null, ['class' => 'form-control']) }}
            @if ($errors->has('bond'))
            <span class="text-danger">{{ $errors->first('bond') }}</span>
            @endif
        </div>
        <?php /*<div class="form-group {{ $errors->has('bond_assistance') ? 'has-error' : '' }}">
            <label class="control-label">Do you require assistance with your bond payment?</label><br>
            Yes {{ Form::radio('bond_assistance', 'yes') }}
            No {{ Form::radio('bond_assistance', 'no') }}
            @if ($errors->has('bond_assistance'))
            <span class="text-danger">{{ $errors->first('bond_assistance') }}</span>
            @endif
        </div> */?>
        <div class="form-group {{ $errors->has('inspected_this_property') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Have you inspected this property?</label><br>
            Yes {{ Form::radio('inspected_this_property', 'yes') }}
            No {{ Form::radio('inspected_this_property', 'no') }}
            @if ($errors->has('inspected_this_property'))
            <span class="text-danger">This field is required.</span>
            @endif
        </div>

        <div class="hide-if-inspected-property-no">
            <div class="form-group {{ $errors->has('inspection_date') ? 'has-error' : '' }}">
                <label class="control-label requiredcls">Inspected Date </label>
                {{ Form::text('inspection_date', null, ['class' => 'form-control', 'id' => 'inspection-date-datepicker']) }}
                @if ($errors->has('inspection_date'))
            <span class="text-danger">This field is required.</span>
            @endif
            </div>

            <div class="form-group">
                <label class="control-label requiredcls">Was the property upon your inspection in a reasonably clean and fair condition? </label><br>
                Yes {{ Form::radio('good_condition', 'yes') }}
                No {{ Form::radio('good_condition', 'no') }}
                @if ($errors->has('good_condition'))
            <span class="text-danger">This field is Required</span>
            @endif
            </div>

            <div class="form-group">
                <label class="control-label">If No, give reason </label><br>
                {{ Form::textarea('not_good_condition_reason', null, ['class' => 'form-control', 'rows' => 3]) }}
            </div>
            <div class="form-group">
                <label class="control-label">Inspection Code</label>
                {{ Form::text('inspection_code', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group custom-select {{ $errors->has('property_found_in') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">How did you find out about this property? </label>
            {{ Form::select('property_found_in', App\Models\Tenant\TenantApplication::PROPERTYFOUNDIN, null, ['class' => 'form-control'] ) }}
            @if ($errors->has('property_found_in'))
            <span class="text-danger">{{ $errors->first('property_found_in') }}</span>
            @endif
        </div>
        <hr>
        <h2>Property Manager Details</h2>
        <div class="form-group {{ $errors->has('agency_name') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Agency Name</label>
            <input list="input-agency-list" name="agency_name" id="input-agency-name-list" class="form-control" value="@if (request('agency_name')){{ request('agency_name') }}@else{{ isset($application) && $application->agency_name ? $application->agency_name : '' }}@endif">

            <datalist id="input-agency-list"></datalist>
            @if ($errors->has('agency_name'))
            <span class="text-danger">{{ $errors->first('agency_name') }}</span>
            @endif
        </div>
        

        <div class="form-group {{ $errors->has('property_manager_name') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Property Manager Name</label>
            <input name="property_manager_name" list="input-property-manager-list" id="input-property-manager-name-list" class="form-control" value="@if (request('property_manager_email')){{ request('property_manager_email') }}@else{{ isset($application) && $application->property_manager_name ? $application->property_manager_name : '' }}@endif">
            <datalist id="input-property-manager-list"></datalist>
            @if ($errors->has('property_manager_name'))
            <span class="text-danger">{{ $errors->first('property_manager_name') }}</span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('property_manager_email') ? 'has-error' : '' }}">
            <label class="control-label requiredcls">Property Manager Email</label>
            {{ Form::email('property_manager_email', request('property_manager_email'), ['class' => 'form-control']) }}
            @if ($errors->has('property_manager_email'))
            <span class="text-danger">{{ $errors->first('property_manager_email') }}</span>
            @endif
        </div>
        <button class="btn btn-primary nextBtn next-button btn-lg pull-right" type="button" >Next</button>
    </div>
</div>

@section('after-scripts')
<script type="text/javascript">
///$(document).ready(function () {
    $( "#reqAddress" ).autocomplete({

            source: function(request, response) {

            $.ajax({
                url: "../get_valid_address",
                data: {
                        term : request.term
                 },
                dataType: "json",
                success: function(data){
                   /*var resp = $.map(data,function(obj){
                        //console.log(obj);
                        return obj.streetAddress;
                   }); */
                   $("#state option:selected").attr("selected",null);

                   ///response(resp);
                   response($.map(data, function (item) {
                      return {
                        value: item.streetAddress,
                        streetno: item.numberFirst,
                        streetnm: item.streetName,
                        streettp: item.streetType,
                        streetsub: item.suburb,
                        streetst: item.state,
                        streetpc: item.postCode,
                      }
                    }));
                }
            });
            },   
            search: function(event, ui) { 
               $('.spinner').show();
            },
            response: function (event, ui) {
                if (!ui.content.length) {
                  var noResult = { value: "", label: "No address found matching your request" };
                  ui.content.push(noResult);
                }
              },
            select: function (event, ui) {
                //console.log('select-'+ui.item.streetno);
                $("#street_number").val(ui.item.streetno);
                $("#street_name").val(ui.item.streetnm);
                $("#street_type").val(ui.item.streettp);
                $("#suburb").val(ui.item.streetsub);
                $("#post_code").val(ui.item.streetpc);
                
                //$('select[name^="state"] option:selected').attr("selected",null);
                //$('select[name^="state"] option[text="'+ui.item.streetst+'"]').attr("selected","selected");
               // $("#state option[text=" + ui.item.streetst +"]").attr("selected","selected") ;
                
                $("#state option:contains(" + ui.item.streetst +")").attr("selected", true);
                //return false;
            },
            minLength: 3
            /*$.ajax({
                method: "POST",
                processData: false,
                contentType: false,
                dataType: "json",
                url: "https://mappify.io/api/rpc/address/autocomplete/",
                data: {
                        "apiKey" : "b51fc733-b52e-4c58-905b-e1ba262b589d",
                        "streetAddress" : request.term,
                        "formatCase" : true
                 },
                 headers: {
                    'Access-Control-Allow-Headers': "x-csrf-token"
                },
                
                success: function(data){
                   var resp = $.map(data,function(obj){
                        console.log(obj);
                       // return obj.result;
                   }); 

                   //response(resp);
                },
                error: function(jqxhr,textStatus,errorThrown)
                    {
                            console.log(jqxhr);
                            console.log(textStatus);
                            console.log(errorThrown);                               

                            //for (key in jqxhr)
                                //console.log(key + ":" + jqxhr[key])

                },

            });*/

            //},
            /*response: function (event, ui) {
                if (!ui.content.length) {
                  var noResult = { value: "", label: "No cities matching your request" };
                  ui.content.push(noResult);
                }
            },*/
            
        });
//});
</script>
@endsection