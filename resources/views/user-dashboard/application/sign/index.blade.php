@extends('layouts.app')

@section('content')
@include('flash::message')

<div class="col-lg-12">
    <h1 class="page-header">Sign Document<a href="/dashboard" class="btn btn-primary pull-right">Redirect</a></h1>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <iframe src="{{ $url }}" width="100%" height="500"></iframe>
    </div>
</div>

@endsection
