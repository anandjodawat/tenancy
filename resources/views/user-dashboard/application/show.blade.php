<?php use \App\Http\Controllers\PropertyManager\ApplicationController; ?>
@extends('layouts.app')

@section('content')

<div class="col-lg-12">
    <h1 class="page-header">Property #{{ $application->id }}
        @if ($application->draft == "1")
        <a href="{{ route('user-dashboard.applications.edit', $application->id) }}" class="btn btn-info" title="Edit Application">
            <i class="fa fa-edit"></i> Edit Application
        </a>
        @endif
    </h1>
</div>

{{-- Property Details --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">
            Property Details
        </div>
        <div class="panel-body show-page-tanent">
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Property Address Applied For?</label><br>
                    <span class="pull-right tenancy-property-address">{{ $application->applied_for }}</span>
                </li> 
                <li class="list-group-item">
                    <label class="control-label">State</label>
                    <span class="pull-right">{{ $application->state_name }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Suburb</label>
                    <span class="pull-right">{{ $application->suburb }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Post Code</label>
                    <span class="pull-right">{{ $application->post_code }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Property Type</label>
                    <span class="pull-right">{{ ucwords($application->propertyType()) }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Number of Bedrooms</label>
                    <span class="pull-right">{{ $application->number_of_bedrooms }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Preferred Commencement Date</label>
                    <span class="pull-right">{{ $application->commencement_date }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Length of Lease (months)</label>
                    <span class="pull-right">{{ $application->length_of_lease }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Weekly Rent</label>
                    <span class="pull-right">{{ $application->weekly_rent }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Monthly Rent</label>
                    <span class="pull-right">{{ $application->monthly_rent }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Bond</label>
                    <span class="pull-right">{{ $application->bond }}</span>
                </li> 
                <li class="list-group-item">
                    <label class="control-label">Do you require assistance with your bond payment?</label>
                    <span class="pull-right">{{ $application->bond_assistance }}</span>
                </li>
            </ul>
        </div>
    </div>
</div>


{{-- Property Manager Details --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Property Manager Details</div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Agency Name</label>
                    <span class="pull-right">{{ $application->agency_name }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Property Manager Name</label>
                    <span class="pull-right">{{ $application->property_manager_name }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Property Manager Email</label>
                    <span class="pull-right">{{ $application->property_manager_email }}</span>
                </li>
            </ul>
        </div>
    </div>
</div>

{{-- Occupancy Details - Occupant --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Occupancy Details - Occupant</div>
        <div class="panel-body">

        @if(!empty($application->joint_app_id))

            @php
            $ja_app = ApplicationController::get_joint_app_user_details($application->joint_app_id);
            @endphp
            <h4>This is a joint application, please find Primary applicant details:</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Name</label>
                    <span class="pull-right">{{ $ja_app->name }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Email</label>
                    <span class="pull-right">{{ $ja_app->email }}</span>
                </li>
            </ul>
            
        @else

            @foreach ($application->occupants as $occupant)
            <h4>Occupant #{{ $loop->index + 1 }}</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Name</label>
                    <span class="pull-right">{{ $occupant->occupant_name }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Age</label>
                    <span class="pull-right">{{ $occupant->occupant_age }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Relationship</label>
                    <span class="pull-right">{{ $occupant->occupantRelationship() }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Will this occupant be on the lease?</label>
                    <span class="pull-right">{{ $occupant->occupant_on_lease }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Will this occupant be as a Joint Tenant?</label>
                    <span class="pull-right">{{ $occupant->occupant_as_joint_tenant }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Person #{{ $loop->index + 1 }} Email Address</label>
                    <span class="pull-right">{{ $occupant->occupant_email }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Person #{{ $loop->index + 1 }} Mobile/Phone</label>
                    <span class="pull-right">{{ $occupant->occupant_mobile_number }}</span>
                </li>
            </ul>
            @endforeach
        @endif
        </div>
    </div>
</div>

{{-- Occupancy Details - Children --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Occupancy Details - Children</div>
        <div class="panel-body">
            @foreach ($application->children as $child)
            <h4>Child #{{ $loop->index + 1 }}</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Age</label>
                    <span class="pull-right">{{ $child->children_age }}</span>
                </li>
            </ul>
            @endforeach
        </div>
    </div>
</div>

{{-- Occupancy Details - Vehicle --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Occupancy Details - Vehicle</div>
        <div class="panel-body">
            @foreach ($application->vehicles as $vehicle)
            <h4>Vehicle #{{ $loop->index + 1 }}</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Type</label>
                    <span class="pull-right">{{ $vehicle->vehicle_type }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Registration Number</label>
                    <span class="pull-right">{{ $vehicle->vehicle_registration }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Make/Model</label>
                    <span class="pull-right">{{ $vehicle->make_model }}</span>
                </li>
            </ul>
            @endforeach
        </div>
    </div>
</div>

{{-- Occupancy Details - pet --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Occupancy Details - Pet</div>
        <div class="panel-body">
            @foreach ($application->pets as $pet)
            <h4>Pet #{{ $loop->index + 1 }}</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Type</label>
                    <span class="pull-right">{{ $pet->pet_type }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Breed</label>
                    <span class="pull-right">{{ $pet->pet_breed }}</span>
                </li>
            </ul>
            @endforeach
        </div>
    </div>
</div>

{{-- Agent Specific --}}
<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Agent Specific Details</div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Has your tenancy ever been terminated by a landlord or agent?</label>
                    <br>
                    <span>- {{ ucwords($application->is_tenant_terminated) }}</span>
                    @if ($application->is_tenant_terminated == 'yes')
                    <br>
                    <b>Reason</b> - {{ $application->tenant_terminated_details }}
                    @endif
                </li>
                <li class="list-group-item">
                    <label class="control-label">Have you ever been refused a property by any landlord or agent?</label>
                    <br>
                    <span>- {{ ucwords($application->is_tenant_refused) }}</span>
                    @if ($application->is_tenant_refused == 'yes')
                    <br>
                    <b>Reason</b> - {{ $application->tenant_refused_details }}
                    @endif
                </li>
                <li class="list-group-item">
                    <label class="control-label">Are you in debt to another landlord or agent?</label>
                    <br>
                    <span>- {{ ucwords($application->is_on_debt) }}</span>
                    @if ($application->is_on_debt == 'yes')
                    <br>
                    <b>Reason</b> - {{ $application->on_debt_details }}
                    @endif
                </li>
                <li class="list-group-item">
                    <label class="control-label">Have any deductions ever been made from your rental bond?</label>
                    <br>
                    <span>- {{ ucwords($application->is_deduction_rental_bond) }}</span>
                    @if ($application->is_deduction_rental_bond == 'yes')
                    <br>
                    <b>Reason</b> - {{ $application->deduction_rental_bond_details }}
                    @endif
                </li>
                <li class="list-group-item">
                    <label class="control-label">Is there any <b>reason</b> known to you that would affect your future rental payments?</label>
                    <br>
                    <span>- {{ ucwords($application->affect_future_rental_payment) }}</span>
                    @if ($application->affect_future_rental_payment == 'yes')
                    <br>
                    <b>Reason</b> - {{ $application->affect_future_rental_payment_details }}
                    @endif
                </li>
                <li class="list-group-item">
                    <label class="control-label">Do you have any other applications pending on other properties?</label>
                    <br>
                    <span>- {{ ucwords($application->is_another_pending_application) }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Do you currently own a property?</label>
                    <br>
                    <span>- {{ ucwords($application->own_a_property) }}</span>
                    @if ($application->own_a_property == 'yes')
                    <br>
                    <b>Reason</b> - {{ $application->own_a_property_details }}
                    @endif
                </li>
                <li class="list-group-item">
                    <label class="control-label">Are you considering buying a property after this tenancy or in the near future?</label>
                    <br>
                    <span>- {{ ucwords($application->buy_another_property) }}</span>
                    @if ($application->buy_another_property == 'yes')
                    <br>
                    Buy - {{ $application->intend_to_buy_after }}
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="col-md-6 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Declaration Details</div>
        <div class="panel-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="control-label">Have you inspected this property?</label>
                    <br><span>- {{ ucwords($application->inspected_this_property) }}</span>
                </li>
                @if ($application->inspected_this_property == 'yes')
                <li class="list-group-item">
                    <label class="control-label">Inspected Date</label> 
                    <span class="pull-right">{{ $application->inspection_date }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Was the property upon your inspection in a reasonably clean and fair condition?</label>
                    <br>
                    <span>- {{ ucwords($application->good_condition) }}</span>
                </li>
                <li class="list-group-item">
                    <label class="control-label">Inspection Code</label>
                    <span class="pull-right">{{ $application->inspection_code }}</span>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-md-12 jQueryEqualHeight3">
    <div class="panel panel-default tan_card">
        <div class="panel-heading">Documents</div>
        <div class="panel-body">
            <ul class="list-group">
                @foreach ($documents as $document)
                 @if (auth()->user()->getFirstMedia($document))
                 <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4>{{ strtoupper(str_replace('_', ' ', $document)) }}</h4>
                            <img src="{{ auth()->user()->getFirstMedia($document)->getUrl() }}" width="100%" height="250px">
                            <hr>
                            <!-- Large modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#application_modal_{{ $loop->index }}">View</button>

                            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="application_modal_{{ $loop->index }}" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <img src="{{ auth()->user()->getFirstMedia($document)->getUrl() }}" width="100%">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
              </div>
             @endif
              @endforeach     
            </ul>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')


<script type="text/javascript">
    function equal_height() {
            // Equal Card Height
            $('.jQueryEqualHeight1').jQueryEqualHeight();

            // Equal Card Height and Text Height
            $('.jQueryEqualHeight2').jQueryEqualHeight('.tan_card .tan_card-body .tan_card-text');
            $('.jQueryEqualHeight2').jQueryEqualHeight('.tan_card');

            // Equal Card Height, Text Height and Title Height
            $('.jQueryEqualHeight3').jQueryEqualHeight('.tan_card .tan_card-body .tan_card-title');
            $('.jQueryEqualHeight3').jQueryEqualHeight('.tan_card .tan_card-body .tan_card-text');
            $('.jQueryEqualHeight3').jQueryEqualHeight('.tan_card');
        }
        $(window).on('load', function(event) {
            equal_height();
        });
        $(window).resize(function(event) {
            equal_height();
        });
    </script>


    @endsection

