@extends('layouts.app')

@section('content')
@include('flash::message')
<h2 class="page-header new-application-title">Profile</h2>
<div class="col-lg-12">
      @if ($errors->count())
        <div class="alert alert-danger">
          @php
            $tab_names = ['Personal Information','Emergency Contact', 'Current Address', 'Previous Address', 'Current Employment', 'Previous Employment', 'Personal Reference', 'Professional Reference', 'Income and Expenditure', 'Monthly Repayments'];
          @endphp
          @foreach($tab_names as $tab)
              @foreach ($errors->all() as $error)
              
                @if(strpos($error, $tab) !== false)
                  <i class="fa fa-close"></i>
                  &nbsp{{ $tab }} missing fields
                  <br>
                  @break
                @endif 
              @endforeach
          @endforeach
        </div>
      @endif
      </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-2 col-md-2 col-sm-2 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <h4 class="fa fa-user"></h4><br/>Personal Information
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-phone"></h4><br/>Emergency Contact
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-street-view"></h4><br/>Current Address
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-map-marker"></h4><br/>Previous Address
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-shopping-bag"></h4><br/>Current Employment
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-briefcase"></h4><br/>Previous Employment
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-comments"></h4><br/>Personal Reference
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-comment"></h4><br/>Professional Reference
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-shopping-cart"></h4><br/>Income And Expenditure
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="fa fa-credit-card"></h4><br/>Monthly Repayments
                </a>
              </div>
            </div>
            {{ Form::model($profile, ['method' => 'patch', 'route' => ['user-dashboard.profile.update', $profile->id, 'action' => 'submit'], 'id' => 'tenant-profile-form', 'name'=>'tenant-profile-form', 'enctype' => 'multipart/form-data']) }}
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                <div class="form-horizontal ten-personal-information">
                    <center>
                      <h1>Personal Information</h1>
                      <hr>
                      @include('user-dashboard.profile.includes._personal_information')
                    </center>
                </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                    <center>
                      <h1>Emergency Contact</h1>
                      <p>(Your trustable relative living somewhere else. You agrees that the person is acknowledged with the use of their information.)</p>
                      <hr>
                      @include('user-dashboard.profile.includes._emergency_contact')
                    </center>
                    </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                    <center>
                      <h1>Current Address Details</h1>
                      <hr>
                      @include('user-dashboard.profile.includes._current_address')
                    </center>
                    </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                    <center>
                      <h1>Previous Address Details</h1>
                      <hr>
                      @include('user-dashboard.profile.includes._previous_address')
                    </center>
                    </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                      <center>
                        <h1>Current Employment Information</h1>
                        <hr>
                        @include('user-dashboard.profile.includes._current_employment')
                      </center>
                  </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                    <center>
                      <h1>Previous Employment Information</h1>
                      <hr>
                      @include('user-dashboard.profile.includes._previous_employment')
                    </center>
                  </div>
                </div>
                <div class="bhoechie-tab-content">
                    <div class="form-horizontal ten-personal-information">
                      <center>
                        <h1>Personal Reference</h1>
                        <p>(Someone who knows you well who will not be living with you.)</p>

                        <hr>
                        @include('user-dashboard.profile.includes._personal_reference')
                    </center>
                    </div>
                </div>
                <div class="bhoechie-tab-content">
                    <div class="form-horizontal ten-personal-information">
                      <center>
                        <h1>Professional Reference</h1>
                        <p>(Work colleagues, associates, etc.)</p>
                        <hr>
                        @include('user-dashboard.profile.includes._professional_reference')
                      </center>
                    </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                    <h1>Income & Expenditure</h1>
                    <hr>
                    @include('user-dashboard.profile.includes._income_expenditure')
                  </div>
                </div>
                <div class="bhoechie-tab-content">
                  <div class="form-horizontal ten-personal-information">
                    <center>
                      <h1>Monthly Repayments</h1>
                      <hr>
                      @include('user-dashboard.profile.includes._repayments')
                    </center>
                  </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
@endsection

@section('after-scripts')

    <script type="text/javascript">
        $(document).ready(function () {
          jQuery( "#personal-information-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}"}).val();
          jQuery( "#current-employment-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#previous-employment-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true, yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#previous-employment-end-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#current-address-move-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#previous-address-movefrom-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          jQuery( "#previous-address-moveto-datepicker" ).datepicker({ dateFormat: 'dd/mm/yy', changeYear:true,yearRange: "{{ config('tenancy-application.year-range') }}" }).val();
          
          smokeChange();
          studyChange();
          currentLivingArrangementChange();
          currentEmploymentChange();
          previousEmploymentChange();
          pensionChange();
            $('.next-button').on('click', function (e) {
              
              var IsValid=$("#tenant-profile-form").valid();
              if(IsValid){
                e.preventDefault();
                var url = $('#tenant-profile-form').attr('action');
                var data = $('#tenant-profile-form').serialize();
                $.ajax({
                  url: url,
                  data: data,
                  method: 'PATCH',
                  success:function (response) {
                    console.log(response);
                  }
                });
                var next = $('.active').next();
                $('.active').removeClass('active');
                next.addClass('active');
              }

            });

            $('.prev-button').on('click', function (e) {
              e.preventDefault();
                var next = $('.active').prev();
                $('.active').removeClass('active');
                next.addClass('active');
            });

            // hide smoke fields
            $('input:radio[name=smoke]').on('change', function () {
                smokeChange();
            });

            // hide studying fields
            $('input:radio[name=studying]').on('change', function () {
                studyChange();
            });

            // hide fields on current address while selected owner
            $('select[name=current_living_arrangement]').on('change', function () {
                currentLivingArrangementChange();
            });

            // hide fileds in current employment situation
            $('select[name=current_employment_work_situation]').on('change', function () {
                console.log('hello');
                currentEmploymentChange();
            });

            // hide fields of previous employment situation
            $('select[name=previous_employment_work_situtation]').on('change', function () {
              previousEmploymentChange();
            });

            // hide pension fields
            $('input:radio[name=pension]').on('change', function () {
                pensionChange();
            });
        });

        function smokeChange()
        {
            var smokeValue = $('input:radio[name=smoke]:checked').val();
            if (smokeValue == 'yes') {
              $('.smoke-hide').removeClass('hide');
            } else {
              $('.smoke-hide').find('input:text').val('');
              $('.smoke-hide').addClass('hide')
            }
        }

        function studyChange()
        {
          var studyValue = $('input:radio[name=studying]:checked').val();
          if (studyValue == 'yes') {
            $('.study-hide').removeClass('hide');
          } else {
            $('.study-hide').find('input:text').val('');
            $('.study-hide').addClass('hide');
          }
        }

        function currentLivingArrangementChange()
        {
          value = $('select[name=current_living_arrangement]').val();
          if (value == '0') {
            $('.hide-owner').addClass('hide');
          } else if (value == '3') {
            $('.current-address-hide-all').find('input:text').val('');
            $('.current-address-hide-all').addClass('hide');
            $('.show-with-parent').removeClass('hide');
          } else {
            $('.current-address-hide-all').removeClass('hide');
          }
        }

        function currentEmploymentChange()
        {
          value = $('select[name=current_employment_work_situation]').val();
          if (value == "1" || value == "2" || value == "3" || value == "4" || value == "5") {
              $('.hide-current-employment').removeClass('hide');
          } else {
              $('.hide-current-employment').find('input:text').val('');
              $('.hide-current-employment').addClass('hide');
          }
        }

        function previousEmploymentChange()
        {
          value = $('select[name=previous_employment_work_situtation]').val();
          if (value == "1" || value == "2" || value == "3" || value == "4" || value == "5") {
              $('.hide-previous-employment').removeClass('hide');
          } else {
            $('.hide-previous-employment').find('input:text').val('');
              $('.hide-previous-employment').addClass('hide');
          }
        }

        function  pensionChange()
        {
          value = $('input:radio[name=pension]:checked').val();
          if (value == 'yes') {
            $('.hide-pension').removeClass('hide');
          } else {
            $('.hide-pension').find('input:text').val('');
            $('.hide-pension').addClass('hide');
          }
        }


          $('#tenant-profile-form').validate({
            ignore: ":hidden",
            rules: {
              // personal information valdiation
              first_name: "required",
              last_name: "required",
              title: "required",
              marital_status: "required",
              mobile: "required",
              institution: "required" ,
              enrollment_number: "required" ,
              course_contact_number: "required",
              institution_phone: "required",
              source_of_income: "required",
              net_weekly_amount: "required",
              guardian_name: "required",
              guardian_relationship: "required",
              guardian_address: "required",
              guardian_phone_number: "required",
              pension:"required",

              // Emergency Contact
              emergency_contact_name: "required",
              emergency_contact_relation: "required",
              emergency_contact_state: "required",
              emergency_contact_street: "required",
              emergency_contact_suburb: "required",
              emergency_contact_email: "required",

              // Current Address
              current_address_street: "required",
              //current_address_street_name: "required",
              current_address_state: "required",
              current_address_suburb: "required",

              // Previous Address
              previous_address_rent: "required",

              // Current Employment
              current_employment_work_situation: "required",
              current_company_name: "required",
              current_manager_name: "required",
              current_manager_phone: "required",
              current_manager_email: "required",
              current_work_position: "required",

              // Previous Employment
              previous_employment_work_situation: "required",
              previous_company_name: "required",
              previous_manager_name: "required",
              previous_manager_phone: "required",
              previous_manager_email: "required",
              previous_work_position: "required",

              // Personal Reference
              personal_ref_name: "required",
              personal_ref_relationship: "required",
              personal_ref_mobile: "required",
              personal_ref_email: "required",
              personal_ref_state: "required",
              personal_ref_street: "required",
              personal_ref_suburb: "required",

              // Professional Reference
              professional_ref_name: "required",
              professional_ref_relationship: "required",
              professional_ref_mobile: "required",
              professional_ref_email: "required",
            }, 
          });
    </script>

@endsection