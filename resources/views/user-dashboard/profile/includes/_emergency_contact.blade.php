<div class="form-group {{ $errors->has('emergency_contact_name') ? 'has-error' : '' }}">
    {{ Form::label('emergency_contact_name', 'Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('emergency_contact_name', null, ['class' => 'form-control']) }}
           @if ($errors->has('emergency_contact_name'))
        <span class="text-danger">{{ $errors->first('emergency_contact_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('emergency_contact_relation') ? 'has-error' : '' }}">
    {{ Form::label('emergency_contact_relation', 'Relation', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('emergency_contact_relation', array_map('ucwords', App\Models\Tenant\TenantProfile::GUARDIANRELATIONSHIP), null, ['class' => 'form-control']) }}
        @if ($errors->has('emergency_contact_relation'))
        <span class="text-danger">{{ $errors->first('emergency_contact_relation') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('emergency_contact_number', 'Mobile Number', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('emergency_contact_number', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group {{ $errors->has('emergency_contact_street') ? 'has-error' : '' }}">
    {{ Form::label('emergency_contact_street', 'Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('emergency_contact_street', null, ['class' => 'form-control']) }}
           @if ($errors->has('emergency_contact_street'))
        <span class="text-danger">{{ $errors->first('emergency_contact_street') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('emergency_contact_suburb', 'Suburb', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('emergency_contact_suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all show-with-parent {{ $errors->has('emergency_contact_state') ? 'has-error' : '' }}">
    {{ Form::label('emergency_contact_state', 'State', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('emergency_contact_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
         @if ($errors->has('emergency_contact_state'))
        <span class="text-danger">{{ $errors->first('emergency_contact_state') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('emergency_contact_home_phone', 'Home Phone', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('emergency_contact_home_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('emergency_contact_work_phone', 'Work Phone', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('emergency_contact_work_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group {{ $errors->has('emergency_contact_email') ? 'has-error' : '' }}">
    {{ Form::label('emergency_contact_email', 'Email Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::email('emergency_contact_email', null, ['class' => 'form-control']) }}
         @if ($errors->has('emergency_contact_email'))
        <span class="text-danger">{{ $errors->first('emergency_contact_email') }}</span>
        @endif
    </div>
</div>

<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>