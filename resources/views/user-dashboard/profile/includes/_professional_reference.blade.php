<div class="form-group">
    {{ Form::label('professional_ref_name', 'Reference Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_occupation', 'Occupation', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_occupation', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_relationship', 'Relation', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('professional_ref_relationship', array_map('ucwords', App\Models\Tenant\TenantProfile::GUARDIANRELATIONSHIP), null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_company', 'Company', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_company', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_phone', 'Phone Number', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_mobile', 'Mobile Number', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_mobile', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_email', 'Email Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_email', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_company_street', 'Address', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_company_street', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_company_suburb', 'Suburb', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('professional_ref_company_suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('professional_ref_company_state', 'State', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('professional_ref_company_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
    </div>
</div>
<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>