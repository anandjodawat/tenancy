<div class="form-group">
    {{ Form::label('repayment_loans', 'Loans', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('repayment_loans', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('repayment_credit_cards', 'Credit Card/s', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('repayment_credit_cards', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('repayment_hire_purchase', 'Hire Purchase', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('repayment_hire_purchase', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('repayment_store_card', 'Store Card/s', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('repayment_store_card', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('repayment_others', 'Others', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('repayment_others', null, ['class' => 'form-control']) }}
    </div>
</div>

{{-- <div class="form-group">
    {{ Form::label('repayment_expenses', 'Expenses', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('repayment_expenses', null, ['class' => 'form-control']) }}
    </div>
</div>
 --}}
<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
{{ Form::submit('Submit', ['class' => 'btn btn-success pull-right ten-submit-btn']) }}
<div class="clearfix"></div>
<hr>