<div class="form-group">
    {{ Form::label('previous_living_arrangement', 'Living Arrangement', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 personal-info">
        {{ Form::select('previous_living_arrangement', array_map('ucwords', App\Models\Tenant\TenantProfile::CURRENTLIVINGARRANGEMENT), null, ['class' => 'form-control']) }}
    </div>
</div>

{{-- <div class="form-group">
    {{ Form::label('previous_address_unit', 'Address Unit', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_unit', null, ['class' => 'form-control']) }}
    </div>
</div> --}}

<div class="form-group">
    {{ Form::label('previous_address_street', 'Address', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_street', null, ['class' => 'form-control']) }}
    </div>
</div>

{{-- <div class="form-group">
    {{ Form::label('previous_address_street_name', 'Street Name', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_street_name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_street_type', 'Street Type', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_street_type', null, ['class' => 'form-control']) }}
    </div>
</div> --}}

<div class="form-group">
    {{ Form::label('previous_address_suburb', 'Suburb', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_state', 'State', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('previous_address_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_post_code', 'Post Code', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::number('previous_address_postcode', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_from', 'Move In Date', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_from', null, ['class' => 'form-control', 'id' => 'previous-address-movefrom-datepicker']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_to', 'Move Out Date', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_to', null, ['class' => 'form-control', 'id' => 'previous-address-moveto-datepicker']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_leave_reason', 'Reason For Leaving', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::textarea('previous_address_leave_reason', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_agent_name', 'Agent Name', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_agent_name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_rent', 'Monthly Rent', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_rent', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_email', 'Landlord Email', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::email('previous_address_email', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_bond_refund', 'Bond Refund', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_address_bond_refund', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_additional_information', 'Do you have previous address additional information?', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        Yes {{ Form::radio('previous_address_additional_information', 'yes') }}
        No {{ Form::radio('previous_address_additional_information', 'no') }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('previous_address_additional_information_detail', 'Details', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::textarea('previous_address_additional_information_detail', null, ['class' => 'form-control']) }}
    </div>
</div>


<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>