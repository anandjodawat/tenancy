<div class="form-group">
    {{ Form::label('current_weekly_rent', 'Current Weekly Rent', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_weekly_rent', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('vehicle_value', 'Vehicle Value', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('vehicle_value', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('vehicle_type', 'Vehicle Owned / Financed?', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('vehicle_type', ['Owned', 'Financed'], null, ['class' => 'form-control', 'placeholder' => '--select vehicle type--']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('income_type', 'Income Type', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('income_type', ['Weekly', 'Monthly', 'Fortnightly'], null, ['class' => 'form-control', 'placeholder' => '--select income type--']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('salary_wages_income', 'Salary/Wages Income', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('salary_wages_income', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('self_employed_income', 'Self Employed Income', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('self_employed_income', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('benefit_income', 'Benefit Income', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('benefit_income', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('other_income', 'Other Income', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('other_income', null, ['class' => 'form-control']) }}
    </div>
</div>

<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>