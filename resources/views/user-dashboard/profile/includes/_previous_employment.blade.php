<div class="form-group">
    {{ Form::label('previous_employment_work_situtation', 'Previous Employment', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 {{-- custom-select --}} personal-info">
        {{ Form::select('previous_employment_work_situtation', array_map('ucwords', App\Models\Tenant\TenantProfile::EMPLOYMENTWORKSITUATION), null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-previous-employment {{ $errors->has('previous_company_name') ? 'has-error' : '' }}">
    {{ Form::label('previous_company_name', 'Company Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('previous_company_name', null, ['class' => 'form-control']) }}
         @if ($errors->has('previous_company_name'))
        <span class="text-danger">{{ $errors->first('previous_company_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-previous-employment {{ $errors->has('previous_manager_name') ? 'has-error' : '' }}">

    {{ Form::label('previous_manager_name', 'Manager Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('previous_manager_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('previous_manager_name'))
        <span class="text-danger">{{ $errors->first('previous_manager_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-previous-employment {{ $errors->has('previous_manager_phone') ? 'has-error' : '' }}">

    {{ Form::label('previous_manager_phone', 'Manager Phone', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('previous_manager_phone', null, ['class' => 'form-control']) }}
        @if ($errors->has('previous_manager_phone'))
        <span class="text-danger">{{ $errors->first('previous_manager_phone') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-previous-employment {{ $errors->has('previous_manager_email') ? 'has-error' : '' }}">

    {{ Form::label('previous_manager_email', 'Manager Email Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::email('previous_manager_email', null, ['class' => 'form-control']) }}
        @if ($errors->has('previous_manager_email'))
        <span class="text-danger">{{ $errors->first('previous_manager_email') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-previous-employment {{ $errors->has('previous_work_position') ? 'has-error' : '' }}">

    {{ Form::label('previous_work_position', 'Position', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('previous_work_position', null, ['class' => 'form-control']) }}
        @if ($errors->has('previous_work_position'))
        <span class="text-danger">{{ $errors->first('previous_work_position') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-previous-employment">
    {{ Form::label('previous_company_street', 'Company Address', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_company_street', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-previous-employment">
    {{ Form::label('previous_company_suburb', 'Suburb', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_company_suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-previous-employment">
    {{ Form::label('previous_company_state', 'State', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('previous_company_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-previous-employment">
    {{ Form::label('previous_industry_type', 'Previous Employment Work Industry', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('previous_industry_type', array_map('ucwords', App\Models\Tenant\TenantProfile::INDUSTRYTYPE), null, ['class' => 'form-control']) }}
    </div>
</div>

{{-- <div class="form-group hide-previous-employment">
    {{ Form::label('previous_employment_nature', 'Employment Nature', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('previous_employment_nature', ['CASUAL', 'FULL TIME', 'PART TIME'], null, ['class' => 'form-control']) }}
    </div>
</div>
 --}}
<div class="form-group hide-previous-employment">
    {{ Form::label('previous_employment_start_date', 'Start Date', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_employment_start_date', null, ['class' => 'form-control', 'id'=>'previous-employment-datepicker']) }}
    </div>
</div>

<div class="form-group hide-previous-employment">
    {{ Form::label('previous_employment_end_date', 'End Date', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_employment_end_date', null, ['class' => 'form-control', 'id'=>'previous-employment-end-datepicker']) }}
    </div>
</div>

<div class="form-group hide-previous-employment">
    {{ Form::label('previous_gross_annual_salary', 'Gross Annual Salary', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('previous_gross_annual_salary', null, ['class' => 'form-control']) }}
    </div>
</div>

<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>