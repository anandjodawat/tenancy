<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {{ Form::label('title', 'Title', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('title', ['Mr.', 'Mrs', 'Miss'], null, ['class' => 'form-control']) }}

        @if ($errors->has('title'))
        <span class="text-danger">{{ $errors->first('title') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    {{ Form::label('first_name', 'First Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) }}
        @if ($errors->has('first_name'))
        <span class="text-danger">{{ $errors->first('first_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('middle_name') ? 'has-error' : '' }}">
    {{ Form::label('middle_name', 'Middle Name', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('middle_name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
    {{ Form::label('last_name', 'Last Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('last_name'))
        <span class="text-danger">{{ $errors->first('last_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('upload', 'Profile Image', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::file('image', ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('dob', 'Date Of Birth', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('dob', null, ['class' => 'form-control' , 'id' => 'personal-information-datepicker']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('gender', 'Gender', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 info-radio-btn">
        Male {{ Form::radio('gender', 'male') }}
        Female {{ Form::radio('gender', 'female') }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('marital_status', 'Marital Status', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('marital_status', array_map('ucwords', App\Models\Tenant\TenantProfile::MARITALSTATUS), null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('home_phone', 'Home Phone', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('home_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('work_phone', 'Work Phone', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('work_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
    {{ Form::label('mobile', 'Mobile', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('mobile', null, ['class' => 'form-control']) }}
        @if ($errors->has('mobile'))
        <span class="text-danger">{{ $errors->first('mobile') }}</span>
        @endif
    </div>
</div>

{{-- <div class="form-group">
    {{ Form::label('fax', 'Fax', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('fax', null, ['class' => 'form-control']) }}
    </div>
</div> --}}

<div class="form-group">
    {{ Form::label('email', 'Email Address', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('email', auth()->user()->email, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('preferred_time_to_be_called', 'Preferred time to be called?', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('preferred_time_to_be_called', ['Anytime', 'Morning', 'Day'], null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('smoke', 'Do you smoke?', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 info-radio-btn">
        Yes {{ Form::radio('smoke', 'yes') }}
        No {{ Form::radio('smoke', 'no') }}
    </div>
</div>

<div class="form-group smoke-hide">
    {{ Form::label('smoke_inside', 'Do you smoke inside?', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 info-radio-btn">
        Yes {{ Form::radio('smoke_inside', 'yes') }}
        No {{ Form::radio('smoke_inside', 'no') }}
    </div>
</div>

<div class="form-group">
    {{ Form::label('stydying', 'Are you stydying?', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 info-radio-btn">
        Yes {{ Form::radio('studying', 'yes') }}
        No {{ Form::radio('studying', 'no') }}
    </div>
</div>

<div class="study-hide">
    <div class="form-group {{ $errors->has('institution') ? 'has-error' : '' }}">
        {{ Form::label('institution', 'Institution Name', ['class' => 'col-md-4 control-label institution requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('institution', null, ['class' => 'form-control']) }}
            @if ($errors->has('institution'))
            <span class="text-danger">{{ $errors->first('institution') }}</span>
            @endif
        </div>
    </div>
 
    <div class="form-group {{ $errors->has('enrollment_number') ? 'has-error' : '' }}">
        {{ Form::label('enrollment_number', 'Enrollment Number', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('enrollment_number', null, ['class' => 'form-control']) }}
            @if ($errors->has('enrollment_number'))
            <span class="text-danger">{{ $errors->first('enrollment_number') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('course_contact_number') ? 'has-error' : '' }}">
        {{ Form::label('course_contact_number', 'Course Contact Number', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('course_contact_number', null, ['class' => 'form-control']) }}
            @if ($errors->has('course_contact_number'))
            <span class="text-danger">{{ $errors->first('course_contact_number') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('institution_phone') ? 'has-error' : '' }}">
        {{ Form::label('institution_phone', 'Institution Phone', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('institution_phone', null, ['class' => 'form-control']) }}
            @if ($errors->has('institution_phone'))
            <span class="text-danger">{{ $errors->first('institution_phone') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('source_of_income') ? 'has-error' : '' }}">
        {{ Form::label('source_of_income', 'Source of Income', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('source_of_income', null, ['class' => 'form-control']) }}
            @if ($errors->has('source_of_income'))
            <span class="text-danger">{{ $errors->first('source_of_income') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('net_weekly_amount') ? 'has-error' : '' }}">
        {{ Form::label('net_weekly_amount', 'Net Weekly Amount', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('net_weekly_amount', null, ['class' => 'form-control']) }}
            @if ($errors->has('net_weekly_amount'))
            <span class="text-danger">{{ $errors->first('net_weekly_amount') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('guardian_name') ? 'has-error' : '' }}">
        {{ Form::label('guardian_name', 'Guardian Name', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('guardian_name', null, ['class' => 'form-control']) }}
            @if ($errors->has('guardian_name'))
            <span class="text-danger">{{ $errors->first('guardian_name') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('guardian_relationship') ? 'has-error' : '' }}">
        {{ Form::label('guardian_relationship', 'Guardian Relationship', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6 custom-select personal-info">
            {{ Form::select('guardian_relationship', array_map('ucwords', App\Models\Tenant\TenantProfile::GUARDIANRELATIONSHIP), null, ['class' => 'form-control']) }}
            @if ($errors->has('guardian_relationship'))
            <span class="text-danger">{{ $errors->first('guardian_relationship') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('guardian_address') ? 'has-error' : '' }}">
        {{ Form::label('guardian_address', 'Guardian Address', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('guardian_address', null, ['class' => 'form-control']) }}
            @if ($errors->has('guardian_address'))
            <span class="text-danger">{{ $errors->first('guardian_address') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('guardian_phone_number') ? 'has-error' : '' }}">
        {{ Form::label('guardian_phone_number', 'Guardian Phone Number', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            {{ Form::text('guardian_phone_number', null, ['class' => 'form-control']) }}
            @if ($errors->has('guardian_phone_number'))
            <span class="text-danger">{{ $errors->first('guardian_phone_number') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('pension') ? 'has-error' : '' }}">
        {{ Form::label('pension', 'Do you get pension?', ['class' => 'col-md-4 control-label requiredcls']) }}

        <div class="col-md-6">
            Yes {{ Form::radio('pension', 'yes') }}
            No {{ Form::radio('pension', 'no') }}

            @if ($errors->has('pension'))
            <span class="text-danger">{{ $errors->first('pension') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group hide-pension">
        {{ Form::label('pension_type', 'Pension Type', ['class' => 'col-md-4 control-label']) }}

        <div class="col-md-6">
            {{ Form::text('pension_type', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group hide-pension">
        {{ Form::label('pension_number', 'Pension Number', ['class' => 'col-md-4 control-label']) }}

        <div class="col-md-6">
            {{ Form::text('pension_number', null, ['class' => 'form-control']) }}
        </div>
    </div>

</div>
<hr>
<button class="btn btn-success next-button pull-right" data-submit="personal-information">
    Next
</button>
<div class="clearfix"></div>
<hr>
