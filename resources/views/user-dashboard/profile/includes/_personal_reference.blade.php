<div class="form-group {{ $errors->has('personal_reference_name') ? 'has-error' : '' }}">
    {{ Form::label('personal_reference_name', 'Reference Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_name', null, ['class' => 'form-control']) }}
          @if ($errors->has('personal_reference_name'))
        <span class="text-danger">{{ $errors->first('personal_reference_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('personal_ref_occupation', 'Occupation', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_occupation', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group {{ $errors->has('personal_ref_relationship') ? 'has-error' : '' }}">

    {{ Form::label('personal_ref_relationship', 'Relation', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('personal_ref_relationship', array_map('ucwords', App\Models\Tenant\TenantProfile::GUARDIANRELATIONSHIP), null, ['class' => 'form-control']) }}
         @if ($errors->has('personal_ref_relationship'))
        <span class="text-danger">{{ $errors->first('personal_ref_relationship') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('personal_ref_phone', 'Phone Number', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group {{ $errors->has('personal_ref_mobile') ? 'has-error' : '' }}">
    {{ Form::label('personal_ref_mobile', 'Mobile Number', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_mobile', null, ['class' => 'form-control']) }}
         @if ($errors->has('personal_ref_mobile'))
        <span class="text-danger">{{ $errors->first('personal_ref_mobile') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('personal_ref_email') ? 'has-error' : '' }}">
    {{ Form::label('personal_ref_email', 'Email Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_email', null, ['class' => 'form-control']) }}
         @if ($errors->has('personal_ref_email'))
        <span class="text-danger">{{ $errors->first('personal_ref_email') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('personal_ref_street') ? 'has-error' : '' }}">
    {{ Form::label('personal_ref_street', 'Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_street', null, ['class' => 'form-control']) }}
         @if ($errors->has('personal_ref_street'))
        <span class="text-danger">{{ $errors->first('personal_ref_street') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('personal_ref_suburb') ? 'has-error' : '' }}">
    {{ Form::label('personal_ref_suburb', 'Suburb', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('personal_ref_suburb', null, ['class' => 'form-control']) }}
         @if ($errors->has('personal_ref_suburb'))
        <span class="text-danger">{{ $errors->first('personal_ref_suburb') }}</span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('personal_ref_state') ? 'has-error' : '' }}">
    {{ Form::label('personal_ref_state', 'State', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('personal_ref_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
         @if ($errors->has('personal_ref_state'))
        <span class="text-danger">{{ $errors->first('personal_ref_state') }}</span>
        @endif
    </div>
</div>
<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>