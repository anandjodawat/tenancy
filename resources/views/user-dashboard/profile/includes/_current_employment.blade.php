<div class="form-group">
    {{ Form::label('current_employment_work_situation', 'Current Employment', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 {{-- custom-select --}} personal-info">
        {{ Form::select('current_employment_work_situation', App\Models\Tenant\TenantProfile::EMPLOYMENTWORKSITUATION, null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-current-employment {{ $errors->has('current_company_name') ? 'has-error' : '' }}">
    {{ Form::label('current_company_name', 'Company Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_company_name', null, ['class' => 'form-control']) }}
           @if ($errors->has('current_company_name'))
        <span class="text-danger">{{ $errors->first('current_company_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-current-employment {{ $errors->has('current_manager_name') ? 'has-error' : '' }}">
    {{ Form::label('current_manager_name', 'Manager Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_manager_name', null, ['class' => 'form-control']) }}
        @if ($errors->has('current_manager_name'))
        <span class="text-danger">{{ $errors->first('current_manager_name') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-current-employment {{ $errors->has('current_manager_phone') ? 'has-error' : '' }}">
    {{ Form::label('current_manager_phone', 'Manager Phone', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_manager_phone', null, ['class' => 'form-control']) }}
        @if ($errors->has('current_manager_phone'))
        <span class="text-danger">{{ $errors->first('current_manager_phone') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-current-employment {{ $errors->has('current_manager_email') ? 'has-error' : '' }}">
    {{ Form::label('current_manager_email', 'Manager Email Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::email('current_manager_email', null, ['class' => 'form-control']) }}
        @if ($errors->has('current_manager_email'))
        <span class="text-danger">{{ $errors->first('current_manager_email') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-current-employment {{ $errors->has('current_work_position') ? 'has-error' : '' }}">
    {{ Form::label('current_work_position', 'Position', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_work_position', null, ['class' => 'form-control']) }}
        @if ($errors->has('current_work_position'))
        <span class="text-danger">{{ $errors->first('current_work_position') }}</span>
        @endif
    </div>
</div>

<div class="form-group hide-current-employment">
    {{ Form::label('current_company_street', 'Company Address', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_company_street', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-current-employment">
    {{ Form::label('current_company_suburb', 'Suburb', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_company_suburb', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-current-employment">
    {{ Form::label('current_company_state', 'State', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('current_company_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group hide-current-employment">
    {{ Form::label('current_industry_type', 'Current Employment Industry', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('current_industry_type', array_map('ucwords', App\Models\Tenant\TenantProfile::INDUSTRYTYPE), null, ['class' => 'form-control']) }}
    </div>
</div>
{{-- 
<div class="form-group hide-current-employment">
    {{ Form::label('current_employment_nature', 'Employment Nature', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('current_employment_nature', ['Casual', 'Full Time', 'Part Time'], null, ['class' => 'form-control', 'placeholder' => 'Select Employment nature']) }}
    </div>
</div> --}}

<div class="form-group hide-current-employment">
    {{ Form::label('current_employment_start_date', 'Start Date', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_employment_start_date', null, ['class' => 'form-control', 'id' => 'current-employment-datepicker']) }}
    </div>
</div>

<div class="form-group hide-current-employment">
    {{ Form::label('current_job_status', 'Job Status', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('current_job_status', ['Still in the Job', 'Finished the Job'], null, ['class' => 'form-control', 'placeholder' => 'Select job status']) }}
    </div>
</div>

<div class="form-group hide-current-employment">
    {{ Form::label('current_gross_annual_salary', 'Gross Annual Salary', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_gross_annual_salary', null, ['class' => 'form-control']) }}
    </div>
</div>

<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>