<div class="form-group show-with-parent">
    {{ Form::label('current_living_arrangement', 'Living Arrangement', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6 personal-info">
        {{ Form::select('current_living_arrangement', App\Models\Tenant\TenantProfile::CURRENTLIVINGARRANGEMENT, null, ['class' => 'form-control']) }}
    </div>
</div>

{{-- <div class="form-group">
    {{ Form::label('current_address_unit', 'Address Unit', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_unit', null, ['class' => 'form-control']) }}
    </div>
</div> --}}

<div class="form-group {{ $errors->has('current_address_street') ? 'has-error' : '' }}">
    {{ Form::label('current_address_street', 'Address', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_street', null, ['class' => 'form-control']) }}
            @if ($errors->has('current_address_street'))
        <span class="text-danger">{{ $errors->first('current_address_street') }}</span>
        @endif
    </div>
</div>

{{-- <div class="form-group {{ $errors->has('current_address_street_name') ? 'has-error' : '' }}">
    {{ Form::label('current_address_street_name', 'Street Name', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_street_name', null, ['class' => 'form-control']) }}
           @if ($errors->has('current_address_street_name'))
        <span class="text-danger">{{ $errors->first('current_address_street_name') }}</span>
        @endif
    </div>
</div> --}}

<div class="form-group {{ $errors->has('current_address_suburb') ? 'has-error' : '' }}">
    {{ Form::label('current_address_suburb', 'Suburb', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_suburb', null, ['class' => 'form-control']) }}
           @if ($errors->has('current_address_suburb'))
        <span class="text-danger">{{ $errors->first('current_address_suburb') }}</span>
        @endif
    </div>
</div>

<div class="form-group show-with-parent {{ $errors->has('current_address_state') ? 'has-error' : '' }}">
    {{ Form::label('current_address_state', 'State', ['class' => 'col-md-4 control-label requiredcls']) }}

    <div class="col-md-6 custom-select personal-info">
        {{ Form::select('current_address_state', App\Models\Tenant\TenantProfile::STATES, null, ['class' => 'form-control']) }}
           @if ($errors->has('current_address_state'))
        <span class="text-danger">{{ $errors->first('current_address_state') }}</span>
        @endif
    </div>
</div>

<div class="form-group">
    {{ Form::label('current_address_post_code', 'Post Code', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_post_code', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all">
    {{ Form::label('current_address_move_in', 'Move In Date', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_move_in', null, ['class' => 'form-control', 'id' => 'current-address-move-datepicker']) }}
    </div>
</div>

<div class="form-group current-address-hide-all hide-owner">
    {{ Form::label('current_address_landlord_name', 'Landlord Name', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_landlord_name', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all hide-owner">
    {{ Form::label('current_address_monthly_rent', 'Monthly Rent', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_monthly_rent', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all hide-owner">
    {{ Form::label('current_address_leave_reason', 'Leave Reason', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::textarea('current_address_leave_reason', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all hide-owner">
    {{ Form::label('current_address_landlord_phone', 'Landlord Phone', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_landlord_phone', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all hide-owner">
    {{ Form::label('current_address_email', 'Landlord Email', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::email('current_address_email', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group current-address-hide-all hide-owner">
    {{ Form::label('current_address_landlord_bond_refund', 'Bond Refund', ['class' => 'col-md-4 control-label']) }}

    <div class="col-md-6">
        {{ Form::text('current_address_landlord_bond_refund', null, ['class' => 'form-control']) }}
    </div>
</div>

<hr>
<button class="btn btn-primary prev-button pull-left">Previous</button>
<button class="btn btn-success next-button pull-right">Next</button>
<div class="clearfix"></div>
<hr>