<!DOCTYPE html>
<html>
<head>
    <title>Tenant Application Form</title>
    <style>
    .page-break {
        page-break-after: always;
    }

    body {
        margin: 0px;
        font-size: 12px;
        font-family: sans-serif;
        margin-left:-25px;
        margin-right:-20px;
    }

    h1 {
        text-align: center;
    }

    h2 {
        font-weight: bold;
        font-size: 11px;
        color: #6d6e71;
        line-height: 14px;
        text-transform: uppercase;
    }
    h3{
        font-size: 14px;
    }


    input[type="checkbox"], input[type="radio"] {
        vertical-align: middle;
        margin-top: 7px;
        line-height: 7px;
    }

    input[type="text"]{
        height: 20px;
        vertical-align: middle;
        /*background-color: rgb(233, 236, 247);*/
        background-color: #f1f1f2;
        border-left-color: #fff;
        border-right-color: transparent;
        border-top-col0or: transparent;
        border-bottom-color: transparent;
    }

    textarea{
     /*background-color: rgb(233, 236, 247);*/
     background-color: #f1f1f2;
     border-color: rgb(233, 236, 247);
     height: 60px;
 }

 label {
    /*position: relative;*/
    border-right-color: grey;

    vertical-align: middle;
    font-size: 11px;


}
small {
 vertical-align: middle;

}
span{
    height: 90px;
    padding-top: 16px;
    padding-bottom: 16px;
}
ol li{
    margin-bottom: 10px;

}

th{
    /*background: rgb(233, 236, 247);*/
    background-color: #f1f1f2;
    height: 20px;
    font-size: 11px;
    font-weight: 400;
    padding-left: 5px;
    padding-right: 5px;
    color: #808285;
    line-height: 13px;
}

td{
    /*background: rgb(224, 225, 233);*/
    background-color: #f1f1f2;
    height: 20px;
    padding-left: 5px;
    padding-right: 5px;
    color: #808285;
    line-height: 13px;
    font-size: 11px;
}

table{
    margin-top: -3px;
    cell-spacing: 0px;
}
.terms-condition p{
    background-color: #f1f1f2;
    color: #808285;
    line-height: 13px;
    font-size: 8px;
    margin-top: 3px;
    margin-bottom: 3px;
    padding-left: 5px;
    padding-right: 5px;

}

.terms-condition ul li{
    background-color: #f1f1f2;
    color: #808285;
    line-height: 13px;
    font-size: 8px;
    margin-top: 3px;
    margin-bottom: 3px;
    padding-left: 5px;
    padding-right: 5px;

}
.terms-condition ul{
    background-color: #f1f1f2;
    color: #808285;
    margin-bottom: 2px;
    margin-top: 2px;
    padding-left: 5px;
    padding-right: 5px;
    font-size: 8px;

}

.rental-rewards .rental-rewards-section, .terms-condition .terms-condition-section {
  background-color: #f1f1f2;
  padding: 10px;
  color: #808285;
}

.rental-rewards p {
    font-size: 11px;
    line-height: 13px;
    margin-top: 3px;
    margin-bottom: 3px;
    background-color: #f1f1f2;
    padding-left: 5px;
    padding-right: 5px;
    color: #808285;
}

.rental-rewards li{
    font-size: 11px;
    line-height: 13px;
    margin-top: 3px;
    margin-bottom: 3px;
    background-color: #f1f1f2;
    padding-left: 5px;
    padding-right: 5px;
    color: #808285;
}

.rental-rewards ol{
    font-size: 11px;
    margin-bottom: 5px;
    margin-top: 5px;
    background-color: #f1f1f2;
    padding-left: 5px;
    padding-right: 5px;
    color: #808285;
}

.logo_class1{
    max-width: 40px;
    width: 100%;
    float: right;
    margin-top:-40px;
    overflow:hidden;
}

.logo_class2{
    max-width: 100px;
    width: 100%;
    float: right;
    margin-top:-40px;
    overflow:hidden;

}

</style>
</head>
<body style="">
<?php
  $url = auth()->user()->agency_logo();
  $headerSection = '<div style="width: 100%; float: left;">';
  if(!empty($url)) {
    $headerSection .= '<img src="' . url($url) . '" style="float: left; max-width: 130px; width: 40%; margin-top:-30px;">';
  }
  $headerSection .= '<h2 style="max-width: 300px; float: right; margin-top:-20px; margin-left:50px; text-align: right; padding-right: 10px;">Residential Tenancy Application</h2>
</div>';

?>


    <div style="width: 100%; float: left;">
      <?php
      if($url){
          ?>
          <div style="float: right;">
              <img src="{{ url($url) }}" style="max-width: 130px; width: 100%; ">
          </div>
          <?php } ?>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br>

    <div style="background:{{ auth()->user()->agency_profile()->preferred_color }}; height: 50px;">&nbsp;</div>

    <br>
    <div>
        <h1 style="width: 100%; text-align: center; text-transform: capitalize; font-size: 48px; color:#000;">Residential Tenancy Application</h1>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


    <div style="width: 90%; margin: 0 auto;">
        <div class="second-page" style="width: 49%; text-align: left; float: left;">
            <h2>AGENT DETAILS</h2>
            <table style="width:100%">
                <tr>
                    <td style="height: 22px;" colspan="2">Agency Name: {{ optional(auth()->user()->agency_profile())->name }}</td>
                </tr>
                <tr>
                    <td style="height: 22px;" colspan="2">Property Manager: {{ auth()->user()->name }}</td>
                </tr>

                <tr>
                    <td style="height: 22px;" colspan="2">Email: {{ auth()->user()->email }}</td>
                </tr>
                <tr>
                    <td style="height: 22px;" colspan="2">Phone Number: {{ optional(auth()->user()->agency_profile())->phone_number }}</td>
                </tr>
                <tr>
                    <td style="height: 22px;" colspan="2">Date Received: &nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 22px;" colspan="2">Application ID: &nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 22px;" colspan="2">Apply Online: http://www.tenancyapplication.com.au/my-agency/{{ optional(auth()->user()->agency_profile())->name }}</td>
                </tr>

            </table>
        </div>

        <div style="width:49%; float: right; bottom: 0;">
          <h2>Supporting Documents</h2>
          <table style="width:100%;" >
            <tr>
              <td colspan="2" style="height: 22px; color: #000;">A minimum of 100 points is required to apply.</td>
           </tr>
            @foreach ($supporting_documents as $document)
            <tr>
                <td style="height: 22px;" colspan="2"><input type="checkbox">&nbsp;{{ $document->supporting_document }}</td>
            </tr>
            @endforeach
          </table>
        </div>
  </div>

<div class="page-break"></div>
@php
$class = "";
if($image_size != null) {
    $class = "logo_class1";
    if($image_size[0]>=300 || $image_size[1]>=300 ){
        $class = "logo_class2";
    }
 if($image_size[0]>=500 || $image_size[1]>=300 ){
        $class = "logo_class1";
    }
}
@endphp
<?php echo $headerSection; ?>
<div style="clear:both;"></div>
<div style="width: 100%; float: left;">
  <hr style="color:{{ auth()->user()->agency_profile()->preferred_color }}; width:100%;">
</div>

<div style="clear:both;"></div>
<div style="width: 90%; margin: 0 auto;">
      <div class="second-page" style="width: 49%; text-align: left; float: left;">
        <h2 style="margin-top: 4px; margin-bottom: 4px;">Property Details</h2>
        <table style="width:100%">
          <tr>
              <td style="height: 42px; vertical-align: top;" colspan="3">Full Address:&nbsp;</td>
          </tr>
          <tr>
              <td style="height: 22px; width: 40%;">Post Code: &nbsp;</td>
              <td style="height: 22px; width: 60%;" colspan="2">Property Code: &nbsp;</td>
          </tr>
          <tr>
            <td style="height: 22px; width: 50%;">No. of Bedroom: &nbsp;</td>
            <td style="height: 22px; width: 50%;" colspan="2">Bond: &nbsp; $</td>
          </tr>
          <tr>
            <td style="height: 22px; width: 20%;">Rent: &nbsp;</td>
            <td style="height: 22px; width: 40%;"><input type="checkbox">&nbsp;Weekly $</td>
            <td style="height: 22px; width: 40%;"><input type="checkbox">&nbsp;Monthly $</td>
          </tr>
          <tr>
            <td style="height: 22px;" colspan="3">Commencement Date: &nbsp;</td>
         </tr>
         <tr>
           <td style="height: 22px;" colspan="3">Preferred Lease Term: &nbsp;</td>
         </tr>
         <tr>
           <td style="height: 22px;" colspan="3">Total No. of Occupants: &nbsp;</td>
         </tr>
         <tr>
           <td style="height: 22px;">Total No. of Children: &nbsp;</td>
           <td style="height: 22px;" colspan="2">Ages: &nbsp;</td>
          </tr>
          <tr>
            <td style="height: 22px;">No. of Vehicles: &nbsp;</td>
            <td style="height: 22px;" colspan="2">No. of Pets: &nbsp;</td>
          </tr>
          <tr>
            <td style="height: 32px; vertical-align: top;" colspan="3">How did you find out about this property?:&nbsp;</td>
          </tr>
          <tr>
            <td style="height: 22px;" colspan="3">Date Inspected:&nbsp;</td>
          </tr>
          <tr>
            <td style="height: 22px;" colspan="3">Inspection Code:&nbsp;</td>
          </tr>
          <tr>
            <td style="height: 104px; vertical-align: top;" colspan="3">Other Occupants Details:&nbsp;</td>
          </tr>
      </table>

      <h2 style="margin-top: 4px; margin-bottom: 4px;">Personal Details</h2>
      <table style="width:100%">
        <tr>
          <td colspan="3" style="height: 22px;">Name: &nbsp;</td>
        </tr>
        <tr>
          <td style="height; 22px;">Gender: &nbsp;</td>
          <td colspan="2" style="height: 22px;">Date of Birth: &nbsp;</td>
        </tr>
        <tr>
          <td style="height; 22px;">Home Phone: &nbsp;</td>
          <td colspan="2" style="height: 22px;">Work Phone: &nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" style="height: 22px;">Mobile: &nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" style="height: 22px;">Email: &nbsp;</td>
        </tr>
      </table>

      <h2 style="margin-top: 4px; margin-bottom: 4px;">Emergency Contact</h2>
      <table style="width:100%">
        <tr>
          <td>Name: &nbsp;</td>
        </tr>
        <tr>
          <td>Relationship: &nbsp;</td>
        </tr>
        <tr>
          <td>Contact Address: &nbsp;</td>
        </tr>
        <tr>
          <td>Home Phone: &nbsp;</td>
        </tr>
        <tr>
          <td>Work Phone: &nbsp;</td>
        </tr>
        <tr>
          <td>Mobile: &nbsp;</td>
        </tr>
        <tr>
          <td>Email: &nbsp;</td>
        </tr>
      </table>




      </div>

    <div class="second-page" style="width: 49%; text-align: left; float: right;">

    <h2 style="margin-top: 4px; margin-bottom: 4px;">Current Address Details</h2>
    <table style="width:100%">
      <tr>
        <td style="height: 44px; vertical-align: top;" colspan="3">What are your current living arrangements?</td>
      </tr>
      <tr>
        <td style="height: 22px; width: 100%;" colspan="3">Address: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px; width: 33.33%;">Suburb: &nbsp;</td>
        <td style="height: 22px; width: 33.33%;">State: &nbsp;</td>
        <td style="height: 22px; width: 33.33%;">Post Code: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">When did you move in?: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 44px; vertical-align: top;" colspan="3">Reason for leaving:</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">Landlord's Name: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">Is your postal address different?: &nbsp;<input type="checkbox">&nbsp;Yes&nbsp;<input type="checkbox">&nbsp;No</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">Address: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">Suburb: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">State: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;" colspan="3">Post Code: &nbsp;</td>
      </tr>
    </table>

    <h2 style="margin-top: 4px; margin-bottom: 4px;">Previous address details</h2>
    <table style="width:100%">
      <tr>
        <td style="height: 44px; vertical-align: top;" colspan="3">What was your previous living arrangements?: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px; width: 100%;" colspan="3">Address: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px; width: 33.33%;">Suburb: &nbsp;</td>
        <td style="height: 22px; width: 33.33%;">State: &nbsp;</td>
        <td style="height: 22px; width: 33.33%;">Post Code: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px; width: 50%;">From Date: &nbsp;</td>
        <td style="height: 22px; width: 50%;" colspan="2">To Date: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 44px; vertical-align: top;" colspan="3">Reason for leaving:</td>
      </tr>
    </table>
  </div>
</div>


<div class="page-break"></div>

<?php echo $headerSection; ?>
<div style="clear:both;"></div>
<div style="width: 100%; float: left;">
  <hr style="color:{{ auth()->user()->agency_profile()->preferred_color }}; width:100%;">
</div>

<div style="clear:both;"></div>

<div style="width: 90%; margin: 0 auto;">
  <div class="second-page" style="width: 49%; text-align: left; float: left;">
    <h2 style="margin-top: 4px; margin-bottom: 4px;">Current Employment Details</h2>
    <table style="width:100%">
      <tr>
        <td style="height: 44px; vertical-align: top;">What is your current work situation?: </td>
      </tr>
      <tr>
        <td><input type="checkbox">Currently Employed&nbsp;<input type="checkbox">Self Employed&nbsp;<input type="checkbox">Unemployed</td>
      </tr>
      <tr>
        <td style="height: 22px;">Company Name: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Manager/Contact Name: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Phone: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Email: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 44px; vertical-align: top;">Current Address: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Industry: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Occupation/Position: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Nature of Employment: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">When did you start?: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Gross Annual Salary Before Tax: &nbsp;</td>
      </tr>
    </table>


    <h2 style="margin-top: 4px; margin-bottom: 4px;">Previous Employment Details</h2>
    <table style="width:100%">
      <tr>
        <td style="height: 44px; vertical-align: top;">What was your previous work?: </td>
      </tr>
      <tr>
        <td><input type="checkbox">Previously Employed&nbsp;<input type="checkbox">Self Employed&nbsp;<input type="checkbox">Unemployed</td>
      </tr>
      <tr>
        <td style="height: 22px;">Company Name: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Manager/Contact Name: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Phone: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Email: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 44px; vertical-align: top;">Address: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Industry: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Occupation/Position: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Nature of Employment: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">When did you start?: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">When did you finish?: &nbsp;</td>
      </tr>
      <tr>
        <td style="height: 22px;">Gross Annual Salary Before Tax: &nbsp;</td>
      </tr>
    </table>
  </div>

  <div class="second-page" style="width: 49%; text-align: left; float: right;">
  <h2 style="margin-top: 4px; margin-bottom: 4px;">Personal References</h2>

  <table style="width:100%">
    <tr>
      <td colspan="2" style="height: 22px;">Someone who knows you well who will not be living with you:</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Personal Reference: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Occupation: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Relationship: &nbsp;</td>
    </tr>
    <tr>
      <td style="height: 22px; width: 50%;">Phone: &nbsp;</td>
      <td style="height: 22px; width: 50%;">Mobile: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Email: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Address: &nbsp;</td>
    </tr>
  </table>


  <h2 style="margin-top: 4px; margin-bottom: 4px;">Professional References</h2>
  <table style="width:100%">
    <tr>
      <td colspan="2" style="height: 22px;">Work colleagues, associates, etc &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Name of Professional Reference 1: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Company Name:&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Relationship: &nbsp;</td>
    </tr>
    <tr>
      <td style="height: 22px; width: 50%;">Phone: &nbsp;</td>
      <td style="height: 22px; width: 50%;">Mobile: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Email: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Company Address: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Name of Professional Reference 2: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Company Name:&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Relationship: &nbsp;</td>
    </tr>
    <tr>
      <td style="height: 22px; width: 50%;">Phone: &nbsp;</td>
      <td style="height: 22px; width: 50%;">Mobile: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Email: &nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" style="height: 22px;">Company Address: &nbsp;</td>
    </tr>
  </table>

  <h2 style="margin-top: 4px; margin-bottom: 4px;">Further Questions</h2>
  <table style="width:100%" cellpadding="2">
      <tr>
          <td style="height: 30px; width: 300px;">Has your tenancy ever been terminated by a landlord or <br/> agent?: <input type="checkbox"> Yes <input type="checkbox">No</td>
      </tr>
      <tr>
          <td>Have you ever been refused a property by any landlord or <br/> agent?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
      <tr>
          <td>Are you in debt to another landlord or agent?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
      <tr>
          <td>Have any deductions ever been made from your rental  <br/> bond?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
      <tr>
          <td>Is there any reason known to you that would affect your  <br/> future rental payments?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
      <tr>
          <td>Do you have any other applications pending on other  <br/> properties?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
      <tr>
          <td style="height: 30px; width: 300px;">Do you currently own a property?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
      <tr>
          <td style="height: 30px; width: 300px;">Are you considering buying a property after this tenancy or in  <br/> the near future?: <input type="checkbox"> Yes <input type="checkbox"> No </td>
      </tr>
  </table>

  </div>
</div>


<div class="page-break"></div>
<?php echo $headerSection; ?>
<div style="clear:both;"></div>
<div style="width: 100%; float: left;">
  <hr style="color:{{ auth()->user()->agency_profile()->preferred_color }}; width:100%;">
</div>

<div style="clear:both;"></div>
<h2 style="margin-top: 4px;">Moving Services</h2>
@if ($setting && $setting->body)
<table style="width:100%; background-color: #fff;">
  <tr>
    <td style="background-color: #fff;">
      @if (auth()->user()->agency_profile()->getFirstMedia(auth()->user()->agency_id . '-' . $setting->key))
      <div class="col-md-3">
        <img src="{{ url('/') }}{{ auth()->user()->agency_profile()->getFirstMedia(auth()->user()->agency_id . '-' . $setting->key)->getUrl() }}" alt="Utility Connection Logo" class="img img-responsive" style="max-height: 40px;">
      </div>
      @endif
    </td>
    <td style="background-color: #fff;" align="right">
        {{ $setting->connection_name }}
        <br />
        {{ $setting->contact_number }}
        <br>
        {{ $setting->value }}
    </td>
  </tr>
</table>
@endif
<br />
<table style="width:100%; margin-top: -10px;">
  <tr>
    <td colspan="3">Selecting the right utility provider can make your move
        easier and may save you money.
        Would you like a call closer to your moving date to help
        arrange connection to your household services? You'll get a
        phone call, email and help to compare and select a plan
        from available suppliers.
    </td>
  </tr>
  <tr>
    <td><input type="checkbox">Electricity</td>
    <td><input type="checkbox">Gas</td>
    <td><input type="checkbox">Telephone</td>
  </tr>
  <tr>
    <td><input type="checkbox">Internet </td>
    <td><input type="checkbox">Bottled gas</td>
    <td><input type="checkbox">Pay TV</td>
  </tr>
  <tr>
    <td><input type="checkbox">Disconnections </td>
    <td><input type="checkbox">Removalist</td>
    <td><input type="checkbox">Vehicle hire</td>
  </tr>
  <tr>
    <td colspan="3"><input type="checkbox">Cleaning services </td>
  </tr>
</table>

<table style="width:100%; margin-top: 6px;">
  <tr>
    <td style="font-size: 10.7482px;">
        @if ($setting && $setting->body)
          @php echo $setting->body; @endphp
        @else
          By submitting this application, you are providing your consent and
          authorisation to the moving services provider to arrange for the connection
          and/or disconnection of the nominated utility products or services as well as
          the purchase of the nominated product. In order to operate their business
          activities efficiently and to facilitate your request they may need to collect
          your personal information. By submitting this application you are providing
          your consent and authorisation for the moving service provider to disclose
          and use your personal information in accordance with their privacy policy.
          By submitting this application, you declare that all of the information
          contained or disclosed in this application is true and correct. You further are
          confirming your understanding and agreement to the Privacy Policy and
          Terms and Conditions which is being provided to you.. <br><br><br><br><br><br><br><br><br>
        @endif
      </td>
    </tr>
  </table>
  <table style="width:100%">
  <tr>
    <td>
      <div style="width:40%; margin-top:5px; float:left;"><br /><br /><p>........................................</p>
        <br>
        <p style="margin-top:-25px;">&nbsp;(Signature)</p>
    </div>

    <div style="width:40%; margin-top:5px; float:right;"><p>&nbsp;<p>........................................</p>
        <br>
        <p style="margin-top:-25px;">&nbsp;(Date)</p>
    </div>
  </td>
  </tr>
  </table>

<div class="page-break"></div>
<?php echo $headerSection; ?>
<div style="clear:both;"></div>
<div style="width: 100%; float: left;">
  <hr style="color:{{ auth()->user()->agency_profile()->preferred_color }}; width:100%;">
</div>

<div style="clear:both;"></div>
{{-- Rental Rewards --}}
<div class="rental-rewards">
  <h2>General Lease Terms</h2>
    <br />
    <div class="rental-rewards-section">
    @if($lease_terms)
        @php echo $lease_terms; @endphp
    @else
      <div style="width: 30%; float: left">
        <p><strong>Bond &amp; Initial Payments :</strong></p>
        <p><strong>We ask for this Terms to Follow :</strong></p>
      </div>
      <br><br><br><br><br><br>
      <ol style="list-style-type: lower-alpha;">
        <li>Bond payments can be made direct to the Bond Board or by Bank Cheque or Money Order.</li>
        <li>The first 2 weeks rent are to be paid by Visa/MasterCard Credit Card or Debit Card, AMEX or Diners.</li>
      </ol>
      <p><strong>Holding Deposit</strong>.</p>
      <p>The holding deposit can only be accepted after the application for tenancy has been approved.</p>
      <p>The holding fee of 1 WEEKS RENT keeps the premises off the market for the prospective tenant for 7 days (or longer by agreement). In consideration of an approved holding fee paid by a prospective tenant, the agent acknowledges that:</p>
      <ol style="list-style-type:lower-roman ">
        <li>The application for tenancy has been approved by the landlord; and</li>
        <li>The premises will not be let during the above period, pending the making of a residential tenancy agreement; and</li>
        <li>If the prospective tenant(s) decide not to enter into such an agreement, the landlord may retain the whole fee; and</li>
        <li>If a residential tenancy agreement is entered into, the holding fee is to be paid towards rent for the residential premises concerned.</li>
        <li>The whole of the fee will be refunded to the prospective tenant if:</li>
        <ol style="list-style-type: lower-alpha;">
          <li>the entering into of the residential tenancy agreement is conditional on the landlord carrying out repairs or other work and the landlord does not carry out the repairs or other work during the specified period</li>
          <li>the landlord/landlord’s agent have failed to disclose a material fact(s) or made misrepresentation(s) before entering into the residential tenancy agreement.</li>
        </ol>
      </ol>

      <p><strong>Ongoing Rent Payments</strong></p>
      <p>Our preferred payment method for rent is Rental Rewards. (Please note that there is a small convenience fee charged for the use of the system). These fees are charged by a third-party payment processor – Rental Rewards</p>
      <p>The fees for the convenience of the use of the services are: (must be able to set-up fee at agent level, as there is variation)</p>
      <ol style = "list-style-type: lower-alpha;">
        <li>Bank Account: $4 Testing.</li>
        <li>BPAY: $4.40 inc GST (internet banking using Rental Rewards Biller and Reference).</li>
        <li>BINt : $45</li>
      </ol>

      <p>Please read above clearly and&nbsp; do as it is asked.</p>
      {{-- Declaration Details --}}
      <h3 style="margin-bottom: 5px; margin-top: 5px;"> Declaration Details</h3>
      <table style="width:100%">
        <tr>
          <th style="width: 80%;">Have you inspected this property?</th>
          <td style="width: 10%;"><input type="checkbox"> Yes </td>
          <td style="width: 10%;"><input type="checkbox"> No </td>
        </tr>
      </table>

      <h3 style="margin-top: 5px; margin-bottom: 5px;">If Yes please fill the following fields.</h3>
      <table style="width:100%">
        <tr>
          <th style="width: 30%;">Inspected Date</th><td style="width: 70%;">&nbsp; </td>
        </tr>
      </table>

      <table style="width:100%">
        <tr>
          <th style="width: 80%;">Was the property upon your inspection in a reasonably clean and fair condition?</th><td style="width: 10%;"><input type="checkbox"> Yes </td><td style="width: 10%;"><input type="checkbox"> No </td>
        </tr>
      </table>

      <table style="width:100%">
        <tr>
          <th style="width: 30%;">Inspection Code</th><td style="width: 70%;">&nbsp; </td>
        </tr>
      </table>
      <br>

      <table style="width:100%">
        <tr>
          <td style="width: 4%;"><input type="checkbox"></td>
          <th style="width: 96%;"> By ticking this box I acknowledge that I have Read, Understood and Agree with the above Tenancy Privacy Statement / Collection Notice & Tenant Declaration and I authorise the use of my digital signature for the purpose of this application </th>
        </tr>
      </table>
    @endif
  </div>
  </div>

    <div class="page-break"></div>

    <?php echo $headerSection; ?>
    <div style="clear:both;"></div>
    <div style="width: 100%; float: left;">
      <hr style="color:{{ auth()->user()->agency_profile()->preferred_color }}; width:100%;">
    </div>

    <div style="clear:both;"></div>

    <div class="terms-condition">
      <h2>Terms and Conditions</h2>
      <div class="terms-condition-section">
        <p><strong>1. Tenancy Privacy Statement / Collection Notice</strong></p>
        <p>Due to the changes in the Privacy Laws, all property managers must ensure that you (the applicant) fully understand the National Privacy Principles and the way they must use your personal information to carry out their role as professional property managers.</p>
        <p>The information, personal or otherwise, provided by the prospective tenant in this application or that which is collected from other sources is necessary for the agent to assess the risk in providing you with the tenancy, to identify the applicant’s identity and to process, evaluate and manage the tenancy.</p>
        <p>The personal information collected about you (the applicant) in this application may be disclosed, by use of the internet or otherwise, to other parties, including:</p>

        <ul>
          <li>Trades People</li>
          <li>Financial Institutions</li>
          <li>Government and Statutory bodies</li>
          <li>Referees</li>
          <li>Solicitors</li>
          <li>Property Evaluators</li>
          <li>Existing or potential clients of the agent</li>
          <li>Rental Bond Authorities</li>
          <li>Tenant Databases</li>
          <li>Other Real Estate Agents</li>
          <li>Other Third Parties as required by law</li>
          <li>Collection Agents</li>
          <li>Verification Services</li>
          <li>Other Landlords</li>
          <li>Body Corporates</li>
        </ul>

        <p>Information already held on tenancy databases may also be disclosed to the Agent and/or landlord. Unless you advise the Agent to the contrary, the Agent may also disclose such information to The Real Estate Institute of your State and to NTD, TRA, TICA, RP DATA, BARCLAY MIS or DATAKATCH for the purpose of documenting all leasing data in the area for the benefit of its members as part of membership services and for others in the property related industries, and so as to assist them in continuing to provide the best possible service to their clients. In providing this information, you (the applicant) agree to its use, unless you advise the Agent differently.</p>

        <p>The privacy policy of your State's Real Estate Institute can be viewed by logging on to www.reia.asn.com.au and selecting your State.</p>
        <p>The privacy policy of NTD can be viewed by logging on to https://www.ntd.net.au or contact them on NTD – 1300 563 826</p>
        <p>The privacy policy of TRA can be viewed by logging on to http://tradingreference.com or contact them on TRA – (02) 9363 9244</p>
        <p>The privacy policy of TICA can be viewed by logging on to https://www.tica.com.au/ or contact them on TICA – 1902 220 346</p>
        <p>The privacy policy of BARCLAY MIS can be viewed on http://barclaymis.com.au or contact them on BARLCAY MIS – 1300 883 916</p>

        <p>The privacy policy of DATAKATCH can be viewed on http://datakatch.com.au or contact them on DATAKATCH – (02) 9086 9388</p>
        <p>The Agent will only disclose information in this way to other parties to achieve the purposes specified above or as allowed under the Privacy Act.</p>

        <p>If you (the applicant) would like to access this information you can do so by contacting the Agent at the address and contact numbers for the property you are interested in renting. You (the applicant) can also correct this information if it is inaccurate, incomplete or out of date.</p>
        <p>If your personal information is not provided to the Agent and you (the applicant) do not consent to the use of this information as specified above, the Agent cannot carry out their duties and may not be able to provide you with the lease/tenancy of the premises.</p>
      </div>
    </div>

    <div class="page-break"></div>
    <?php echo $headerSection; ?>
    <div style="clear:both;"></div>
    <div style="width: 100%; float: left;">
      <hr style="color:{{ auth()->user()->agency_profile()->preferred_color }}; width:100%;">
    </div>

    <div style="clear:both;"></div>

    <div class="terms-condition">
      <div class="terms-condition-section">
        <p><strong>2. Tenant Declaration</strong></p>
        <ul>
          <li>I acknowledge that this is an application to lease the property for which I am applying and that my application is subject to the owner's approval and the availability of the premises on the due date. No action will be taken against the landlord or agent should the premises not be ready for occupation on the due date or if my application is unsuccessful.</li>
          <li>I acknowledge that the processing period for my application could be up to 2 working days and in some circumstances longer. Unless contacted earlier by staff from the real estate agent in question - I will expect this time frame.</li>
          <li>I acknowledge that the landlord and landlord's agent will rely on the truth of my answers in assessing the application for tenancy</li>
          <li> I hereby offer to rent the property from the owner under a lease to be prepared by the Agent pursuant to the Residential Tenancies Act.</li>
          <li>I acknowledge that I will be required to pay rent and a rental bond subject to the conditions of the Agent</li>
          <li>I acknowledge that an inquiry, independent or otherwise, may be made on all applicants applying for this property, to verify the validity of the personal details that have been supplied and to check my credit worthiness. If I default under a rental agreement, the Agent may disclose details of any such default to any person whom the Agent reasonably considers has an interest receiving such information.</li>
          <li>I/we have been given the opportunity to view a copy of the standard terms and conditions that would be included in a lease, should my application be successful</li>
          <li>I declare that all information contained in this application is true and correct and given of my own free will and can be based as fact.</li>
          <li>I acknowledge that the agent in question cannot confirm that any phone lines to the property are operable or able to be reconnected. I understand that’s it’s the tenants responsibility to check with the telephone provider before proceeding with the tenancy to confirm the situation with the telephone line. Ensuring the main switch is in the off position for power connection remains the responsibly of the tenant .</li>
        </ul>

        <table style="width:100%">
          <tr>
            <td style="width: 4%;"><input type="checkbox"></td>
            <th style="width: 96%;">I acknowledge that I have Read, Understood and Agree with the General Lease Terms, Tenancy Privacy Statement / Collection Notice & Tenant Declaration </th>
          </tr>
        </table>

        <br>
        <br>
        <br>
        <table style="width:100%">
        <tr>
          <td>
            <div style="width:40%; margin-top:5px; float:left;"><p>........................................</p>
              <br>
              <p style="margin-top:-25px;">&nbsp;(Signature)</p>
          </div>

          <div style="width:40%; margin-top:5px; float:right;"><p>&nbsp;........................................</p>
              <br>
              <p style="margin-top:-25px;">&nbsp;(Date)</p>
          </div>
        </td>
        </tr>
        </table>
      </div>
    </div>
  </body>
</html>
