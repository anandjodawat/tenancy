<br>
<div class="general-lease-terms">
<p><strong>Bond &amp; Initial Payments :</strong></p>

<p><strong>We ask for this Terms to Follow : </strong></p>
<ol style="list-style-type: lower-alpha;">
    <li>Bond payments can be made direct to the Bond Board or by Bank Cheque or Money Order.</li>
    <li>The first 2 weeks rent are to be paid by Visa/MasterCard Credit Card or Debit Card, AMEX or Diners.</li>
</ol>

<p><strong>Holding Deposit</strong>.</p>

<p>The holding deposit can only be accepted after the application for tenancy has been approved.</p>

<p>The holding fee of 1 WEEKS RENT keeps the premises off the market for the prospective tenant for 7 days (or longer by agreement). In consideration of an approved holding fee paid by a prospective tenant, the agent acknowledges that:</p>
<ol style="list-style-type: lower-roman;">
    <li>The application for tenancy has been approved by the landlord</li>
    <li>The premises will not be let during the above period, pending the making of a residential tenancy agreement</li>
    <li>If the prospective tenant(s) decide not to enter into such an agreement, the landlord may retain the whole fee</li>
    <li>If a residential tenancy agreement is entered into, the holding fee is to be paid towards rent for the residential premises concerned.</li>
    <li>The whole of the fee will be refunded to the prospective tenant if:
        <ol style="list-style-type: lower-alpha;">
            <li>the entering into of the residential tenancy agreement is conditional on the landlord carrying out repairs or other work and the landlord does not carry out the repairs or other work during the specified period</li>
            <li>the landlord/landlord’s agent have failed to disclose a material fact(s) or made misrepresentation(s) before entering into the residential tenancy agreement.</li>
        </ol>
    </li>
</ol>

<p><strong>Ongoing Rent Payments</strong></p>

<p>Our preferred payment method for rent is Rental Rewards. (Please note that there is a small convenience fee charged for the use of the system). These fees are charged by a third-party payment processor – Rental Rewards</p>

<p>The fees for the convenience of the use of the services are: (must be able to set-up fee at agent level, as there is variation)</p>
<ol style="list-style-type: lower-alpha;">
    <li>Bank Account: $4 Testing.</li>
    <li>BPAY: $4.40 inc GST (internet banking using Rental Rewards Biller and Reference).</li>
    <li>BINt : $45</li>
</ol>

<p>Please read above clearly and do as it is asked.</p>
</div>