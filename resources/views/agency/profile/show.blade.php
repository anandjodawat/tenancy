@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<style type="text/css">
.tab-content>.active {
    margin-top: 40px;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
         {{--    <div class="page-title">
                <h1>Profile</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li>Agency </li>
                    <li class="active">Profile</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>
                Profile
                <div class="pull-right">
                    <a href="{{ route('agency.profile.edit', $profile->id) }}" class="btn btn-primary btn-sm">
                        Edit
                    </a>
                </div>
            </strong>
        </div>
        <div class="card-body">
            @if (auth()->user()->agency_profile())
            <div class="custom-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show" data-toggle="tab" href="#custom-profile-details" aria-selected="true">
                            Profile Details
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#custom-general-lease-terms" aria-selected="false">
                            General Lease Terms
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#custom-supporting-documents" aria-selected="false">  
                            Supporting Documents
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#custom-preferred-color" aria-selected="false">  
                            Agency Preferred Color
                        </a>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    <div class="tab-pane fade active show" id="custom-profile-details" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                        @include('agency.profile._profile_details')
                    </div>
                    <div class="tab-pane fade general-lease-terms" id="custom-general-lease-terms" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                        @if (auth()->user()->agency_profile()->leaseTerms)
                        @php
                        echo auth()->user()->agency_profile()->leaseTerms ? auth()->user()->agency_profile()->leaseTerms->lease_terms : '';
                        @endphp
                        @else
                        @include('agency.profile.default-lease-term')
                        @endif
                    </div>
                    <div class="tab-pane fade" id="custom-supporting-documents" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                        <ul class="list-group">
                            @foreach (auth()->user()->agency_profile()->supportingDocument as $document)
                            <li class="list-group-item">
                                <div class="col-md-4">
                                    <b>{{ ucwords(str_replace('_', ' ', App\Models\Agency\SupportingDocument::DOCUMENTNAME[$document->document_id])) }}</b>
                                </div>
                                <div class="col-md-8">{{ $document->supporting_document }}</div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="custom-preferred-color" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                        @if (auth()->user()->agency_profile()->preferred_color == null)
                            No Data
                        @else
                            {{-- {{ auth()->user()->agency_profile()->preferred_color }} --}}
                            <div style="background-color: {{ auth()->user()->agency_profile()->preferred_color }}; height: 10px;"></div>
                        @endif
                    </div>
                </div>
            </div>
            @else
            Please create a profile.
            @endif
        </div>
    </div>
</div>
@endsection