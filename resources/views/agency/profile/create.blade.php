@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                {{-- <h1>Profile</h1> --}}
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li>Agency </li>
                    <li>Profile</li>
                    <li class="active">Create</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Create</strong> Profile
        </div>
        {{ Form::open(['route' => 'agency.profile.store', 'class' => 'form-horizontal', 'method' => 'post']) }}
        <div class="card-body card-block">
            @include('agency.profile.form')    
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-sm pull-right">
                <i class="fa fa-dot-circle-o"></i> Submit
            </button>
            <button type="reset" class="btn btn-danger btn-sm">
                <i class="fa fa-ban"></i> Cancel
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>

@endsection
