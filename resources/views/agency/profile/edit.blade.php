@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<style type="text/css">
.tab-content>.active {
    margin-top: 40px;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Profile</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="{{ route('agency.dashboard') }} ">Dashboard</a></li>
                   
                    <li><a href="{{ route('agency.profile.show', auth()->user()->id) }} ">Profile</a></li>
                    <li class="active">Edit</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>
                Profile
            </strong>
        </div>
        <div class="card-body">
            @if (auth()->user()->agency_profile())
            <div class="custom-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show" data-toggle="tab" href="#custom-profile-details" aria-selected="true">
                            Profile Details
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#custom-general-lease-terms" aria-selected="false">
                            General Lease Terms
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#custom-supporting-documents" aria-selected="false">  
                            Supporting Documents
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#custom-preferred-color" aria-selected="false">  
                            Agency Preferred Color
                        </a>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    <div class="tab-pane fade active show" id="custom-profile-details" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                        @include('agency.profile.includes._profile_details')

                    </div>
                    <div class="tab-pane fade" id="custom-general-lease-terms" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                        @include('agency.profile.includes._general_lease_terms', ['profile' => $profile])
                    </div>
                    <div class="tab-pane fade" id="custom-supporting-documents" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                     @include('agency.profile.includes._supporting_documents')
                 </div>
                 <div class="tab-pane fade" id="custom-preferred-color" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                    @include('agency.profile.includes._preferred_color')
                </div>
            </div>
        </div>
    </div>
    @else
    Please create a profile.
    @endif
</div>
</div>
</div>
@endsection

@section ('after-scripts')
<script>
    CKEDITOR.replace( 'lease-term-edit' );
</script>
<link rel="stylesheet" type="text/css" href="/colorpicker/bootstrap-colorpicker.min.css">
<link rel="stylesheet" type="text/css" href="/colorpicker/bootstrap-colorpicker-plus.min.css">
<script src="/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="/colorpicker/bootstrap-colorpicker-plus.min.js"></script>
<script>
    jQuery(function(){
        change_color();
        var demo3 = jQuery('.colorpickerplus-embed .colorpickerplus-container');
        demo3.colorpickerembed();
        demo3.on('changeColor', function(e){
            var color = jQuery('.colorpicker-element').val();
            if(color.length==0) {
                jQuery('#demo3').val('transparent').css('background-color', '#fff');
            }
            else {
                jQuery('#demo3').val(color).css('background-color', color);
            }
        });
    });

    function change_color() 
    {
        color = jQuery('#demo3').val();
        if(color.length==0) {
            jQuery('#demo3').val('transparent').css('background-color', '#fff');
        }
        else {
            jQuery('#demo3').val(color).css('background-color', color);
        }
    }
</script>

@endsection