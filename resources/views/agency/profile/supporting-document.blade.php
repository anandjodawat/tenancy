<p>We require the following information to be supplied to process an application:</p>
<ul>
   <li>40 points* Primary/Photo ID. or National card</li>
   <li>20 points* proof of address bank statement, utility bill, motor vehicle registration.(Eg. Birth Certificate, Student Card, Medicare Card, Health Care Card, Vehicle Registration)</li>
   <li>15 points Rental History/tenent ledger or rental reference/older referene</li>
   <li>15 points* Employment/Proof of Income. (Eg. Payslips, Letter of Employment, Employment Reference)</li>
   <li>10 points for lease Reference</li>
</ul>