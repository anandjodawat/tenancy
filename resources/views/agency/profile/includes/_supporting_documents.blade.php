{{ Form::model(null, ['method' => 'patch', 'route' => ['agency.supporting-documents.update', auth()->user()->agency_id]]) }}
@foreach ($documents as $document)
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="name" class="form-control-label">
                <a href="{{ route('agency.supporting-documents.delete', ['id'=>$document->id]) }}" data-method="DELETE" title="Delete Supporting Document" class="btn btn-danger btn-sm">
                    <i class="fa fa-trash"></i>
                </a>
                {{ ucwords(str_replace('_', ' ', App\Models\Agency\SupportingDocument::DOCUMENTNAME[$document->document_id])) }}
            </label>
        </div>
        <div class="col-md-6">
            {{-- {{ Form::select('document_id[]', array_map('ucwords', str_replace('_', ' ', App\Models\Agency\SupportingDocument::DOCUMENTNAME)), $document->document_id, ['class' => 'form-control']) }} --}}
            {{ Form::hidden('document_id[]', $document->document_id) }}
            {{ Form::textarea('description[]', $document->supporting_document, ['class' => 'form-control', 'rows' => '2']) }}
        </div>
    </div>
<hr>
@endforeach
<center>
    <a href="{{ route('agency.supporting-documents.create') }}" class="btn btn-primary">Add More</a>
</center>
<button type="submit" class="btn btn-primary btn-sm pull-right">
    <i class="fa fa-dot-circle-o"></i> Update
</button>
<a href="{{ route('agency.profile.show', auth()->user()->id) }}" class="btn btn-danger btn-sm">
    <i class="fa fa-ban"></i> Cancel
</a>
{{ Form::close() }}