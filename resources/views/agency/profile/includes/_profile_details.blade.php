{{ Form::model($profile, ['route' => ['agency.profile.update', $profile->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) }}
<div class="row form-group">
    <div class="col col-md-3">
        <label for="name" class="form-control-label">
            Agency Name
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('name', null, ['class' => 'form-control']) }}

        @if ($errors->has('name'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="agency_id" class="form-control-label">
            Agency ID
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('agency_id', null, ['class' => 'form-control']) }}

        @if ($errors->has('agency_id'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('agency_id') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="email" class="form-control-label">
            Email Address
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::email('email', null, ['class' => 'form-control']) }}
        @if ($errors->has('email'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="address" class="form-control-label">
            Agency Address
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('address', null, ['class' => 'form-control']) }}
        @if ($errors->has('address'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('address') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="suburb" class="form-control-label">
            Suburb
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('suburb', null, ['class' => 'form-control']) }}

        @if ($errors->has('suburb'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('suburb') }}</strong>
        </span>
        @endif
    </div>
</div>


<div class="row form-group">
    <div class="col col-md-3">
        <label for="state" class="form-control-label">
            State
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::select('state', App\Models\Agency::STATES, null, ['class' => 'form-control']) }}
        @if ($errors->has('state'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('state') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="post_code" class="form-control-label">
            Post Code
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('post_code', null, ['class' => 'form-control']) }}
        @if ($errors->has('post_code'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('post_code') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="phone_number" class="form-control-label">
            Phone Number
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('phone_number', null, ['class' => 'form-control']) }}
        @if ($errors->has('phone_number'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('phone_number') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="registration_number" class="form-control-label">
            Registration Number
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('registration_number', null, ['class' => 'form-control']) }}
        @if ($errors->has('registration_number'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('registration_number') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="website" class="form-control-label">
            Website
        </label>
    </div>
    <div class="col-12 col-md-9">
        {{ Form::text('website', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="logo" class="form-control-label">
            Choose Agency Logo
        </label>
    </div>
    <div class="col-12 col-md-6">
        {{ Form::file('logo', ['class' => 'form-control']) }}
    </div>
    @if (auth()->user()->agency_profile()->getFirstMedia('agency_logos'))
    <div class="col-md-3">
        <img src="{{ auth()->user()->agency_profile()->getFirstMedia('agency_logos')->getUrl() }}" alt="Agency Logo" class="img img-responsive" style="max-height: 40px;">
    </div>
    @endif
</div>
<hr>
<button type="submit" class="btn btn-primary btn-sm pull-right">
    <i class="fa fa-dot-circle-o"></i> Update
</button>
<a href="{{ route('agency.profile.show', auth()->user()->id) }}" class="btn btn-danger btn-sm">
    <i class="fa fa-ban"></i> Cancel
</a>
{{ Form::close() }}

