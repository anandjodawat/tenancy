{{ Form::model($profile->leaseTerms ?? null, ['method' => 'post', 'route' => ['agency.general-lease-terms.store']]) }}
<div class="row form-group">

    <div class="col-12 col-md-12">
        <textarea class="form-control" id="lease-term-edit" name="lease_terms">
            @if ($profile->leaseTerms)
                {{ $profile->leaseTerms->lease_terms }}
            @else
                @include('agency.profile.default-lease-term')
            @endif
        </textarea>
    </div>
</div>

<hr>
<button type="submit" class="btn btn-primary btn-sm pull-right">
    <i class="fa fa-dot-circle-o"></i> Update
</button>
<a href="{{ route('agency.profile.show', auth()->user()->id) }}" class="btn btn-danger btn-sm">
    <i class="fa fa-ban"></i> Cancel
</a>

{{ Form::close() }}
