{{ Form::open(['route' => ['agency.profile.update', $profile->id], 'method' => 'patch']) }}
<div class="row form-group">
    <div class="col col-md-3">
        <label for="preferred_color" class="form-control-label">
            Enter Your Color Code
        </label>
    </div>
    <div class="col-12 col-md-6">
        {{-- {{ Form::text('preferred_color', auth()->user()->agency_profile()->preferred_color, ['class' => 'form-control']) }} --}}
        {{ Form::text('preferred_color', auth()->user()->agency_profile()->preferred_color, ['class' => 'form-control', 'id' => 'demo3', 'required']) }}
        <div class="colorpickerplus-embed">
          <div class="colorpickerplus-container"> </div>
        </div>

        @if ($errors->has('preferred_color'))
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('preferred_color') }}</strong>
        </span>
        @endif
    </div>
    {{-- <div class="col-12 col-md-3">
        <div id="select-a-color">
            <img src="/images/select.png">
        </div>
    </div> --}}
</div>


{{ Form::hidden('type', 'preferred_color') }}
<hr>
<button type="submit" class="btn btn-primary btn-sm pull-right">
    <i class="fa fa-dot-circle-o"></i> Update
</button>
<a href="{{ route('agency.profile.show', auth()->user()->id) }}" class="btn btn-danger btn-sm">
    <i class="fa fa-ban"></i> Cancel
</a>
{{ Form::close() }}

