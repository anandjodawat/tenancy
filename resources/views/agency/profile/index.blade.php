@extends('agency.layouts.app')

@section('page-header')
    <h1 class="page-title">
        {{ app_name() }}
        <small>{{ trans('strings.agency.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-md-6 col-md-offset-3">
                <div class="text-center">
                    <img src="http://www.gravatar.com/avatar/dfd6b10215e2e61b5b56789294d068f5.jpg?s=80&d=mm&r=g" alt="User profile picture" style="margin: auto;">

                    <h3 class="profile-username">{{ $profile->agency_name }}</h3>

                    <p class="text-muted">{{ $profile->agency_id }}</p>

                    <a href="{{ route('agency.profile.edit', $profile->id) }}" class="btn btn-primary">Edit Profile</a>
                </div>
                <hr>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Email Address</b> <span class="pull-right">{{ $profile->email }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Address Street Number</b><span class="pull-right">{{ $profile->address_street_number }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Address Street Unit</b><span class="pull-right">{{ $profile->address_street_unit }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Address Street Name</b><span class="pull-right">{{ $profile->address_street_name }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Address Street Type</b><span class="pull-right">{{ $profile->street_type }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Suburb</b><span class="pull-right">{{ $profile->suburb }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>State</b><span class="pull-right">{{ $profile->state }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Postal Code</b><span class="pull-right">{{ $profile->post_code }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Phone Number</b><span class="pull-right">{{ $profile->phone_number }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Mobile Number</b><span class="pull-right">{{ $profile->mobile_number }}</span>
                    </li>
                    {{-- <li class="list-group-item">
                        <b>Fax</b><span class="pull-right">{{ $profile->fax }}</span>
                    </li> --}}
                    <li class="list-group-item">
                        <b>Web Address</b><span class="pull-right">{{ $profile->web_address }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>General Lease Terms</b><span class="pull-right">{{ $profile->general_lease_terms }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection