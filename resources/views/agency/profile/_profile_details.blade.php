<div class="mx-auto d-block">
    @if ($profile->getFirstMedia('agency_logos'))
    <img src="{{ $profile->getFirstMedia('agency_logos')->getUrl() }}" alt="Profile Image" class="mx-auto d-block" width="30%" style="max-width: 180px;">
    @else 
    <img class="rounded-circle mx-auto d-block" src="/images/default_user.png" alt="Profile Image" style="max-width: 180px;">
    @endif
    <h5 class="text-sm-center mt-2 mb-1">
        {{ auth()->user()->name }}<br>
    </h5>
</div>
<hr>
<div class="col-md-6 offset-md-3">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <span> Agency Name <span class="pull-right">{{ $profile->name }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Agency ID <span class="pull-right">{{ $profile->agency_id }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Agency Email <span class="pull-right">{{ $profile->email }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Agency Address <span class="pull-right">{{ $profile->address }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Suburb <span class="pull-right">{{ $profile->suburb }}</span></span>
        </li>

        <li class="list-group-item">
            <span> State <span class="pull-right">{{ $profile->state_name }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Postal Code <span class="pull-right">{{ $profile->post_code }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Phone Number <span class="pull-right">{{ $profile->phone_number }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Registration Number <span class="pull-right">{{ $profile->registration_number }}</span></span>
        </li>

        <li class="list-group-item">
            <span> Web Address <span class="pull-right">{{ $profile->website }}</span></span>
        </li>
    </ul>
</div>