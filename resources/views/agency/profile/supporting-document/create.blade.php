@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<style type="text/css">
.tab-content>.active {
    margin-top: 40px;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            {{-- <div class="page-title">
                <h1>Supporting Documents</h1>
            </div> --}}
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a  href="{{ route('property-manager.dashboard')}}">Agency </a></li>
                    <li>Profile</li>
                    <li class="active">Supporting Document</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4>
                Add Supporting Document
            </h4>
        </div>
        <div class="card-body">
            {{ Form::model(null, ['method' => 'post', 'route' => 'agency.supporting-documents.store']) }}
            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="name" class="form-control-label">
                        Document Name
                    </label>
                </div>
                <div class="col-md-6">
                    <select name="document_id" class="form-control">
                        <option value="">--select a document type--</option>
                        @foreach (App\Models\Agency\SupportingDocument::DOCUMENTNAME as $document)
                        @php
                            $check = App\Models\Agency\SupportingDocument::where('agency_id', auth()->user()->agency_id)->where('document_id', $loop->index)->first();
                        @endphp
                            @if (!$check)
                            <option value="{{ $loop->index }}">{{ ucwords(str_replace('_', ' ', $document)) }}</option>
                            @endif
                        @endforeach
                    </select>
                    @if ($errors->has('document_id'))
                        <span class="help-block">
                            <strong class="text-danger">This field is required</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3">
                    <label class="form-control-label" for="description">
                        Description
                    </label>
                </div>
                <div class="col-md-6">
                    {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => '2']) }}
                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-primary btn-sm pull-right">
                <i class="fa fa-dot-circle-o"></i> Submit
            </button>
            <a href="{{ route('agency.profile.show', auth()->user()->id) }}" class="btn btn-danger btn-sm">
                <i class="fa fa-ban"></i> Cancel
            </a>
            {{ Form::close() }}
        </div>
    </div>
</div>
</div>
@endsection
