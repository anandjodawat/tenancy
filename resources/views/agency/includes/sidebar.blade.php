<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->full_name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.agency.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- search form (Optional) -->
        {{ Form::open(['route' => 'admin.search.index', 'method' => 'get', 'class' => 'sidebar-form']) }}
        <div class="input-group">
            {{ Form::text('q', Request::get('q'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('strings.agency.general.search_placeholder')]) }}

            <span class="input-group-btn">
                    <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span><!--input-group-btn-->
        </div><!--input-group-->
    {{ Form::close() }}
    <!-- /.search form -->
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            {{-- General open --}}
            <li class="header">{{ trans('menus.agency.sidebar.general') }}</li>
            <li class="{{ active_class(Active::checkUriPattern('agency/dashboard')) }}">
                <a href="{{ route('agency.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.agency.sidebar.dashboard') }}</span>
                </a>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('admin/reports*')) }}">
                <a href="#">
                    <i class="fa fa-file-text-o"></i>
                    <span>Reports</span>
                </a>
            </li>
            {{-- general close --}}

            {{-- User Management open --}}
            <li class="header">User Management</li>
            <li class="{{ active_class(Active::checkUriPattern('agency/*')) }} treeview">
                <li class="{{ active_class(Active::checkUriPattern('agency/user*')) }} treeview">

                    <a href="{{ route('agency.user.index') }}">
                        <i class="fa fa-user"></i>
                        <span>{{ trans('labels.agency.user.user') }}</span>
                    </a>
                </li>
            </li>
            <li class="{{ active_class(Active::checkUriPattern('agency/property-managers*')) }}">
                <a href="{{ route('agency.property-managers.index') }}">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('labels.agency.user.property-manager') }}</span>
                </a>
            </li>
            {{-- User management close --}}
        </ul>
    </section>
</aside>

