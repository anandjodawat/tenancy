@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    {{-- <h1>Contact us</h1> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Messages</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Messages
                            </strong>
                        </div>
                        <div class="card-body">
                            <table class="table" id="agency-contact-us">
                                <thead>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Subject</th>
                                   <th>Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script src="/property-manager/assets/js/vendor/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/userdashboard/datatables/datatables.bootstrap.min.css">
    <script src="/css/userdashboard/datatables/datatables.min.js"></script>
    <script src="/css/userdashboard/datatables/datatables.bootstrap.min.js"></script>
    <script src="/css/userdashboard/datatables/datatables.buttons.min.js"></script>

    {{-- {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }} --}}
    {{ Html::script("/js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(document).ready(function () {
            $('#agency-contact-us').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '/agency/get-contact-us',
                    method: 'GET',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'subject', name: 'subject'},
                    {data: 'action', name: 'action', searchable: false, sortable: false}
                ],
            });
        });
    </script>
@endsection