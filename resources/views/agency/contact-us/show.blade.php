@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    {{-- <h1>Contact us</h1> --}}
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Messages</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Messages Details
                            </strong>
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <b>First Name : </b>
                                    {{ $contact->first_name }}
                                </li>
                                <li class="list-group-item">
                                    <b>Last Name : </b>
                                    {{ $contact->last_name }}
                                </li>
                                <li class="list-group-item">
                                    <b>Email Address</b>
                                    {{ $contact->email }}
                                </li>
                                <li class="list-group-item">
                                    <b>Subject</b>
                                    {{ $contact->subject }}
                                </li>
                                <li class="list-group-item">
                                    <b>Message</b>
                                    {{ $contact->message }}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection