@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#"><b>Dashboard</b></a></li>
                    <li class="active"><b>Property Manager</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Property Managers
                        </strong>
                        <div class="pull-right">
                            <a href="{{ route('agency.property-managers.create') }}" class="btn btn-primary">Add Property Manager</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" id="property-managers-table">
                           <thead>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th class="no-sort">Action</th>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <span class="badge badge-{{ $user->confirmed == '1' ? 'success' : 'warning' }}">
                                        {{ $user->confirmed == '1' ? 'ACTIVE' : 'INACTIVE' }}
                                    </span>
                                </td>
                                <td>{{ $user->created_at->format('Y/m/d H:i') }}</td>
                                <td>
                                    <a href="{{ route('agency.property-managers.edit', $user->id) }}" class="btn btn-info btn-sm" title="Edit Property Manager">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ route('agency.property-managers.delete', ['id'=>$user->id]) }}" class="btn btn-danger btn-sm" title="Delete Property Manager" data-method="DELETE">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
@endsection

@section ('after-scripts')
<link rel="stylesheet" type="text/css" href="/datatables/datatables.bootstrap.min.css">
<script src="/datatables/datatables.min.js"></script>
<script src="/datatables/datatables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready( function () {
        jQuery('[data-method="delete"]').on('click', function (e) {
            event.preventDefault();
            var url = jQuery(this).attr('href');
            var confirmation = confirm("are you sure you want to delete this message?");
            if(confirmation) {
                jQuery.ajax({
                    url: url,
                    method: "DELETE",
                    success: function (response) {
                        location.reload();
                    },
                    error: function () {
                        console.log('error')
                    }
                });
            }               
        });

         jQuery('#property-managers-table').DataTable({
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
            } ],
            "order": [[3, "desc"]]
        });
    });
</script>
@endsection
