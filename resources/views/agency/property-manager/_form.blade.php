
    <div class="form-group">
        {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.first_name')]) }}
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
        </span>
    </div>
    <div class="form-group">
        {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.last_name')]) }}
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
        </span>
    </div>
    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('email') }}</strong>
        </span>
    </div>
    <div class="form-group">
        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('password') }}</strong>
        </span>
    </div>
    <div class="form-group">
        {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
        <span class="help-block">
            <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
        </span>
    </div>
</div>
<div class="clearfix"></div>
<hr>