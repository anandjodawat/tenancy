@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#"><b>Dashboard</b></a></li>
                    <li><a href="#"><b>Property Manager</b></a></li>
                    <li class="active"><b>Create</b></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">
                            Add Property Managers
                        </strong>
                    </div>
                    {{ Form::open(['route' => 'agency.property-managers.store', 'method' => 'post']) }}
                    <div class="card-body">
                        <div class="col-md-6">
                            @include('agency.property-manager._form')
                            <a href="{{ route('agency.property-managers.index') }}" class="btn btn-danger">Cancel</a>
                            {{ Form::submit('Submit', ['class' => 'btn btn-success pull-right']) }}
                        </div>
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
    @endsection
