@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>User</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Agency Users</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    @include('agency.user.user-header-buttons')
    
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Create Users
                            </strong>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <th>{{ trans('labels.backend.access.users.table.last_name') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.first_name') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.email') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.confirmed') }}</th>
                                    {{-- <th>{{ trans('labels.backend.access.users.table.roles') }}</th> --}}
                                    {{-- <th>{{ trans('labels.backend.access.users.table.social') }}</th> --}}
                                    <th>{{ trans('labels.backend.access.users.table.created') }}</th>
                                    <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </thead>
                                <tbody>
                                   @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->confirmed ? "YES" : "NO" }}</td>
                                            <td>{{ $user->created_at->diffForHumans() }}</td>
                                            <td>{{ $user->updated_at->diffForHumans() }}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-xs" title="Edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="#" title="View" class="btn btn-info btn-xs">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="#" title="Delete" class="btn btn-danger btn-xs">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection