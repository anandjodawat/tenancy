
        <div class="row form-group">
            {{ Form::label('first_name', trans('validation.attributes.backend.access.users.first_name'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-6">
                {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.first_name')]) }}
            </div><!--col-lg-6-->
        </div><!--form control-->

        <div class="row form-group">
            {{ Form::label('last_name', trans('validation.attributes.backend.access.users.last_name'),
             ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-6">
                {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.last_name')]) }}
            </div><!--col-lg-6-->
        </div><!--form control-->

        <div class="row form-group">
            {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-6">
                {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
            </div><!--col-lg-6-->
        </div><!--form control-->

        <div class="row form-group">
            {{ Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-6">
                {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}
            </div><!--col-lg-6-->
        </div><!--form control-->

        <div class="row form-group">
            {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-6">
                {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) }}
            </div><!--col-lg-6-->
        </div><!--form control-->

        <div class="row form-group">
            {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-1">
                {{ Form::checkbox('status', '1', true) }}
            </div><!--col-lg-1-->
        </div><!--form control-->

        <div class="row form-group" style="display: none">
            {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-1">
                {{ Form::checkbox('confirmed', '1', true) }}
            </div><!--col-lg-1-->
        </div><!--form control-->

        @if (! config('access.users.requires_approval'))
            <div class="row form-group" style="display: none">
                <label class="col-lg-4 form-control-label">{{ trans('validation.attributes.backend.access.users.send_confirmation_email') }}<br/>
                    <small>{{ trans('strings.backend.access.users.if_confirmed_off') }}</small>
                </label>

                <div class="col-lg-1">
                    {{ Form::checkbox('confirmation_email', '1', true) }}
                </div><!--col-lg-1-->
            </div><!--form control-->
        @endif

        <div class="row form-group">
            {{ Form::label('associated_roles', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-4 form-control-label']) }}

            <div class="col-lg-3">
                <select name="assignees_roles[]" id="assignees_roles" class="form-control" >
                    @if (count($roles) > 0)
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ (collect(old('assignees_roles'))->contains($role->id)) ? 'selected':'' }}>{{ ucwords(str_replace('-', ' ', $role->name)) }}</option>
                        @endforeach
                    @else
                        {{ trans('labels.backend.access.users.no_roles') }}
                    @endif
                </select>
            </div>
        </div>

        <div class="pull-left">
            {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
        </div>
        <div class="pull-right">
            {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-md']) }}
        </div>


