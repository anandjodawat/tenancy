@extends('property-manager.layouts.app')

@section('title')
{{ app_name() }}
@endsection

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>User</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Agency Users</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
    @include('agency.user.user-header-buttons')
    
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">
                                Agency Users
                            </strong>
                        </div>
                        <div class="card-body">
                            {{ Form::open(['route' => 'agency.user.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

                                <input type="hidden" name="created_by_id" value="{{ auth()->user()->id }}">
                                <input type="hidden" name="consultancy_id" value="{{ auth()->user()->consultancy_id }}">
                                @include('agency.user.form')

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection