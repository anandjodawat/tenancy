<div class="card-body">
    <a href="{{ route('agency.user.index') }}" class="btn btn-primary">
        <i class="fa fa-users"></i>
        View All Users
    </a>

    <a href="{{ route('agency.user.create') }}" class="btn btn-primary">
        <i class="fa fa-users"></i>
        Add User
    </a>
</div>

