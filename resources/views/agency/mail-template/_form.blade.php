<div class="row form-group {{ $errors->has('type') ? 'has-error' : '' }}">
    {{ Form::label('type', 'Type', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        {{-- {{ Form::select('type', $type, null, ['class' => 'form-control', 'placeholder' => '--select mail type--']) }} --}}
        <select name="type" class="form-control">
            <option>--select mail type--</option>
            @foreach ($type as $name)
                @if (auth()->user()->agency->mail_templates->where('type', $loop->index)->count() > 0)
                @else
                    <option value="{{ $loop->index }}">{{ $name }}</option>
                @endif
            @endforeach
        </select>
        @if ($errors->has('type'))
            <span class="text-danger">{{ $errors->first('type') }}</span>
        @endif
    </div>
</div>

<div class="row form-group {{ $errors->has('body') ? 'has-error' : '' }}">
    {{ Form::label('forBody', 'Body', ['class' => 'col-md-4 form-control-label']) }}
    <div class="col-md-6">
        <textarea name="body" class="form-control" id="mail-template-ckeditor">
        </textarea>
        @if ($errors->has('body'))
            <span class="text-danger">{{ $errors->first('body') }}</span>
        @endif
    </div>
</div>
