@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                {{-- <h1>
                    Mail Template | 
                     <a href="{{ route('agency.mail-templates.create') }}" class="btn btn-primary btn-sm">Create New</a> 
                </h1> --}}
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href"{{ route('agency.dashboard') }}">Agency</li>
                    <li class="active">Mail Template</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12 ten-mail-templates">
    <div class="card">
        <div class="card-header">
            <strong class="card-title" style="font-size: 1.5em;">
                    Mail Templates
            </strong>
       </div>            
    </div>
        <div class="card-body">
            <div class="default-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        @foreach ($templates as $template)
                        <a class="nav-item nav-link {{ $loop->index == 0 ? 'active show' : '' }}" id="nav-mail-template-{{ $loop->index }}" data-toggle="tab" href="#tab-mail-template-{{ $loop->index }}" role="tab" aria-controls="nav-home" aria-selected="true">
                            {{ ucwords(str_replace('-', ' ', $template->mail_type)) }}
                        </a>
                        @endforeach
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                    @foreach ($templates as $template)
                    <div class="tab-pane fade {{ $loop->index == 0 ? 'active show' : '' }}" id="tab-mail-template-{{ $loop->index }}" role="tabpanel" aria-labelledby="nav-home-tab">
                        <p>@php
                        echo $template->body;
                    @endphp</p>
                    <hr>
                    <a href="{{ route('agency.mail-templates.edit', $template->id) }}" class="btn btn-primary btn-sm pull-right">
                        <i class="fa fa-edit"></i>
                        Edit
                    </a>
                </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection