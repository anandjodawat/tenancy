@extends('property-manager.layouts.app')

@section('page-header')
<h1 class="page-title">
    {{ app_name() }}
    <small>{{ trans('strings.property-manager.dashboard.title') }}</small>
</h1>
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                {{-- <h1>Mail Template</h1> --}}
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href"{{ route('agency.dashboard') }}">Agency </a></li>
                    <li class="active">Mail Template</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('flash::message')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Mail</strong> Template
        </div>
        {{ Form::open(['route' => 'agency.mail-templates.store', 'method' => 'post']) }}
        <div class="card-body card-block">
            @include('agency.mail-template._form')
        </div>
        <div class="card-footer">
                {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
        </div>
        {{ Form::close() }}
    </div>
</div>

@endsection

@section('after-scripts')

<script>
    CKEDITOR.replace( 'mail-template-ckeditor' );

    jQuery(document).ready( function () {
        jQuery('select[name=type]').on('change', function () {
            var current = jQuery(this).val();
            console.log(current);
            var html ="";

            // for request deposit default template
            if (current == '0') {

                html = '<p>Dear &nbsp;</p>\
                        <p>Your Tenancy Application has been Accepted for address: &nbsp;.<br />\
                        &nbsp;</p>\
                        \
                        <p>Please Click pay now button to pay your holding deposit.</p>\
                        \
                        <p>\
                        {{ auth()->user()->name }}<br />\
                        <br />\
                        {{ auth()->user()->agency_profile() ? auth()->user()->agency_profile()->name : '' }}<br />\
                        {{ auth()->user()->email }}<br />\
                        @if (auth()->user()->agency_profile())\
                            <a href="{{ auth()->user()->agency_profile()->website }}">{{ auth()->user()->agency_profile()->website }}</a><br />\
                        @endif\
                        <br />\
                        \
                        @if (auth()->user()->agency_logo())\
                            <img src="{{ auth()->user()->agency_logo() }}" style="width:100px" /></p>\
                        @endif\
                        ';
            }

            if (current == '1') {
                html = '<p>Dear &nbsp;</p>\
                            \
                        <p>\
                            Your Tenancy Application has been Approved for address: &nbsp;.<br />\
                            &nbsp;\
                        </p>\
                        <p>Please Click button below to setup ongoing payment.</p>\
                        \
                                \
                        <p><br />\
                            {{ auth()->user()->name }}<br />\
                            <br />\
                            {{ auth()->user()->email }}<br />\
                            @if (auth()->user()->agency_profile())\
                            <a href="{{ auth()->user()->agency_profile()->website }}">{{ auth()->user()->agency_profile()->website }}</a>\
                            @endif\
                        </p>\
                            \
                        @if (auth()->user()->agency_logo())\
                        <table style="width:100px">\
                            <tbody>\
                                <tr>\
                                    <td><img src="{{ auth()->user()->agency_logo() }}" width="100" /></td>\
                                </tr>\
                            </tbody>\
                        </table>\
                        @endif\
                        ';
            }

            if (current == '3' || current == '2' || current == '4') {
                html = '<p>No Templates available</p>';
            }

            CKEDITOR.instances['mail-template-ckeditor'].setData(html);
        });
    });
</script>

@endsection