jQuery(document).ready( function () {
      jQuery('[data-method="DELETE"]').on('click', function(e) {
        e.preventDefault();
        var confirmation = confirm("Are you sure you want to delete?");
        if(confirmation) {
            var url = jQuery(this).attr('href');
            jQuery.ajax({
                url: url,
                type: "get",
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log('error')
                }
            });
        }
        else{
            return;
        }
    });
});