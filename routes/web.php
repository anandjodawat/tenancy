<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', 'LanguageController@swap');

/* ----------------------------------------------------------------------- */

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/Frontend/');
});

/* ----------------------------------------------------------------------- */

/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__.'/Backend/');

});

/*
 * Consultancy Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Agency', 'prefix' => 'agency', 'as' => 'agency.', 'middleware' => 'agency'], function () {
    /*
     * These routes need view-consultancy permission
     * (good if you want to allow more than one group in the consultancy,
     * then limit the consultancy features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    includeRouteFiles(__DIR__.'/Agency/');
});


/**
 * Property manager routes
 * Namespaces indicate folder structure
 */

Route::group(['namespace' => 'PropertyManager', 'prefix' => 'property-manager', 'as' => 'property-manager.', 'middleware' => 'property-manager'], function () {
    /**
     * These routes need view-property-dashboard permission
     */
    includeRouteFiles(__DIR__.'/PropertyManager/');
});

Route::group(['namespace' => 'UserDashboard', 'as' => 'user-dashboard.', 'middleware' => 'user-dashboard'], function () {
    /**
     * These routes need view-property-dashboard permission
     */
    includeRouteFiles(__DIR__.'/UserDashboard/');
});

Route::get('terms-and-conditions', function () {
    return view('frontend.termsandconditions');
});

Route::get('privacy-policy', function () {
    return view('frontend.privacypolicy');
});

Route::get('about-us', function () {
    return view('frontend.aboutus');
});

Route::get('tenant', function () {
    return view('frontend.tenant');
});

Route::get('propertymanager', function () {
    return view('frontend.propertymanager');
});

Route::get('contact-us', function () {
    return view('frontend.contactus');
});

Route::get('pdf', function () {
    return view('pdf');
});

Route::get('getting-started', function () {
    return view('frontend.getting-started');
});

Route::get('property-manager-faq', function () {
    return view('frontend.pm-faq');
});

Route::get('tenant-faq', function () {
    return view('frontend.tenant-faq');
});

Route::resource('employment-confirmation', 'EmploymentConfirmationController');

Route::resource('history-confirmation', 'HistoryConfirmationController');

Route::resource('docusign', 'API\DocusignController');

Route::get('/media/{file}','MediaServerController@serveImages')
            ->where(['file'=>'.*']);

Route::get('thank-you', function () {
    return "Thank You!";
});