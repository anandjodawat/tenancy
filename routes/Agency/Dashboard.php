<?php

/**
 * All route names are prefixed with 'consultancy.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');


Route::post('user/get', 'UserTableController')->name('user.get');

Route::resource('user', 'UserController');

Route::resource('profile', 'ProfileController');

Route::resource('property-managers', 'PropertyManagerController');

// Agency contact us page
Route::resource('contact-us', 'ContactUsController');
Route::get('get-contact-us', 'ContactUsController@get_contact_us')->name('get-contact-us');

// Mail tempalte
Route::resource('mail-templates', 'MailTemplateController');

// Lease terms and supporting documents
Route::resource('general-lease-terms', 'GeneralLeaseTermController');
Route::resource('supporting-documents', 'SupportingDocumentController');

Route::get('delete-property-managers', 'PropertyManagerController@delete')->name('property-managers.delete');

Route::get('delete-supporting-documents', 'SupportingDocumentController@delete')->name('supporting-documents.delete');