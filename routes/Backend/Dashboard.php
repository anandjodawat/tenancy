<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::group(['prefix' => 'user-management', 'namespace' => 'UserManagement', 'as' => 'user-management.'], function () {
	// Tenant Users
	Route::resource('tenant-users', 'TenantUserController');

	// Agencies
	Route::resource('agencies', 'AgencyController');

	// Property Managers
	Route::resource('property-managers', 'PropertyManagerController');

	//Confirm agency
	Route::get('confirmagency', 'PropertyManagerController@agencyconfirm')->name('property-managers.agencyconfirm'); 
});

	

Route::group(['namespace' => 'Tenant'], function () {
	Route::resource('applications', 'ApplicationController');
});

Route::group(['prefix' => 'reports', 'namespace' => 'Report', 'as' => 'reports.'], function () {
	Route::resource('bondsure', 'BondsureController');
	Route::resource('connection-services', 'ConnectionServiceController');
	Route::resource('sent-invitations', 'InvitationSentController');
	Route::resource('request-deposits', 'RequestDepositController');
	Route::resource('ongoing-payments', 'RequestOngoingPaymentController');
	Route::resource('total-applications', 'TotalApplicationController');
	Route::resource('uploaded-tenants', 'UploadedTenantController');
});
