<?php

/**
 * All route names are prefixed with 'admin.consultancy'.
 */

Route::group(['middleware' => 'access.routeNeedsRole:1'], function () {
	//For dataTable
	Route::get('agencies/get', 'AgencyController@getConsultancyData')->name('agencies.get');

	//Activate Consultancy
	Route::get('agencies/activate', 'AgencyController@activateConsultancy')->name('agencies.activate'); 

});