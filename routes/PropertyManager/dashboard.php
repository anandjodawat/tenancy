<?php

Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::resource('tenants', 'TenantController');
Route::resource('property-invitation-tenants', 'InvitationTenantController');
Route::resource('upload-tenants', 'UploadTenantController');
Route::post('import-tenant', 'UploadTenantController@import')->name('import-tenant');
Route::post('send-mail', 'UploadTenantController@send_mail')->name('send-mail');
Route::get('download-excel', 'UploadTenantController@download_excel')->name('download-excel');

Route::resource('office-tenants', 'OfficeTenantController');

// Applications
Route::resource('applications', 'ApplicationController');
Route::get('get-applications', 'ApplicationController@get_applications')->name('get-applications');
Route::get('change-application-status', 'ApplicationController@change_status')->name('change-application-status');
Route::get('applications-delete', 'ApplicationController@delete')->name('applications.delete');

// Total Applications
Route::resource('total-applications', 'TotalApplicationController');

Route::resource('tenant-applications', 'TenancyApplicationController');

Route::resource('tenancy-pdf-application', 'ApplicationPdfController');

Route::resource('request-deposits', 'RequestDepositController');

Route::get('request-deposits-autocomplete', 'RequestDepositController@autocomplete')->name('request-deposits.autocomplete');

Route::resource('deposit-sales', 'DepositSalesController');

Route::resource('ongoing-payments', 'OngoingPaymentController');

Route::resource('moving-service-applications', 'MovingServiceController');

Route::resource('maintenance-requests', 'MaintenanceRequestController');

Route::resource('profile', 'ProfileController');

// Mail outs
Route::resource('mail-outs', 'MailOutController');

// Settings
Route::resource('settings', 'SettingsController');
Route::get('getservices', 'SettingsController@getservices')->name('settings.getservices');

// Invitation for property
Route::resource('property-invitation', 'PropertyInvitationController');

// Email application
Route::resource('email-application', 'EmailApplicationController');
Route::post('forward-application', 'EmailApplicationController@forward_application')->name('forward-application');
Route::post('reference-application', 'EmailApplicationController@history_confirmation')->name('reference-application');
Route::post('employment-application', 'EmailApplicationController@employment_confirmation')->name('employment-application');

// Properties
Route::resource('properties', 'PropertyController');
Route::post('parse-properties', 'PropertyController@parse')->name('properties.parse-properties');
Route::get('delete-properties', 'PropertyController@delete_property')->name('properties.delete-properties');

// Connection Services
Route::resource('connection-services', 'ConnectionServiceController');

//Property Invitaitons
Route::get('delete-property-invitation', 'PropertyInvitationController@delete')->name('property-invitation.delete');

//Tanent delete
Route::get('delete-tenant', 'TenantController@delete')->name('tenants.delete');

// prospective tenant
Route::resource('prospective-tenants', 'PerspectiveTenantsController');

//upload tenants
Route::get('delete-uploaded-tenants', 'UploadTenantController@delete_uploaded_tenant')->name('upload-tenants.delete_uploaded_tenant');

Route::post('delete-uploaded-tenants', 'UploadTenantController@delete_multiple_tenant');
// Bondsure request sent
Route::resource('bond-requests', 'BondsureRequestController');

// Personalized apply now link
Route::get('personalized-apply-now-link', function () {
	return view('property-manager.apply-now-link');
})->name('personalized-apply-now-link');

