<?php

Breadcrumbs::register('admin.consultancy.index', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.consultancy.index'), route('admin.consultancy.index', $id));
});

Breadcrumbs::register('admin.consultancy.create', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.consultancy.index');
    $breadcrumbs->push(trans('menus.backend.consultancy.create'), route('admin.consultancy.create', $id));
});

Breadcrumbs::register('admin.consultancy.create', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.consultancy.index');
    $breadcrumbs->push(trans('menus.backend.consultancy.create'), route('admin.consultancy.create', $id));
});