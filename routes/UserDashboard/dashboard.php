<?php

Route::resource('profile', 'ProfileController');

Route::resource('applications', 'ApplicationController');
Route::get('sign-document/{id}', 'SignDocumentController@index');
Route::resource('maintenance-requests', 'MaintenanceRequestController');
Route::post('save-application-media', 'ApplicationController@save_media')->name('save-application-media');
Route::post('remove-application-media', 'ApplicationController@remove_media')->name('remove-application-media');
Route::post('save-as-draft', 'ApplicationController@save_as_draft')->name('save-as-draft');
Route::patch('save-as-draft', 'ApplicationController@save_as_draft')->name('save-as-draft');

Route::get('applications-delete', 'ApplicationController@delete')->name('applications.delete');

Route::get('account', function () {
	return view('frontend.user.account');
});

// get ajax list
Route::get('agency-ajax-list', 'ApplicationController@get_agency_list');
Route::get('agency-property-manager-list', 'ApplicationController@get_property_manager_list');
Route::get('agency-property-manager-email', 'ApplicationController@get_property_manager_email');
Route::get('agency-application-details', 'ApplicationController@agency_application_details');
Route::get('get_valid_address', 'ApplicationController@get_valid_address');