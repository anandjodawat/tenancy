<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\EmploymentConfirmedMail;
use App\Models\EmpolymentConfirmation;

class EmploymentConfirmationController extends Controller
{
    public function index()
    {
        return view('employment-confirmation');
    }

    public function store( Request $request )
    {
        $this->validate($request, [
            'date' => 'required',
            'company_name' => 'required',
            'employee_name' => 'required',
            'employee_position' => 'required',
            'work_duration_from' => 'required',
            'work_duration_to' => 'required',
            'completed_by' => 'required',
            'employer_position' => 'required',
            'submission_date' => 'required'
        ]);

        $empConfirmation = EmpolymentConfirmation::create([
            'date' => $request->date,
            'company_name' => $request->company_name,
            'employee_name' => $request->employee_name,
            'employee_position' => $request->employee_position,
            'work_duration_from' => $request->work_duration_from,
            'work_duration_to' => $request->work_duration_to,
            'completed_by' => $request->completed_by,
            'employer_position' => $request->employer_position,
            'submission_date' => $request->submission_date,
            'employment_id' => $request->employment_id
        ]);

        Mail::to($request->manager_email)->send(new EmploymentConfirmedMail($empConfirmation));

        echo "Thank you for your time<br>";

        echo "<a href='/'>click here</a> to redirect...";
    }
}
