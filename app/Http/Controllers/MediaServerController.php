<?php
namespace App\Http\Controllers;

/**
 * Class MediaServerController.
 */
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request;
use Spatie\MediaLibrary\Models\Media;

 class MediaServerController extends Controller{

    public function serveImages ($file)
    {
    	$allowed = false;
    	$media_id = explode('/',$file)[0];

    	$media = Media::find($media_id);
    	$model_type = $media->model_type;
    	$model_id = $media->model_id;
    	//dd($media);

    	if (Auth::check()) {
			if(auth()->user()->hasRole('administrator')){
			    $allowed = true;
			}/*else if(){
				// check if logged in user is tenant and the media belongs to that tenant.
					if($media->getURL() == '/'.Request::path()){
								$allowed = true;
						}
			}else if(){
				// check if logged in user is property manager or agency manager and they have received the application from that tenant.
				if($media->getURL() == '/'.Request::path()){
								$allowed = true;
						}
			}*/
		}

		$allowed = true;//remove this after adding protection logic
		

		if($allowed){
				$storagePath = storage_path('app/media/'.$file);
		     if( ! \File::exists($storagePath)){
		        return view('errors.404');     	
		     }
		     if(strtolower(pathinfo($file, PATHINFO_EXTENSION))=='pdf'){
		     	return response()->file($storagePath);
		     }else{
        		return \Image::make($storagePath)->response();		     	
		     }
        }
		return view('errors.404');     
    }
}