<?php

namespace App\Http\Controllers\Agency;

use Auth;
use App\Models\Agency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agency\AgencyLeaseTerm;
use App\Models\Agency\SupportingDocument;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->agency) {
            return redirect(route('agency.profile.create'));
        }

        $profile = Auth::user()->agency;

        $documents = $profile->supportingDocument;

        return view('agency.profile.index', compact('profile', 'documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->agency) {
            return redirect(route('agency.profile.index'));
        }
        return view('agency.profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            //'agency_id' => 'unique:agencies',
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'email' => 'required',
            //'registration_number' => 'required|unique:agencies',
            'suburb' => 'required',
            'state' => 'required',
            'post_code' => 'required'
        ]);

        $agency = Agency::create([
            'name' => $request->name,
            'agency_id' => $request->agency_id,
            'user_id' => auth()->user()->id,
            'address' => $request->address,
            'phone_number' => $request->phone_number,
            'email' => auth()->user()->email,
            'registration_number' => $request->registration_number,
            'website' => $request->website,
            'state' => $request->state,
            'post_code' => $request->post_code,
            'suburb' => $request->suburb
        ]);

        auth()->user()->update([
            'agency_id' => $agency->id
        ]);

        return redirect(route('agency.profile.show', $agency->id))->withFlashSuccess('Profile Successfully Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->agency) {
            return redirect(route('agency.profile.create'));
        }

        $profile = Auth::user()->agency;

        return view('agency.profile.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Agency::find(auth()->user()->agency_id);

        $documents = $profile->supportingDocument;

        return view('agency.profile.edit', compact('profile', 'documents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agency = Agency::find($id);
        if (request('type') && request('type') == 'preferred_color') {
            $agency->update([
                'preferred_color' => $request->preferred_color == 'transparent' ? '#fff' : $request->preferred_color
            ]);
            flash('Your Agency Preferred Color Has Been Updated.')->success();
            return redirect()->back();
        }
        $this->validate($request, [
            'name' => 'required',
            //'agency_id' => 'unique:agencies,agency_id,'.$id,
            'address' => 'required',
            'phone_number' => 'required|numeric',
            'email' => 'required',
            //'registration_number' => 'required|unique:agencies,registration_number,'.$id,
            'suburb' => 'required',
            'state' => 'required',
            'post_code' => 'required'
        ]);


        $agency->update([
            'name' => $request->name,
            'agency_id' => $request->agency_id,
            'address' => $request->address,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'registration_number' => $request->registration_number,
            'website' => $request->website,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code
        ]);

        // for agency logo
        if ($request->file('logo')) {
            if ($agency->getFirstMedia('agency_logos')) {
                $media_id = $agency->getFirstMedia('agency_logos')->id;
                $agency->deleteMedia($media_id);
            } 
            $agency->addMedia($request->file('logo'))->toMediaCollection('agency_logos', 'media');
        }

        flash('Agency details successfully updated')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
