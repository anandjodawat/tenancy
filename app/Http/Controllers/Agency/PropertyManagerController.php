<?php

namespace App\Http\Controllers\Agency;

use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\UserRegistered;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

use Illuminate\Support\Facades\Mail;
use App\Mail\UserNeedsConfirmationMail;
use App\Mail\PropertyManagerCreatedMail;

class PropertyManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('agency_id', auth()->user()->agency_id)->whereHas('roles', function($q){
                    $q->where('name', 'property-manager');
                })->get();

        return view('agency.property-manager.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agency.property-manager.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required', 
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',

        ]);
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'status' => '1',
            'confirmed' => '1',
            'created_by_id' => auth()->user()->id,
            'agency_id' => auth()->user()->agency->id,
        ]);


        flash('Property Manager Created Successfully.')->success();

        event(new UserRegistered($user));
        
        Mail::to($user->email)->send(new PropertyManagerCreatedMail);
        
        // $user->notify(new UserNeedsConfirmation($user->confirmation_code));
        // Mail::to($user->email)->send(new UserNeedsConfirmationMail($user->confirmation_code));
        
        $user->attachRole(3);

        return redirect()->route('agency.property-managers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('agency.property-manager.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required', 
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
        ]);

        $user = User::find($id);
        
        if ($request->password) {
            $this->validate($request, [
                'password' => 'string|min:6|confirmed',
            ]);

            $user->update([
                'password' => bcrypt($request->password)
            ]);
        }

        $user = User::find($id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
        ]);


        flash('Property Manager Successfully Updated.')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = user::find($id)->delete();

        flash('Property manager deleted successfully');

        return redirect()->back()->withFlashSuccess('Property manager deleted successfully');
    }
        public function delete()
    {
         user::find(request('id'))->delete();


        flash('Property manager deleted successfully');

        return response()->json([], 200);
    }
}
