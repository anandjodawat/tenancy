<?php

namespace App\Http\Controllers\Agency;

use Illuminate\Http\Request;
use App\Models\Agency\MailTemplate;
use App\Http\Controllers\Controller;

class MailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        foreach (MailTemplate::MAILTYPE as $key => $type) {
            if (MailTemplate::whereAgencyId(auth()->user()->agency_id)->whereType($key)->count() == '0') {
                MailTemplate::create([
                    'type' => $key, 
                    'body' => MailTemplate::templates($key),
                    'created_by' => auth()->user()->id,
                    'agency_id' => auth()->user()->agency_id
                ]);
            }
        }
        $templates = MailTemplate::whereAgencyId(auth()->user()->agency_id)
            ->latest()
            ->get();

        return view('agency.mail-template.index', compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = MailTemplate::MAILTYPE;
        return view('agency.mail-template.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);

        if (MailTemplate::whereAgencyId(auth()->user()->agency_id)->whereType($request->type)->count() > 0) {
            $template = MailTemplate::whereAgencyId(auth()->user()->agency_id)->whereType($request->type)->first();
            $template->update([
                // 'type' => $request->type, 
                'body' => $request->body,
            ]);
            flash('Mail Template Updated Successfully.')->success();

            return redirect()->back();
        }

        MailTemplate::create([
            'type' => $request->type, 
            'body' => $request->body,
            'created_by' => auth()->user()->id,
            'agency_id' => auth()->user()->agency_id
        ]);

        flash('Mail Template Created Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = MailTemplate::MAILTYPE;
        $template = MailTemplate::find($id);
        return view('agency.mail-template.edit', compact('type', 'template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mailTemplate = MailTemplate::find($id);

        $data = [
            // 'type' => $request->type, 
            'body' => $request->body,
            'agency_id' => auth()->user()->agency_id
        ];

        if ($mailTemplate) {
            flash('Template Updated Successfully.')->success();
            $mailTemplate->update($data);
        } else {
            flash('Template Created Successfully.')->success();
            MailTemplate::create($data);
        }

        return redirect()->route('agency.mail-templates.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
