<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	$inbox = TenantApplication::applicationForAgency()
            ->count();

    	$accepted = TenantApplication::applicationForAgency()
            ->whereStatus(2)
	    	->count();

	    $approved = TenantApplication::applicationForAgency()
            ->whereStatus(3)
	    	->count();

        return view('agency.dashboard', compact(
        	'inbox',
        	'accepted',
        	'approved'
        ));
    }
}
