<?php

namespace App\Http\Controllers\Agency;

use App\Models\Agency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agency\AgencyLeaseTerm;

class GeneralLeaseTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'lease_terms' => 'nullable'
        ]);

        $agency = Agency::find(auth()->user()->agency_id);

        if ($agency->leaseTerms) {
            $agency->leaseTerms->update([
                'lease_terms' => $request->lease_terms
            ]);
        } else {
            AgencyLeaseTerm::create([
                'agency_id' => $agency->id,
                'lease_terms' => $request->lease_terms
            ]);
        }

        flash('Agency Lease Terms Updated Successfully')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $this->validate($request, [
            'lease_terms' => 'required'
        ]);

        $agency = Agency::find(auth()->user()->agency_id);

        if ($agency->leaseTerms) {
            $agency->leaseTerms->update([
                'lease_terms' => $request->lease_terms
            ]);
        } else {
            AgencyLeaseTerm::create([
                'agency_id' => $agency->id,
                'lease_terms' => $request->lease_terms
            ]);
        }

        flash('Agency Lease Terms Updated Successfully')->success();

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
