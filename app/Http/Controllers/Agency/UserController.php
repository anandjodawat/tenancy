<?php

namespace App\Http\Controllers\Agency;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Consultancy\User\ConsultancyUserRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;

/**
 * Class UserController.
 */
class UserController extends Controller
{
    /**
     * @var ConsultancyUserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param ConsultancyUserRepository $users
     * @param RoleRepository $roles
     */
    public function __construct(ConsultancyUserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->middleware('permissions:view-agency');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = User::whereAgencyId(auth()->user()->agency->id)->latest()->get();
        return view('agency.user.index', compact('users'));
    }

    public function create(ManageUserRequest $request)
    {
        return view('agency.user.create')
            ->withRoles($this->roles->getAll()->where('agency_user_role', '1'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'status' => $request->status,
            'confirmed' => 1,
            'created_by_id' => auth()->user()->id,
            'agency_id' => auth()->user()->agency->id,
        ]);

        $user->attachRoles($request->assignees_roles);
        
        // $this->users->create(
        //     [
        //         'data' => $request->only(
        //             'first_name',
        //             'last_name',
        //             'email',
        //             'password',
        //             'status',
        //             'confirmed',
        //             'confirmation_email',
        //             'created_by_id',
        //             'agency_id',
        //         ),
        //         'roles' => $request->only('assignees_roles'),
        //     ]);

        return redirect()->route('agency.user.index')->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request)
    {
        return view('agency.user.edit')
            ->withUser($user)
            ->withUserRoles($user->roles->pluck('id')->all())
            ->withRoles($this->roles->getAll());
    }

    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->users->update($user,
            [
                'data' => $request->only(
                    'first_name',
                    'last_name',
                    'email',
                    'status',
                    'confirmed'/*,
                    'created_by_id'*/
                ),
                'roles' => $request->only('assignees_roles'),
            ]);

        return redirect()->route('agency.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('agency.user.index')->with('success','User deleted successfully');
    }
}
