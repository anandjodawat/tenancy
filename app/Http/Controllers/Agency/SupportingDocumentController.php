<?php

namespace App\Http\Controllers\Agency;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agency\SupportingDocument;

class SupportingDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agency.profile.supporting-document.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'document_id' => 'required',
            // 'description' => 'required'
        ]);

        SupportingDocument::create([
            'agency_id' => auth()->user()->agency_id,
            'supporting_document' => $request->description,
            'document_id' => $request->document_id
        ]);

        flash('Supporting Document Details Added Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        foreach ($request->description as $key => $description) {
            $supportingDocument = SupportingDocument::where('agency_id', auth()->user()->agency_id)
            ->where('document_id', $request->document_id[$key])
            ->first();

            if ($supportingDocument) {
                $supportingDocument->update([
                    'supporting_document' => $description
                ]);
            }
        }

        flash('Supporting Documents Updated Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SupportingDocument::find($id)->delete();

        flash('Supporting Document Details Deleted Successfully.')->success();

        return response()->json([], 200);
    }
     public function delete()
    {
        SupportingDocument::find(request('id'))->delete();

        flash('Supporting Document Details Deleted Successfully.')->success();

        return response()->json([], 200);
    }
}
