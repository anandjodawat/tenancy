<?php

namespace App\Http\Controllers\UserDashboard;

use PDF;
use App\Models\Agency;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\Tenant\TenantApplication;
use App\Models\Application\ApplicationPet;
use App\Mail\Application\MovingServiceMail;
use App\Models\Application\ApplicationVehicle;
use App\Models\Application\ApplicationOccupant;
use App\Models\Application\ApplicationChildren;
use App\Mail\Application\NewApplicationSubmitted;
use App\Mail\Application\InviteJointApplicant;
use App\Models\Application\ApplicationMovingService;
use App\Mail\Application\PropertyManager\NewApplicationSubmittedForPropertyManager;

// docusign
use DocuSign;
use App\API\TenancyDocuSign;
use DocuSign\eSign\Api\EnvelopesApi;
use App\Rules\UserDashboard\AgencyName;
use DocuSign\eSign\Api\EnvelopesApi\GetDocumentOptions;
use GuzzleHttp\Client;


class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = $this->application_filter();

        return view('user-dashboard.application.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->tenantProfile && auth()->user()->tenantProfile->status == '1') {

            //CHECK WEATHER APPLICANT IS JOINT APPLICANT THAN PREFILL PROP DETAILS
            $isJointApplicant = ApplicationOccupant::whereOccupantEmail(auth()->user()->email)
                                ->Where('occupant_as_joint_tenant',  'yes')
                                ->latest()
                                ->first();
            if($isJointApplicant){

                $jointAppAlreadySubmitted = TenantApplication::whereJointAppId($isJointApplicant->application_id)->count();
                //echo $jointAppAlreadySubmitted;exit;
                if($jointAppAlreadySubmitted==0){
                    $primaryApplicantion = TenantApplication::whereId($isJointApplicant->application_id)
                                                            //->where('joint_app_id', '!=', $isJointApplicant->application_id)
                                                            ->latest()->first();
                    //echo '<pre>';print_r($primaryApplicantion);exit;    
                }
                
            }

            $latestApplicationExistance = TenantApplication::whereUserId(auth()->user()->id)->count() > 0;
            $latestOccupants = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->occupants : null;

            $latestChildren = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->children : null;

            $latestVehicle = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->vehicles : null;

            $latestPets = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->pets : null;

            $latestApplication = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first() : null;

            return view('user-dashboard.application.create', compact('relationships', 'latestOccupants', 'latestChildren', 'latestVehicle', 'latestPets', 'latestApplication', 'primaryApplicantion'));
        }
        flash('Please Complete Your Profile.')->error();
        return redirect()->route('user-dashboard.profile.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // Property Details
            'applied_for' => 'required',
            'state' => 'required',
            'suburb' => 'required',
            'post_code' => 'required|numeric',
            'property_type' => 'required',
            'number_of_bedrooms' => 'required',
            'commencement_date' => 'required',
            'length_of_lease' => 'required',
            'weekly_rent' => 'required',
            'monthly_rent' => 'required',
            'bond' => 'required',
            //'bond_assistance' => 'required',
            'property_found_in' => 'required',

            // Property Manager Details
            'agency_name' => ['required',new AgencyName],

            'property_manager_name' => 'required',
            'property_manager_email' => 'required',

            // Occupancy Details
            'number_of_occupant' => 'required',
            'number_of_children' => 'required',
            'number_of_vehicles' => 'required',
            'number_of_pets' => 'required',

            // Agent Specific
            'is_tenant_terminated' => 'required',
            'tenant_terminated_details' => 'required_if:is_tenant_terminated,yes',
            'is_tenant_refused' => 'required',
            'tenant_refused_details' => 'required_if:is_tenant_refused,yes',
            'is_on_debt' => 'required',
            'on_debt_details' => 'required_if:is_on_debt,yes',
            'is_deduction_rental_bond' => 'required',
            'deduction_rental_bond_details' => 'required_if:is_deduction_rental_bond,yes',
            'affect_future_rental_payment' => 'required',
            'affect_future_rental_payment_details' => 'required_if:affect_future_rental_payment,yes',
            'is_another_pending_application' => 'required',
            'own_a_property' => 'required',
            'own_a_property_details' => 'required_if:own_a_property,yes',
            'buy_another_property' => 'required',
            'intend_to_buy_after' => 'required_if:buy_another_property,yes',

            // Moving Service
            'need_moving_service' => 'required',
            'moving_service_agreement' => 'required',

            // Property Declaration
            'inspected_this_property' => 'required',
            'inspection_date' => 'required_if:inspected_this_property,yes',
            'good_condition' => 'required_if:inspected_this_property,yes',

            // Licence and aggreement
            'licence_and_agreement' => 'required',
        ]);

        $application = $this->save_application_form($request);

        $this->save_application_occupant($application->id);
        $this->save_application_children($application->id);
        $this->save_application_pet($application->id);
        $this->save_application_vehicle($application->id);
        $this->save_moving_service($application->id);

        $this->get_docusign_url($application->id);

        $application->update([
            'status' => '1',
            'draft' => '0'
        ]);

        $agency = Agency::whereName($application->agency_name)->first();

        if ($agency) {
            $setting_check = Setting::where('agency_id', $agency->id)->first();
            if ($setting_check && $setting_check->key == '1') {
                Mail::to($setting_check->value)->send(new MovingServiceMail($application->id));
            }
        }


        Mail::to(auth()->user()->email)->send(new NewApplicationSubmitted(auth()->user(), $application->id));
        Mail::to($application->property_manager_email)->send(new NewApplicationSubmittedForPropertyManager(auth()->user(), $application));

        flash('Application submitted successfully. Please Sign Your Document.')->success();
        return redirect('/sign-document/'.$application->id);
    }

    private function save_application_form($request)
    {
        $values = [
            // Property Details
            'applied_for' => $request->applied_for,
            'state' => $request->state,
            'suburb' => $request->suburb,
            'post_code' => $request->post_code,
            'property_type' => $request->property_type,
            'number_of_bedrooms' => $request->number_of_bedrooms,
            'commencement_date' => $request->commencement_date,
            'length_of_lease' => $request->length_of_lease,
            'weekly_rent' => $request->weekly_rent,
            'monthly_rent' => $request->monthly_rent,
            'bond' => $request->bond,
            'bond_assistance' => $request->bond_assistance,
            'property_found_in' => $request->property_found_in,

            // Property Manager Details
            'agency_name' => $request->agency_name,
            'property_manager_name' => $request->property_manager_name,
            'property_manager_email' => $request->property_manager_email,

            // Occupancy Details
            'number_of_occupant' => $request->number_of_occupant,
            'number_of_children' => $request->number_of_children,
            'number_of_vehicles' => $request->number_of_vehicles,
            'number_of_pets' => $request->number_of_pets,

            // Agent Specific
            'is_tenant_terminated' => $request->is_tenant_terminated,
            'tenant_terminated_details' => $request->tenant_terminated_details,
            'is_tenant_refused' => $request->is_tenant_refused,
            'tenant_refused_details' => $request->tenant_refused_details,
            'is_on_debt' => $request->is_on_debt,
            'on_debt_details' => $request->on_debt_details,
            'is_deduction_rental_bond' => $request->is_deduction_rental_bond,
            'deduction_rental_bond_details' => $request->deduction_rental_bond_details,
            'affect_future_rental_payment' => $request->affect_future_rental_payment,
            'affect_future_rental_payment_details' => $request->affect_future_rental_payment_details,
            'is_another_pending_application' => $request->is_another_pending_application,
            'own_a_property' => $request->own_a_property,
            'own_a_property_details' => $request->own_a_property_details,
            'buy_another_property' => $request->buy_another_property,
            'intend_to_buy_after' => $request->intend_to_buy_after,

            // Moving Service
            'need_moving_service' => $request->need_moving_service,
            'moving_service_name' => $request->utility_name,
            'moving_service_agreement' => $request->moving_service_agreement,

            // Property Declaration
            'inspected_this_property' => $request->inspected_this_property,
            'inspection_date' => $request->inspection_date,
            'good_condition' => $request->good_condition,
            'inspection_code' => $request->inspection_code,
            'not_good_condition_reason' => $request->not_good_condition_reason,

            // Licence and aggreement
            'licence_and_agreement' => $request->licence_and_agreement,
            'user_id' => auth()->user()->id,
            'joint_app_id' => $request->joint_app_id,
        ];

        if (request('application_id')) {
            TenantApplication::find(request('application_id'))->update($values);
            $application = TenantApplication::find(request('application_id'));
            $application->occupants()->delete();
            $application->children()->delete();
            $application->vehicles()->delete();
            $application->pets()->delete();
            $application->moving_service()->delete();

            return $application;
        }
        return TenantApplication::create($values);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        if (request('view') == 'pdf') {
            $document_names = TenantApplication::DOCUMENTNAME;
            $application = TenantApplication::find($id);


            if ($application->envelope_id) {
                $docuSign = new TenancyDocuSign('user-dashboard.application.pdf', $id);
                $account_id = $docuSign->accountId();

                $envelopeOption = new EnvelopesApi($docuSign->apiClient());

                $envelopeStatus =  json_decode($envelopeOption->getEnvelope($account_id, $application->envelope_id));
                //echo public_path()."/pdf/my_document".$application->envelope_id.".pdf"; die;
                if ($envelopeStatus->status == 'completed') {
                    if (!file_exists(public_path()."/pdf/my_document".$application->envelope_id.".pdf")) {

                        $options = new GetDocumentOptions();
                        $options->setCertificate(TRUE);

                        $document = $envelopeOption->getDocument($account_id, 1, $application->envelope_id, $options);
                        //echo $document->getPathname(); die;
                        file_put_contents(public_path()."/pdf/my_document".$application->envelope_id.".pdf", file_get_contents($document->getPathname()));
                    }
                } else {
                    $this->get_docusign_url($application->id);
                    return redirect('/sign-document/'.$application->id);
                }
                $url = "/pdf/my_document".$application->envelope_id.".pdf";
                return view('user-dashboard.application.sign.index', compact('url'));
            }

            $property_manager = User::whereEmail($application->property_manager_email)->get();
            $agency = "";
            if ($property_manager->count() > 0) {
                $property_manager = User::whereEmail($application->property_manager_email)->first();
                if ($property_manager->agency_id) {
                    $agency = Agency::find($property_manager->agency_id);
                } else {
                    flash('There is an error with Property Manager Details')->error();
                    return redirect('/dashboard');
                }
            } else {
                flash('Property Manager is not Found in Our System.')->error();
                return redirect('/dashboard');
            }

            $lease_terms = $agency && $agency->leaseTerms ? $agency->leaseTerms->lease_terms : '';
            $supporting_documents = $agency->supportingDocument;
            //echo "<pre>"; print_r($supporting_documents); die;
            //$setting = Setting::whereAgencyId($agency->id)->first();
            $setting = Setting::where('agency_id', $agency->id)
                        ->Where('is_active', 'yes')
                        ->first();

            $pdf = PDF::loadView('user-dashboard.application.pdf', compact('lease_terms', 'supporting_documents', 'application', 'document_names', 'agency', 'setting'));
            return $pdf->stream();
        }
        $documents = TenantApplication::DOCUMENTNAME;
        $application = TenantApplication::find($id);

        if (auth()->user()->can('view', $application)) {
            return view('user-dashboard.application.show', compact('application', 'documents'));
        }
        else
        {
            flash('Permission Denied.')->error();
            return redirect('/dashboard');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $latestApplicationExistance = TenantApplication::whereUserId(auth()->user()->id)->count() > 0;
        $latestOccupants = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->occupants : null;

        $latestChildren = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->children : null;

        $latestVehicle = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->vehicles : null;

        $latestPets = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first()->pets : null;

        $latestApplication = $latestApplicationExistance ? TenantApplication::whereUserId(auth()->user()->id)->latest()->first() : null;

        $application = TenantApplication::find($id);
        if (!auth()->user()->can('update', $application)) {
            flash('Permission Denied.')->error();
            return redirect('/dashboard');
        }
        return view('user-dashboard.application.edit', compact('application', 'relationships', 'latestOccupants', 'latestChildren', 'latestVehicle', 'latestPets', 'latestApplication'));
        // return view('user-dashboard.application.edit', compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            // Property Details
            'applied_for' => 'required',
            'state' => 'required',
            'suburb' => 'required',
            'post_code' => 'required',
            'property_type' => 'required',
            'number_of_bedrooms' => 'required',
            'commencement_date' => 'required',
            'length_of_lease' => 'required',
            'weekly_rent' => 'required',
            'monthly_rent' => 'required',
            'bond' => 'required',
            //'bond_assistance' => 'required',
            'property_found_in' => 'required',

            // Property Manager Details
            'agency_name' => 'required',
            'property_manager_name' => 'required',
            'property_manager_email' => 'required',

            // Occupancy Details
            'number_of_occupant' => 'required',
            'number_of_children' => 'required',
            'number_of_vehicles' => 'required',
            'number_of_pets' => 'required',

            // Agent Specific
            'is_tenant_terminated' => 'required',
            'tenant_terminated_details' => 'required_if:is_tenant_terminated,yes',
            'is_tenant_refused' => 'required',
            'tenant_refused_details' => 'required_if:is_tenant_refused,yes',
            'is_on_debt' => 'required',
            'on_debt_details' => 'required_if:is_on_debt,yes',
            'is_deduction_rental_bond' => 'required',
            'deduction_rental_bond_details' => 'required_if:is_deduction_rental_bond,yes',
            'affect_future_rental_payment' => 'required',
            'affect_future_rental_payment_details' => 'required_if:affect_future_rental_payment,yes',
            'is_another_pending_application' => 'required',
            'own_a_property' => 'required',
            'own_a_property_details' => 'required_if:own_a_property,yes',
            'buy_another_property' => 'required',
            'intend_to_buy_after' => 'required_if:buy_another_property,yes',

            // Moving Service
            'need_moving_service' => 'required',
            'moving_service_agreement' => 'required',

            // Property Declaration
            'inspected_this_property' => 'required',
            'inspection_date' => 'required_if:inspected_this_property,yes',
            'good_condition' => 'required_if:inspected_this_property,yes',

            // Licence and aggreement
            'licence_and_agreement' => 'required',
        ]);

        TenantApplication::find($id)->update([
             // Property Details
            'applied_for' => $request->applied_for,
            'state' => $request->state,
            'suburb' => $request->suburb,
            'post_code' => $request->post_code,
            'property_type' => $request->property_type,
            'number_of_bedrooms' => $request->number_of_bedrooms,
            'commencement_date' => $request->commencement_date,
            'length_of_lease' => $request->length_of_lease,
            'weekly_rent' => $request->weekly_rent,
            'monthly_rent' => $request->monthly_rent,
            'bond' => $request->bond,
            'bond_assistance' => $request->bond_assistance,
            'property_found_in' => $request->property_found_in,

            // Property Manager Details
            'agency_name' => $request->agency_name,
            'property_manager_name' => $request->property_manager_name,
            'property_manager_email' => $request->property_manager_email,

            // Occupancy Details
            'number_of_occupant' => $request->number_of_occupant,
            'number_of_children' => $request->number_of_children,
            'number_of_vehicles' => $request->number_of_vehicles,
            'number_of_pets' => $request->number_of_pets,

            // Agent Specific
            'is_tenant_terminated' => $request->is_tenant_terminated,
            'tenant_terminated_details' => $request->tenant_terminated_details,
            'is_tenant_refused' => $request->is_tenant_refused,
            'tenant_refused_details' => $request->tenant_refused_details,
            'is_on_debt' => $request->is_on_debt,
            'on_debt_details' => $request->on_debt_details,
            'is_deduction_rental_bond' => $request->is_deduction_rental_bond,
            'deduction_rental_bond_details' => $request->deduction_rental_bond_details,
            'affect_future_rental_payment' => $request->affect_future_rental_payment,
            'affect_future_rental_payment_details' => $request->affect_future_rental_payment_details,
            'is_another_pending_application' => $request->is_another_pending_application,
            'own_a_property' => $request->own_a_property,
            'own_a_property_details' => $request->own_a_property_details,
            'buy_another_property' => $request->buy_another_property,
            'intend_to_buy_after' => $request->intend_to_buy_after,

            // Moving Service
            'need_moving_service' => $request->need_moving_service,
            'moving_service_name' => $request->utility_name,
            'moving_service_agreement' => $request->moving_service_agreement,

            // Property Declaration
            'inspected_this_property' => $request->inspected_this_property,
            'inspection_date' => $request->inspection_date,
            'good_condition' => $request->good_condition,
            'inspection_code' => $request->inspection_code,
            'not_good_condition_reason' => $request->not_good_condition_reason,

            // Licence and aggreement
            'licence_and_agreement' => $request->licence_and_agreement,
            'draft' => '0',
            'status' => '1'
        ]);


        $application = TenantApplication::find($id);
        $this->get_docusign_url($id);

        $agency = Agency::whereName($application->agency_name)->first();

        if ($agency) {
            $setting_check = Setting::where('agency_id', $agency->id)->where('key', '0')->first();
            if ($setting_check && $setting_check->value == 'email') {
                $email = Setting::where('agency_id', $agency->id)->where('key', '1')->first();
                Mail::to($email->value)->send(new MovingServiceMail($application->id));
            }
        }


        Mail::to(auth()->user())->send(new NewApplicationSubmitted(auth()->user(), $application->id));
        Mail::to($application->property_manager_email)->send(new NewApplicationSubmittedForPropertyManager(auth()->user(), $application));

        flash('Application Updated Successfully.')->success();
        return redirect('/sign-document/'.$application->id);

        // return redirect()->route('user-dashboard.applications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete()
    {
        TenantApplication::find(request('id'))->delete();

        flash('Application moved to trash.')->success();
        return redirect('/applications');
    }

    private function save_moving_service($application_id)
    {
        if (request('moving_service_id') != null) {
            
            foreach (request('moving_service_id') as $key => $option) {
                ApplicationMovingService::create([
                    'application_id' => $application_id,
                    'moving_service_id' => $option
                ]);
            }
            
        }
    }

    private function save_application_occupant($application_id)
    {
        for ($i=0; $i < request('number_of_occupant'); $i++) {
            ApplicationOccupant::create([
                'occupant_name' => request('occupant_name')[$i],
                'occupant_age' => request('occupant_age')[$i],
                'occupant_relationship' => request('occupant_relationship')[$i],
                'occupant_on_lease' => request('occupant_on_lease')[$i],
                'occupant_as_joint_tenant' => request('occupant_as_joint_tenant')[$i],
                'occupant_email' => request('joint_occupant_email')[$i],
                'occupant_mobile_number' => request('joint_occupant_mobile')[$i],
                'application_id' => $application_id
            ]);

            //SEND MAIL TO JOINT APPLICANT FOR RESUBMITION OF THIS APPLICATION
            if(request('occupant_as_joint_tenant')[$i]=='yes'){
                $joint_occupant_email = request('joint_occupant_email')[$i];
                $joint_occupant_name = request('occupant_name')[$i];
                Mail::to($joint_occupant_email)->send(new InviteJointApplicant(auth()->user(), $application_id, $joint_occupant_name ));
            }

        }
    }

    private function save_application_children($application_id)
    {
        for ($i=0; $i < request('number_of_children'); $i++) {
            ApplicationChildren::create([
                'children_age' => request('children_age')[$i],
                'application_id' => $application_id
            ]);
        }
    }

    private function save_application_pet($application_id)
    {
        for ($i=0; $i < request('number_of_pets'); $i++) {
            ApplicationPet::create([
                'application_id' => $application_id,
                'pet_type' => request('pet_type')[$i],
                'pet_breed' => request('pet_breed')[$i]
            ]);
        }
    }

    private function save_application_vehicle($application_id)
    {
        for ($i=0; $i < request('number_of_vehicles'); $i++) {
            ApplicationVehicle::create([
                'application_id' => $application_id,
                'vehicle_type' => request('vehicle_type')[$i],
                'vehicle_registration' => request('vehicle_registration')[$i],
                'make_model' => request('make_model')[$i]
            ]);
        }
    }

    public function save_media(Request $request)
    {
        // print_r($_FILES);
        // echo $request->hasFile('file');

        // return $request;
        if ($request->file('file')) {
            // echo $request->file->path();
            if (auth()->user()->getFirstMedia($request->name)) {
                auth()->user()->deleteMedia(auth()->user()->getFirstMedia($request->name)->id);
            }
            auth()->user()->addMedia($request->file('file'))->toMediaCollection($request->name, 'media');
            return 'Upload Successful';
        } else {
            return 'No image found';
        }
    }

    public function remove_media(Request $request)
    {
        //return $request->name;
        if (auth()->user()->getFirstMedia($request->name)) {
            auth()->user()->deleteMedia(auth()->user()->getFirstMedia($request->name)->id);

            return 'Removed Successfully';
        } else {
            return 'No image found';
        }
    }

    public function save_as_draft(Request $request)
    {
        $application = $this->save_application_form($request);

        $this->save_application_occupant($application->id);
        $this->save_application_children($application->id);
        $this->save_application_pet($application->id);
        $this->save_application_vehicle($application->id);
        $this->save_moving_service($application->id);

        TenantApplication::find($application->id)->update([
            'draft' => '1'
        ]);

        return response()->json(TenantApplication::find($application->id), 200);
    }

    public function get_agency_list()
    {
        return Agency::where('name', 'like', '%'.request('value').'%')
        ->get();
    }

    public function get_property_manager_list()
    {
        $agency = Agency::whereName(request('agency_name'))->first();

        if ($agency) {
            $users = User::whereAgencyId($agency->id)->whereHas('roles', function($q){
                $q->where('name', 'property-manager');
            })->get();
        } else {
            $users = [];
        }

        return $users;
    }

    public function get_property_manager_email()
    {
        return User::find(request('user_id'))->email;
    }

    public function agency_application_details()
    {
        $agency = Agency::whereName(request('agency_name'))->first();
        $setting = Setting::whereAgencyId($agency->id)->Where('is_active', 'yes')->first();
        $data = ['lease_terms' => '', 'supporting_documents' => '', 'logo' => ''];
        if ($agency) {
            $data['lease_terms'] = optional($agency->leaseTerms)->lease_terms;
            $data['supporting_documents'] = '';
            // $data['supporting_documents'] = $agency->supportingDocument;
            if ($agency->getFirstMedia('agency_logos')) {
                $data['logo'] = $agency->getFirstMedia('agency_logos')->getUrl();
            }

            /*if ($agency->getFirstMedia('utility_connection')) {
                $data['utility_connection_logo'] = $agency->getFirstMedia('utility_connection')->getUrl();
            }*/
            if (!is_null($setting) &&  $agency->getFirstMedia($agency->id.'-'.$setting->key)){
                $data['utility_connection_logo'] = $agency->getFirstMedia($agency->id.'-'.$setting->key)->getUrl();
            }

            $data['utility_email'] = $setting ? $setting->value : '';
            //$data['utility_name'] = $setting && $setting->key == '1' ? $setting->connection_name : 'Compare And Connect';
            $data['utility_name'] = $setting->connection_name;

            if ($setting && $setting->body) {
                $data['terms_body'] = $setting->body;
            }

            $data['key'] = $setting ? $setting->key : '';
            $data['supporting_documents'] = $agency->supportingDocument;
        }

        return response()->json($data, 200);

    }

    private function application_filter()
    {

        if (request('type') == 'draft') {
            $applications = TenantApplication::whereUserId(auth()->user()->id)
            ->whereDraft(1);
        } elseif (request('type')  == 'sent') {
            $applications =  TenantApplication::whereDraft('0')
            ->whereUserId(auth()->user()->id);
        } elseif(request('type') == 'approved') {
            $applications = TenantApplication::whereStatus(3)
            ->whereUserId(auth()->user()->id);
        } elseif (request('type') == 'accepted') {
            $applications = TenantApplication::whereStatus(2)
            ->whereUserId(auth()->user()->id);
        } else {
            $applications = TenantApplication::whereUserId(auth()->user()->id);
        }

        return $applications->latest()->get();

    }

    public function get_docusign_url($application_id)
    {
        $application = TenantApplication::find($application_id);

//docusign data
        $docuSign = new TenancyDocuSign('user-dashboard.application.pdf', $application_id);
        $account_id = $docuSign->accountId();
        $data = json_decode($docuSign->sendData());
         //dd($account_id);
         //dd('ok');
        $envelopeOption = new EnvelopesApi($docuSign->apiClient());


        $envelopId = $data->envelopeId;

        $recipient_view_request = new \DocuSign\eSign\Model\RecipientViewRequest();

       // set where the recipient is re-directed once they are done signing

       // $recipient_view_request->setReturnUrl('http://tenancyapplication.entryinsights.com/property-manager/tenancy-pdf-application');

       if (env('APP_ENV') == 'production') {
        $recipient_view_request->setReturnUrl('https://tenancyapplication.com.au/thank-you');
       } else {
        $recipient_view_request->setReturnUrl('http://tenancyapplication.entryinsights.com/thank-you');
       }

       // configure the embedded signer

        $recipient_view_request->setUserName($application->user->name);

        $recipient_view_request->setEmail($application->user->email);

       // must reference the same clientUserId that was set for the recipient when they

       // were added to the envelope in step 2

        $recipient_view_request->setClientUserId($application->user->id);

       // used to indicate on the certificate of completion how the user authenticated

        $recipient_view_request->setAuthenticationMethod("email");



         //$get_data = $envelopeOption->createRecipientView($account_id, $envelopId, $recipient_view_request);//"7fe693ea-dea6-438d-8169-34ed8abd38c6"

        try {
            $get_data = $envelopeOption->createRecipientView($account_id, $envelopId, $recipient_view_request);//"7fe693ea-dea6-438d-8169-34ed8abd38c6"
        } catch (DocuSign\eSign\ApiException $e){
            dd("Error connecting Docusign : " . $e->getResponseBody()->errorCode . " " . $e->getResponseBody()->message);
        }
        // $get_data = $envelopeOption->createRecipientView($account_id, $envelopId);//"7fe693ea-dea6-438d-8169-34ed8abd38c6"


        $data = json_decode($get_data);
        if (isset($get_data['http_code'])) {
            dd("Error connecting Docusign : ");
        } else {
            $application->update([
                'docusign_url' => $data->url,
                'envelope_id' => $envelopId
            ]);
            // $document = $envelopeOption->getDocument($account_id, 1, $envelopId);
            // file_put_contents(public_path().'/pdf/'.$envelopId.".pdf", file_get_contents($document->getPathname()));
        }
        return;
        // return $get_data;
    }


    public function get_valid_address(Request $request){

        $client = new Client();

        $term = $request->get('term');

        if ( isset($term) && $term!='' ) {

            $res = $client->request('POST', 'https://mappify.io/api/rpc/address/autocomplete/', [
                'headers' => [
                    'Accept'     => 'application/json'
                ],
                'json' => [
                    'apiKey' => "b51fc733-b52e-4c58-905b-e1ba262b589d",
                    'streetAddress' => $term,
                    'formatCase' => true
                ]
            ]);

            $results = array();
           
            if ($res->getStatusCode() == 200) { // 200 OK

                $response_data = $res->getBody()->getContents();
                $response_data = json_decode($response_data);
                //echo '<pre>';print_r($response_data->result);
                //$results[] = [ 'fulladdress' => $query->address.', '.$query->suburb.' '.$query->state.' '.$query->post_code ];

                return response()->json($response_data->result);
            }

            
        }

    }

}
