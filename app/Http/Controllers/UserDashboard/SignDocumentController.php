<?php

namespace App\Http\Controllers\UserDashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;

class SignDocumentController extends Controller
{
    public function index($id)
    {
    	$url = TenantApplication::find($id)->docusign_url;
    	return view('user-dashboard.application.sign.index', compact('url'));
    }
}
