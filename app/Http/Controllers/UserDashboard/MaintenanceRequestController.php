<?php

namespace App\Http\Controllers\UserDashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\MaintenanceRequestMail;
use App\Models\Tenant\MaintenanceRequest;
use App\Mail\TenantMaintenanceRequestMail;


class MaintenanceRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maintenanceRequest = MaintenanceRequest::where('user_id', auth()->user()->id)->latest()->get();
        return view('user-dashboard.maintenance-request.index', compact('maintenanceRequest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user-dashboard.maintenance-request.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'street_address' => 'required',
            'state' => 'required',
            'suburb' => 'required',
            'post_code' => 'required|numeric',
            'tenant' => 'required',
            'name' => 'required',
            'email' => 'required',
            'preferred_time' => 'required',
            'preferred_date' => 'required',
            'property_manager_email' => 'required'
        ]);

        $maintenance = MaintenanceRequest::create([
            'street_address' => $request->street_address,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code,
            'tenant' => $request->tenant,
            'name' => $request->name,
            'mobile_phone' => $request->mobile_phone,
            'home_phone' => $request->home_phone,
            'work_phone' => $request->work_phone,
            'email' => $request->email,
            'details' => $request->details,
            'preferred_date' => $request->preferred_date,
            'preferred_time' => $request->preferred_time,
            'maintenance_existed' => $request->maintenance_existed,
            'property_manager_email' => $request->property_manager_email,
            'user_id' => auth()->user()->id
        ]);

        if ($request->file('file1')) {
            $maintenance->addMedia($request->file('file1'))->toMediaCollection('attachment1', 'media');
        }

        if ($request->file('file2')) {
            $maintenance->addMedia($request->file('file2'))->toMediaCollection('attachment2', 'media');
        }

        Mail::to($request->property_manager_email)->send(new MaintenanceRequestMail($maintenance));
        Mail::to(auth()->user()->email)->send(new TenantMaintenanceRequestMail($maintenance));

        flash('Maintenance Request Sent Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $maintenanceRequest = MaintenanceRequest::find($id);
        
        return view('user-dashboard.maintenance-request.show', compact('maintenanceRequest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maintenanceRequest = MaintenanceRequest::find($id);

        return view('user-dashboard.maintenance-request.edit', compact('maintenanceRequest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'street_address' => 'required',
            'state' => 'required',
            'suburb' => 'required',
            'post_code' => 'required|numeric',
            'tenant' => 'required',
            'name' => 'required',
            'email' => 'required',
            'preferred_time' => 'required',
            'preferred_date' => 'required',
            'property_manager_email' => 'required'
        ]);

        $maintenance = MaintenanceRequest::find($id);

        $maintenance->update([
            'street_address' => $request->street_address,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code,
            'tenant' => $request->tenant,
            'name' => $request->name,
            'mobile_phone' => $request->mobile_phone,
            'home_phone' => $request->home_phone,
            'work_phone' => $request->work_phone,
            'email' => $request->email,
            'details' => $request->details,
            'preferred_date' => $request->preferred_date,
            'preferred_time' => $request->preferred_time,
            'maintenance_existed' => $request->maintenance_existed,
            'property_manager_email' => $request->property_manager_email,
        ]);

        if ($request->file('file1')) {
            $maintenance->addMedia($request->file('file1'))->toMediaCollection('attachment1', 'media');
        }

        if ($request->file('file2')) {
            $maintenance->addMedia($request->file('file2'))->toMediaCollection('attachment2', 'media');
        }

        flash('Maintenance Request Updated Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
