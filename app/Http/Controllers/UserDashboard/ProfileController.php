<?php

namespace App\Http\Controllers\UserDashboard;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantProfile;
use App\Traits\SaveTenantProfileTrait;

class ProfileController extends Controller
{
    use SaveTenantProfileTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $profile = TenantProfile::whereUserId(auth()->user()->id)->first();

        if (!$profile) {
            $profile = TenantProfile::create([
                'user_id' => auth()->user()->id,
                'email' => auth()->user()->email
            ]);

            $this->save_tenant_profile();
        }

        $profile = DB::table('tenant_profiles')
            ->join('tenant_emergency_contacts', 'tenant_profiles.id', '=', 'tenant_emergency_contacts.profile_id')
            ->join('tenant_current_addresses', 'tenant_profiles.id', '=', 'tenant_current_addresses.profile_id')
            ->join('tenant_previous_addresses', 'tenant_profiles.id', '=', 'tenant_previous_addresses.profile_id')
            ->join('tenant_current_employments', 'tenant_profiles.id', '=', 'tenant_current_employments.profile_id')
            ->join('tenant_previous_employments', 'tenant_profiles.id', '=', 'tenant_previous_employments.profile_id')
            ->join('tenant_personal_references', 'tenant_profiles.id', '=', 'tenant_personal_references.profile_id')
            ->join('tenant_professional_references', 'tenant_profiles.id', '=', 'tenant_professional_references.profile_id')
            ->join('tenant_income_expenditures', 'tenant_profiles.id', '=', 'tenant_income_expenditures.profile_id')
            ->join('tenant_repayments', 'tenant_profiles.id', '=', 'tenant_repayments.profile_id')
            ->whereUserId(auth()->user()->id)
            ->first();

        if (!$profile) {
            $profile = TenantProfile::whereUserId(auth()->user()->id)->first();
        }

        return view('user-dashboard.profile.edit', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $this->save_tenant_profile();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->ajax()) {
            $this->validate_tenant_profile($request);
        }

        if ($request->file('image')) {
            if (auth()->user()->getFirstMedia('profile_images')) {
                $media_id = auth()->user()->getFirstMedia('profile_images')->id;
                auth()->user()->deleteMedia($media_id);
            } 
            auth()->user()->addMedia($request->file('image'))->toMediaCollection('profile_images', 'media');
        }
        $this->store($request);
        flash('Your profile has been stored successfully.')->success();

        if($request->ajax()) {
            return response()->json('success', 200);
        } else {
            auth()->user()->tenantProfile->update([
                'status' => '1'
            ]);
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
