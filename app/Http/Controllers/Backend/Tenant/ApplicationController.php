<?php

namespace App\Http\Controllers\Backend\Tenant;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;
use App\Models\Access\User\User;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = TenantApplication::latest()->get();
        return view('backend.tenant.application.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*$application = TenantApplication::find($id);
        $user = User::find($application->user_id);
        $documents = TenantApplication::DOCUMENTNAME;

        $profile = DB::table('tenant_profiles')
            ->join('tenant_emergency_contacts', 'tenant_profiles.id', '=', 'tenant_emergency_contacts.profile_id')
            ->join('tenant_current_addresses', 'tenant_profiles.id', '=', 'tenant_current_addresses.profile_id')
            ->join('tenant_previous_addresses', 'tenant_profiles.id', '=', 'tenant_previous_addresses.profile_id')
            ->join('tenant_current_employments', 'tenant_profiles.id', '=', 'tenant_current_employments.profile_id')
            ->join('tenant_previous_employments', 'tenant_profiles.id', '=', 'tenant_previous_employments.profile_id')
            ->join('tenant_personal_references', 'tenant_profiles.id', '=', 'tenant_personal_references.profile_id')
            ->join('tenant_professional_references', 'tenant_profiles.id', '=', 'tenant_professional_references.profile_id')
            ->join('tenant_income_expenditures', 'tenant_profiles.id', '=', 'tenant_income_expenditures.profile_id')
            ->join('tenant_repayments', 'tenant_profiles.id', '=', 'tenant_repayments.profile_id')
            ->whereUserId($application->user_id)
            ->first();

        return view('backend.tenant.application.show', compact('application', 'user', 'documents','profile'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TenantApplication::find($id)->delete();

        flash('Application moved to trash');
        return redirect()->back();
    }
}
