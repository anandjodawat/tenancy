<?php

namespace App\Http\Controllers\Backend\UserManagement;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

//use App\Models\Consultancy;
use App\Models\Access\User\User;
use App\Models\Agency;

//se App\Repositories\Backend\ConsultancyRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use Illuminate\View\Middleware\ShareErrorsFromSession;

//use App\Http\Requests\Backend\Consultancy\ManageConsultancyRequest;
//use App\Http\Requests\Backend\Consultancy\UpdateConsultancyRequest;


class AgencyController extends Controller
{

    /**
     * @var RoleRepository
     */
    protected $consultancy;

    /**
     * @param UserRepository $users
     * @param RoleRepository $consultancy
     */
    /*public function __construct(ConsultancyRepository $consultancy)
    {
        $this->consultancy = $consultancy;
    }*/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agencies = Agency::latest()->get();
        return view('backend.user.agency.index', compact('agencies'));
    }

    /*public function getConsultancyData()
    {
        return Datatables::of($this->consultancy->getForDataTable(1, request('status')))

        ->escapeColumns(['name','address','district','phone','website','registration_no','email','mobile','status','verified',])

            ->addColumn('actions', function ($consultancy) {
                return $consultancy->action_buttons;
            })
            ->setRowClass(function ($consultancy) {
                return '';
            })
            ->make(true);
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.agency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:consultancy',
        ]);
        
        if ($validator->fails()) {
            return redirect()->route('admin.consultancy.create')
                ->withInput()
                ->withErrors($validator);
        }

        Consultancy::create($request->all());
        return redirect()->route('admin.consultancy.index')
                        ->with('success','Consultancy created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function show(Consultancy $consultancy, ManageConsultancyRequest $request)
    {
        $users = User::consultancyUsers($consultancy->id)->latest()->get();

        return view('backend.agency.show', compact('consultancy', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Consultancy $consultancy, ManageConsultancyRequest $request)
    {
        return view('backend.agency.edit', compact('consultancy'));
    }

    /**
     * @param Consultancy              $consultancy
     * @param UpdateConsultancyRequest $request
     *
     * @return mixed
     */
    
    public function update(Consultancy $consultancy, UpdateConsultancyRequest $request)
    {
        $this->consultancy->update($consultancy,
            [
                'data' => $request->only(
                    'name',
                    'address',
                    'district',
                    'phone',
                    'website',
                    'registration_no',
                    'email',
                    'mobile',
                    'status',
                    'verified'
                )
            ]);

        return redirect()->route('admin.agencies.index')->withFlashSuccess(trans('alerts.backend.consultancy.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    { 

        Agency::find($id)->delete();
        User::where('agency_id', '=', $id)->delete();
        return redirect()->back()->withFlashSuccess('Agency moved to trash');
        //$this->consultancy->delete($consultancy);

        //return redirect()->route('admin.consultancy.index')->withFlashSuccess(trans('alerts.backend.consultancy.deleted'));
    }

    public function activateConsultancy()
    {
        $id = request('consultancy_id');
        
        $consultancy = Consultancy::find($id);

        $consultancy->update(['status' => 'active']);

        if(!$consultancy->user->first()->hasRole(2))
        {
            $consultancy->user->first()->attachRole(2);
        }

        return redirect()->route('admin.agencies.index')->withFlashSuccess(trans('alerts.backend.consultancy.activated'));
    }
}
