<?php

namespace App\Http\Controllers\Backend\Access\Permission;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Models\Access\Permission\Permission;
use App\Events\Backend\Access\Permission\PermissionCreated;
use App\Events\Backend\Access\Permission\PermissionUpdated;
use App\Events\Backend\Access\Permission\PermissionDeleted;
use App\Repositories\Backend\Access\Permission\PermissionRepository;

class PermissionController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $permission;

    /**
     * @param UserRepository $users
     * @param RoleRepository $permission
     */
    public function __construct(PermissionRepository $permission)
    {
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.access.permissions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.access.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions|min:5',
            'display_name' => 'required|min:5'
        ]);

        $permission = Permission::create([
            'name' => request('name'),
            'display_name'=> request('display_name')
        ]);

        event(new PermissionCreated($permission));

        return redirect(route('admin.access.permission.index'))
            ->withFlashSuccess('New Permission Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);
        return view('backend.access.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:5|unique:permissions,name,'.$id,
            'display_name' => 'required|min:5'
        ]);

        $permission = Permission::find($id);
        $permission->update([
            'name' => request('name'),
            'display_name' => request('display_name')
        ]);

        event(new PermissionUpdated($permission));

        return redirect(route('admin.access.permission.index'))
            ->withFlashSuccess('Permission successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);

        event(new PermissionDeleted($permission));

        $permission->delete();

        return redirect()->back()->withFlashDanger('Permission successfully deleted.');
    }

    public function getPermission()
    {
        $permissions = Permission::select(['id', 'name', 'display_name', 'created_at']);

        return Datatables::of($permissions)
            ->addColumn('action', function ($permission) {
                return $permission->action_buttons;
            })->make(true);
    }
}
