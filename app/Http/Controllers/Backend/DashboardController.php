<?php

namespace App\Http\Controllers\Backend;

use Cache;
use App\Models\Consultancy;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.dashboard');
    }
}
