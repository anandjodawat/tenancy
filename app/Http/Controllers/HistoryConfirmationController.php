<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\HistoryConfirmedMail;
use App\Models\HistoryConfirmation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class HistoryConfirmationController extends Controller
{
    public function index()
    {
        return view('history-confirmation');
    }

    public function store( Request $request )
    {
        $this->validate($request, [
            'date' => 'required',
            'agency' => 'required',
            'tenant_name' => 'required',
            'property_address' => 'required',
            'listed_as_lease' => 'required',
            'property_duration_from' => 'required',
            'property_duration_to' => 'required',
            'terminate_tenancy' => 'required',
            'arrears' => 'required',
            'breach_notices' => 'required',
            'damage' => 'required',
            'inspection_like' => 'required',
            'pets' => 'required',
            'vacate_date' => 'required',
            'bond_refund_full' => 'required',        
            'rate_tenant' => 'required|numeric',
            'rent_again' => 'required',
            'completed_by' => 'required',
            'employer_position' => 'required',
            'submission_date' => 'required',
            'personal_ref_id' => 'required'
        ]);

        $history = HistoryConfirmation::create([
            'date' => $request->date,
            'agency' => $request->agency,
            'tenant_name' => $request->tenant_name,
            'property_address' => $request->property_address,
            'listed_as_lease' => $request->listed_as_lease,
            'property_duration_from' => $request->property_duration_from,
            'property_duration_to' => $request->property_duration_to,
            'terminate_tenancy' => $request->terminate_tenancy,
            'reason_for_termination' => $request->reason_for_termination,
            'arrears' => $request->arrears,
            'breach_notices' => $request->breach_notices,
            'reason_for_breach_notices' => $request->reason_for_breach_notices,
            'damage' => $request->damage,
            'damage_reason' => $request->damage_reason,
            'inspection_like' => $request->inspection_like,
            'pets' => $request->pets,
            'complaints_damage' => $request->complaints_damage,
            'vacate_date' => $request->vacate_date,
            'bond_refund_full' => $request->bond_refund_full,
            'bond_refund_reason' => $request->bond_refund_reason,
            'rate_tenant' => $request->rate_tenant,
            'rent_again' => $request->rent_again,
            'additional_comments' => $request->additional_comments,
            'completed_by' => $request->completed_by,
            'employer_position' => $request->employer_position,
            'submission_date' => $request->submission_date,
            'personal_ref_id' => $request->personal_ref_id
        ]);

        Mail::to($request->manager_email)->send(new HistoryConfirmedMail($history));

        echo "Thank you for your time<br>";

        echo "<a href='/'>click here</a> to redirect..."; //asjdhshj
    }
}
