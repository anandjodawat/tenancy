<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;
use App\Models\Application\ApplicationPet;
use App\Models\Application\ApplicationVehicle;
use App\Models\Application\ApplicationOccupant;
use App\Models\Application\ApplicationChildren;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'hello';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // Property Details
            'applied_for' => 'required',
            'state' => 'required',
            'suburb' => 'required',
            'post_code' => 'required',
            'property_type' => 'required',
            'number_of_bedrooms' => 'required',
            'commencement_date' => 'required',
            'length_of_lease' => 'required',
            'weekly_rent' => 'required',
            'monthly_rent' => 'required',
            'bond' => 'required',
            'bond_assistance' => 'required',
            'property_found_in' => 'required',

            // Property Manager Details
            'agency_name' => 'required',
            'property_manager_name' => 'required',
            'property_manager_email' => 'required',

            // Occupancy Details
            'number_of_occupant' => 'required',
            'number_of_children' => 'required',
            'number_of_vehicles' => 'required',
            'number_of_pets' => 'required',

            // Agent Specific
            'is_tenant_terminated' => 'required',
            'tenant_terminated_details' => 'required_if:is_tenant_terminated,yes',
            'is_tenant_refused' => 'required',
            'tenant_refused_details' => 'required_if:is_tenant_refused,yes',
            'is_on_debt' => 'required',
            'on_debt_details' => 'required_if:is_on_debt,yes',
            'is_deduction_rental_bond' => 'required',
            'deduction_rental_bond_details' => 'required_if:is_deduction_rental_bond,yes',
            'affect_future_rental_payment' => 'required',
            'affect_future_rental_payment_details' => 'required_if:affect_future_rental_payment,yes',
            'is_another_pending_application' => 'required',
            'own_a_property' => 'required',
            'own_a_property_details' => 'required_if:own_a_property,yes',
            'buy_another_property' => 'required',
            'intend_to_buy_after' => 'required_if:buy_another_property,yes',

            // Moving Service
            'need_moving_service' => 'required',
            'moving_service_agreement' => 'required',

            // Property Declaration
            'inspected_this_property' => 'required',
            'inspection_date' => 'required_if:inspected_this_property,yes',
            'good_condition' => 'required_if:inspected_this_property,yes',
            'inspection_code' => 'required_if:inspected_this_property,yes',

            // Licence and aggreement
            'licence_and_agreement' => 'required',
        ]);

        $application = TenantApplication::create([
            // Property Details
            'applied_for' => $request->applied_for,
            'state' => $request->state,
            'suburb' => $request->suburb,
            'post_code' => $request->post_code,
            'property_type' => $request->property_type,
            'number_of_bedrooms' => $request->number_of_bedrooms,
            'commencement_date' => $request->commencement_date,
            'length_of_lease' => $request->length_of_lease,
            'weekly_rent' => $request->weekly_rent,
            'monthly_rent' => $request->monthly_rent,
            'bond' => $request->bond,
            'bond_assistance' => $request->bond_assistance,
            'property_found_in' => $request->property_found_in,

            // Property Manager Details
            'agency_name' => $request->agency_name,
            'property_manager_name' => $request->property_manager_name,
            'property_manager_email' => $request->property_manager_email,

            // Occupancy Details
            'number_of_occupant' => $request->number_of_occupant,
            'number_of_children' => $request->number_of_children,
            'number_of_vehicles' => $request->number_of_vehicles,
            'number_of_pets' => $request->number_of_pets,

            // Agent Specific
            'is_tenant_terminated' => $request->is_tenant_terminated,
            'tenant_terminated_details' => $request->tenant_terminated_details,
            'is_tenant_refused' => $request->is_tenant_refused,
            'tenant_refused_details' => $request->tenant_refused_details,
            'is_on_debt' => $request->is_on_debt,
            'on_debt_details' => $request->on_debt_details,
            'is_deduction_rental_bond' => $request->is_deduction_rental_bond,
            'deduction_rental_bond_details' => $request->deduction_rental_bond_details,
            'affect_future_rental_payment' => $request->affect_future_rental_payment,
            'affect_future_rental_payment_details' => $request->affect_future_rental_payment_details,
            'is_another_pending_application' => $request->is_another_pending_application,
            'own_a_property' => $request->own_a_property,
            'own_a_property_details' => $request->own_a_property_details,
            'buy_another_property' => $request->buy_another_property,
            'intend_to_buy_after' => $request->intend_to_buy_after,

            // Moving Service
            'need_moving_service' => $request->need_moving_service,
            'moving_service_agreement' => $request->moving_service_agreement,

            // Property Declaration
            'inspected_this_property' => $request->inspected_this_property,
            'inspection_date' => $request->inspection_date,
            'good_condition' => $request->good_condition, 
            'inspection_code' => $request->inspection_code,

            // Licence and aggreement
            'licence_and_agreement' => $request->licence_and_agreement,
            'user_id' => auth()->user()->id
        ]);

        $this->save_application_occupant($application->id);
        $this->save_application_children($application->id);
        $this->save_application_pet($application->id);
        $this->save_application_vehicle($application->id);

        $application->update([
            'status' => '1'
        ]);

        flash('Your application has beed successfully submitted')->successs();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function save_application_occupant($application_id)
    {
        for ($i=0; $i < request('number_of_occupant'); $i++) {
            ApplicationOccupant::create([
                'occupant_name' => request('occupant_name')[$i],
                'occupant_age' => request('occupant_age')[$i],
                'occupant_relationship' => request('occupant_relationship')[$i],
                'application_id' => $application_id
            ]);
        }
    }

    private function save_application_children($application_id)
    {
        for ($i=0; $i < request('number_of_children'); $i++) {
            ApplicationChildren::create([
                'children_age' => request('children_age')[$i],
                'application_id' => $application_id
            ]);
        }
    }

    private function save_application_pet($application_id)
    {
        for ($i=0; $i < request('number_of_pets'); $i++) {
            ApplicationPet::create([
                'application_id' => $application_id,
                'pet_type' => request('pet_type')[$i],
                'pet_breed' => request('pet_breed')[$i]
            ]);
        }
    }

    private function save_application_vehicle($application_id)
    {
        for ($i=0; $i < request('number_of_vehicles'); $i++) {
            ApplicationVehicle::create([
                'application_id' => $application_id,
                'vehicle_type' => request('vehicle_type')[$i],
                'vehicle_registration' => request('vehicle_registration')[$i],
                'make_model' => request('make_model')[$i]
            ]);
        }
    }
}
