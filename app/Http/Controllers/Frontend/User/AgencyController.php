<?php

namespace App\Http\Controllers\Frontend\User;

use Auth;
use App\Models\Agency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'index';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->agency_id) {
            return redirect(route('frontend.user.agency-details.edit', Auth::user()->agency_id));
        } 
        return view('frontend.agency.details.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:agencies',
            'agency_id' => 'required|unique:agencies',
            'registration_number' => 'required',
            'email' => 'required|unique:agencies',
            'suburb' => 'required',
            'state' => 'required',
            'post_code' => 'required'
        ]);

        $agency = Agency::create([
            'name' => $request->name,
            'agency_id' => $request->agency_id,
            'user_id' => auth()->user()->id,
            'address' => $request->address,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'registration_number' => $request->registration_number,
            'website' => $request->website,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code
        ]);

        Auth::user()->update([
            'agency_id' => $agency->id
        ]);

        return redirect('/')->withFlashSuccess('Agency Details Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agency = Agency::find($id);
        return view('frontend.agency.details.edit', compact('agency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
