<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantProfile;
use App\Traits\SaveTenantProfileTrait;
use App\Models\Tenant\TenantApplication;
use App\Models\Tenant\MaintenanceRequest;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    use SaveTenantProfileTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	if (auth()->user()->hasRole('agency-admin')) {
            return redirect()->route('admin.dashboard');
        }

        $profile = TenantProfile::whereUserId(auth()->user()->id)
        ->first();

        if (!$profile) {
            $profile = TenantProfile::create([
                'user_id' => auth()->user()->id,
                'email' => auth()->user()->email
            ]);
            $this->save_tenant_profile();
        } else {
            $profile = TenantProfile::whereUserId(auth()->user()->id)->first();
        }

        $totalMaintenanceRequest = MaintenanceRequest::where('user_id', auth()->user()->id)->latest()->count();
 
        $draft = TenantApplication::whereUserId(auth()->user()->id)->whereDraft('1')->count();
        $sent = TenantApplication::whereDraft('0')->whereUserId(auth()->user()->id)->count();
        $approved = TenantApplication::whereUserId(auth()->user()->id)->whereStatus('3')->count();
        $accepted = TenantApplication::whereUserId(auth()->user()->id)->whereStatus('2')->count();
        return view('frontend.user.dashboard', compact(
            'profile',
            'draft',
            'sent',
            'approved',
            'accepted',
            'totalMaintenanceRequest'
        ));
    }
}
