<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (auth()->user()) {
            return redirect()->route('frontend.user.dashboard');
        }
        return view('frontend.index');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        return view('frontend.macros');
    }

    public function with_agency($agency)
    {
        return redirect()->route('user-dashboard.applications.create', ['agency_name' => $agency]);
    }
}
