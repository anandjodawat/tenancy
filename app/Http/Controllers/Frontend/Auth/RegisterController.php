<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Models\Agency;
use App\Http\Controllers\Controller;
use App\Models\Agency\SupportingDocument;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        // Where to redirect users after registering
        $this->redirectTo = route(homeRoute());

        $this->user = $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)//, Consultancy $consultancy
    {  
        if ($request->login_as == 'agency-admin') {
            $this->validate($request, [
                'name' => 'required|unique:agencies',
                'agency_id' => 'required|unique:agencies',
                //'agency_id' => 'unique:agencies',
                'address' => 'required',
                'phone_number' => 'required|numeric',
                'email' => 'required',
                //'registration_number' => 'required|unique:agencies',
                'suburb' => 'required',
                'state' => 'required',
                'post_code' => 'required'
            ]);
        }
        if (config('access.users.confirm_email') || config('access.users.requires_approval')) { 
            $user = $this->user->create($request->only('first_name', 'last_name', 'email', 'password', 'login_as'));

            if ($request->login_as == 'agency-admin') { 
                $agency = Agency::create([
                    'name' => $request->name,
                    'agency_id' => $request->agency_id,
                    'user_id' => $user->id,
                    'address' => $request->address,
                    'phone_number' => $request->phone_number,
                    'email' => $user->email,
                    'registration_number' => $request->registration_number,
                    'website' => $request->website,
                    'state' => $request->state,
                    'post_code' => $request->post_code,
                    'suburb' => $request->suburb
                ]);

                $user->update([
                    'agency_id' => $agency->id,
                ]);

                foreach (SupportingDocument::DOCUMENTNAME as $key => $value) {
                    SupportingDocument::create([
                        'agency_id' => $agency->id,
                        'supporting_document' => config('tenancy-application.document_description')[$key],
                        'document_id' => $key
                    ]);
                }

                if ($request->file('logo')) {
                    $agency->addMedia($request->file('logo'))->toMediaCollection('agency_logos', 'media');
                }
                                

            }
            event(new UserRegistered($user));

            flash('User registration successful. Please verify your email address.')->success();
            return redirect()->back();

            // return redirect($this->redirectPath())->withFlashSuccess(
            //     config('access.users.requires_approval') ?
            //         trans('exceptions.frontend.auth.confirmation.created_pending') :
            //         trans('exceptions.frontend.auth.confirmation.created_confirm')
            // );
        } else {
            access()->login($this->user->create($request->only('first_name', 'last_name', 'email', 'password')));
            event(new UserRegistered(access()->user()));

            return redirect($this->redirectPath());
        }
    }
}
