<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\Frontend\Contact\SendContact;
use App\Http\Requests\Frontend\Contact\SendContactRequest;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.contact');
    }

    /**
     * @param SendContactRequest $request
     *
     * @return mixed
     */
    public function send(SendContactRequest $request)
    {

        return redirect()->back()->withFlashSuccess(trans('alerts.frontend.contact.sent'));
    }

    public function store(Request $request)
    {
        // $contact = Contact::create([
        //     'first_name' => $request->first_name,
        //     'last_name' => $request->last_name,
        //     'email' => $request->email,
        //     'subject' => $request->subject,
        //     'message' => $request->message
        // ]);
        
        if (env('APP_ENV') == 'production') {
            $this->validate($request, [
                'g-recaptcha-response' => 'required'
            ]);
        }
        
        $contact = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message
        ];
        if (env('APP_ENV') == 'production') {
            $email = 'abraham@tenancyapplication.com.au';
        } else {
            $email = 'xsarozzz@gmail.com';
        }
        Mail::to($email)->send(new ContactMail($contact));

        flash('You message has been sent.')->info();
        return redirect()->back();
    }
}
