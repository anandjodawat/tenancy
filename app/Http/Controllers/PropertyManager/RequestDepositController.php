<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Models\RequestDeposit;
use App\Mail\RequestDepositMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Agency\Property;

class RequestDepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = RequestDeposit::whereSentBy(auth()->user()->id)
            ->latest()
            ->get();

        return view('property-manager.request-deposit.index', compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('property-manager.request-deposit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tenant_code' => 'required',
            'name'=> 'required',
            'address'=> 'required',
            'payment_amount'=> 'required|numeric',
            'send_to'=> 'required',
            'subject'=> 'required',
            'body'=> 'required',
        ]);

        $requestDeposit = RequestDeposit::create([
            'tenant_code' => $request->tenant_code,
            'name' => $request->name,
            'address' => $request->address,
            'contact_number' => $request->contact_number,
            'payment_amount' => $request->payment_amount,
            'send_to' => $request->send_to,
            'subject' => $request->subject,
            'body' => $request->body,
            'sent_by' => auth()->user()->id
        ]);

        $file = "";

        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
        }

        //$ee = new RequestDepositMail($requestDeposit->id, $file);
        //$eeee = $ee->build();
        //echo "<pre>"; print_r($ee);
        //echo "<pre>"; print_r($eeee); die;

        Mail::to($requestDeposit->send_to)->send(new RequestDepositMail($requestDeposit->id, $file));

        flash('Request Deposit email sent successfully.')->success();

        return redirect()->back();
    }


    public function autocomplete(Request $request){

        $action = $request->get('action');

        if($action=='getName'){

            $term = $request->get('term');
            $results = array();

            $queries = User::where('first_name', 'LIKE', '%'. $term. '%')
                            ->orWhere('last_name', 'LIKE', '%'.$term.'%')
                            ->take(10)
                            ->get();
            foreach ($queries as $query)
            {
                $results[] = [ 'name' => $query->name ];
            }
            return response()->json($results);

        } elseif ($action=='getAddress') {

            $term = $request->get('term');
            $results = array();

            $queries = Property::where('address', 'LIKE', '%'. $term. '%')
                            ->orWhere('suburb', 'LIKE', '%'.$term.'%')
                            ->orWhere('state', 'LIKE', '%'.$term.'%')
                            ->orWhere('post_code', 'LIKE', '%'.$term.'%')
                            ->take(10)
                            ->get();
            foreach ($queries as $query)
            {
                $results[] = [ 'fulladdress' => $query->address.', '.$query->suburb.' '.$query->state.' '.$query->post_code ];
            }

            return response()->json($results);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
