<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;

class DashboardController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole('agency-admin')) {
            $inbox = TenantApplication::applicationForAgency()
            ->count();

            $accepted = TenantApplication::applicationForAgency()
            ->whereStatus(2)
            ->count();

            $approved = TenantApplication::applicationForAgency()
            ->whereStatus(3)
            ->count();
        } else {
            $inbox = TenantApplication::applicationForPropertyManager()->count();

            $accepted = TenantApplication::applicationForPropertyManager()
            ->whereStatus(2)
            ->count();

            $approved = TenantApplication::applicationForPropertyManager()
            ->whereStatus(3)
            ->count();
        }

        return view('property-manager.dashboard', compact(
          'inbox',
          'accepted',
          'approved'
      ));
    }
}
