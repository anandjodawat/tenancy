<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Mail\BondRequestMail;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;
use Illuminate\Support\Facades\Mail;
use App\Models\BondRequest;
use GuzzleHttp\Client;

class BondsureRequestController extends Controller
{

    public function __construct()
      {
        
      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$applications = TenantApplication::applicationForAgency()
            ->applicationForPropertyManager()
            ->filterByStatus(request('status'))
            ->search(request('search'))
            ->where('bond_assistance', 'yes')
            ->get();*/

        $applications = BondRequest::latest()->whereSentBy(auth()->user()->id)->get();
            
        return view('property-manager.bondsure.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('property-manager.bondsure.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tenant_code' => 'required',
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
            'payment_amount'=> 'required|numeric',
            'mobile_number' => 'required|numeric',
            'email' => 'required',
            'subject' => 'required',
            'body' => 'required'
        ]);


        $ongoingPayment = BondRequest::create([
            'tenant_code' => $request->tenant_code,
            'title' => $request->title,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'address' => $request->address,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'postcode' => $request->postcode,
            'payment_amount' => $request->payment_amount,
            'mobile_number' => $request->mobile_number,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'subject' => $request->subject,
            'body' => $request->body,
            'sent_by' => auth()->user()->id,
            'finance_period' => $request->finance_period
        ]);


        //** EASY BOND PAY SOAP CALL **//
        $efund_contract_link = 'https://efund.com.au/Application/UpdateDetails.aspx?paynow&ebp=1';
        $xml_post_string =  '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body><CreateeasyBondpayDocuments xmlns="http://efund.com.au/express/"><easyBondpayExpress>
            <ClientRequestID>'.$ongoingPayment->id.'</ClientRequestID>
            <BrokerLoginID>Zenith</BrokerLoginID>
            <Password>Test</Password>
            <IssueOption>2</IssueOption>
            <IssueCopy>2</IssueCopy>
            <ResidentialProperty>'.$request->address.' '.$request->suburb.' '.$request->state.'</ResidentialProperty>
            <Postcode>'.$request->postcode.'</Postcode>
            <ApplicantName>'.config('tenancy-application.title')[$request->title].' '.$request->first_name.' '.$request->last_name.'</ApplicantName>
            <ClientCode>'.$request->tenant_code.'</ClientCode>
            <ClientPhone>'.$request->mobile_number.'</ClientPhone>
            <ClientFax>?</ClientFax>
            <ClientMobile>'.$request->mobile_number.'</ClientMobile>
            <ClientEmail>'.$request->email.'</ClientEmail>
            <PropertyManager>'.auth()->user()->first_name.' '.auth()->user()->last_name.'</PropertyManager>
            <ContactName>'.auth()->user()->first_name.' '.auth()->user()->last_name.'</ContactName>
            <BondAmount>'.number_format((float)$request->payment_amount, 2, '.', '').'</BondAmount>
            <RentAdvance>0.00</RentAdvance>
            <FinancePeriod>'.$request->finance_period.'</FinancePeriod>
            <BrokerURL>https://tenancyapplication.com.au</BrokerURL>
            <BrokerExitURL>https://tenancyapplication.com.au</BrokerExitURL>
            <PMAgencyID>?</PMAgencyID>
            <PMUserID>?</PMUserID>
         </easyBondpayExpress></CreateeasyBondpayDocuments>
  </soap:Body>
</soap:Envelope>';

        $client = new Client();

        try {

            $response = $client->request('POST', 'https://efund.com.au/expresstest/Service.asmx?WSDL',
                [
                    'body'    => $xml_post_string,
                    'headers' => [
                        'Content-Type' => 'text/xml; charset=UTF8',
                        "Accept" => "text/xml",
                        "Cache-Control" => "no-cache",
                        "Pragma" => "no-cache",
                        "SOAPAction" => "http://efund.com.au/express/CreateeasyBondpayDocuments",
                        "Content-length" => strlen($xml_post_string),
                    ]
                ]
            );


            if ($response->getStatusCode() === 200) {
                // Success!
                $response_data = $response->getBody()->getContents();

                $res_xml = simplexml_load_string($response_data);
                //dd($xml);
                $res_xml->registerXPathNamespace('ef', 'http://efund.com.au/express/');
                $res_elements = $res_xml->xpath('//soap:Envelope/soap:Body/ef:CreateeasyBondpayDocumentsResponse/ef:CreateeasyBondpayDocumentsResult');
                //echo '<pre>';print_r($res_elements);
                
                foreach ($res_elements as $res_element) {
                    $efund_contract_link = $res_element->eFundLink;
                    $efund_contract_id = $res_element->ContractID;
                }

                $ongoingPayment->update([
                    'efund_contract_link' => $efund_contract_link,
                    'efund_contract_id' => $efund_contract_id
                ]);

            } else {
                echo 'Response Failure !!!';
            }

        } catch (Exception $e) {
            echo 'Exception:' . $e->getMessage();
        }
        //** EASY BOND PAY SOAP CALL **//


        $file = "";

        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
        }

        Mail::to($ongoingPayment->email)->send(new BondRequestMail($file, $ongoingPayment->id, $efund_contract_link));

        flash('Bond Request sent successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
