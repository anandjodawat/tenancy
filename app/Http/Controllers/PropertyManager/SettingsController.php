<?php

namespace App\Http\Controllers\PropertyManager;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::where('agency_id', auth()->user()->agency_id)
                    ->Where('is_active', 'yes')
                    ->first();

        $setting_dropdown = Setting::where('agency_id', auth()->user()->agency_id)
                    //->Where('is_active', '!=' , 'yes')
                    ->orderBy('key', 'asc')
                    ->get();

        return view('property-manager.settings.index', compact('setting', 'setting_dropdown'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'value' => 'required',
            'connection_name'=> 'required',
        ]);

        $agency_id = auth()->user()->agency_id;
        $agency = auth()->user()->agency_profile();

        $setting_check = Setting::where('agency_id', $agency_id)
                         ->Where('key', $request->key)
                         ->first();

        if ($setting_check && $setting_check->count() > 0) {

            // for utility connection logo
            $prov_logo = '';
            if ($request->file('file')) {
                $collection = $agency_id.'-'.$request->key;
                $prov_logo = $request->file('file')->getClientOriginalName();
                if ($agency->getFirstMedia($collection)) {
                    $media_id = $agency->getFirstMedia($collection)->id;
                    $agency->deleteMedia($media_id);
                }
                $agency->addMedia($request->file('file'))
                        //->usingFileName($prov_logo)
                        ->toMediaCollection($collection, 'media');
                $setting_check->update([
                    'provider_logo' => $prov_logo
                ]); 
            }
            $setting_check->update([
                'key' => $request->key,
                'value' => $request->key == '0' ? null : $request->value,
                'agency_id' => $agency_id,
                'contact_number' => $request->key == '0' ? null : $request->contact_number,
                'body' => $request->body,
                'connection_name' => $request->key == '0' ? null : $request->connection_name,
                'is_active' => 'yes'
            ]);   

            $succ_mesg = 'Moving Service Setting Updated Successfully.';            
                           
        } else { 

            // for utility connection logo
            $prov_logo = '';
            if ($request->file('file')) {
                $collection = $agency_id.'-'.$request->key;
                /*if ($agency->getFirstMedia($request->name)) {
                    $media_id = $agency->getFirstMedia($request->name)->id;
                    $agency->deleteMedia($media_id);
                }*/
                $prov_logo = $request->file('file')->getClientOriginalName();
                $agency->addMedia($request->file('file'))
                        //->usingFileName($prov_logo)
                        ->toMediaCollection($collection, 'media');
            }

            Setting::create([
                'key' => $request->key,
                'value' => $request->key == '0' ? null : $request->value,
                'agency_id' => $agency_id,
                'contact_number' => $request->key == '0' ? null : $request->contact_number,
                'body' => $request->body,
                'connection_name' => $request->key == '0' ? null : $request->connection_name,
                'is_active' => 'yes',
                'provider_logo' => $prov_logo
            ]);

            $succ_mesg = 'Moving Service Setting Added Successfully.';
        }

        $setting_check2 = Setting::where('agency_id', $agency_id)
                             ->Where('key', '!=', $request->key)
                             ->get();

        if($setting_check2->count() > 0){
            foreach ($setting_check2 as $sv) {

                Setting::where('id', $sv->id)
                         ->update(['is_active' => 'no']);
            }  
        }       

        if ($request->key == '0' && $agency->getFirstMedia('utility_connection')) {
            $media_id = $agency->getFirstMedia('utility_connection')->id;
            $agency->deleteMedia($media_id);
        }

        flash($succ_mesg)->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getservices(Request $request)
    {
        $action = $request->get('action');

        if($action=='getSettings'){

            $key = $request->get('term');
            $results = array();
            
            $queries = Setting::where('agency_id', auth()->user()->agency_id)
                         ->Where('key', $key)
                         ->first();

            $provider_logo='';
            if($queries->provider_logo!=''){
                $provider_logo = auth()->user()->agency_profile()->getFirstMedia(auth()->user()->agency_id.'-'.$queries->key)->getUrl();
            }             
            $results = array('connection_name' => $queries->connection_name, 'value' => $queries->value, 'contact_number' => $queries->contact_number, 'body'=>$queries->body, 'provider_logo' => $provider_logo);

            return response()->json($results);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
