<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Models\DepositSale;
use App\Mail\DepositSalesMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class DepositSalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = DepositSale::latest()
            ->get();

        return view('property-manager.deposit-sales.index', compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('property-manager.deposit-sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tenant_code' => 'required',
            'name'=> 'required',
            'address'=> 'required',
            'payment_amount'=> 'required|numeric',
            'send_to'=> 'required',
            'subject'=> 'required',
            'body'=> 'required',
        ]);

        $depositSales = DepositSales::create([
            'tenant_code' => $request->tenant_code,
            'name' => $request->name,
            'address' => $request->address,
            'contact_number' => $request->contact_number,
            'payment_amount' => $request->payment_amount,
            'send_to' => $request->send_to,
            'subject' => $request->subject,
            'body' => $request->body,
            'sent_by' => auth()->user()->id
        ]);
        
        Mail::to($depositSales->send_to)->send(new DepositSalesMail($depositSales->id));

        flash('Request Deposit email sent successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
