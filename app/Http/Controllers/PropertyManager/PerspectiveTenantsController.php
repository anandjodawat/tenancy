<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Models\PropertyInvitation;
use App\Models\Tenant\UploadTenant;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;

class PerspectiveTenantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!request('tenants') || request('tenants') == 'system-tenants') {
            $tenants = $this->get_system_tenants();
        }

        if (request('tenants') == 'uploaded-tenants') {
            $tenants = $this->get_uploaded_tenants();
        }

        if (request('tenants') == 'invited-tenants') {
            $tenants = $this->get_invited_tenants();
        }

        return view('property-manager.prospective-tenant.index', compact('tenants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_system_tenants()
    {
         if (auth()->user()->hasRole('agency-admin')) {
            $tenants =  TenantApplication::whereAgencyName(auth()->user()->agency->name)
                ->select('user_id')
                ->groupBy('user_id')
                ->get();
                // ->paginate(10); 
        } else {
            $tenants = TenantApplication::wherePropertyManagerEmail(auth()->user()->email)
                ->select('user_id')
                ->groupBy('user_id')
                ->get();
                // ->paginate(10);
        }

        return $tenants;
    }

    public function get_uploaded_tenants()
    {
        return UploadTenant::whereAgencyId(auth()->user()->agency_id)->select('name', 'email', 'created_at')->latest()->get();
    }

    public function get_invited_tenants()
    {
        if (auth()->user()->hasRole('agency-admin')) {
            return PropertyInvitation::whereAgencyId(auth()->user()->agency_id)->select('tenant_email')->groupBy('tenant_email')->get();
        }
        return PropertyInvitation::whereSentBy(auth()->user()->id)->select('tenant_email')->groupBy('tenant_email')->get();
    }
}
