<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Mail\OngoingRequestMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\RequestOngoingPayment;

class OngoingPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = RequestOngoingPayment::latest()->whereSentBy(auth()->user()->id)->get();
        return view('property-manager.ongoing-payment.index', compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('property-manager.ongoing-payment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'tenant_code' => 'required',
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
            'mobile_number' => 'required|numeric',
            'phone_number' => 'sometimes|nullable|numeric',
            'email' => 'required',
            'subject' => 'required',
            'body' => 'required'
        ]);

        $ongoingPayment = RequestOngoingPayment::create([
            'tenant_code' => $request->tenant_code,
            'title' => $request->title,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'address' => $request->address,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'postcode' => $request->postcode,
            'mobile_number' => $request->mobile_number,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'subject' => $request->subject,
            'body' => $request->body,
            'sent_by' => auth()->user()->id,
        ]);

        $file = "";

        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
        }

        Mail::to($ongoingPayment->email)->send(new OngoingRequestMail($file, $ongoingPayment->id));

        flash('Ongoing payment request sent successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
