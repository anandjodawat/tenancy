<?php

namespace App\Http\Controllers\PropertyManager;

use Excel;
use File;
use Illuminate\Http\Request;
use App\Models\Agency\Property;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::latest()
        ->whereAgencyId(auth()->user()->agency_id)
        ->search(request('search'))
        ->get();

        return view('property-manager.property.index', compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('property-manager.property.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->type && $request->type == 'excel') {
            if (session()->has('data') && session()->has('excel_fields')) {
                $address = session('excel_fields')[$request->address];
                $suburb = session('excel_fields')[$request->suburb];
                $state = session('excel_fields')[$request->state];
                $post_code = session('excel_fields')[$request->post_code];
                $rent_amount = session('excel_fields')[$request->rent_amount];

                foreach (session('data') as $key => $row) {
                    $found_state = array_search(strtoupper($row->$state), config('tenancy-application.states'));
                    if (strtoupper($row->state) == 'NSW') {
                        $found_state = "0";
                    } else {
                        $found_state = $found_state !== false ? $found_state : null;
                    }
                    Property::create([
                        'address' => $row->$address,
                        'suburb' => $row->$suburb,
                        'state' =>  $found_state,
                        'post_code' => $row->$post_code,
                        'rent_amount' => $row->$rent_amount,
                        'created_by' => auth()->user()->id,
                        'agency_id' => auth()->user()->agency_id
                    ]);
                }

                flash('Properties successfully added from excel.')->success();
            } else {
                flash('Sorry! Something went wrong.')->error();
            }
            session()->forget(['data', 'excel_fields']);
            return redirect()->route('property-manager.properties.index');
        }
        $this->validate($request, [
            'address' => 'required',
            'suburb' => 'required',
            'rent_amount' => 'numeric',
            'post_code' => 'required',
            'state' => 'required'
        ]);

        Property::create([
            'address' => $request->address,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code,
            'rent_amount' => $request->rent_amount,
            'created_by' => auth()->user()->id,
            'agency_id' => auth()->user()->agency_id
        ]);

        flash('New Property Added Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = Property::find($id);
        return view('property-manager.property.edit', compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $property = Property::find($id);

        $this->validate($request, [
            'address' => 'required',
            'suburb' => 'required',
            'rent_amount' => 'numeric',
            'post_code' => 'required',
            'state' => 'required'
        ]);

        $property->update([
            'address' => $request->address,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code,
            'rent_amount' => $request->rent_amount,
        ]);

        flash('Property Updated Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Property::find($id)->delete();

        return response()->json([], 200);
    }

    public function delete_property()
    {
        Property::find(request('id'))->delete();
        return response()->json([], 200);
    }

    public function parse(Request $request)
    {
        $path = $request->file('file')->getRealPath();
        $extension = File::extension($request->file->getClientOriginalName());
        if ($extension == "xlsx" || $extension == "xls") {
            $data = Excel::load($path, function($reader) {})->get();
            $excel_fields = array_keys($data[0]->toArray());
        } else {
            flash('File extension is not valid.')->error();
            return redirect()->back();
        }

        session()->put('data', $data);
        session()->put('excel_fields', $excel_fields);

        return view('property-manager.property.match-fields', compact('excel_fields'));
    }
}
