<?php

namespace App\Http\Controllers\PropertyManager;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Access\User\User;
use App\Models\Tenant\TenantProfile;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\ApplicationApprovedMail;
use App\Mail\ApplicationAcceptedMail;
use App\Models\Tenant\TenantApplication;
use App\Mail\Application\PropertyManager\ApplicationStatusChange;
use App\Models\Application\ApplicationMovingService;
use App\Models\Setting;
use App\Models\Agency;
use GuzzleHttp\Client;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Ht
     tp\Response
     */
    public function index()
    {
        $applications = TenantApplication::applicationForAgency()
            ->applicationForPropertyManager()
            ->filterByStatus(request('status'))
            ->search(request('search'))
            ->latest()
            //->withTrashed()
            ->get();

        return view('property-manager.application.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = TenantApplication::find($id);
        $user = User::find($application->user_id);
        $documents = TenantApplication::DOCUMENTNAME;

        $profile = DB::table('tenant_profiles')
            ->join('tenant_emergency_contacts', 'tenant_profiles.id', '=', 'tenant_emergency_contacts.profile_id')
            ->join('tenant_current_addresses', 'tenant_profiles.id', '=', 'tenant_current_addresses.profile_id')
            ->join('tenant_previous_addresses', 'tenant_profiles.id', '=', 'tenant_previous_addresses.profile_id')
            ->join('tenant_current_employments', 'tenant_profiles.id', '=', 'tenant_current_employments.profile_id')
            ->join('tenant_previous_employments', 'tenant_profiles.id', '=', 'tenant_previous_employments.profile_id')
            ->join('tenant_personal_references', 'tenant_profiles.id', '=', 'tenant_personal_references.profile_id')
            ->join('tenant_professional_references', 'tenant_profiles.id', '=', 'tenant_professional_references.profile_id')
            ->join('tenant_income_expenditures', 'tenant_profiles.id', '=', 'tenant_income_expenditures.profile_id')
            ->join('tenant_repayments', 'tenant_profiles.id', '=', 'tenant_repayments.profile_id')
            ->whereUserId($application->user_id)
            ->first();

        $connectionServices = ApplicationMovingService::Where('application_id', $id)->get();
        
        $allservices = '';
        if ( !empty($connectionServices) ) {
            
            foreach ( $connectionServices as $connectionService ) {
                
                $allservices .= ApplicationMovingService::MOVINGSERVICEOPTION[$connectionService->moving_service_id].', ';
            }
            $allservices = rtrim($allservices, ', ');
        }

        return view('property-manager.application.show', compact('application', 'user', 'documents','profile', 'allservices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $application = TenantApplication::find($request->application_id);

        $user = User::find($application->user_id);

        $application->update([
            'status' => $request->status
        ]);

        Mail::to($user->email)->send(new ApplicationStatusChange(strtoupper(TenantApplication::find($id)->status_name), $application->id));

        flash('Application status changed successfully.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TenantApplication::find($id)->delete();

        flash('Application moved to trash')->error();

        return response()->json([], 200);
    }

     public function delete()
    {
        TenantApplication::find(request('id'))->delete();

        flash('Application moved to trash')->error();

        return response()->json([], 200);
    }

    public function get_applications()
    {
        if (auth()->user()->hasRole('agency-admin')) {
             $applications = TenantApplication::whereDraft('0')
                ->whereAgencyName(auth()->user()->agency->name)
                ->select(['id', 'created_at', 'state', 'user_id', 'status']);
        } else {
            $applications = TenantApplication::whereDraft('0')
                ->wherePropertyManagerEmail(auth()->user()->email)
                ->select(['id', 'created_at', 'state', 'user_id', 'status']);
        }
        if (request('status')) {
            if (request('status') != 'inbox') {
                $index = array_search(request('status'), TenantApplication::APPLICATIONSTATUS);
                $applications = TenantApplication::whereStatus($index)
                    ->wherePropertyManagerEmail(auth()->user()->email)
                    ->select(['id', 'created_at', 'state', 'user_id', 'status']);
            }
        }

        return Datatables::of($applications)
            ->addColumn('status', function ($application) {
                return strtoupper($application->status_name);
            })->addColumn('state', function ($application) {
                return $application->state_name;
            })->addColumn('sent_by', function ($application) {
                return $application->user->name;
            })->addColumn('email', function ($application) {
                return $application->user->email;
            })->addColumn('action', function ($application) {
                return $application->action_button;
            })->with('user')->make(true);
    }

    public function change_status()
    {
        $application_id = request('application_id');

        $application = TenantApplication::find(request('application_id'));

        $user = User::find($application->user_id);

        $application->update([
            'status' => request('status')
        ]);

        $tenantApplication = TenantApplication::find(request('application_id'));


        //######SEND DATA TO MYCONNECT##########//
        $agency = Agency::whereName($application->agency_name)->first();
        $setting = Setting::whereAgencyId($agency->id)->Where('is_active', 'yes')->first();
        $utility_name = $setting->connection_name;

        $connectionServices = ApplicationMovingService::Where('application_id', $application_id)->get();
        
        $allservices = '';
        $servicesArr = array();
        if ( !empty($connectionServices) ) {
            
            foreach ( $connectionServices as $connectionService ) {
                
                $allservices .= ApplicationMovingService::MOVINGSERVICEOPTION[$connectionService->moving_service_id].';';

                $servicesArr[ strtolower(ApplicationMovingService::MOVINGSERVICEOPTION[$connectionService->moving_service_id]) ] = true;
            }
            $allservices = rtrim($allservices, ';');
            
        }
        

        //## MYCONNECT REST CALL ##//
        if ( (strtolower($utility_name) == 'my connect' || strtolower($utility_name) == 'myconnect') && $application->need_moving_service=='yes' && request('status')=='3' ) {

            $client = new Client();

            $secret = 'T7UEr43jYu9TgUA4L4PuQULf8zS7KaL96xQ8x546fBHzSLpLCYasyrhUmmyCwxDk';
            $agent_id = 'MYA0005307';

            $utctime = gmdate("Y-m-d:H");

            $secretkeyWithDateHrs = $secret.':'.$utctime;
            $secretkey = base64_encode(hash("sha256", $secretkeyWithDateHrs, True)); 

            $startdate_arr = explode('/', $application->commencement_date);
            $startdate   =  $startdate_arr[2].'-'.$startdate_arr[1].'-'.$startdate_arr[0];

            $res = $client->request('POST', 'https://test.services.myconnect.com.au/api/submit/1/', [
                'headers' => [
                    'Accept'     => 'application/json',
                    'Authorization'     => 'Bearer '.$secretkey,
                ],
                'json' => [
                    'Title_1' => strtoupper(config('tenancy-application.title')[$user->tenantProfile->title]), //optional
                    'FirstName_1' => $user->first_name,
                    'LastName_1' => $user->last_name,
                    'Phone_1' => $user->tenantProfile->mobile,
                    'EmailAddress_1' => $user->email,
                    
                    'Start_Date' => $startdate,
                    'Tenancy_Length' => $application->length_of_lease,

                    'Unit_Type' => config('tenancy-application.myconnect-property-type')[$application->property_type],
                    //'Unit_Number' => request('commencement_date'), //optional
                    'Street_Number' => 'NA',
                    'Street_Name' => 'NA',
                    'Street_Type' => 'STREET',
                    'Suburb' => $application->suburb,
                    'State' => config('tenancy-application.states')[$application->state],
                    'Postcode' => $application->post_code,

                    //'Building_Name' => '2012', //optional
                    'Address_Text' => $application->applied_for,
                    'Services_Requested' => $allservices,
                    'Connection_Type' => 'Tenant',
                    
                    'Agent_ID' => $agent_id,
                    'Agent_Name'=> $application->agency_name,
                    'Manager_Name'=> $application->property_manager_name,
                    'Record_ID'=>$application_id
                ]
            ]);
           
            if ($res->getStatusCode() == 200) { // 200 OK
                $response_data = $res->getBody()->getContents();
                $response_data = json_decode($response_data);
                //echo '<pre>';print_r($response_data);
                $application->update([
                    'moving_service_ref_no' => $response_data->reference,
                    'moving_service_name' => $utility_name
                ]);
            }
        }
        //######SEND DATA TO COMPARE AND CONNECT##########//
        //$possibleUtility = array('compare & connect', 'compare and connect' );
        if ( (strtolower($utility_name) == 'compare & connect' || strtolower($utility_name) == 'compare and connect') && $application->need_moving_service=='yes' && request('status')=='3' ) {

            $client = new Client();

            $api_token = '2CGJXtZpXgH6nhcP3H5nKOzITYqrTEIR'; //TEST
            //$api_token = 'HpGTASFoDUzmQjmD4tp932KsrgdXiF3s'; //PROD

            $startdate_arr = explode('/', $application->commencement_date);
            $startdate   =  $startdate_arr[2].'-'.$startdate_arr[1].'-'.$startdate_arr[0];

            $passData = [
                    'salutation' => config('tenancy-application.title')[$user->tenantProfile->title], //optional
                    'firstName' => $user->first_name,
                    'lastName' => $user->last_name,
                    'PhoneNumber' => $user->tenantProfile->mobile,
                    'email' => $user->email,
                    'dateOfBirth' => $user->tenantProfile->dob,
                    'accountType' => 'Residential',

                    /*'identificationType' => "Drivers License",
                    'driversLicense' => "38477272",
                    "driversLicenceType" => "Drivers License",
                    "licenceExpiry" => "2018-01-01",
                    "licenceState" => "NSW",*/

                    
                    /*"energy"  => false,
                    "gas"  => false,
                    "internet" => true,
                    "paytv" => true,*/
                    
                    //'unitNumber' => 'A', //optional
                    //'streetNumber' => '345',
                    //'streetName' => 'Canterburry',
                    //'streetType' => 'Street',
                    'suburb' => $application->suburb,
                    'state' => config('tenancy-application.states')[$application->state],
                    'postcode' => $application->post_code,
                    "salesType" => "Move-In",
                    "connectionDate" => $startdate,

                    //'agentId' => $agent_id,
                    'propertyManager'=> $application->agency_name,

                    ];
            $data = array_merge($passData, $servicesArr);   
            //print_r($data);exit;
            
            $res = $client->request('POST', 'https://test01.utilityworld.com.au/api/v3/api/customer/create?api_token='.$api_token, [
                'headers' => [
                    'Accept'     => 'application/json',
                ],
                'json' => $data
            ]);
           
            if ($res->getStatusCode() == 200) { // 200 OK
                $response_data = $res->getBody()->getContents();
                $response_data = json_decode($response_data);
                //echo '<pre>';print_r($response_data);
                $application->update([
                    'moving_service_ref_no' => $response_data->uniqueId,
                    'moving_service_name' => $utility_name
                ]);
            }
        }
        //exit;

        $agencyOwnerEmail = $tenantApplication->property_manager_email;

        if ($tenantApplication->status_name == 'accepted') {
            Mail::to($user->email)->send(new ApplicationAcceptedMail($tenantApplication->id));
        }

        if ($tenantApplication->status_name == 'approved') {
            Mail::to($user->email)->send(new ApplicationApprovedMail($tenantApplication->id));
        }

        Mail::to($user->email)->send(new ApplicationStatusChange(strtoupper($tenantApplication->status_name), request('application_id'), 'user'));
        Mail::to($agencyOwnerEmail)->send(new ApplicationStatusChange(strtoupper($tenantApplication->status_name), request('application_id'), 'agency'));

        flash('Application status changed successfully.');

        return redirect()->back();
    }

    public static function get_joint_appid($id)
    { 
            $application = TenantApplication::where('joint_app_id', $id)
                    ->select('id')
                    ->latest()
                    ->get();
                    
            return $application;                   
    }

    public static function get_joint_app_user_details($appid)
    { 

        $application = TenantApplication::find($appid);
        $user = User::find($application->user_id);
             
        return $user;                   
    }

}
