<?php

namespace App\Http\Controllers\PropertyManager;

use PDF;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;

// docusign
use DocuSign;
use App\API\TenancyDocuSign;
use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Api\EnvelopesApi\GetDocumentOptions;

class ApplicationPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lease_terms = auth()->user()->agency_profile() && auth()->user()->agency_profile()->leaseTerms ? auth()->user()->agency_profile()->leaseTerms->lease_terms : '';
        $supporting_documents = auth()->user()->agency_profile()->supportingDocument;
        $setting = Setting::where('agency_id', auth()->user()->agency_id)
                    ->Where('is_active', 'yes')
                    ->first();

        $image_url = auth()->user()->agency_logo();
        if($image_url){
         $image_size = null; //getimagesize(url($image_url));
           // $width =  $image_size[0];
           // $height = $image_size[1];
           // return $width;
     } else {
        $image_size = null;
            // return 'no image';
    }
        //return $image_url;
    $pdf = PDF::loadView('pdf', compact('lease_terms', 'supporting_documents', 'setting', 'image_size'));
    return $pdf->stream();
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document_names = TenantApplication::DOCUMENTNAME;

        $lease_terms = auth()->user()->agency_profile() && auth()->user()->agency_profile()->leaseTerms ? auth()->user()->agency_profile()->leaseTerms->lease_terms : '';
        $supporting_documents = auth()->user()->agency_profile()->supportingDocument;

        $application = TenantApplication::find($id);
        if ($application->envelope_id) {
            $docuSign = new TenancyDocuSign('user-dashboard.application.pdf', $id);
            $account_id = $docuSign->accountId();

            $envelopeOption = new EnvelopesApi($docuSign->apiClient());

            $envelopeStatus =  json_decode($envelopeOption->getEnvelope($account_id, $application->envelope_id));
            //if ($envelopeStatus->status == 'completed') { // We commented this condition to show the PDF without signed for Property Manager under Applications listing.
                if (!file_exists(public_path()."/pdf/my_document".$application->envelope_id.".pdf")) {

                    $options = new GetDocumentOptions();
                    $options->setCertificate(TRUE);

                    $document = $envelopeOption->getDocument($account_id, 1, $application->envelope_id, $options);
                    file_put_contents(public_path()."/pdf/my_document".$application->envelope_id.".pdf", file_get_contents($document->getPathname()));
                }
            //}
            $url = "/pdf/my_document".$application->envelope_id.".pdf";
            return view('property-manager.application.signed-pdf', compact('url'));
        }

        $setting = Setting::where('agency_id', auth()->user()->agency_id)
                    ->Where('is_active', 'yes')
                    ->first();

        $agency = auth()->user()->agency_profile();
        $pdf = PDF::loadView('property-manager.application.pdf', compact('lease_terms', 'supporting_documents', 'application', 'document_names', 'agency', 'setting'));
        if (request('action') == 'download') {
          // return $pdf->download();
          return $pdf->stream();
        } else {
          return $pdf->stream();
        }
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
