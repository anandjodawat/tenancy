<?php

namespace App\Http\Controllers\PropertyManager;

use File;
use Excel;
use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Mail\TenantRegistration;
use App\Models\Tenant\UploadTenant;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class UploadTenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tenants = UploadTenant::latest()
        ->whereAgencyId(auth()->user()->agency_id)
        ->Search(request('search'))
        ->SentMail()
        ->FailedMail()
        ->get();

        return view('property-manager.upload-tenant.index', compact('tenants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('property-manager.upload-tenant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->type && $request->type == 'excel') {
            if (session()->has('tenant_data') && session()->has('tenant_excel_fields')) {
                $name = $request->name ? session('tenant_excel_fields')[$request->name] : null;
                $lease_name = $request->lease_name ? session('tenant_excel_fields')[$request->lease_name] : null;
                $post_line1 = $request->post_line1 ? session('tenant_excel_fields')[$request->post_line1] : null;
                $post_line2 = $request->post_line2 ? session('tenant_excel_fields')[$request->post_line2] : null;
                $post_line3 = $request->post_line3 ? session('tenant_excel_fields')[$request->post_line3] : null;
                $suburb = $request->suburb ? session('tenant_excel_fields')[$request->suburb] : null;
                $state = $request->state ? session('tenant_excel_fields')[$request->state] : null;
                $property_suburb = $request->property_suburb ? session('tenant_excel_fields')[$request->property_suburb] : null;
                $post_code = $request->post_code ? session('tenant_excel_fields')[$request->post_code] : null;
                $email = $request->email ? session('tenant_excel_fields')[$request->email] : null;
                $mobile = $request->mobile ? session('tenant_excel_fields')[$request->mobile] : null;
                $property_address1 = $request->property_address1 ? session('tenant_excel_fields')[$request->property_address1] : null;
                $property_address2 = $request->property_address2 ? session('tenant_excel_fields')[$request->property_address2] : null;
                $property_address3 = $request->property_address3 ? session('tenant_excel_fields')[$request->property_address3] : null;
                $property_state = $request->property_state ? session('tenant_excel_fields')[$request->property_state] : null;
                $property_post_code = $request->property_post_code ? session('tenant_excel_fields')[$request->property_post_code] : null;
                $property_alpha = $request->property_alpha ? session('tenant_excel_fields')[$request->property_alpha] : null;
                $property_manager = $request->property_manager ? session('tenant_excel_fields')[$request->property_manager] : null;
                $property_manager_email = $request->property_manager_email ? session('tenant_excel_fields')[$request->property_manager_email] : null;
                $property_manager_mobile = $request->property_manager_mobile ? session('tenant_excel_fields')[$request->property_manager_mobile] : null;
                $property_manager_first_name = $request->property_manager_first_name ? session('tenant_excel_fields')[$request->property_manager_first_name] : null;
                $property_manager_last_name = $request->property_manager_last_name ? session('tenant_excel_fields')[$request->property_manager_last_name] : null;
                $rent_amount1 = $request->rent_amount1 ? session('tenant_excel_fields')[$request->rent_amount1] : null;
                $rent_per1 = $request->rent_per1 ? session('tenant_excel_fields')[$request->rent_per1] : null;
                $tenant_code = $request->tenant_code ? session('tenant_excel_fields')[$request->tenant_code] : null;

                foreach (session('tenant_data') as $key => $row) {
                    UploadTenant::create([
                       'name' =>$row->$name,
                       'lease_name' =>$row->$lease_name,
                       'post_line1' =>$row->$post_line1,
                       'post_line2' =>$row->$post_line2,
                       'post_line3' =>$row->$post_line3,
                       'suburb' =>$row->$suburb,
                       'state' =>$row->$state,
                       'property_suburb' => $row->$property_suburb,
                       'post_code' =>$row->$post_code,
                       'email' =>$row->$email,
                       'mobile' =>$row->$mobile,
                       'property_address1' =>$row->$property_address1,
                       'property_address2' =>$row->$property_address2,
                       'property_address3' =>$row->$property_address3,
                       'property_state' =>$row->$property_state,
                       'property_post_code' =>$row->$property_post_code,
                       'property_alpha' =>$row->$property_alpha,
                       'property_manager' =>$row->$property_manager,
                       'property_manager_email' =>$row->$property_manager_email,
                       'property_manager_mobile' =>$row->$property_manager_mobile,
                       'property_manager_first_name' =>$row->$property_manager_first_name,
                       'property_manager_last_name' =>$row->$property_manager_last_name,
                       'rent_amount1' =>$row->$rent_amount1,
                       'rent_per1' =>$row->$rent_per1,
                       'tenant_code' =>$row->$tenant_code,
                       'agency_id' => auth()->user()->agency_id 
                   ]);
                }

                flash('Properties successfully added from excel.')->success();
            } else {
                flash('Sorry! Something went wrong.')->error();
            }
            session()->forget(['tenant_data', 'tenant_excel_fields']);
            return redirect()->route('property-manager.upload-tenants.index');
        }
        $this->validate($request, [
            'email' => 'required'
        ]);
        $tenant = UploadTenant::create([
            'name' =>$request->name,
            'lease_name' =>$request->lease_name,
            'post_line1' =>$request->post_line1,
            'post_line2' =>$request->post_line2,
            'post_line3' =>$request->post_line3,
            'suburb' =>$request->suburb,
            'state' =>$request->state,
            'property_suburb' => $request->property_suburb,
            'post_code' =>$request->post_code,
            'email' =>$request->email,
            'mobile' =>$request->mobile,
            'property_address1' =>$request->property_address1,
            'property_address2' =>$request->property_address2,
            'property_address3' =>$request->property_address3,
            'property_state' =>$request->property_state,
            'property_post_code' =>$request->property_post_code,
            'property_alpha' =>$request->property_alpha,
            'property_manager' =>$request->property_manager,
            'property_manager_email' =>$request->property_manager_email,
            'property_manager_mobile' =>$request->property_manager_mobile,
            'property_manager_first_name' =>$request->property_manager_first_name,
            'property_manager_last_name' =>$request->property_manager_last_name,
            'rent_amount1' =>$request->rent_amount1,
            'rent_per1' =>$request->rent_per1,
            'tenant_code' =>$request->tenant_code,
            'agency_id' => auth()->user()->agency_id
        ]);

        flash('New Tenant Added Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tenant = UploadTenant::find($id);

        return view('property-manager.upload-tenant.show', compact('tenant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tenant = UploadTenant::find($id);
        return view('property-manager.upload-tenant.edit', compact('tenant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required'
        ]);
        $tenant = UploadTenant::find($id);

        $tenant->update([
            'name' =>$request->name,
            'lease_name' =>$request->lease_name,
            'post_line1' =>$request->post_line1,
            'post_line2' =>$request->post_line2,
            'post_line3' =>$request->post_line3,
            'suburb' =>$request->suburb,
            'state' =>$request->state,
            'property_suburb' => $request->property_suburb,
            'post_code' =>$request->post_code,
            'email' =>$request->email,
            'mobile' =>$request->mobile,
            'property_address1' =>$request->property_address1,
            'property_address2' =>$request->property_address2,
            'property_address3' =>$request->property_address3,
            'property_state' =>$request->property_state,
            'property_post_code' =>$request->property_post_code,
            'property_alpha' =>$request->property_alpha,
            'property_manager' =>$request->property_manager,
            'property_manager_email' =>$request->property_manager_email,
            'property_manager_mobile' =>$request->property_manager_mobile,
            'property_manager_first_name' =>$request->property_manager_first_name,
            'property_manager_last_name' =>$request->property_manager_last_name,
            'rent_amount1' =>$request->rent_amount1,
            'rent_per1' =>$request->rent_per1,
            'tenant_code' =>$request->tenant_code,
        ]);

        flash('Tenant Updated Successfully.')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UploadTenant::find($id)->delete();
        return response()->json([], 200);
    }
    public function delete_uploaded_tenant()
    {
        UploadTenant::find(request('id'))->delete();
        flash('Tenant has been successfully removed')->error();
        return response()->json([], 200);
    }

    public function delete_multiple_tenant(Request $request)
    {
        foreach ($request->users as $key => $id) {
            UploadTenant::find($id)->delete();
        }
        flash('Tenant has been successfully removed')->error();

        return response()->json($request->users, 200);
    }

    public function import(Request $request)
    {
        $path = $request->file('file')->getRealPath();
        $extension = File::extension($request->file->getClientOriginalName());
        if ($extension == "xlsx" || $extension == "xls") {
            $data = Excel::load($path, function($reader) {})->get();
            $excel_fields = array_keys($data[0]->toArray());
        } else {
            flash('File extension is not valid.')->error();
            return redirect()->back();
        }

        session()->put('tenant_data', $data);
        session()->put('tenant_excel_fields', $excel_fields);
        //echo '<pre>';print_r($data);exit;

        return view('property-manager.upload-tenant.match-fields', compact('excel_fields'));

        //validate the xls file
        // $this->validate($request, array(
        //     'file'      => 'required'
        // ));

        // if($request->hasFile('file')){
        //     $extension = File::extension($request->file->getClientOriginalName());
        //     if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

        //         $path = $request->file->getRealPath();
        //         $data = Excel::load($path, function($reader) {
        //         })->get();
        //         if(!empty($data) && $data->count()){

        //             foreach ($data as $key => $value) {
        //                 if (User::whereEmail($value->email)->count() < 1) {
        //                     $tenant = UploadTenant::create([
        //                         'name' =>$value->name,
        //                         'lease_name' =>$value->lease_name,
        //                         'post_line1' =>$value->postalline1,
        //                         'post_line2' =>$value->postalline2,
        //                         'post_line3' =>$value->postalline3,
        //                         'suburb' =>$value->tenantsuburb,
        //                         'state' =>$value->tenantstate,
        //                         'property_suburb' => $value->propsuburb,
        //                         'post_code' =>$value->tenantpcode,
        //                         'email' =>$value->email,
        //                         'mobile' =>$value->mobile,
        //                         'property_address1' =>$value->propadd1,
        //                         'property_address2' =>$value->propadd2,
        //                         'property_address3' =>$value->propadd3,
        //                         'property_state' =>$value->propstate,
        //                         'property_post_code' =>$value->proppcode,
        //                         'property_alpha' =>$value->propalpha,
        //                         'property_manager' =>$value->propmanager,
        //                         'property_manager_email' =>$value->propmanageremail,
        //                         'property_manager_mobile' =>$value->propmanagermobile,
        //                         'property_manager_first_name' =>$value->propmanfirstname,
        //                         'property_manager_last_name' =>$value->propmanlastname,
        //                         'rent_amount1' =>$value->rentamt1,
        //                         'rent_per1' =>$value->rentper1,
        //                         'tenant_code' =>$value->tentcode,
        //                         'agency_id' => auth()->user()->agency_id
        //                     ]);
        //                 }
        //             }
        //             flash('Tenant Uploaded Successfully.')->success();
        //         }

        //         return back();

        //     }else {
        //         flash('File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!')->error();
        //         return back();
        //     }
        // }
    }

    public function send_mail(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'template' => 'required'
        ]);

        if ($request->users == null) {
            flash('Please Select Tenants to Send Mail.')->error();
            return redirect()->back();
        }

        foreach ($request->users as $key => $user) {
            $tenant = UploadTenant::find($user);
            if ($tenant->email) {
                Mail::to($tenant->email)->send(new TenantRegistration($request->subject, $request->template, $tenant));
                $tenant->update([
                    'email_status' => 1
                ]);
            }
        }

        flash('Mail Sent Successfully.')->success();

        return redirect()->back();
    }

    public function download_excel()
    {    
        return response()->download(public_path().'/file/blank_excel.xls', 'sample_excel.xls');
    }

    // public function delete_uploaded_tenant(Request $request)
    // {
    //     foreach ($request->users as $key => $user) {
    //         UploadTenant::find($user)->delete();
    //     }
    //     flash('Uploaded Tenant Deleted Successfully.')->error();
    // }
}
