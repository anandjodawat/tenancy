<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Models\Agency\Property;
use App\Models\InvitationTenant;
use App\Models\PropertyInvitation;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\PropertyInvitationMail;

class PropertyInvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invitations = PropertyInvitation::whereAgencyId(auth()->user()->agency_id)
        ->latest()
        ->search(request('search'))
        ->get();

        return view('property-manager.property-invitation.index', compact('invitations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $properties = Property::whereAgencyId(auth()->user()->agency_id)->latest()->select('address', 'id', 'suburb', 'state', 'post_code')->get();
        return view('property-manager.property-invitation.create', compact('properties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emails = str_replace(' ', '', $request->tenant_emails);
        //echo $emails; die;
        $emails = explode(',', $emails);
        $this->validate( $request, [
            'tenant_emails' => 'required',
            'property_id' => 'required_if:others,!=,yes',
            'inspection_date' => 'required',
            'body' => 'required',

            'address_for_invite' => 'required_if:others,==,yes',
            'suburb' => 'required_if:others,==,yes',
            'state' => 'required_if:others,==,yes',
            'post_code' => 'required_if:others,==,yes',
            'rent_amount' => 'required_if:others,==,yes',
        ]);

        if ($request->others == 'yes') {
            $property_id = $this->save_property_information($request);
        }

        foreach ($emails as $key => $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $propertyInvitation = PropertyInvitation::create([
                    'tenant_email' => $email,
                    'property_id' => $request->others == 'yes' ? null : $request->property_id,
                    'inspection_date' => $request->inspection_date,
                    'sent_by' => auth()->user()->id,
                    'agency_id' => auth()->user()->agency_id,
                    'body' => $request->body,
                ]);

                if ($request->others == 'yes') {
                    $propertyInvitation->update([
                        'property_id' => $property_id
                    ]);
                }

                Mail::to($email)->send(new PropertyInvitationMail($propertyInvitation->id));
            }
        }

        flash('Property Invitation sent successfully to valid email addresses.')->success();

        return redirect()->back();
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PropertyInvitation::find($id)->delete();
        return response()->json([], 200);
    }
    public function delete()
    {
        PropertyInvitation::find(request('id'))->delete();
        flash("Property Invitation has been successfully deleted")->error();
        return response()->json([], 200);
    }
    private function save_property_information($request)
    {
        $property = Property::create([
            'address' => $request->address_for_invite,
            'suburb' => $request->suburb,
            'state' => $request->state,
            'post_code' => $request->post_code,
            'rent_amount' => $request->rent_amount,
            'created_by' => auth()->user()->id,
            'agency_id' => auth()->user()->agency_id
        ]);

        return $property->id;
    }
}
