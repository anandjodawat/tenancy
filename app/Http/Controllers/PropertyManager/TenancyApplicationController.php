<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantApplication;

class TenancyApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (auth()->user()->hasRole('agency-admin')) {
            $applications =  TenantApplication::whereAgencyName(auth()->user()->agency->name)
            ->select('id', 'user_id', 'applied_for', 'created_at')
                ->whereUserId(request('tenant_id'))
                ->get();
        } else {
            $applications = TenantApplication::wherePropertyManagerEmail(auth()->user()->email)
                ->select('id', 'user_id', 'applied_for', 'created_at')
                ->whereUserId(request('tenant_id'))
                ->get();
        }

        return view('property-manager.tenant.application.index', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
