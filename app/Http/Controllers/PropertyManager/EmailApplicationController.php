<?php

namespace App\Http\Controllers\PropertyManager;

use Illuminate\Http\Request;
use App\Mail\ForwardApplicationMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\HistoryConfirmationMail;
use App\Mail\ForwardToApplicationMail;
use App\Mail\EmploymentConfirmationMail;
use App\Models\Tenant\TenantApplication;
use App\Models\Application\EmailApplication;
use App\Models\Application\ApplicationForward;
use App\Mail\Application\PropertyManager\EmailApplicationMail;

class EmailApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request('mail') == 'employment-confirmation') {
            $this->employment_confirmation();
            return redirect()->back();
        } 

        $application = TenantApplication::find(request('application_id'));
        return view('property-manager.application.email-application.index', compact('application'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sent_to' => 'required',
            'subject' => 'required',
        ]);
        
        $email_application = EmailApplication::create([
            'applicant_name'    =>      $request->applicant_name,
            'application_id'    =>      $request->application_id,
            'sent_to'           =>      $request->sent_to,
            'notify_for'        =>      $request->notify_for,
            'subject'           =>      $request->subject,
            'sent_by'           =>      auth()->user()->id
        ]);

        flash('Email notification sent successfully')->success();

        Mail::to($request->sent_to)->send(new EmailApplicationMail($email_application->id));

        return redirect()->route('property-manager.applications.show', $request->application_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function employment_confirmation(Request $request)
    {
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
        } else {
            flash('Please Upload a File for Employment Confirmation.')->error();
            return redirect()->back();
        }

        flash('Employment confirmation mail has been sent.')->success();

        Mail::to($request->send_to)->send(new EmploymentConfirmationMail($request->send_to, $request->body, $request->subject, $file, $request->user_id));

        return redirect()->back();
    }

    public function history_confirmation(Request $request)
    {
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
        } else {
            flash('Please Upload a File for History Confirmation.')->error();
            return redirect()->back();
        }

        flash('History Confirmation Mail Sent Successfully');

        Mail::to($request->send_to)->send(new HistoryConfirmationMail($request->send_to, $request->body, $request->subject, $file, $request->user_id, auth()->user()->email));

        return redirect()->back();
    }

    public function forward_application(Request $request)
    {
        if ($request->hasFile('attachment')) {
            $file = $request->file('attachment');
        } else {
            flash('Please upload a file to forward.')->error();
            return redirect()->back();
        }

        Mail::to(request('send_to'))->send(new ForwardToApplicationMail($file, $request->application_id, request('send_to')));
        Mail::to(TenantApplication::find($request->application_id)->user->email)->send(new ForwardApplicationMail($file, $request->application_id, request('send_to')));

        ApplicationForward::create([
            'application_id' => $request->application_id,
            'sent_by' => auth()->user()->id,
            'sent_to' => request('send_to')
        ]);

        flash('Mail forwarded successfully')->success();
        return redirect()->back();
    }
}
