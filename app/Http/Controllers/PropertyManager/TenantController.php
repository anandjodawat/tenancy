<?php

namespace App\Http\Controllers\PropertyManager;

use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Access\User\User;
use App\Models\HistoryConfirmation;
use App\Http\Controllers\Controller;
use App\Models\Tenant\TenantProfile;
use App\Models\EmpolymentConfirmation;
use App\Models\Tenant\TenantApplication;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('agency-admin')) {
            $tenants =  TenantApplication::whereAgencyName(auth()->user()->agency->name)
                ->select('user_id')
                ->groupBy('user_id')
                ->latest()
                ->get(); 
        } else {
            $tenants = TenantApplication::wherePropertyManagerEmail(auth()->user()->email)
                ->select('user_id')
                ->groupBy('user_id')
                ->latest()
                ->get();
        }

        return view('property-manager.tenant.index', compact('tenants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $profile = DB::table('tenant_profiles')
            ->join('tenant_emergency_contacts', 'tenant_profiles.id', '=', 'tenant_emergency_contacts.profile_id')
            ->join('tenant_current_addresses', 'tenant_profiles.id', '=', 'tenant_current_addresses.profile_id')
            ->join('tenant_previous_addresses', 'tenant_profiles.id', '=', 'tenant_previous_addresses.profile_id')
            ->join('tenant_current_employments', 'tenant_profiles.id', '=', 'tenant_current_employments.profile_id')
            ->join('tenant_previous_employments', 'tenant_profiles.id', '=', 'tenant_previous_employments.profile_id')
            ->join('tenant_personal_references', 'tenant_profiles.id', '=', 'tenant_personal_references.profile_id')
            ->join('tenant_professional_references', 'tenant_profiles.id', '=', 'tenant_professional_references.profile_id')
            ->join('tenant_income_expenditures', 'tenant_profiles.id', '=', 'tenant_income_expenditures.profile_id')
            ->join('tenant_repayments', 'tenant_profiles.id', '=', 'tenant_repayments.profile_id')
            ->whereUserId($id)
            ->first();

        $user = User::find($id);

        //$employmentConfirmation = EmpolymentConfirmation::where('employment_id', $user->id)->count() > 0 ? EmpolymentConfirmation::where('employment_id', $user->id)->first() : "";
        //$historyConfirmation = HistoryConfirmation::where('personal_ref_id', $user->id)->count() > 0 ? HistoryConfirmation::where('personal_ref_id', $user->id)->first() : "";

        $documents = TenantApplication::DOCUMENTNAME;
        return view('property-manager.tenant.show', compact('user', 'documents', 'profile', 'employmentConfirmation', 'historyConfirmation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete() 
    { 
        $user = user::find(request('id'))->delete();

        flash('Tenant User deleted successfully');

        return response()->json([], 200);
    }
}
