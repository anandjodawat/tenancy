<?php

namespace App\Http\Composers\Consultancy;

use Illuminate\View\View;
use App\Repositories\Consultancy\User\ConsultancyUserRepository;

/**
 * Class ConsultancySidebarComposer.
 */
class ConsultancySidebarComposer
{
    /**
     * @var ConsultancyUserRepository
     */
    protected $userRepository;

    /**
     * ConsultancySidebarComposer constructor.
     *
     * @param ConsultancyUserRepository $userRepository
     */
    public function __construct(ConsultancyUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param View $view
     *
     * @return bool|mixed
     */
    public function compose(View $view)
    {
        $view->with('logged_in_user', access()->user());
    }
}
