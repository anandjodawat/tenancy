<?php

namespace App\Http\Middleware;

use Closure;

class UserDashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('property-manager')) {
            return redirect()->route('property-manager.dashboard');
        }

        if (auth()->user()->hasRole('agency-admin')) {
            return redirect()->route('property-manager.dashboard');
        }
        
        return $next($request);
    }
}
