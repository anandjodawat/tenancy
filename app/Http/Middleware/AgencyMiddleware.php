<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AgencyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole('agency-admin')) {
            return redirect()->route('/')->withFlashDanger('Agency details not found!');
        }

        return $next($request);
    }
}
