<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class PropertyManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole('agency-admin') && !Auth::user()->hasRole('property-manager')) {
            return redirect('/')->withFlashDanger('You are not authorized');
        }
        
        return $next($request);
    }
}
