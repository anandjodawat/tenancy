<?php

namespace App\Http\Requests\Backend\Note;

use App\Http\Requests\Request;

/**
 * Class ManageStudentNoteRequest.
 */
class ManageStudentNoteRequest extends Request
{
    /**
     * Determine if the student is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
