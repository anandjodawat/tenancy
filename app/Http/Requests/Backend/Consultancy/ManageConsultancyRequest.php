<?php

namespace App\Http\Requests\Backend\Consultancy;

use App\Http\Requests\Request;

/**
 * Class ManageConsultancyRequest.
 */
class ManageConsultancyRequest extends Request
{
    /**
     * Determine if the consultancy is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->hasRole(1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
