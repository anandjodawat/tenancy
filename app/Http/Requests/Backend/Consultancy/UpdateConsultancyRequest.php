<?php

namespace App\Http\Requests\Backend\Consultancy;

use App\Http\Requests\Request;

/**
 * Class UpdateConsultancyRequest.
 */
class UpdateConsultancyRequest extends Request
{	
	/**
     * Determine if the consultancy is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return access()->hasRole(1);
        return true;
    }

    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:191',
        ];
    }
}
