<?php

namespace App\Http\Requests\Backend\Student;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreStudentRequest.
 */
class StoreStudentRequest extends Request
{
    /**
     * Determine if the student is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'     => 'required|max:191',
            'last_name'  => 'required|max:191',
            'email'    => ['required', 'email', 'max:191', Rule::unique('students')],
        ];
    }
}
