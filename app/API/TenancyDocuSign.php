<?php
namespace App\Api;

use PDF;
use DocuSign;
use App\Models\Agency;
use App\Models\Setting;
use App\Models\Access\User\User;
use App\Models\Tenant\TenantApplication;

/**
 * Docusign 
 */
class TenancyDocuSign
{
    public $location;
    public $apiClient;
    public $application_id;
    public function __construct($location, $application_id)
    {
        $this->application_id = $application_id;
        $this->location = $location;
    }
    public function sendData()
    {
        $application = TenantApplication::find($this->application_id);
        $document_names = TenantApplication::DOCUMENTNAME;
        $agency = Agency::whereName($application->agency_name)->first();
        $client = DocuSign::create();
        $anchor_string = '(Signature)';
        $setting = Setting::whereAgencyId($agency->id)->first();

        $lease_terms = $agency->leaseTerms ? $agency->leaseTerms->lease_terms : '';
        $supporting_documents = $agency->supportingDocument ?? [];
        $pdf = PDF::loadView($this->location, compact('agency', 'lease_terms', 'supporting_documents', 'application', 'document_names', 'setting'));

        try {
            return $client->envelopes->createEnvelope($client->envelopeDefinition([
                'status'        => 'sent',
                'email_subject' => '[DocuSign PHP SDK] - Signature Request Sample',
                'recipients'    => $client->recipients([
                    'signers' => [
                        $client->signer([
                            'name'           => $application->user->name,
                            'email'          => $application->user->email,
                            'client_user_id' => $application->user->id,
                            'role_name'      => 1,
                            'recipient_id'   => 1,
                            'tabs'           => $client->tabs([
                                'sign_here_tabs' => [
                                    $client->signHere([
                                        'anchor_string'                => $anchor_string,
                                        'anchor_x_offset'              => '40',
                                        'anchor_y_offset'              => '-9',
                                        'anchor_ignore_if_not_present' => false,
                                        'anchor_units'                 => 'pixels',
                                        'recipient_id'                 => 1,
                                    ]),
                                ],
                            ]),
                        ]),
                    ],
                ]),
                'documents'     => [
                    $client->document([
                        'document_base64' => base64_encode($pdf->stream()),
                        'name'            => 'Tenancy Application',
                        'document_id'     => 1,
                        'file_extension'  => 'pdf',
                    ]),
                ],
            ]));
        } catch (DocuSign\eSign\ApiException $e){
            return $e->getResponseBody()->errorCode . " " . $e->getResponseBody()->message;
        }
        // catch (Exception $e) {
        //     dd($e->getResponseBody());
        // }
    }

    public function accountId()
    {
        $username = env('DOCUSIGN_USERNAME');
        $password = env('DOCUSIGN_PASSWORD');
        $integrator_key = env('DOCUSIGN_INTEGRATOR_KEY');     

        // change to production before going live
        $host = "https://www.docusign.net/restapi";

        // create configuration object and configure custom auth header
        $config = new DocuSign\eSign\Configuration();
        $config->setHost($host);
        $config->addDefaultHeader("X-DocuSign-Authentication", "{\"Username\":\"" . $username . "\",\"Password\":\"" . $password . "\",\"IntegratorKey\":\"" . $integrator_key . "\"}");

        // instantiate a new docusign api client
        $this->apiClient = new DocuSign\eSign\ApiClient($config);
        $accountId = null;
        
        try 
        {
            //*** STEP 1 - Login API
            $authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($this->apiClient);
            $options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
            $loginInformation = $authenticationApi->login($options);
            if(isset($loginInformation))
            {
                $loginAccount = $loginInformation->getLoginAccounts()[0];
                if(isset($loginInformation))
                {
                    $accountId = $loginAccount->getAccountId();
                    if(!empty($accountId))
                    {
                       return $accountId;
                        if(!empty($envelop_summary))
                        {
                            return;
                        }
                    }
                }
            }
        }
        catch (DocuSign\eSign\ApiException $e)
        {
            return $e->getResponseBody()->errorCode . " " . $e->getResponseBody()->message;
        }
    }
    
    public function apiClient(){
        return $this->apiClient;
    }
}
?>