<?php

namespace App\API;

use App\Models\Access\User\User;
use App\Models\Tenant\TenantApplication;

// API
use GuzzleHttp\Client;

/**
 * Creates array for sending data through API
 */
class Utility
{
    public $application;

    function __construct($application)
    {
        $this->application = $application;
    }

    public function getData()
    {
        $user = User::find($this->application->user_id);

        return [
            "salutation" =>    "Mr.",
            "firstName" =>     $user->first_name,
            "lastName" =>    $user->last_name,
            "phoneNumber" =>     $user->tenantProfile->mobile,
            "email" =>     $user->email,
            "postcode" =>    $this->application->post_code,
            "streetNumber" =>    "100",
            "streetName" =>    "Sample",
            "streetType" =>    "Street",
            "suburb" =>    $this->application->suburb,
            "state" =>     config('tenancy-application.states')[$this->application->state],
            "accountType" =>     "Residential",
            "salesType" =>     "Better Deal",
            "dateOfBirth" =>     $user->tenantProfile->dob,
            "connectionDate" =>    "2017-10-20",
            "propertyManager" =>     $this->application->property_manager_email,
        ];
    }

    public function sendData($data)
    {
        $post = json_encode($data);
        $domain = "prod.utilityworld.com.au";
        $token = "HpGTASFoDUzmQjmD4tp932KsrgdXiF3s";
      //$url ="https://test01.utilityworld.com.au/api/v2/api/customer/create?api_token=DoNLK2iEba0vdwG8Yr3UTUYRqcNhSIxCiLSRBKEvQqDxzmD4TG";
      //https://prod.utilityworld.com.au/api/v2/documentation
        $url ="https://".$domain."/api/v2/api/customer/create?api_token=".$token;
        $client = new Client([
            'headers' => ['content-Type' => 'application/json']
        ]);

        $response = $client->request('POST', $url, ['body' => $post]);
        
        return json_decode($response->getBody());

    }
}