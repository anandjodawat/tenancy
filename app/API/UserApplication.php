<?php

namespace App\API;

use App\Models\Access\User\User;
use App\Models\Tenant\TenantApplication;

// API
use GuzzleHttp\Client;

/**
 * Creates array for sending data through API
 */
class UserApplication
{
    public $application;

    function __construct($application)
    {
        $this->application = $application;
    }

    public function getData()
    {
        $user = User::find($this->application->user_id);

        return [
            "first_name"    =>  $user->first_name,
            "last_name"     =>  $user->last_name,
            "gender"        =>  ucfirst($user->tenantProfile->gender),
            "unit"          =>  "1",
            "add1"          =>  $this->application->applied_for,
            "add2"          =>  "Test Address 2",
            "city"          =>  "Bedrock",
            "state"         =>  $this->application->state_name,
            "postcode"      =>  1234,
            "mobile"        =>  $user->tenantProfile->mobile,
            "email"         =>  $user->email,
            // "birth_date"    =>  $user->tenantProfile->dob,
            "birth_date"    => "01/01/1992",
            "bond"          =>  $this->application->bond,
            "tenancy_months"=>  $this->application->length_of_lease < 7 ? 6 : 12,
            "residential_status"=>  $user->tenantProfile->tenantCurrentAddress->current_living_arrangement,
            "employment_status"=>   $user->tenantProfile->tenantCurrentEmployment->current_employment_work_situation,
            "number_of_adults"=>    $this->application->number_of_occupant,
            "number_of_children"=> $this->application->number_of_children,
            "is_co_renting"=>1
        ];
    }

    public function sendData($data)
    {
        // $domain = "bondsuretest:edward@bs-test.terah.com.au";
        $domain = "bondsuretest:edward@apply.bondsure.com.au";
        $token = "5d5ba508-4420-11e8-842f-0ed5f89f718b";
        
        $client = new Client([
            'headers' => ['content-Type' => 'application/json', 'Accept' => 'application/json', 'X-Auth-Token' => $token]
        ]);

        $response = $client->request('POST', 'https://'.$domain.'/api/v1.0/partner/referrals.json', ['body' => json_encode($data)]);
        
        return json_decode($response->getBody());

    }
}