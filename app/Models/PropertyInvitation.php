<?php

namespace App\Models;

use App\Models\Agency\Property;
use App\Models\Access\User\User;
use App\Models\InvitationTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyInvitation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'property_id',
        'tenant_email',
        'inspection_date',
        'sent_by',
        'agency_id',
        'body'
    ];

     protected $dates = ['deleted_at'];

    public function sentBy()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }

    public function scopeSearch($query, $search)
    {
        if ($search) {
            return $query->where('tenant_email', 'like', '%'.$search.'%')
                ->orWhere('address_for_invite', 'like', '%'.$search.'%');
        }
        return $query;
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

    public function getNameAttribute()
    {
        return $this->first_name." ".$this->last_name;
    }

    public function getStandardDateAttribute()
    {
        return $this->created_at->day.'-'.$this->created_at->month.'-'.$this->created_at->year;
    }
}
