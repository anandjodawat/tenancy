<?php

namespace App\Models\Access\User\Traits\Attribute;

/**
 * Class ConsultancyUserAttribute.
 */
trait ConsultancyUserAttribute
{
    /**
     * @return string
     */
    public function getConsultancyUserEditButtonAttribute()
    {
        return '<a href="'.route('agency.user.edit', $this).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getConsultancyUserDeleteButtonAttribute()
    {
        //if ($this->id != access()->id() && $this->id != 1) {
            return '<a href="'.route('agency.user.destroy', $this).'"
                 data-method="delete"
                 data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                 data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';
       /* }

        return '';*/
    }

    /**
     * @return string
     */
    public function getConsultancyUserActionButtonsAttribute()
    {
        return
            $this->consultancy_user_edit_button.$this->consultancy_user_delete_button;
    }
}
