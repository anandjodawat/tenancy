<?php

namespace App\Models\Access\User\Traits;

use App\Models\Access\User\User;
use App\Notifications\Frontend\Auth\UserNeedsPasswordReset;

// Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\UserNeedsPasswordResetMail;
use DB;

/**
 * Class UserSendPasswordReset.
 */
trait UserSendPasswordReset
{
    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        // $email = DB::table('password_resets')->where('token', $token)->first()->email;
        Mail::to($this->email)->send(new UserNeedsPasswordResetMail($token));

        // $this->notify(new UserNeedsPasswordReset($token));
    }
}
