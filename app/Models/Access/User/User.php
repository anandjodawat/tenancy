<?php

namespace App\Models\Access\User;

use App\Models\Agency;
use App\Models\AgentProfile;
use Illuminate\Notifications\Notifiable;
use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\UserSendPasswordReset;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;

// for medialibrary
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use App\Models\Tenant\TenantProfile;

/**
 * Class User.
 */
class User extends Authenticatable implements HasMedia
{
    use UserScope,
        UserAccess,
        Notifiable,
        SoftDeletes,
        UserAttribute,
        UserRelationship,
        UserSendPasswordReset,
        HasMediaTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'status', 'confirmation_code', 'confirmed', 'agency_id', 'created_by_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = ['full_name', 'name'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.users_table');
    }

    public function agency()
    {
        return $this->hasOne(Agency::class, 'user_id');
    }

    public function agency_profile()
    {
        return Agency::find(auth()->user()->agency_id);
    }

    public function belongingAgency()
    {
        return $this->belongsTo(Agency::class, 'agency_id');
    }

    public function agency_logo()
    {
        $agency = Agency::find(auth()->user()->agency_id);

        if ($agency && $agency->getFirstMedia('agency_logos')) {
            return $agency->getFirstMedia('agency_logos')->getUrl();
        }
        return false;
    }

    public function tenantProfile()
    {
        return $this->hasOne(TenantProfile::class, 'user_id');
    }
}
