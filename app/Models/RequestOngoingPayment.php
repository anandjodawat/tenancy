<?php

namespace App\Models;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;

class RequestOngoingPayment extends Model
{
    protected $fillable = [
        'tenant_code',
        'title',
        'first_name',
        'last_name',
        'dob',
        'address',
        'suburb',
        'state',
        'postcode',
        'mobile_number',
        'phone_number',
        'email',
        'subject',
        'body',
        'sent_by',
        'email_status'
    ];

    public function sentBy()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }

    public function getNameAttribute()
    {
        return $this->first_name." ".$this->last_name;
    }
}
