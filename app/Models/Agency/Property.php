<?php

namespace App\Models\Agency;
use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'address',
		'suburb',
		'state',
		'post_code',
		'rent_amount',
		'created_by',
		'agency_id'
	];

	protected $dates = ['deleted_at'];

	public function createdBy()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	public function scopeSearch($query, $search)
	{
		return $query->where('address', 'like', '%'.$search.'%')
			->orwhere('post_code', 'like', '%'.$search.'%')
			->orwherehas('createdBy', function ($query) use ($search) {
            $query->where('first_name', 'like', '%'.$search.'%');
        })->orwhereHas('createdBy', function ($query) use ($search) {
            $query->where('last_name', 'like', '%'.$search.'%');
        })->orWhereHas('createdBy', function ($query) use ($search) {
            $query->where('email', 'like', '%'.$search.'%');
        });
			
	}	
}
