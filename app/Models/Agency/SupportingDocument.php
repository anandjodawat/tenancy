<?php

namespace App\Models\Agency;

use App\Models\Agency;
use Illuminate\Database\Eloquent\Model;

class SupportingDocument extends Model
{
    protected $fillable = [
        'agency_id',
        'supporting_document',
        'document_id'
    ];

    CONST DOCUMENTNAME = [
        'primary_photo',
        'secondary_photo',
        'proof_of_address',
        'proof_of_income',
        'previous_rental_receipt',
        'previous_lease_agreement',
        'others'
    ];

    public function agency()
    {
        $this->belongsTo(Agency::class, 'agency_id');
    }

    public function getDocumentNameAttribute()
    {
    	return self::DOCUMENTNAME[$this->document_id];
    }
}
