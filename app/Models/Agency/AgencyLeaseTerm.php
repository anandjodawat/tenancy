<?php

namespace App\Models\Agency;

use App\Models\Agency;
use Illuminate\Database\Eloquent\Model;

class AgencyLeaseTerm extends Model
{
    protected $fillable = [
        'agency_id',
        'lease_terms'
    ];

    public function agency()
    {
        $this->belongsTo(Agency::class, 'agency_id');
    }
}
