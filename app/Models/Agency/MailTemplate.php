<?php

namespace App\Models\Agency;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;

class MailTemplate extends Model
{
    protected $fillable = [
        'type',
        'body',
        'created_by',
        'agency_id'
    ];

    CONST MAILTYPE = [
        'Request Deposit',
        'Request Ongoing',
        // 'Request One Off',
        // 'Test Email Template',
        // 'Request Any Deposit',
        'Bond Request',
        'Invite Tenant To Apply'
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function getMailTypeAttribute()
    {
        return self::MAILTYPE[$this->type];
    }

    public static function templates($key)
    {
        switch ($key) {
            case 0:
                // For Request Deposit template:
                if (auth()->user()->agency_profile()) {
                    $agencyName = auth()->user()->agency_profile()->name;
                    $link = '<a href="'.auth()->user()->agency_profile()->website.'">'.auth()->user()->agency_profile()->website.'</a><br />';
                }
                $template = '<p>Dear [[name]], &nbsp;</p>
                <p>Your Tenancy Application has been Accepted for address:</p>
                <p>[[property_address]]</p>
                <p>This is your reference number: [[tenant_code]]</p>
                <p>Total Amount: $[[amount]]</p>
                <p>Please Click pay now button to pay your holding deposit.</p>
                <p>[[pay_now]]</p>
                <p>
                <br />
                '.auth()->user()->name.'<br />
                '.$agencyName.'<br />
                '.auth()->user()->email.'<br />
                '.$link.'
                <br />';
            break;
            case 2:
                // For Bond request
                if (auth()->user()->agency_profile()) {
                    $agencyName = auth()->user()->agency_profile()->name;
                    $link = '<a href="'.auth()->user()->agency_profile()->website.'">'.auth()->user()->agency_profile()->website.'</a><br />';
                }

                $template = '<p>Dear [[first_name]],</p>

                <p>Your Tenancy Application has been Approved for address:</p>

                <p>[[property_address]]</p>

                <p>Please Click PAY NOW button below to pay your bond deposit.</p>

                <p>If you need bond assistance, then we can help you with EasyBondPay</p>

                <p>[[EasyBondPayLogo]]</p>

                <p>Please click on the EasyBondPay button below to assist you with Bond Payment.</p>

                <p>[[EasyBondPayButton]]</p>

                <p><br />
                '.auth()->user()->name.'<br />
                <br />
                '.$agencyName.'<br />
                '.auth()->user()->email.'<br />
                '.$link.'
                <br />';
            break;

            case 3:
                // For Invite tenant to Apply:
                if (auth()->user()->agency_profile()) {
                    $agencyName = auth()->user()->agency_profile()->name;
                    $link = '<a href="'.auth()->user()->agency_profile()->website.'">'.auth()->user()->agency_profile()->website.'</a><br />';
                }
                $template = '<p>Dear [[name]], &nbsp;</p>
                <p>Thank you for inspecting the property at [[property_address]]</p>
                <p>You are now invited to apply.</p>
                <p>[[apply_now]]</p>
                <p>
                '.auth()->user()->name.'<br />
                '.$agencyName.'<br />
                '.auth()->user()->email.'<br />
                '.$link.'
                <br />';
            break;


            default:
                //For Ongoing Payment or default:
                if (auth()->user()->agency_profile()) {
                    $link = '<a href="'.auth()->user()->agency_profile()->website.'">'.auth()->user()->agency_profile()->website.'</a><br />';
                } else {
                    $link = '';
                }
                $template = '<p>Dear [[first_name]], &nbsp;</p>
                <p>Your Tenancy Application has been Approved for address:</p>
                <p>[[property_address]]</p>
                <p>Please click the button below to setup ongoing payment.</p>
                <p>[[setup_ongoing_payment]]</p>
                <p><br />
                ' . auth()->user()->name . '<br />' . auth()->user()->email . '<br />' . $link . '</p>';
                break;
        }

        return $template;
    }
}
