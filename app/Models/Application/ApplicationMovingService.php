<?php

namespace App\Models\Application;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\TenantApplication;

class ApplicationMovingService extends Model
{
	protected $fillable = [
		'moving_service_id',
		'application_id'
	];

	CONST MOVINGSERVICEOPTION = [
        'Electricity',
        'Gas',
        'Telephone',
        'Internet',
        'Bottled Gas',
        'Pay TV',
        'Disconnections',
        'Removalist',
        'Vehicle Hire',
        'Cleaning Services'
    ];

    public function getServiceNameAttribute()
    {
        return self::MOVINGSERVICEOPTION[$this->moving_service_id];
    }

    public function application()
    {
        return $this->belongsTo(TenantApplication::class, 'application_id');
    }
}
