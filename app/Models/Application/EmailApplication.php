<?php

namespace App\Models\Application;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\TenantApplication;

class EmailApplication extends Model
{
    protected $fillable = [
    	'applicant_name',
    	'application_id',
    	'sent_to',
    	'notify_for',
    	'subject',
    	'sent_by'
    ];

    public function sentBy()
    {
    	return $this->belongsTo(User::class, 'sent_by');
    }

    public function application()
    {
        return $this->belongsTo(TenantApplication::class, 'application_id');
    }
}
