<?php

namespace App\Models\Application;

use App\Models\TenantApplication;
use Illuminate\Database\Eloquent\Model;

class ApplicationPet extends Model
{
 	protected $fillable = [
 		'application_id', 'pet_type', 'pet_breed'
 	];

 	public function application()
    {
    	return $this->belongsTo(TenantApplication::class, 'application_id');
    }
}
