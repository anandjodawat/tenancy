<?php

namespace App\Models\Application;

use App\Models\TenantApplication;
use Illuminate\Database\Eloquent\Model;

class ApplicationChildren extends Model
{
 	protected $fillable = [
 		'children_age', 'application_id'
 	];

 	public function application()
    {
    	return $this->belongsTo(TenantApplication::class, 'application_id');
    }
}
