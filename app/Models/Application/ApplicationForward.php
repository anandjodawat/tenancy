<?php

namespace App\Models\Application;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;

class ApplicationForward extends Model
{
    protected $fillable = [
    	'application_id',
    	'sent_by',
    	'sent_to'
    ];

    public function sentBy()
    {
    	return $this->belongsTo(User::class, 'sent_by');
    }
}
