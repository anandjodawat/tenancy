<?php

namespace App\Models\Application;

use App\Models\Access\User\User;
use App\Models\TenantApplication;
use Illuminate\Database\Eloquent\Model;

class ApplicationOccupant extends Model
{
    protected $fillable = [
        'occupant_name', 'occupant_age', 'occupant_relationship', 'application_id', 'occupant_on_lease', 'occupant_as_joint_tenant', 'occupant_email', 'occupant_mobile_number'
    ];

    CONST RELATIONSHIP = ['aunty','brother','colleague','cousin','daughter','defacto','employer','father','father in law','friend','grand daughter','grand son','grand father','grand mother','husband','lawyer','manager','mother','mother in law','nephew','niece','non immediate relative','partner','previous employer','sister','son','supervisor','uncle','wife','other'
       ];

    public function application()
    {
        return $this->belongsTo(TenantApplication::class, 'application_id');
    }

    public function occupantRelationship()
    {
    	return $this->occupant_relationship ? self::RELATIONSHIP[$this->occupant_relationship] : '';
    }
}
