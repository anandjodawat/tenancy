<?php

namespace App\Models\Application;

use App\Models\TenantApplication;
use Illuminate\Database\Eloquent\Model;

class ApplicationVehicle extends Model
{
    protected $fillable = [
        'application_id', 'vehicle_type', 'vehicle_registration', 'make_model'
    ];

    public function application()
    {
        return $this->belongsTo(TenantApplication::class, 'application_id');
    }
}
