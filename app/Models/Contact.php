<?php

namespace App\Models;

use App\Traits\Agency\ContactUsTrait;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	use ContactUsTrait;

    protected $fillable = [
    	'first_name', 'last_name', 'email', 'subject', 'message', 'replied_by'
    ];

    public function getActionButtonAttribute()
    {
    	return $this->contact_us_buttons;
    }
}
