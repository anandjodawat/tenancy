<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryConfirmation extends Model
{
    protected $fillable = [
        'date',
        'agency',
        // 'fax',
        'tenant_name',
        'property_address',
        'listed_as_lease',
        'property_duration_from',
        'property_duration_to',
        'terminate_tenancy',
        'reason_for_termination',
        'arrears',
        'breach_notices',
        'reason_for_breach_notices',
        'damage',
        'damage_reason',
        'inspection_like',
        'pets',
        'complaints_damage',
        'vacate_date',
        'bond_refund_full',
        'bond_refund_reason',
        'rate_tenant',
        'rent_again',
        'additional_comments',
        'completed_by',
        'employer_position',
        'submission_date',
        'personal_ref_id'
    ];
}
