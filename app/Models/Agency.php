<?php

namespace App\Models;

use App\Models\Access\User\User;
use App\Models\Agency\MailTemplate;
use App\Models\Agency\AgencyLeaseTerm;
use Illuminate\Database\Eloquent\Model;
use App\Models\Agency\SupportingDocument;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
    
    protected $fillable = [
        'name',
        'agency_id',
        'user_id',
        'address',
        'phone_number',
        'email',
        'registration_number',
        'status',
        'website',
        'suburb',
        'state',
        'post_code',
        'preferred_color'
    ];

    protected $dates = ['deleted_at'];

    CONST STATES = [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getStateNameAttribute()
    {
        return self::STATES[$this->state];
    }

    public function leaseTerms()
    {
        return $this->hasOne(AgencyLeaseTerm::class, 'agency_id');
    }

    public function supportingDocument()
    {
        return $this->hasMany(SupportingDocument::class, 'agency_id');
    }

    public function logo()
    {
        if ($this->getFirstMedia('agency_logos')) {
            return $this->getFirstMedia('agency_logos')->getUrl();
        }
        return false;
    }

    public function mail_templates()
    {
        return $this->hasMany(MailTemplate::class, 'agency_id');
    }
}
