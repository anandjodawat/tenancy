<?php

namespace App\Models;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;

class RequestDeposit extends Model
{
    protected $fillable = [
        'tenant_code',
        'name',
        'address',
        'contact_number',
        'payment_amount',
        'send_to',
        'subject',
        'body',
        'sent_by',
        'email_status'
    ];

    public function sentBy()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }
}
