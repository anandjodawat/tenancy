<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantRepayment extends Model
{
    protected $fillable = [
    	'repayment_loans',
        'repayment_credit_cards',
        'repayment_hire_purchase',
        'repayment_store_card',
        'repayment_others',
        'repayment_expenses',
        'profile_id'
    ];
}
