<?php

namespace App\Models\Tenant;

use App\Models\EmpolymentConfirmation;
use Illuminate\Database\Eloquent\Model;

class TenantCurrentEmployment extends Model
{
    protected $fillable = [
    	'current_employment_work_situation',
        'current_company_name',
        'current_manager_name',
        'current_manager_phone',
        'current_manager_email',
        'current_work_position',
        'current_company_state',
        'current_company_street',
        'current_company_suburb',
        'current_industry_type',
        'current_employment_nature',
        'current_employment_start_date',
        'current_job_status',
        'current_gross_annual_salary',
        'profile_id'
    ];

    public function confirm()
    {
        return $this->hasMany(EmpolymentConfirmation::class, 'employment_id');
    }
}
