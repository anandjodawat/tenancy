<?php

namespace App\Models\Tenant;

use App\Models\HistoryConfirmation;
use Illuminate\Database\Eloquent\Model;

class TenantPersonalReference extends Model
{
    protected $fillable = [
        'personal_ref_name',
        'personal_ref_occupation',
        'personal_ref_relationship',
        'personal_ref_phone',
        'personal_ref_mobile',
        'personal_ref_email',
        'personal_ref_state',
        'personal_ref_suburb',
        'personal_ref_street',
        'profile_id'
    ];

    public function confirm()
    {
        return $this->hasMany(HistoryConfirmation::class, 'personal_ref_id');
    }
}
