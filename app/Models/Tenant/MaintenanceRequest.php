<?php

namespace App\Models\Tenant;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class MaintenanceRequest extends Model implements HasMedia
{
    use HasMediaTrait;
    
    protected $fillable = [
        'street_address',
        'suburb',
        'state',
        'post_code',
        'tenant',
        'name',
        'mobile_phone',
        'home_phone',
        'work_phone',
        'email',
        'details',
        'preferred_date',
        'preferred_time',
        'maintenance_existed',
        'property_manager_email',
        'user_id',
        'status'
    ];


    CONST STATUSNAME = [
        'pending',
        'forwarded',
        'accepted',
        'approved',
        'declined',
        'archieved'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getStatusNameAttribute()
    {
        return self::STATUSNAME[$this->status];
    }
}
