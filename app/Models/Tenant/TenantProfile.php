<?php

namespace App\Models\Tenant;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\TenantRepayment;
use App\Models\Tenant\TenantCurrentAddress;
use App\Models\Tenant\TenantPreviousAddress;
use App\Models\Tenant\TenantEmergencyContact;
use App\Models\Tenant\TenantCurrentEmployment;
use App\Models\Tenant\TenantIncomeExpenditure;
use App\Models\Tenant\TenantPersonalReference;
use App\Models\Tenant\TenantPreviousEmployment;
use App\Models\Tenant\TenantProfessionalReference;

class TenantProfile extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'dob',
        'gender',
        'marital_status',
        'home_phone',
        'work_phone',
        'mobile',
        // 'fax',
        'smoke',
        'smoke_inside',
        'studying',
        'institution',
        'enrollment_number',
        'course_contact_number',
        'institution_phone',
        'source_of_income',
        'net_weekly_amount',
        'guardian_name',
        'guardian_relationship',
        'guardian_address',
        'guardian_phone_number',
        'pension',
        'pention_type',
        'pension_number',
        'email',
        'status'
    ];

    CONST MARITALSTATUS = [
        'single',
        'divorced',
        'engaged',
        'partner living with me',
        'seperated',
        'widow',
        'married'
    ];

    CONST GUARDIANRELATIONSHIP = [
        // 'accountant',
        'aunty',
        // 'boss',
        'brother',
        'colleague',
        'cousin',
        'daughter',
        'defacto',
        'employer',
        'father',
        'father in law',
        'friend',
        'grand daughter',
        'grand son',
        'grand father',
        'grand mother',
        'husband',
        'lawyer',
        'manager',
        'mother',
        'mother in law',
        'nephew',
        'niece',
        'non immediate relative',
        'partner',
        'previous employer',
        'sister',
        'son',
        'supervisor',
        'uncle',
        'wife',
        'other'
    ];

    CONST CURRENTLIVINGARRANGEMENT = [
        'Owner',
        'Renting Through An Agent',
        'Renting Through Private Landlord',
        'With Parent',
        'Sharing',
        'Other'
    ];

    CONST STATES = [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ];

    CONST EMPLOYMENTWORKSITUATION = [
        'Not Employed',
        'Own Business',
        'Contractor',
        'FT Employed',
        'PT Employed',
        'Casual',
        // 'Employed',
        'Retired',
        'Pensioner',
        'Student'
    ];

    CONST INDUSTRYTYPE = [
        'accounting',
        'administrator and office support',
        'advertising arts and media',
        'banking and financial services',
        'call center and customer service',
        'community services and development',
        'construction',
        'consulting and strategy',
        'design and architecture',
        'education and training',
        'engineering',
        'farming animals and conservation',
        'government and defense',
        'healthcare and medical',
        'hospitality and tourism',
        'human resource and recruitment',
        'information and communication technology',
        'insurance and superannuation',
        'legal',
        'manufacturing transport and logistics',
        'marketing and communications',
        'mining resources and energy',
        'real estate and property',
        'retail and consumer products',
        'sales',
        'science and technology',
        'self employment',
        'sports and recreation',
        'trades and services',
        'other'
    ];

    public function employmentStatus($value)
    {
        if (isset($value)) {
            return self::EMPLOYMENTWORKSITUATION[$value];
        }

        return;
    }

    public function livingArrangement($value)
    {
        if (isset($value)) {
            return self::CURRENTLIVINGARRANGEMENT[$value];
        }

        return;
    }

    public function getTenantTitleAttribute()
    {
        return $this->title == '1' ? 'MRS' : $this->title == '2' ? 'MISS' : 'MR';
    }

    public function getTenantMaritalAttribute()
    {
        if (isset($this->marital_status)) {
            return self::MARITALSTATUS[$this->marital_status];
        }
        return;
    }
    
    public function getTenantEmergencyStateAttribute()
    {
        if (isset($this->emergency_contact_state)) {
            return self::STATES[$this->emergency_contact_state];
        }
        return;
    }
    
    public function getTenantCurrentAddressStateAttribute()
    {
        if (isset($this->current_address_state)) {
            return self::STATES[$this->current_address_state];
        }
        return;
    }
    
    public function getTenantPreviousAddressStateAttribute()
    {
        if (isset($this->previous_address_state)) {
            return self::STATES[$this->previous_address_state];
        }
        return;
    }

    public function tenantRelationName($value)
    {
        if (isset($value)) {
            return self::GUARDIANRELATIONSHIP[$value];
        }
        return;
    }


    public function getTenantEmergencyRelationAttribute()
    {
        if (isset($this->emergency_contact_relation)) {
            return self::GUARDIANRELATIONSHIP[$this->emergency_contact_relation];
        }
        return;
    }

    public function getTenantLivingArrangementAttribute()
    {
        if (isset($this->current_living_arrangement)) {
            return self::CURRENTLIVINGARRANGEMENT[$this->current_living_arrangement];
        }
        return;
    }

    public function getTenantPreviousLivingArrangementAttribute()
    {
        if (isset($this->previous_living_arrangement)) {
            return self::CURRENTLIVINGARRANGEMENT[$this->previous_living_arrangement];
        }
        return;
    }

    public function industryType()
    {
        return self::INDUSTRYTYPE[$this->industry_type];
    }

    public function tenantCurrentAddress()
    {
        return $this->hasOne(TenantCurrentAddress::class, 'profile_id');
    }

    public function tenantCurrentEmployment()
    {
        return $this->hasOne(TenantCurrentEmployment::class, 'profile_id');
    }

    public function tenantEmergencyContact()
    {
        return $this->hasOne(TenantEmergencyContact::class, 'profile_id');
    }

    public function tenantIncomeExpenditure()
    {
        return $this->hasOne(TenantIncomeExpenditure::class, 'profile_id');
    }

    public function tenantPersonalReference()
    {
        return $this->hasOne(TenantPersonalReference::class, 'profile_id');
    }

    public function tenantPreviousAddress()
    {
        return $this->hasOne(TenantPreviousAddress::class, 'profile_id');
    }

    public function tenantPreviousEmployment()
    {
        return $this->hasOne(TenantPreviousEmployment::class, 'profile_id');
    }

    public function tenantProfessionalReference()
    {
        return $this->hasOne(TenantProfessionalReference::class, 'profile_id');
    }

    public function tenantRepayment()
    {
        return $this->hasOne(TenantRepayment::class, 'profile_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
