<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantCurrentAddress extends Model
{
    protected $fillable = [
    	'current_living_arrangement',
        'current_address_unit',
        'current_address_street',
        'current_address_street_name',
        'current_address_state',
        'current_address_suburb',
        'current_address_post_code',
        'current_address_move_in',
        'current_address_landlord_name',
        'current_address_monthly_rent',
        'current_address_leave_reason',
        'current_address_landlord_phone',
        'current_address_landlord_email',
        'current_address_landlord_bond_refund',
        'profile_id'
    ];

    CONST STATES = [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ];

    public function getStateNameAttribute()
    {
        $stateCode = isset($this->current_address_state) ? $this->current_address_state : '';
        if ($stateCode != '') :
          return self::STATES[$this->current_address_state];
        else :
          return '';
        endif;
    }
}
