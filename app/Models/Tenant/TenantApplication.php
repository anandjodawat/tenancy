<?php

namespace App\Models\Tenant;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use App\Models\Application\ApplicationPet;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Application\ApplicationForward;
use App\Models\Application\ApplicationVehicle;
use App\Models\Application\ApplicationOccupant;
use App\Models\Application\ApplicationChildren;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Models\Application\ApplicationMovingService;
use App\Traits\PropertyManager\ApplicationAttribute;

class TenantApplication extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
    use ApplicationAttribute;
    
    protected $fillable = [
    	 // Property Details
        'applied_for',
        'street_number',
        'street_name',
        'street_type',
        'state',
        'suburb',
        'post_code',
        'property_type',
        'number_of_bedrooms',
        'commencement_date',
        'length_of_lease',
        'weekly_rent',
        'monthly_rent',
        'bond',
        'bond_assistance', 
        'property_found_in',

        // Property Manager Details
        'agency_name',
        'property_manager_name',
        'property_manager_email',

        // Occupancy Details
        'number_of_occupant',
        'number_of_children',
        'number_of_vehicles',
        'number_of_pets',

        // Agent Specific
        'is_tenant_terminated', 
        'tenant_terminated_details',
        'is_tenant_refused', 
        'tenant_refused_details',
        'is_on_debt', 
        'on_debt_details',
        'is_deduction_rental_bond', 
        'deduction_rental_bond_details',
        'affect_future_rental_payment', 
        'affect_future_rental_payment_details',
        'is_another_pending_application', 
        'own_a_property', 
        'own_a_property_details',
        'buy_another_property', 
        'intend_to_buy_after',

        // Moving Service
        'need_moving_service', 
        'moving_service_agreement', 
        'moving_service_ref_no',
        'moving_service_name',

        // Property Declaration
        'inspected_this_property', 
        'inspection_date',
        'good_condition',
        'not_good_condition_reason',
        'inspection_code',

        // Licence and aggreement
        'licence_and_agreement',
        'status',
        'draft',
        'user_id',
        'bondsure_url',
        'docusign_url',
        'envelope_id',
        'joint_app_id'
    ];

    protected $dates = ['deleted_at'];
    CONST STATES = [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ];

    CONST PROPERTYTYPE = [
    	'house',
    	'unit apartment',
    	'town house',
    	'block of units',
    	'granny flats',
    	'acreage',
    ];

    CONST PROPERTYFOUNDIN = [
    	'Rental List',
    	'Internet',
    	'Newspaper',
    	'Lease Board',
    	'Other'
    ];

    CONST MOVINGSERVICEOPTION = [
        'Electricity',
        'Gas',
        'Telephone',
        'Internet',
        'Bottled Gas',
        'Pay TV',
        'Disconnections',
        'Removalist',
        'Vehicle Hire',
        'Cleaning Services'
    ];

    CONST APPLICATIONSTATUS = [
        'forwarded',
        'pending',
        'accepted',
        'approved',
        'declined',
        'archieved'
    ];

    CONST STATUSBUTTONS = [
        'btn btn-warning btn-xs',
        'btn btn-secondary btn-xs',
        'btn btn-success btn-xs',
        'btn btn-outline-success btn-xs',
        'btn btn-danger btn-xs',
        'btn btn-outline-danger btn-xs'
    ];

    CONST STATUSICONS = [
        'fa fa-dot-circle-o',
        'fa fa-mail-forward',
        'fa fa-thumbs-up',
        'fa fa-check-square-o',
        'fa fa-warning',
        'fa fa-times'
    ];

    CONST DOCUMENTNAME = [
        'primary_photo',
        'secondary_photo',
        'proof_of_address',
        'proof_of_income',
        'previous_rental_receipt',
        'previous_lease_agreement',
        'others'
    ];

    CONST STREETTYPE = [
        'STREET',
        'ROAD',
        'AVENUE',
        'CLOSE',
        'CRESCENT',
        'COURT',
        'DRIVE',
        'PARADE',
        'PLACE',
        'ALLEY',
        'AMBLE',
        'APPROACH',
        'ARCADE',
        'ARTERIAL',
        'BAY',
        'BEND',
        'BRAE',
        'BREAK',
        'BOULEVARD',
        'BOARDWALK',
        'BOWL',
        'BYPASS',
        'CIRCLE',
        'CIRCUS',
        'CIRCUIT',
        'CHASE',
        'CORNER',
        'COMMON',
        'CONCOURSE',
        'CROSS',
        'COURSE',
        'CREST',
        'CRUISEWAY',
        'COVE',
        'DALE',
        'DELL',
        'DENE',
        'DIVIDE',
        'DOMAIN',
        'EAST',
        'EDGE',
        'ENTRANCE',
        'ESPLANADE',
        'EXTENSION',
        'FLATS',
        'FORD',
        'FREEWAY',
        'GATE',
        'GARDENS',
        'GLADES',
        'GLEN',
        'GULLY',
        'GRANGE',
        'GREEN',
        'GROVE',
        'GATEWAY',
        'HILL',
        'HOLLOW',
        'HEATH',
        'HEIGHTS',
        'HUB',
        'HIGHWAY',
        'ISLAND',
        'JUNCTION',
        'LANE',
        'LINK',
        'LOOP',
        'LOWER',
        'LANEWAY',
        'MALL',
        'MEW',
        'MEWS',
        'NOOK',
        'NORTH',
        'OUTLOOK',
        'PATH',
        'POCKET',
        'PARKWAY',
        'PLAZA',
        'PROMENADE',
        'PASS',
        'PASSAGE',
        'POINT',
        'PURSUIT',
        'PATHWAY',
        'QUADRANT',
        'QUAY',
        'REACH',
        'RIDGE',
        'RESERVE',
        'REST',
        'RETREAT',
        'RIDE',
        'RISE',
        'ROUND',
        'ROW',
        'RISING',
        'RETURN',
        'RUN',
        'SLOPE',
        'SQUARE',
        'SOUTH',
        'STRIP',
        'STEPS',
        'SUBWAY',
        'TERRACE',
        'THROUGHWAY',
        'TOR',
        'TRACK',
        'TRAIL',
        'TURN',
        'TOLLWAY',
        'UPPER',
        'VALLEY',
        'VISTA',
        'VIEW/S',
        'WAY',
        'WOOD',
        'WEST',
        'WALK',
        'WALKWAY',
        'WATERS',
        'WATERWAY',
        'WYND',
        'PARK'
    ];

    public function getPropertyFoundInNameAttribute()
    {
        return $this->property_found_in ? self::PROPERTYFOUNDIN[$this->property_found_in] : null;
    }

    public function getStateNameAttribute()
    {
        return $this->state || $this->state == '0' ? self::STATES[$this->state] : null;
    }

    public function getStatusNameAttribute()
    {
        return self::APPLICATIONSTATUS[$this->status];
    }
    

    public function propertyType()
    {
        return $this->property_type ? self::PROPERTYTYPE[$this->property_type] : null;
    }

    public function occupants()
    {
        return $this->hasMany(ApplicationOccupant::class, 'application_id');
    }

    public function children()
    {
        return $this->hasMany(ApplicationChildren::class, 'application_id');
    }

    public function vehicles()
    {
        return $this->hasMany(ApplicationVehicle::class, 'application_id');
    }

    public function pets()
    {
        return $this->hasMany(ApplicationPet::class, 'application_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getActionButtonAttribute()
    {
        return $this->action_buttons;
    }
    
    public function scopeSearch($query, $search)
    { 
        return $query->whereHas('user', function ($query) use ($search) {
            $query->where('first_name', 'like', '%'.$search.'%');
        })->orwhereHas('user', function ($query) use ($search) {
            $query->where('last_name', 'like', '%'.$search.'%');
        })->orWhereHas('user', function ($query) use ($search) {
            $query->where('email', 'like', '%'.$search.'%');
        });
    }

    public function moving_service()
    {
        return $this->hasMany(ApplicationMovingService::class, 'application_id');
    }

    public function scopeApplicationForAgency($query)
    {
        
        if (auth()->user()->hasRole('agency-admin')) {
            return $query->whereDraft('0')
            ->whereAgencyName(auth()->user()->agency_profile()->name)
            ->select(['id', 'created_at', 'state', 'user_id', 'status', 'joint_app_id']);
        }

        return;
    }

    public function scopeApplicationForPropertyManager($query)
    {
        if (auth()->user()->hasRole('property-manager')) {
            return $query->whereDraft('0')
            ->wherePropertyManagerEmail(auth()->user()->email)
            ->select(['id', 'created_at', 'state', 'user_id', 'status']);
        }

        return;
    }

    public function scopeFilterByStatus($query, $status)
    {
        if ($status) {
            if ($status != 'inbox') {
                $index = array_search($status, self::APPLICATIONSTATUS);
                if (auth()->user()->hasRole('property-manager')) {
                    return $query->whereStatus($index)
                    ->wherePropertyManagerEmail(auth()->user()->email)
                    ->select(['id', 'created_at', 'state', 'user_id', 'status']);
                }

                if (auth()->user()->hasRole('agency-admin')) {
                    return $query->whereStatus($index)
                    ->whereAgencyName(auth()->user()->agency->name)
                    ->select(['id', 'created_at', 'state', 'user_id', 'status']);
                }
            }
        }
    }

    public function forwardedTo()
    {
        return $this->hasOne(ApplicationForward::class, 'application_id');
    }
}
