<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantPreviousAddress extends Model
{
    protected $fillable = [
    	'previous_living_arrangement',
        'previous_address_unit',
        'previous_address_street',
        'previous_address_street_name',
        'previous_address_street_type',
        'previous_address_state',
        'previous_address_suburb',
        'previous_address_postcode',
        'previous_address_from',
        'previous_address_to',
        'previous_address_leave_reason',
        'previous_address_agent_name',
        'previous_address_rent',
        'previous_address_email',
        'previous_address_bond_refund',
        'previous_address_additional_information',
        'previous_address_additional_information_detail',
        'profile_id'
    ];

    CONST STATES = [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ];

    public function getStateNameAttribute()
    {
        return self::STATES[$this->previous_address_state];
    }
}
