<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantIncomeExpenditure extends Model
{
    protected $fillable = [
        'current_weekly_rent',
        'vehicle_value',
        'vehicle_type',
        'income_type',
        'salary_wages_income',
        'self_employed_income',
        'benefit_income',
        'other_income',
        'profile_id'
    ];
}
