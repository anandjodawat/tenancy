<?php

namespace App\Models\Tenant;

use App\Models\Agency;
use Illuminate\Database\Eloquent\Model;

class UploadTenant extends Model
{
    protected $fillable = [
        'name',
        'lease_name',
        'email',
        'post_line1',
        'post_line2',
        'post_line3',
        'suburb',
        'state',
        'post_code',
        'mobile',
        'property_address1',
        'property_address2',
        'property_address3',
        'property_state',
        'property_suburb',
        'property_post_code',
        'property_alpha',
        'property_manager',
        'property_manager_email',
        'property_manager_mobile',
        'property_manager_first_name',
        'property_manager_last_name',
        'email_status',
        'rent_amount1',
        'rent_per1',
        'tenant_code',
        'agency_id'
    ];

    public function scopeSentMail($query)
    {
        if (request('action') == 'filter' && request('mail') == 'sent_mail') {
            return $query->whereEmailStatus(1);
        }
        return $query;
    }

    public function scopeFailedMail($query)
    {
        if (request('action') == 'filter' && request('mail') == 'failed_mail') {
            return $query->whereEmailStatus(0);
        }
        return $query;
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%'.$search.'%')
            ->orWhere('email', 'like', '%'.$search.'%')
            ->orWhere('mobile', 'like', '%'.$search.'%');
    }

    public function agencyData()
    {
        return $this->belongsTo(Agency::class, 'agency_id');
    }
}
