<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantEmergencyContact extends Model
{
    protected $fillable = [
        'emergency_contact_name',
        'emergency_contact_relation',
        'emergency_contact_number',
        'emergency_contact_state',
        'emergency_contact_suburb',
        'emergency_contact_street',
        'emergency_contact_home_phone',
        'emergency_contact_work_phone',
        'emergency_contact_mobile',
        'emergency_contact_email',
        'profile_id'
    ];

    CONST STATES = [
        'NSW', 'ACT', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA'
    ];

    public function getStateNameAttribute()
    {
        if (isset($this->emergency_contact_state)) {
            return self::STATES[$this->emergency_contact_state];
        }
        return;
    }
}
