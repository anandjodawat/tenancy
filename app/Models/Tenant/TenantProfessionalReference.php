<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantProfessionalReference extends Model
{
    protected $fillable = [
        'professional_ref_name',
        'professional_ref_company',
        'professional_ref_relationship',
        'professional_ref_phone',
        'professional_ref_mobile',
        'professional_ref_email',
        'professional_ref_company_state',
        'professional_ref_company_street',
        'professional_ref_company_suburb',
        'professional_ref_occupation',
        'profile_id'
    ];
}
