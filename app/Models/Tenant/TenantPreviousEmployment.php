<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Model;

class TenantPreviousEmployment extends Model
{
    protected $fillable = [
        'previous_employment_work_situtation',
        'previous_company_name',
        'previous_manager_name',
        'previous_manager_phone',
        'previous_manager_email',
        'previous_work_position',
        'previous_company_state',
        'previous_company_street',
        'previous_company_suburb',
        'previous_industry_type',
        'previous_employment_nature',
        'previous_employment_start_date',
        'previous_employment_end_date',
        'previous_gross_annual_salary',
        'profile_id'
    ];

}
