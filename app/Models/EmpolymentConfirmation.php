<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpolymentConfirmation extends Model
{
    protected $fillable = [
        'date', 'company_name', 'employee_name', 'employee_position', 'work_duration_from', 'work_duration_to', 'completed_by' ,'employer_position', 'submission_date', 'employment_id'
    ];
}
