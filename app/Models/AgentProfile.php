<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentProfile extends Model
{
    protected $fillable = [
        'agency_name',
        'agency_id',
        'username',
        'address_street_number',
        'address_street_unit',
        'address_street_name',
        'street_type',
        'suburb',
        'state',
        'postal_code',
        'email',
        'phone',
        // 'fax',
        'mobile_number',
        'web_address',
        'general_lease_terms',
        'user_id'
    ];
}
