<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
    	'key',
    	'value',
    	'agency_id',
    	'contact_number', // needed when email selected
    	'body', // needed when utility is selected and has terms and condition
    	'connection_name', // when agency chooses connection as others
    	'is_active',
    	'provider_logo'
    ];
}
