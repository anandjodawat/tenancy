<?php

namespace App\Models;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;

class InvitationTenant extends Model
{
    protected $fillable = [
    	'first_name',
    	'last_name',
    	'email',
    	'created_by'
    ];

    public function createdBy()
    {
    	return $this->belongsTo(User::class, 'created_by');
    }
}
