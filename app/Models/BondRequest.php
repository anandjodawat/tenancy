<?php

namespace App\Models;

use App\Models\Access\User\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tenant\TenantApplication;

class BondRequest extends Model
{
    protected $fillable = [
        'tenant_code',
        'title',
        'first_name',
        'last_name',
        'dob',
        'address',
        'suburb',
        'state',
        'postcode',
        'payment_amount',
        'mobile_number',
        'phone_number',
        'email',
        'subject',
        'body',
        'sent_by',
        'email_status',
        'efund_contract_link',
        'efund_contract_id',
        'finance_period'
    ];

    public function sentBy()
    {
        return $this->belongsTo(User::class, 'sent_by');
    }

    public function getNameAttribute()
    {
        return config('tenancy-application.title')[$this->title]." ".$this->first_name." ".$this->last_name;
    }

    public function getStateNameAttribute()
    {
        return $this->state || $this->state == '0' ? TenantApplication::STATES[$this->state] : null;
    }
}
