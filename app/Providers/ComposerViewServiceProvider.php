<?php

namespace App\Providers;

use Auth;
use App\Models\Consultancy;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\Consultancy\StudentList;

class ComposerViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['frontend.layouts.app'], function ($view) {
            $view->with('valid_consultancy', true);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
