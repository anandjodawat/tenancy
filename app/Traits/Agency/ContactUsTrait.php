<?php

namespace App\Traits\Agency;

trait ContactUsTrait
{
	public function getContactUsShowButtonAttribute()
	{
		return '<a href="'.route('agency.contact-us.show', $this).'" class="btn btn-xs btn-info"><i class="fa fa-eye" title="Details"></i></a> ';
	}

	public function getContactUsButtonsAttribute()
	{
		return $this->contact_us_show_button;
	}
}