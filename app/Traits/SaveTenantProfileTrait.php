<?php

namespace App\Traits;
use App\Models\Tenant\TenantProfile;
use App\Models\Tenant\TenantRepayment;
use App\Models\Tenant\TenantCurrentAddress;
use App\Models\Tenant\TenantPreviousAddress;
use App\Models\Tenant\TenantEmergencyContact;
use App\Models\Tenant\TenantCurrentEmployment;
use App\Models\Tenant\TenantIncomeExpenditure;
use App\Models\Tenant\TenantPersonalReference;
use App\Models\Tenant\TenantPreviousEmployment;
use App\Models\Tenant\TenantProfessionalReference;

/**
* saves the data in tenant profile
*/
trait SaveTenantProfileTrait
{
    public function validate_tenant_profile ($request)
    {
        $this->validate($request, [
            // personal information
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'required',
            'institution' => 'required_if:studying,==,yes',
            'enrollment_number' => 'required_if:studying,==,yes',
            'course_contact_number' => 'required_if:studying,==,yes',
            'institution_phone' => 'required_if:studying,==,yes',
            'source_of_income' => 'required_if:studying,==,yes',
            'net_weekly_amount' => 'required_if:studying,==,yes',
            'guardian_name' => 'required_if:studying,==,yes',
            'guardian_relationship' => 'required_if:studying,==,yes',
            'guardian_address' => 'required_if:studying,==,yes',
            'guardian_phone_number' => 'required_if:studying,==,yes',
            'pension' => 'required_if:studying,==,yes',

            //Emergency contact
            'emergency_contact_name' => 'required',
            'emergency_contact_relation' => 'required',
            'emergency_contact_state' => 'required',
            'emergency_contact_street' => 'required',
            'emergency_contact_suburb' => 'required',
            'emergency_contact_email' => 'required',

            //previous address
            'previous_address_rent' => 'numeric',
            // Current Address
            'current_address_street' => 'required',
            //'current_address_street_name' => 'required',
            'current_address_state' => 'required',
            'current_address_suburb' => 'required',

            // Current Employment
            'current_employment_work_situation' => 'required',
            'current_company_name' => 'required_if:current_employment_work_situation,==,1|required_if:current_employment_work_situation,==,2',
            'current_manager_name' => 'required_if:current_employment_work_situation,==,1|required_if:current_employment_work_situation,==,2',
            'current_manager_phone' => 'required_if:current_employment_work_situation,==,1|required_if:current_employment_work_situation,==,2',
            'current_manager_email' => 'required_if:current_employment_work_situation,==,1|required_if:current_employment_work_situation,==,2',
            'current_work_position' => 'required_if:current_employment_work_situation,==,1|required_if:current_employment_work_situation,==,2',

            // Previous Employment
            'previous_employment_work_situtation' => 'required',
            'previous_company_name' => 'required_if:previous_employment_work_situation,==,1|required_if:previous_employment_work_situation,==,2',
            'previous_manager_name' => 'required_if:previous_employment_work_situation,==,1|required_if:previous_employment_work_situation,==,2',
            'previous_manager_phone' => 'required_if:previous_employment_work_situation,==,1|required_if:previous_employment_work_situation,==,2',
            'previous_manager_email' => 'required_if:previous_employment_work_situation,==,1|required_if:previous_employment_work_situation,==,2',
            'previous_work_position' => 'required_if:previous_employment_work_situation,==,1|required_if:previous_employment_work_situation,==,2',

            // Personal Reference
            'personal_ref_name' => 'required',
            'personal_ref_relationship' => 'required',
            'personal_ref_mobile' => 'required',
            'personal_ref_email' => 'required',
            'personal_ref_state' => 'required',
            'personal_ref_street' => 'required',
            'personal_ref_suburb' => 'required',

            // Professional Reference
            'professional_ref_name' => 'required',
            'professional_ref_relationship' => 'required',
            'professional_ref_mobile' => 'required',
            'professional_ref_email' => 'required',
        ],
        [
            //personal information
            'title.required' => 'Personal Information - Title field is required.',
            'first_name.required' => 'Personal Information - First Name field required.',
            'last_name.required' => 'Personal Information - Last Name field is required.',
            'mobile.required' => 'Personal Information - Mobile number field is required.',
            'institution.required_if' => 'Personal Information - institution name field is required.',
            'enrollment_number.required_if' => 'Personal Information - Enrollment number field is required.',
            'course_contact_number.required_if' => 'Personal Information - Course Contact Number field is required.',
            'institution_phone.required_if' => 'Personal Information - institution Phone field is required.',
            'source_of_income.required_if' => 'Personal Information - Source of Income field is required.',
            'net_weekly_amount.required_if' => 'Personal Information - Net Weekly Amount field is required.',
            'guardian_name.required_if' => 'Personal Information - Guardian Name field is required.',
            'guardian_relationship.required_if' => 'Personal Information - Guardian Relation field is required.',
            'guardian_address.required_if' => 'Personal Information - Guardian Address field is required.',
            'guardian_phone_number.required_if' => 'Personal Information - Guardian Phone number field is required.',
            'pension.required_if' => 'Personal Information - Pension field is required.',

            //emergency contact
            'emergency_contact_name.required' => 'Emergency Contact - Emergency Contact Name field is required.',
            'emergency_contact_relation.required' => 'Emergency Contact - Emergency Contact Relation field is required.',
            'emergency_contact_state.required' => 'Emergency Contact - Emergency Contact State field is required.',
            'emergency_contact_street.required' => 'Emergency Contact - Emergency Contact Street field is required.',
            'emergency_contact_suburb.required' => 'Emergency Contact - Emergency Contact Suburb field is required.',
            'emergency_contact_email.required' => 'Emergency Contact - Emergency Contact Email field is required.',

            //previous address
            'previous_address_rent.numeric' => 'Previous Address - Rent must be a number.',
            // Current Address
            'current_address_street.required' => 'Current Address - Address Street field is required.',
            'current_address_street_name.required' => 'Current Address - Address Street Name field is required.',
            'current_address_state.required' => 'Current Address -  Address  State field is required.',
            'current_address_suburb.required' => 'Current Address - Address Suburb field is required.',

            // Current Employment
            'current_employment_work_situation.required' => 'Current Employment - Work Situation field is required.',
            'current_company_name.required_if' => 'Current Employment - Company Name field is required.',
            'current_manager_name.required_if' => 'Current Employment - Manager Name field is required.',
            'current_manager_phone.required_if' => 'Current Employment - Manager Phone field is required.',
            'current_manager_email.required_if' => 'Current Employment - Manager Email field is required.',
            'current_work_position.required_if' => 'Current Employment - Work Position field is required.',

            // Current Employment
            'previous_employment_work_situtation.required' => 'Previous Employment - Work Situation field is required.',
            'previous_manager_name.required_if' => 'Previous Employment - Manager Name field is required.',
            'previous_manager_phone.required_if' => 'Previous Employment - Manager Phone field is required.',
            'previous_manager_email.required_if' => 'Previous Employment - Manager Email field is required.',
            'previous_work_position.required_if' => 'Previous Employment - Work Position field is required.',

            // Personal Reference
            'personal_ref_name.required' => 'Personal Reference - Reference Name field is required.',
            'personal_ref_relationship.required' => 'Personal Reference - Reference Relationship field is required.',
            'personal_ref_mobile.required' => 'Personal Reference -  Reference mobile field is required.',
            'personal_ref_email.required' => 'Personal Reference - Reference email field is required.',
            'personal_ref_state.required' => 'Personal Reference - Reference State field is required.',
            'personal_ref_street.required' => 'Personal Reference - Reference Street field is required.',
            'personal_ref_suburb.required' => 'Personal Reference - Reference sub field is required.',

            // Professional Reference
            'professional_ref_name.required' => 'Professional Reference - Reference name field is required.',
            'professional_ref_relationship.required' => 'Professional Reference - Reference relationship field is required.',
            'professional_ref_mobile.required' => 'Professional reference - Reference mobile field is required.',
            'professional_ref_email.required' => 'Professional reference  - Reference emnail field is required.',
        ]);
    }
    public function save_tenant_profile()
    {
        $profile = TenantProfile::whereUserId(auth()->user()->id)->first();

        $this->save_personal_information($profile);
        $this->save_emergency_contact($profile);
        $this->save_current_address($profile);
        $this->save_previous_address($profile);
        $this->save_current_employment($profile);
        $this->save_previous_employment($profile);
        $this->save_personal_reference($profile);
        $this->save_professional_reference($profile);
        $this->save_income_expenditure($profile);
        $this->save_repayments($profile);
    }

    private function save_personal_information($profile)
    {
        $profile->update([
            'title' => request('title'),
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'middle_name' => request('middle_name'),
            'dob' => request('dob'),
            'gender' => request('gender'),
            'marital_status' => request('marital_status'),
            'home_phone' => request('home_phone'),
            'work_phone' => request('work_phone'),
            'mobile' => request('mobile'),
            // 'fax' => request('fax'),
            'preferred_time_to_be_called' => request('preferred_time_to_be_called'),
            'smoke' => request('smoke'),
            'smoke_inside' => request('smoke_inside'),
            'studying' => request('studying'),
            'institution' => request('institution'),
            'enrollment_number' => request('enrollment_number'),
            'course_contact_number' => request('course_contact_number'),
            'institution_phone' => request('institution_phone'),
            'source_of_income' => request('source_of_income'),
            'net_weekly_amount' => request('net_weekly_amount'),
            'guardian_name' => request('guardian_name'),
            'guardian_relationship' => request('guardian_relationship'),
            'guardian_address' => request('guardian_address'),
            'guardian_phone_number' => request('guardian_phone_number'),
            'pension' => request('pension'),
            'pention_type' => request('pention_type'),
            'pension_number' => request('pension_number')
        ]);
    }

    private function save_emergency_contact($profile)
    {
        TenantEmergencyContact::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'emergency_contact_name' => request('emergency_contact_name'),
            'emergency_contact_relation' => request('emergency_contact_relation'),
            'emergency_contact_number' => request('emergency_contact_number'),
            'emergency_contact_state' => request('emergency_contact_state'),
            'emergency_contact_street' => request('emergency_contact_street'),
            'emergency_contact_suburb' => request('emergency_contact_suburb'),
            'emergency_contact_home_phone' => request('emergency_contact_home_phone'),
            'emergency_contact_work_phone' => request('emergency_contact_work_phone'),
            'emergency_contact_mobile' => request('emergency_contact_mobile'),
            'emergency_contact_email' => request('emergency_contact_email')
        ]);
    }

    private function save_current_address($profile)
    {

        TenantCurrentAddress::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'current_living_arrangement' => request('current_living_arrangement'),
            'current_address_unit' => request('current_address_unit'),
            'current_address_street' => request('current_address_street'),
            'current_address_street_name' => request('current_address_street_name'),
            'current_address_state' => request('current_address_state'),
            'current_address_suburb' => request('current_address_suburb'),
            'current_address_post_code' => request('current_address_post_code'),
            'current_address_move_in' => request('current_address_move_in'),
            'current_address_landlord_name' => request('current_address_landlord_name'),
            'current_address_monthly_rent' => request('current_address_monthly_rent'),
            'current_address_leave_reason' => request('current_address_leave_reason'),
            'current_address_landlord_phone' => request('current_address_landlord_phone'),
            'current_address_landlord_email' => request('current_address_landlord_email'),
            'current_address_landlord_bond_refund' => request('current_address_landlord_bond_refund')
        ]);
    }

    private function save_previous_address($profile)
    {
        TenantPreviousAddress::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'previous_living_arrangement' => request('previous_living_arrangement'),
            'previous_address_unit' => request('previous_address_unit'),
            'previous_address_street' => request('previous_address_unit'),
            'previous_address_street_name' => request('previous_address_street_name'),
            'previous_address_street_type' => request('previous_address_street_type'),
            'previous_address_state' => request('previous_address_state'),
            'previous_address_suburb' => request('previous_address_suburb'),
            'previous_address_postcode' => request('previous_address_postcode'),
            'previous_address_from' => request('previous_address_from'),
            'previous_address_to' => request('previous_address_to'),
            'previous_address_leave_reason' => request('previous_address_leave_reason'),
            'previous_address_agent_name' => request('previous_address_agent_name'),
            'previous_address_rent' => request('previous_address_rent'),
            'previous_address_email' => request('previous_address_email'),
            'previous_address_bond_refund' => request('previous_address_bond_refund'),
            'previous_address_additional_information' => request('previous_address_additional_information'),
            'previous_address_additional_information_detail' => request('previous_address_additional_information_detail')
        ]);
    }

    private function save_current_employment($profile)
    {
        TenantCurrentEmployment::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'current_employment_work_situation' => request('current_employment_work_situation'),
            'current_company_name' => request('current_company_name'),
            'current_manager_name' => request('current_manager_name'),
            'current_manager_phone' => request('current_manager_phone'),
            'current_manager_email' => request('current_manager_email'),
            'current_work_position' => request('current_work_position'),
            'current_company_state' => request('current_company_state'),
            'current_company_street' => request('current_company_street'),
            'current_company_suburb' => request('current_company_suburb'),
            'current_industry_type' => request('current_industry_type'),
            'current_employment_nature' => request('current_employment_nature'),
            'current_employment_start_date' => request('current_employment_start_date'),
            'current_job_status' => request('current_job_status'),
            'current_gross_annual_salary' => request('current_gross_annual_salary'),
        ]);
    }

    public function save_previous_employment($profile)
    {
        TenantPreviousEmployment::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'previous_employment_work_situtation' => request('previous_employment_work_situtation'),
            'previous_company_name' => request('previous_company_name'),
            'previous_manager_name' => request('previous_manager_name'),
            'previous_manager_phone' => request('previous_manager_phone'),
            'previous_manager_email' => request('previous_manager_email'),
            'previous_work_position' => request('previous_work_position'),
            'previous_company_state' => request('previous_company_state'),
            'previous_company_street' => request('previous_company_street'),
            'previous_company_suburb' => request('previous_company_suburb'),
            'previous_industry_type' => request('previous_industry_type'),
            'previous_employment_nature' => request('previous_employment_nature'),
            'previous_employment_start_date' => request('previous_employment_start_date'),
            'previous_employment_end_date' => request('previous_employment_end_date'),
            'previous_gross_annual_salary' => request('previous_gross_annual_salary')
        ]);
    }

    private function save_personal_reference($profile)
    {
        TenantPersonalReference::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'personal_ref_name' => request('personal_ref_name'),
            'personal_ref_occupation' => request('personal_ref_occupation'),
            'personal_ref_relationship' => request('personal_ref_relationship'),
            'personal_ref_phone' => request('personal_ref_phone'),
            'personal_ref_mobile' => request('personal_ref_mobile'),
            'personal_ref_email' => request('personal_ref_email'),
            'personal_ref_state' => request('personal_ref_state'),
            'personal_ref_street' => request('personal_ref_street'),
            'personal_ref_suburb' => request('personal_ref_suburb')
        ]);
    }

    private function save_professional_reference($profile)
    {
        TenantProfessionalReference::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'professional_ref_name' => request('professional_ref_name'),
            'professional_ref_company' => request('professional_ref_company'),
            'professional_ref_relationship' => request('professional_ref_relationship'),
            'professional_ref_occupation' => request('professional_ref_occupation'),
            'professional_ref_phone' => request('professional_ref_phone'),
            'professional_ref_mobile' => request('professional_ref_mobile'),
            'professional_ref_email' => request('professional_ref_email'),
            'professional_ref_company_state' => request('professional_ref_company_state'),
            'professional_ref_company_street' => request('professional_ref_company_street'),
            'professional_ref_company_suburb' => request('professional_ref_company_suburb')
        ]);
    }

    private function save_income_expenditure($profile)
    {
        TenantIncomeExpenditure::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'current_weekly_rent' => request('current_weekly_rent'),
            'vehicle_value' => request('vehicle_value'),
            'vehicle_type' => request('vehicle_type'),
            'income_type' => request('income_type'),
            'salary_wages_income' => request('salary_wages_income'),
            'self_employed_income' => request('self_employed_income'),
            'benefit_income' => request('benefit_income'),
            'other_income' => request('benefit_income')
        ]);
    }

    private function save_repayments($profile)
    {
        $repayment = TenantRepayment::updateOrCreate([
            'profile_id' => $profile->id
        ], [
            'repayment_loans' => request('repayment_loans'),
            'repayment_credit_cards' => request('repayment_credit_cards'),
            'repayment_hire_purchase' => request('repayment_hire_purchase'),
            'repayment_store_card' => request('repayment_store_card'),
            'repayment_others' => request('repayment_others'),
            'repayment_expenses' => request('repayment_expenses')
        ]);
    }
}