<?php

namespace App\Traits\PropertyManager;

trait ApplicationAttribute
{
	public function getShowButtonAttribute()
	{
		return '<a href="'.route('property-manager.applications.show', $this).'" class="btn btn-xs btn-info"><i class="fa fa-eye" title="Details"></i></a> ';
	}

	public function getActionButtonsAttribute()
	{
		return $this->show_button;
	}
}