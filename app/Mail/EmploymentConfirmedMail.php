<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmploymentConfirmedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $employment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emp)
    {
        $this->employment = $emp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $employmentConfirmed = $this->employment;
        return $this->view('emails.employment-confirmed', compact('employmentConfirmed'));
    }
}
