<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TenantRegistration extends Mailable
{
    use Queueable, SerializesModels;

    public $mails_out_subject, $mails_out_template, $mails_out_tenant;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $template, $tenant)
    {
        $this->mails_out_subject = $subject;
        $this->mails_out_template = $template;
        $this->mails_out_tenant = $tenant;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tenant = $this->mails_out_tenant;

        $agency_id = auth()->user()->agency_profile() ? auth()->user()->agency_profile()->agency_id : '';

        //$customerReference = $tenant->tenant_code != null ? $tenant->tenant_code : $tenant->id.$tenant->suburb;
        $customerReference = $tenant->tenant_code;
        $firstName = explode(' ', $tenant->name)[0] ?? '';
        $lastName = explode(' ', $tenant->name)[1] ?? '';

        $url = "https://pay.rentalrewards.com.au/Register/".$agency_id."?customerReference=".$customerReference."&title=&firstName=".$firstName."&lastName=".$lastName."&dateOfBirth=&address1=".$tenant->post_line1."&address2=".$tenant->post_line2."&suburb=".$tenant->suburb."&state=".$tenant->state."&postcode=".$tenant->post_code."&email=".$tenant->email."&mobile=".$tenant->mobile."&phone=".$tenant->phone;

        $template = $this->mails_out_template;
        return $this->from(auth()->user()->email, auth()->user()->name)->subject($this->mails_out_subject)->view('emails.tenant-register-link', compact('template', 'url'));
    }
}
