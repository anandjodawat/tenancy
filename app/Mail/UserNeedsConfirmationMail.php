<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserNeedsConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $confirmation, $unconfirmedUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($confirmation, $user)
    {
        $this->confirmation = $confirmation;
        $this->unconfirmedUser = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $confirmation_code = $this->confirmation;
        $user = $this->unconfirmedUser;
        return $this->subject('Please Verify your Email Address')->view('emails.user.user-needs-confirmation', compact('confirmation_code', 'user'));
    }
}
