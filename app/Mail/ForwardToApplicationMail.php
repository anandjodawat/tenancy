<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForwardToApplicationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $file, $tenantApplication, $send_to;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file, $id, $send_to)
    {
        $this->tenantApplication = TenantApplication::find($id);
        $this->file = $file;
        $this->send_to = $send_to;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $application = $this->tenantApplication;
        $to = $this->send_to;
        return $this->subject($application->user->name."'s' application has been forwarded to ".$this->send_to)
            ->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.property-manager.forward-to-application', compact('application', 'to'))
            ->attach($this->file->getRealPath(),
                [
                    'as' => $this->file->getClientOriginalName(),
                    'mime' => $this->file->getClientMimeType(),
                ]);
    }
}
