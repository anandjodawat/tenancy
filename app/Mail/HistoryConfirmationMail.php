<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HistoryConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $sender, $body, $subject, $file, $user, $property_manager;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $body, $subject, $file, $user, $property_manager)
    {
        $this->user = $user;
        $this->sender = $sender;
        $this->body = $body;
        $this->subject = $subject;
        $this->file = $file;
        $this->property_manager = $property_manager;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = $this->body;
        $send_to = $this->sender;
        $user_id = $this->user;
        $manager_email = $this->property_manager;
        return $this->subject($this->subject)
            ->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.property-manager.history-confirmation', compact('body', 'send_to', 'user_id', 'manager_email'))
            ->attach($this->file->getRealPath(),
                [
                    'as' => $this->file->getClientOriginalName(),
                    'mime' => $this->file->getClientMimeType(),
                ]);
    }
}
