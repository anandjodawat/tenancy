<?php

namespace App\Mail\Application;

use App\Models\Agency;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;

class MovingServiceMail extends Mailable
{
    use Queueable, SerializesModels;

    public $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($application_id)
    {
        $this->application = TenantApplication::find($application_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $agency = Agency::whereName($this->application->agency_name)->first();
        $agency_logo = $agency && $agency->getFirstMedia('agency_logos') ? $agency->getFirstMedia('agency_logos')->getUrl() : false;
        $tenantApplication = $this->application;
        return $this->from(env('MAIL_FROM_ADDRESS'))
                    ->view('emails.user.moving-service-email', compact('agency_logo', 'tenantApplication'));
    }
}
