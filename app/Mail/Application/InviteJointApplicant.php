<?php

namespace App\Mail\Application;

use App\Models\Agency;
use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Access\User\User;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;
// API
use App\API\UserApplication;
use App\API\Utility;

class InviteJointApplicant extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $application_id, $joint_occupant_name)
    {
        $this->user = $user;
        $this->application = TenantApplication::find($application_id);
        $this->joint_occupant_name = $joint_occupant_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $agency = Agency::whereName($this->application->agency_name)->first();
        $agency_logo = $agency && $agency->getFirstMedia('agency_logos') ? $agency->getFirstMedia('agency_logos')->getUrl() : false;
        $tenantApplication = $this->application;
        $user_name = $this->user->name;
        $joint_occupant_name = $this->joint_occupant_name;

        if ($agency) {
            $setting_check = Setting::where('agency_id', $agency->id)->first();
            if ($setting_check && $setting_check->key == '0') {
                if ($this->application->need_moving_service == 'yes') {
                    $utilityApi = new Utility($this->application);

                    $data = $utilityApi->getData();

                    $sentData = $utilityApi->sendData($data);
                }
            }
        }

        return $this->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.invite-joint-applicant', compact('agency_logo', 'tenantApplication', 'user_name', 'joint_occupant_name'));
    }
}
