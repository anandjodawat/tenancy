<?php

namespace App\Mail\Application\PropertyManager;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Tenant\TenantProfile;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Application\ApplicationMovingService;

class ApplicationStatusChange extends Mailable
{
    use Queueable, SerializesModels;

    public $status;
    public $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($status, $application_id, $sendto)
    {
        $this->status = $status;
        $this->application = TenantApplication::find($application_id);
        $this->sendto = $sendto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $status = $this->status;
        $application = $this->application;
        $sendto = $this->sendto;
        $application_id = $application->id;
        $profile = TenantProfile::whereUserId($application->user_id)->first();

        $connectionServices = ApplicationMovingService::Where('application_id', $application->id)->get();
        
        $allservices = '';
        if ( !empty($connectionServices) ) {
            
            foreach ( $connectionServices as $connectionService ) {
                
                $allservices .= ApplicationMovingService::MOVINGSERVICEOPTION[$connectionService->moving_service_id].', ';
            }
            $allservices = rtrim($allservices, ', ');
            
        }

        return $this->from(env('MAIL_FROM_ADDRESS'))
            ->view('emails.applicationstatuschange', compact('status', 'application', 'profile', 'application_id', 'allservices', 'sendto'));
    }
}
