<?php

namespace App\Mail\Application\PropertyManager;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Application\EmailApplication;

class EmailApplicationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email_application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_application_id)
    {
        $this->email_application = EmailApplication::find($email_application_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailApplication = $this->email_application;
        return $this->from(auth()->user()->email, auth()->user()->name)->subject($emailApplication->subject)->view('emails.property-manager.email-application-notification', compact('emailApplication'));
    }
}
