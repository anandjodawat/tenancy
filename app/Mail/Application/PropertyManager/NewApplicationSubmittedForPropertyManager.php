<?php

namespace App\Mail\Application\PropertyManager;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Access\User\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewApplicationSubmittedForPropertyManager extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $property_address, $property_manager_email, $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $application)
    {
        $this->user = $user;
        $this->property_address = $application->applied_for;
        $this->property_manager_email = $application->property_manager_email;
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $property_address = $this->property_address;
        $property_manager_email = $this->property_manager_email;
        $newApplication = $this->application->id;

        $property_manager = User::whereEmail($property_manager_email)->first();

        return $this->from(auth()->user()->email, auth()->user()->name)
            ->subject('New Application Recieved ')
            ->view('emails.property-manager.new-application-created', compact('user', 'property_address', 'property_manager', 'newApplication'));
    }
}
