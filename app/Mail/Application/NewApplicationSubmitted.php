<?php

namespace App\Mail\Application;

use App\Models\Agency;
use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Access\User\User;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;
// API
use App\API\UserApplication;
use App\API\Utility;

class NewApplicationSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $application_id)
    {
        $this->user = $user;
        $this->application = TenantApplication::find($application_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->application->bond_assistance == 'yes') {
            $apiApplication = new UserApplication($this->application);

            $apiData = $apiApplication->getData();

            $sentData = $apiApplication->sendData($apiData);

            if ($sentData != null) {
                $bondsure_url = $sentData->customer_url;
            }
        }
        if(isset($bondsure_url))
        {
            TenantApplication::find($this->application->id)->update(['bondsure_url'=>$bondsure_url]);
        }
        $agency = Agency::whereName($this->application->agency_name)->first();
        $agency_logo = $agency && $agency->getFirstMedia('agency_logos') ? $agency->getFirstMedia('agency_logos')->getUrl() : false;
        $tenantApplication = $this->application;

        if ($agency) {
            $setting_check = Setting::where('agency_id', $agency->id)->first();
            if ($setting_check && $setting_check->key == '0') {
                if ($this->application->need_moving_service == 'yes') {
                    $utilityApi = new Utility($this->application);

                    $data = $utilityApi->getData();

                    $sentData = $utilityApi->sendData($data);
                }
            }
        }

        return $this->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.new-application-created', compact('agency_logo', 'bondsure_url', 'tenantApplication'));
    }
}
