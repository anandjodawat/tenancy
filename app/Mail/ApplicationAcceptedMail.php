<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicationAcceptedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $tenantApplication;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->tenantApplication = TenantApplication::find($id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $application = $this->tenantApplication;

        $customerReference = $application->applied_for.$application->id;
        $mobile = $application->user->tenantProfile ? $application->user->tenantProfile->mobile : '';
        
        /*$belongingAgency =$deposit->sentBy->belongingAgency;
        $agency_id = $belongingAgency->agency_id;*/
        $agency_id = auth()->user()->agency_profile() ? auth()->user()->agency_profile()->agency_id : ''; 

        $url = "https://pay.rentalrewards.com.au/".$agency_id."?CustomerReference=".$customerReference."&PaymentAmount=".$application->monthly_rent."&CustomerName=".$application->user->name."&ContactNumber=".$mobile."&CustomerEmail=".$application->user->email;

        return $this->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.property-manager.application-accepted-deposit-request', compact('application', 'url'));
    }
}
