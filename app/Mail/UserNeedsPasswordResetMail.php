<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserNeedsPasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $password_token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->password_token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $token = $this->password_token;
        return $this->subject('Tenancy Application: Reset Password')->view('emails.user.user-password-reset', compact('token'));
    }
}
