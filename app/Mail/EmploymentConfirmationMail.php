<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmploymentConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $sender, $body, $subject, $file, $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $body, $subject, $file, $user)
    {
        $this->user = $user;
        $this->sender = $sender;
        $this->body = $body;
        $this->subject = $subject;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = $this->body;
        $send_to = $this->sender;
        $user_id = $this->user;
        $manager_email = auth()->user()->email;

        return $this->subject($this->subject)
            ->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.property-manager.employment-confirmation', compact('body', 'send_to', 'user_id', 'manager_email'))
            ->attach($this->file->getRealPath(),
                [
                    'as' => $this->file->getClientOriginalName(),
                    'mime' => $this->file->getClientMimeType(),
                ]);
    }
}
