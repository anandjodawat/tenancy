<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\BondRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BondRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $bondRequest, $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file, $id, $efund_link)
    {
        $this->file = $file;
        $this->bondRequest = BondRequest::find($id);
        $this->efund_link = $efund_link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $payment = $this->bondRequest;

        //echo "<pre>"; print_r($payment); die;

        $payment->update([
            'email_status' => 1
        ]);

        $bondRequest = $payment;

        $easyfund_link = $this->efund_link;

        $customerReference = $bondRequest->tenant_code;

        $belongingAgency =$bondRequest->sentBy->belongingAgency;
        $agency_id = $belongingAgency->agency_id;

        $tenant_name = $bondRequest->first_name;

        $address = $bondRequest->address . ", " . $bondRequest->suburb . ", " . config('tenancy-application.states')[$bondRequest->state] . ", " . $bondRequest->postcode;

        $url = "https://pay.rentalrewards.com.au/Register/".$agency_id."?customerReference=".$customerReference."&title=".$bondRequest->title."&firstName=".$bondRequest->first_name."&lastName=".$bondRequest->last_name."&dateOfBirth=".$bondRequest->dob."&address1=".$bondRequest->address."&address2=&suburb=".$bondRequest->suburb."&state=".config('tenancy-application.states')[$bondRequest->state]."&postcode=".$bondRequest->postcode."&email=".$bondRequest->email."&mobile=".$bondRequest->mobile_number."&phone=".$bondRequest->phone_number;

        if ($this->file) {
            $email =  $this->from(auth()->user()->email, auth()->user()->name)
            ->subject($bondRequest->subject)->view('emails.property-manager.bond-request', compact('payment', 'url', 'address', 'tenant_name', 'easyfund_link'));

            //foreach($this->file as $fileParameters){

                $email->attach($this->file->getRealPath(), [
                    'as' => $this->file->getClientOriginalName(),
                    'mime' => $this->file->getClientMimeType(),
                ]);
            //}
            return $email;

        } else {
            return $this->from(auth()->user()->email, auth()->user()->name)
            ->subject($bondRequest->subject)->view('emails.property-manager.bond-request', compact('payment', 'url', 'address', 'tenant_name', 'easyfund_link'));
        }
    }
}
