<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Tenant\TenantApplication;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicationApprovedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $tenantApplication;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->tenantApplication = TenantApplication::find($id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $application = $this->tenantApplication;

        $customerReference = $application->id.$application->suburb;

        $title = $application->user->tenantProfile->tenant_title;

        $dob = $application->user->tenantProfile->dob;
        
        /*$belongingAgency =$deposit->sentBy->belongingAgency;
        $agency_id = $belongingAgency->agency_id;*/
        $agency_id = auth()->user()->agency_profile() ? auth()->user()->agency_profile()->agency_id : ''; 
        
        $url = "https://pay.rentalrewards.com.au/Register/".$agency_id."?customerReference=".$customerReference."&title=".$title."&firstName=".$application->user->first_name."&lastName=".$application->user->last_name."&dateOfBirth=".$dob."&address1=".$application->applied_for."&address2=&suburb=".$application->suburb."&state=".config('tenancy-application.states')[$application->state]."&postcode=".$application->post_code."&email=".$application->user->email."&mobile=".$application->user->tenantProfile->mobile."&phone=".$application->user->tenantProfile->home_phone;

        return $this->from(auth()->user()->email, auth()->user()->name)
            ->view('emails.property-manager.application-approved-deposit-request', compact('application', 'url'));
    }
}
