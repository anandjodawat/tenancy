<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Agency\Property;
use App\Models\PropertyInvitation;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PropertyInvitationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $propertyInvitation, $property;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invitation_id)
    {
        $this->propertyInvitation = PropertyInvitation::find($invitation_id);
        $this->property = Property::find($this->propertyInvitation->address_for);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $invitation = $this->propertyInvitation;

        //echo "<pre>"; print_r($invitation);
        $address = $this->property;
        //echo "<pre>"; print_r($address);

        $url = route('frontend.auth.login', ['applied_for' => $invitation->property->address, 'suburb' => $invitation->property->suburb, 'state' => $invitation->property->state, 'post_code' => $invitation->property->post_code, 'weekly_rent' => $invitation->property->rent_amount, 'agency_name' => auth()->user()->agency_profile()->name, 'property_manager_name' => auth()->user()->name, 'property_manager_email' => auth()->user()->email]);
        return $this->from(auth()->user()->email, auth()->user()->name)
            ->subject('You Are Invited To Apply For Property')
            ->view('emails.property-manager.property-invitation', compact('invitation', 'address', 'url'));
    }
}
