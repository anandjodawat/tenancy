<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\DepositSale;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DepositSalesMail extends Mailable
{
    use Queueable, SerializesModels;

    public $DepositSales;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request_deposit_id)
    {
        $this->DepositSales = DepositSale::find($request_deposit_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $deposit = $this->DepositSales;

        $deposit->update([
            'email_status' => 1
        ]);

        $customerReference = $deposit->address.$deposit->id;
        $belongingAgency =$deposit->sentBy->belongingAgency;
        $agency_id = $belongingAgency->agency_id;

        $url = "https://pay.rentalrewards.com.au/".$agency_id."?CustomerReference=".$customerReference."&PaymentAmount=".$deposit->payment_amount."&CustomerName=".$deposit->name."&ContactNumber=".$deposit->contact_number."&CustomerEmail=".$deposit->send_to;

        return $this->view('emails.property-manager.send-deposit-sales', compact('deposit', 'url'));
    }
}
