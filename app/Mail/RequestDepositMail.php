<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\RequestDeposit;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestDepositMail extends Mailable
{
    use Queueable, SerializesModels;

    public $requestDeposit, $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request_deposit_id, $file)
    {
        $this->file = $file;
        $this->requestDeposit = RequestDeposit::find($request_deposit_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $deposit = $this->requestDeposit;

        $deposit->update([
            'email_status' => 1
        ]);

        $customer_address = $deposit->address;
        $customerReference = $deposit->tenant_code; //$deposit->address.$deposit->id;
        $belongingAgency =$deposit->sentBy->belongingAgency;
        $agency_id = $belongingAgency->agency_id;
        $tenant_name = $deposit->name;
        $amount = $deposit->payment_amount;
        $tenant_code = $customerReference;

        $url = "https://pay.rentalrewards.com.au/".$agency_id."?CustomerReference=".$customerReference."&PaymentAmount=".$deposit->payment_amount."&CustomerName=".$deposit->name."&ContactNumber=".$deposit->contact_number."&CustomerEmail=".$deposit->send_to;

        if ($this->file) {
            $email = $this->from(auth()->user()->email, auth()->user()->name)
            ->subject($deposit->subject)->view('emails.property-manager.send-request-deposit', compact('deposit', 'url', 'customer_address', 'tenant_code', 'tenant_name', 'amount'));
            /*->attach($this->file->getRealPath(),
                [
                    'as' => $this->file->getClientOriginalName(),
                    'mime' => $this->file->getClientMimeType(),
                ]);*/

            foreach($this->file as $fileParameters) {
                $email->attach($fileParameters->getRealPath(), [
                    'as' => $fileParameters->getClientOriginalName(),
                    'mime' => $fileParameters->getClientMimeType(),
                ]);
            }

            return $email;

        } else {
            return $this->from(auth()->user()->email, auth()->user()->name)
            ->subject($deposit->subject)->view('emails.property-manager.send-request-deposit', compact('deposit', 'url', 'customer_address', 'tenant_code', 'tenant_name', 'amount'));
        }
    }
}
