<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\RequestOngoingPayment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OngoingRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $ongoingPayment, $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($file, $id)
    {
        $this->file = $file;
        $this->ongoingPayment = RequestOngoingPayment::find($id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $payment = $this->ongoingPayment;

        //echo "<pre>"; print_r($payment); die;

        $payment->update([
            'email_status' => 1
        ]);

        $ongoingPayment = $payment;

        $customerReference = $ongoingPayment->tenant_code;

        $belongingAgency =$ongoingPayment->sentBy->belongingAgency;
        $agency_id = $belongingAgency->agency_id;
        $tenant_name = $ongoingPayment->first_name;

        $address = $ongoingPayment->address . ", " . $ongoingPayment->suburb . ", " . config('tenancy-application.states')[$ongoingPayment->state] . ", " . $ongoingPayment->postcode;

        $url = "https://pay.rentalrewards.com.au/Register/".$agency_id."?customerReference=".$customerReference."&title=".$ongoingPayment->title."&firstName=".$ongoingPayment->first_name."&lastName=".$ongoingPayment->last_name."&dateOfBirth=".$ongoingPayment->dob."&address1=".$ongoingPayment->address."&address2=&suburb=".$ongoingPayment->suburb."&state=".config('tenancy-application.states')[$ongoingPayment->state]."&postcode=".$ongoingPayment->postcode."&email=".$ongoingPayment->email."&mobile=".$ongoingPayment->mobile_number."&phone=".$ongoingPayment->phone_number;

        if ($this->file) {
            $email =  $this->from(auth()->user()->email, auth()->user()->name)
            ->subject($ongoingPayment->subject)->view('emails.property-manager.ongoing-payment-request', compact('payment', 'url', 'address', 'tenant_name'));

            foreach($this->file as $fileParameters){

                $email->attach($fileParameters->getRealPath(), [
                    'as' => $fileParameters->getClientOriginalName(),
                    'mime' => $fileParameters->getClientMimeType(),
                ]);
            }

            return $email;

        } else {
            return $this->from(auth()->user()->email, auth()->user()->name)
            ->subject($ongoingPayment->subject)->view('emails.property-manager.ongoing-payment-request', compact('payment', 'url', 'address', 'tenant_name'));
        }
    }
}
