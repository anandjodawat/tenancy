<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HistoryConfirmedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $history;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($history)
    {
        $this->history = $history;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $historyConfirmed = $this->history;
        return $this->view('emails.history-confirmed', compact('historyConfirmed'));
    }
}
