<?php

namespace App\Mail\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Access\User\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        return $this->view('emails.user-confirmed', compact('user'));
    }
}
