<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MaintenanceRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $maintenance;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($maintenance)
    {
        $this->maintenance = $maintenance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $maintenanceRequest = $this->maintenance;
        return $this->from(auth()->user()->email, auth()->user()->name)->view('emails.property-manager.maintenance-request', compact('maintenanceRequest'));
    }
}
