<?php

namespace App\Listeners\Consultancy\Student;

class StudentListEventListener
{
    /**
     * @var string
     */
    private $history_slug = 'Student_list';

    /**
     * @param $event
     */
    public function onCreated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->studentList->id)
            ->withText('added <strong>'.$event->studentList->student->first_name.' to '.$event->studentList->studentList->title.' List.</strong>')
            ->withIcon('plus')
            ->withClass('bg-green')
            ->log();
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->studentList->id)
            ->withText('updated <strong>'.$event->studentList->student_id.'</strong> <strong> to '.$event->studentList->student_lists_id.' List.</strong>')
            ->withIcon('save')
            ->withClass('bg-aqua')
            ->log();
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->studentList->id)
            ->withText('deleted <strong>'.$event->studentList->student->first_name.' from '.$event->studentList->studentList->title.' List. </strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->log();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Student\StudentListCreated::class,
            'App\Listeners\Consultancy\Student\StudentListEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Student\StudentListUpdated::class,
            'App\Listeners\Consultancy\Student\StudentListEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Student\StudentListDeleted::class,
            'App\Listeners\Consultancy\Student\StudentListEventListener@onDeleted'
        );
    }
}
