<?php

namespace App\Listeners\Consultancy\Student;

class StudentEventListener
{
    /**
     * @var string
     */
    private $history_slug = 'Student';

    /**
     * @param $event
     */
    public function onCreated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->student->id)
            ->withText('created <strong>'.$event->student->first_name.'</strong>')
            ->withIcon('plus')
            ->withClass('bg-green')
            ->log();
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->student->id)
            ->withText('updated <strong>'.$event->student->first_name.'</strong>')
            ->withIcon('save')
            ->withClass('bg-aqua')
            ->log();
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->student->id)
            ->withText('deleted <strong>'.$event->student->first_name.'</strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->log();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Student\StudentCreated::class,
            'App\Listeners\Consultancy\Student\StudentEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Student\StudentUpdated::class,
            'App\Listeners\Consultancy\Student\StudentEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Student\StudentDeleted::class,
            'App\Listeners\Consultancy\Student\StudentEventListener@onDeleted'
        );
    }
}
