<?php

namespace App\Events\Backend\Consultancy;

use Illuminate\Queue\SerializesModels;

/**
 * Class ConsultancyUpdated.
 */
class ConsultancyUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $consultancy;

    /**
     * @param $consultancy
     */
    public function __construct($consultancy)
    {
        $this->consultancy = $consultancy;
    }
}
