<?php

namespace App\Events\Backend\Student;

use Illuminate\Queue\SerializesModels;

/**
 * Class StudentListCreated.
 */
class StudentListCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $studentList;

    /**
     * @param $studentList
     */
    public function __construct($studentList)
    {
        $this->studentList = $studentList;
    }
}
