<?php

namespace App\Events\Backend\Student;

use Illuminate\Queue\SerializesModels;

/**
 * Class StudentListUpdated.
 */
class StudentListUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $studentList;

    /**
     * @param $studentList
     */
    public function __construct($studentList)
    {
        $this->studentList = $studentList;
    }
}
