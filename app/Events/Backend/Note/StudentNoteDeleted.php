<?php

namespace App\Events\Backend\Note;

use Illuminate\Queue\SerializesModels;

/**
 * Class StudentNoteDeleted.
 */
class StudentNoteDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $student_note;

    /**
     * @param $student_note
     */
    public function __construct($student_note)
    {
        $this->student_note = $student_note;
    }
}
