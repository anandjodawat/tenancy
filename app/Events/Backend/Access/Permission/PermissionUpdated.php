<?php

namespace App\Events\Backend\Access\Permission;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PermissionUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $permission;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($permission)
    {
        $this->permission = $permission;
    }
}
