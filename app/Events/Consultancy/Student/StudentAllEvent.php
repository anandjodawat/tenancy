<?php

namespace App\Events\Consultancy\Student;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use App\Models\Consultancy\Student\Student;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StudentAllEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $student;
    public $action;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Student $student, $action)
    {
        $this->student = $student;
        $this->action = $action;
    }
}
