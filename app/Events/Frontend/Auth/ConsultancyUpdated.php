<?php

namespace App\Events\Frontend\Auth;

use Illuminate\Queue\SerializesModels;

/**
 * Class ConsultancyUpdated.
 */
class ConsultancyUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $consultancy;

    /**
     * @param $consultancy
     */
    public function __construct($consultancy)
    {
        $this->consultancy = $consultancy;
    }
}
