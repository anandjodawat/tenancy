<?php

namespace App\Repositories\Consultancy\Note;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Consultancy\Note\StudentNote;
use App\Events\Backend\Note\StudentNoteCreated;
use App\Events\Backend\Note\StudentNoteUpdated;
use App\Events\Backend\Note\StudentNoteDeleted;

/**
 * Class StudentNoteRepository.
 */
class StudentNoteRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = StudentNote::class;
   
    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($trashed, $student_id)
    {
        $dataTableQuery = $this->query()
            ->select([
                config('student_note.student_note_table').'.id',
                config('student_note.student_note_table').'.student_id',
                config('student_note.student_note_table').'.user_id',
                config('student_note.student_note_table').'.note',
                config('student_note.student_note_table').'.created_at',
                config('student_note.student_note_table').'.updated_at',
            ])
            ->where('student_id', $student_id);

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        return $dataTableQuery->get();
    }
    
    /**
     * @param array $input
     */
    public function create($input)
    {
        $data = $input['data'];

        $student_note = $this->createStudentNoteStub($data);

        DB::transaction(function () use ($student_note, $data) {
            if ($student_note->save()) {

                event(new StudentNoteCreated($student_note));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.student_note.create_error'));
        });
    }

    /**
     * @param Model $student_note
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $student_note, array $input)
    {
        $data = $input['data'];

        $student_note->note = $data['note'];

        DB::transaction(function () use ($student_note, $data) {
            if ($student_note->save()) {

                event(new StudentNoteUpdated($student_note));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.student_note.update_error'));
        });
    }

     /**
     * @param Model $student_note
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Model $student_note)
    {
        if ($student_note->delete()) {
            event(new StudentNoteDeleted($student_note));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.student_note.delete_error'));
    }

    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createStudentNoteStub($input)
    {
        $student_note = self::MODEL;
        
        $student_note = new $student_note;

        $student_note->student_id = $input['student_id'];
        $student_note->user_id = $input['user_id'];
        $student_note->note = $input['note'];

        return $student_note;
    }
}
