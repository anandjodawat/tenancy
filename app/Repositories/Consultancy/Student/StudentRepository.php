<?php

namespace App\Repositories\Consultancy\Student;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\Consultancy\Student\Student;
use App\Events\Backend\Student\StudentCreated;
use App\Events\Backend\Student\StudentUpdated;
use App\Events\Backend\Student\StudentDeleted;

/**
 * Class StudentRepository.
 */
class StudentRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Student::class;
   
    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
     public function getForDataTable($trashed, $consultancy_id)
    {
        $dataTableQuery = $this->query()
            ->select([
                config('student.students_table').'.id',
                config('student.students_table').'.first_name',
                config('student.students_table').'.middle_name',
                config('student.students_table').'.last_name',
                config('student.students_table').'.dob',
                config('student.students_table').'.gender',
                config('student.students_table').'.phone',
                config('student.students_table').'.mobile',
                config('student.students_table').'.address',
                config('student.students_table').'.district',
                config('student.students_table').'.email',
                config('student.students_table').'.remarks',
                config('student.students_table').'.consultancy_id',
                config('student.students_table').'.user_id',
            ])
            ->where('consultancy_id', $consultancy_id)
            ->orderBy('id', 'desc');

        return $dataTableQuery->get();
    }
    
    /**
     * @param array $input
     */
    public function create($input)
    {
        $data = $input['data'];

        $student = $this->createStudentStub($data);

        DB::transaction(function () use ($student, $data) {
            if ($student->save()) {

                event(new StudentCreated($student));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.student.create_error'));
        });
    }

    /**
     * @param Model $student
     * @param array $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $student, array $input)
    {
        $data = $input['data'];

        $this->checkStudentByEmail($data, $student);

        $student->first_name = $data['first_name'];
        $student->middle_name = $data['middle_name'];
        $student->last_name = $data['last_name'];
        $student->dob = $data['dob'];
        $student->gender = $data['gender'];
        $student->phone = $data['phone'];
        $student->mobile = $data['mobile'];
        $student->address = $data['address'];
        $student->district = $data['district'];
        $student->email = $data['email'];
        $student->remarks = $data['remarks'];

        DB::transaction(function () use ($student, $data) {
            if ($student->save()) {

                event(new StudentUpdated($student));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.student.update_error'));
        });
    }

     /**
     * @param Model $student
     *
     * @throws GeneralException
     *
     * @return bool
     */

    public function delete(Model $student)
    {
        if ($student->delete()) {
            event(new StudentDeleted($student));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.student.delete_error'));
    }

    /**
     * @param  $input
     * @param  $student
     *
     * @throws GeneralException
     */
    protected function checkStudentByEmail($input, $student)
    {
        //Figure out if email is not the same
        if ($student->email != $input['email']) {
            //Check to see if email exists
            if ($this->query()->where('email', '=', $input['email'])->first()) {
                throw new GeneralException(trans('exceptions.backend.student.email_error'));
            }
        }
    }

    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createStudentStub($input)
    {
        $student = self::MODEL;
        
        $student = new $student;
        $student->first_name = $input['first_name'];
        $student->middle_name = $input['middle_name'];
        $student->last_name = $input['last_name'];
        $student->dob = $input['dob'];
        $student->gender = $input['gender'];
        $student->phone = $input['phone'];
        $student->mobile = $input['mobile'];
        $student->address = $input['address'];
        $student->district = $input['district'];
        $student->email = $input['email'];
        $student->remarks = $input['remarks'];
        $student->consultancy_id = $input['consultancy_id'];
        $student->user_id = $input['user_id'];

        return $student;
    }
}
