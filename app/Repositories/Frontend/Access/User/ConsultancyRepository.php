<?php

namespace App\Repositories\Frontend\Access\User;

use App\Models\Access\User\User;

use App\Models\Consultancy;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

use App\Models\Access\User\SocialLogin;
use App\Events\Frontend\Auth\UserConfirmed;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Events\Frontend\Auth\ConsultancyUpdated;
use App\Events\Frontend\Auth\ConsultancyCreated;


/**
 * Class ConsultancyRepository.
 */
class ConsultancyRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Consultancy::class;

    /**
     * @param array $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.consultancy.already_exists'));
        }

        DB::transaction(function () use ($input) {
            $consultancy = $this->createConsultancyStub($input);
            
            if ($consultancy->save()) {

                event(new ConsultancyCreated($consultancy));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.consultancy.create_error'));
        });
    }

    /**
     * @param Model $consultancy
     * @param  $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function update(Model $consultancy, array $input)
    {
        $data = $input['data'];

        $this->checkConsultancyByEmail($data, $consultancy);

        $consultancy->name = $data['name'];
        $consultancy->address = $data['address'];
        $consultancy->district = $data['district'];
        $consultancy->phone = $data['phone'];
        $consultancy->website = $data['website'];
        $consultancy->registration_no = $data['registration_no'];
        $consultancy->email = $data['email'];
        $consultancy->mobile = $data['mobile'];

        DB::transaction(function () use ($consultancy, $data) {
            if ($consultancy->save()) {

                event(new ConsultancyUpdated($consultancy));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.consultancy.update_error'));
        });
    }
     
    /**
     * @param  $input
     *
     * @return mixed
     */
    public function createConsultancyStub($input)
    {
        $consultancy = self::MODEL;
        $consultancy = new $consultancy;
        $consultancy->name = $input['name'];
        $consultancy->address = $input['address'];
        $consultancy->district = $input['district'];
        $consultancy->phone = $input['phone'];
        $consultancy->website = $input['website'];
        $consultancy->registration_no = $input['registration_no'];
        $consultancy->email = $input['email'];
        $consultancy->mobile = $input['mobile'];

        return $consultancy;
    }

    protected function checkConsultancyByEmail($input, $consultancy)
    {
        //Figure out if email is not the same
        if ($consultancy->email != $input['email']) {
            //Check to see if email exists
            if ($this->query()->where('email', '=', $input['email'])->first()) {
                throw new GeneralException(trans('exceptions.backend.consultancy.email_error'));
            }
        }
    }
}