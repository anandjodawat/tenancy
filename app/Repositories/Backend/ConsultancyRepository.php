<?php

namespace App\Repositories\Backend;

use App\Models\Consultancy;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Events\Backend\Consultancy\ConsultancyCreated;
use App\Events\Backend\Consultancy\ConsultancyDeleted;
use App\Events\Backend\Consultancy\ConsultancyUpdated;

/**
 * Class ConsultancyRepository.
 */
class ConsultancyRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Consultancy::class;

    /**
     * @param array $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.consultancy.already_exists'));
        }

        DB::transaction(function () use ($input) {
            $consultancy = $this->createConsultancyStub($input);
            
            if ($consultancy->save()) {

                event(new ConsultancyCreated($consultancy));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.consultancy.create_error'));
        });
    }

    /**
     * @param Model $consultancy
     * @param  $input
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function update(Model $consultancy, array $input)
    {
        $data = $input['data'];

        $this->checkConsultancyByEmail($data, $consultancy);

        $consultancy->name = $data['name'];
        $consultancy->address = $data['address'];
        $consultancy->district = $data['district'];
        $consultancy->phone = $data['phone'];
        $consultancy->website = $data['website'];
        $consultancy->registration_no = $data['registration_no'];
        $consultancy->email = $data['email'];
        $consultancy->mobile = $data['mobile'];
        $consultancy->status = $data['status'];
        $consultancy->verified = isset($data['verified']) ? 1 : 0;

        DB::transaction(function () use ($consultancy, $data) {
            if ($consultancy->save()) {
                /*$this->checkUserRolesCount($roles);
                $this->flushRoles($roles, $user);*/

                event(new ConsultancyUpdated($consultancy));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.consultancy.update_error'));
        });
    }

    public function delete(Model $consultancy)
    {
        if ($consultancy->delete()) {
            event(new ConsultancyDeleted($consultancy));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

    /**
     * @param int  $status
     * @param bool $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $request_status)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->query()
            // ->with('roles')
            ->select([
                'id',
                'name',
                'address',
                'district',
                'phone',
                'website',
                'registration_no',
                'email',
                'mobile',
                'status',
                'verified'
            ]);

        // if ($trashed == 'true') {
        //     return $dataTableQuery->onlyTrashed();
        // }
        if (!$request_status) {
            return $dataTableQuery->get();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->where('status', $request_status)->get();
    }

    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createConsultancyStub($input)
    {
        $consultancy = self::MODEL;
        $consultancy = new $consultancy;
        $consultancy->name = $input['name'];
        $consultancy->address = $input['address'];
        $consultancy->district = $input['district'];
        $consultancy->phone = $input['phone'];
        $consultancy->website = $input['website'];
        $consultancy->registration_no = $input['registration_no'];
        $consultancy->email = $input['email'];
        $consultancy->mobile = $input['mobile'];
        $consultancy->status = $input['status'];
        $consultancy->verified = isset($input['verified']) ? 1 : 0;

        return $consultancy;
    }

    protected function checkConsultancyByEmail($input, $consultancy)
    {
        //Figure out if email is not the same
        if ($consultancy->email != $input['email']) {
            //Check to see if email exists
            if ($this->query()->where('email', '=', $input['email'])->first()) {
                throw new GeneralException(trans('exceptions.backend.consultancy.email_error'));
            }
        }
    }
}
