<?php

namespace App\Policies;

use App\Models\Access\User\User;
use App\Models\Tenant\TenantApplication;
use Illuminate\Auth\Access\HandlesAuthorization;

class TenantApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the tenantApplication.
     *
     * @param  \App\Models\Access\User\User  $user
     * @param  \App\Models\Tenant\TenantApplication  $tenantApplication
     * @return mixed
     */
    public function view(User $user, TenantApplication $tenantApplication)
    {
         return auth()->user()->id == $tenantApplication->user_id;
    }

    /**
     * Determine whether the user can create tenantApplications.
     *
     * @param  \App\Models\Access\User\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the tenantApplication.
     *
     * @param  \App\Models\Access\User\User  $user
     * @param  \App\Models\Tenant\TenantApplication  $tenantApplication
     * @return mixed
     */
    public function update(User $user, TenantApplication $tenantApplication)
    {
        return auth()->user()->id == $tenantApplication->user_id;
    }

    /**
     * Determine whether the user can delete the tenantApplication.
     *
     * @param  \App\Models\Access\User\User  $user
     * @param  \App\Models\Tenant\TenantApplication  $tenantApplication
     * @return mixed
     */
    public function delete(User $user, TenantApplication $tenantApplication)
    {
        return auth()->user()->id == $tenantApplication->user_id;
    }
}
