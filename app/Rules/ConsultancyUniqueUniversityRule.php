<?php

namespace App\Rules;

use Auth;
use App\Models\Consultancy\University;
use Illuminate\Contracts\Validation\Rule;
use App\Models\Consultancy\ConsultancyUniversity;

class ConsultancyUniqueUniversityRule implements Rule
{
    public $message;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_numeric($value)) {
            return true;
        }
        $university = University::find($value);
        if ($university == null) {
            $this->message = "This university is not in our system.";
            return false;
        }
        $this->message = $university->name.' is already on our system.';
        return !ConsultancyUniversity::where('university_id', $value)
            ->where('consultancy_id', Auth::user()->consultancy_id)
            ->count() > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
