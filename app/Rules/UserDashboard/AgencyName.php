<?php

namespace App\Rules\UserDashboard;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Agency;
class AgencyName implements Rule
{
        public $message;
        public $result;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        
        if (!Agency::whereName($value)->first()) {
            
            return false;
        }
       return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Agency Does not exists.';
    }
}
