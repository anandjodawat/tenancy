<?php

namespace App\Rules;

use Auth;
use App\Models\Country\Country;
use Illuminate\Contracts\Validation\Rule;
use App\Models\Consultancy\ConsultancyCountry;

class ConsultancyUniqueCountryRule implements Rule
{
    public $message;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (Country::find($value) == null) {
            $this->message = "You have to choose the available countries from the list.";
            return false;
        }
        $result = ConsultancyCountry::whereConsultancyId(Auth::user()->consultancy_id)
            ->whereCountryId($value)
            ->get();

        $this->message = "Country".Country::find($value)->name."is already in your list.";
        return $result->count() == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
