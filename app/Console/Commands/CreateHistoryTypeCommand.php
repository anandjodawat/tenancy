<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\History\HistoryType;

class CreateHistoryTypeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:history-types {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creats a history type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        HistoryType::create([
            'name' => $this->argument('name'),
        ]);

        echo "history type ".$this->argument('name')." created.";
    }
}
