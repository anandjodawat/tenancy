<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBondRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bond_request', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('title');
            $table->string('tenant_code')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dob')->nullable();
            $table->string('address');
            $table->string('suburb');
            $table->integer('state');
            $table->string('postcode');
            $table->string('mobile_number');
            $table->string('phone_number');
            $table->string('email');
            $table->string('subject');
            $table->longText('body');
            $table->integer('sent_by')->unsigned()->nullable();
            $table->integer('email_status')->default(0);

            $table->timestamps();

            $table->foreign('sent_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bond_request', function ( Blueprint $table ) {
            $table->dropForeign('bond_request_sent_by_foreign');
        });

        Schema::dropIfExists('bond_request');
    }
}
