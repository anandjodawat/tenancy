<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantPersonalReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_personal_references', function (Blueprint $table) {
            $table->increments('id');

            $table->string('personal_ref_name')->nullable();
            $table->string('personal_ref_occupation')->nullable();
            $table->integer('personal_ref_relationship')->nullable();
            $table->string('personal_ref_phone')->nullable();
            $table->string('personal_ref_mobile')->nullable();
            $table->string('personal_ref_email')->nullable();
            $table->integer('personal_ref_state')->nullable();
            $table->string('personal_ref_street')->nullable();
            $table->string('personal_ref_suburb')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_personal_references', function (Blueprint $table) {
            $table->dropForeign('tenant_personal_references_profile_id_foreign');
        });
        
        Schema::dropIfExists('tenant_personal_references');
    }
}
