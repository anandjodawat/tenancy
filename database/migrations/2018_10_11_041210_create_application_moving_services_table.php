<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationMovingServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_moving_services', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moving_service_id');
            $table->integer('application_id')->unsigned();

            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_moving_services');
    }
}
