<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_invitations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tenant_email')->nullable();
            $table->integer('sent_by')->unsigned()->nullable();
            $table->string('inspection_date')->nullable();
            $table->integer('property_id')->unsigned()->nullable();
            $table->integer('agency_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('set null');

            $table->foreign('sent_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_invitations', function (Blueprint $table) {
            $table->dropForeign('property_invitations_sent_by_foreign');
            $table->dropForeign('property_invitations_property_id_foreign');
        });

        Schema::dropIfExists('property_invitations');
    }
}
