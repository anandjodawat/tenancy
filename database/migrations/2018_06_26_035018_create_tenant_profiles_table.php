<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();

            // Personal Information
            $table->integer('title')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('dob')->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->integer('marital_status')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('mobile')->nullable();
            // $table->string('fax')->nullable();
            $table->string('email');
            $table->enum('preferred_time_to_be_called', ['anytime', 'morning', 'day'])->nullable();
            $table->enum('smoke', ['yes', 'no'])->nullable();
            $table->enum('smoke_inside', ['yes', 'no'])->nullable();
            $table->enum('studying', ['yes', 'no'])->nullable();
            $table->string('institution')->nullable();
            $table->string('enrollment_number')->nullable();
            $table->string('course_contact_number')->nullable();
            $table->string('institution_phone')->nullable();
            $table->string('source_of_income')->nullable();
            $table->string('net_weekly_amount')->nullable();
            $table->string('guardian_name')->nullable();
            $table->integer('guardian_relationship')->nullable();
            $table->string('guardian_address')->nullable();
            $table->string('guardian_phone_number')->nullable();
            $table->enum('pension', ['yes', 'no'])->nullable();
            $table->string('pention_type')->nullable();
            $table->string('pension_number')->nullable();
            $table->integer('status')->default(0)->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_profiles', function (Blueprint $table) {
            $table->dropForeign('tenant_profiles_user_id_foreign');
        });

        Schema::dropIfExists('tenant_profiles');
    }
}
