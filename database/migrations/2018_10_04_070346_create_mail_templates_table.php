<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_templates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('type');
            $table->longText('body')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('agency_id')->unsigned();

            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->foreign('agency_id')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_templates', function (Blueprint $table) {
            $table->dropForeign('mail_templates_created_by_foreign');
            $table->dropForeign('mail_templates_agency_id_foreign');
        });

        Schema::dropIfExists('mail_templates');
    }
}
