<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantPreviousAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_previous_addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('previous_living_arrangement')->nullable();
            $table->string('previous_address_unit')->nullable();
            $table->string('previous_address_street')->nullable();
            $table->string('previous_address_street_name')->nullable();
            $table->string('previous_address_street_type')->nullable();
            $table->integer('previous_address_state')->nullable();
            $table->string('previous_address_suburb')->nullable();
            $table->integer('previous_address_postcode')->nullable();
            $table->string('previous_address_from')->nullable();
            $table->string('previous_address_to')->nullable();
            $table->text('previous_address_leave_reason')->nullable();
            $table->string('previous_address_agent_name')->nullable();
            $table->integer('previous_address_rent')->nullable();
            $table->string('previous_address_email')->nullable();
            $table->string('previous_address_bond_refund')->nullable();
            $table->enum('previous_address_additional_information', ['yes', 'no'])->nullable();
            $table->text('previous_address_additional_information_detail')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_previous_addresses', function (Blueprint $table) {
            $table->dropForeign('tenant_previous_addresses_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_previous_addresses');
    }
}
