<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_childrens', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('children_age');
            $table->integer('application_id')->unsigned();

            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_childrens', function (Blueprint $table) {
            $table->dropForeign('application_childrens_application_id_foreign');
        });

        Schema::dropIfExists('application_childrens');
    }
}
