<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_tenants', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();
            $table->string('lease_name')->nullable();
            $table->string('post_line1')->nullable();
            $table->string('post_line2')->nullable();
            $table->string('post_line3')->nullable();
            $table->string('suburb')->nullable();
            $table->string('state')->nullable();
            $table->string('post_code')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('email_status')->default(0);

            $table->string('property_address1')->nullable();
            $table->string('property_address2')->nullable();
            $table->string('property_address3')->nullable();
            $table->string('property_state')->nullable();
            $table->string('property_suburb')->nullable();
            $table->string('property_post_code')->nullable();
            $table->string('property_alpha')->nullable();
            $table->string('property_manager')->nullable();
            $table->string('property_manager_email')->nullable();
            $table->string('property_manager_mobile')->nullable();
            $table->string('property_manager_first_name')->nullable();
            $table->string('property_manager_last_name')->nullable();

            $table->string('rent_amount1')->nullable();
            $table->string('rent_per1')->nullable();
            $table->string('tenant_code')->nullable();
            $table->integer('agency_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_tenants');
    }
}
