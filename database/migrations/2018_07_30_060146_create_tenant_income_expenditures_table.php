<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantIncomeExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_income_expenditures', function (Blueprint $table) {
            $table->increments('id');

            $table->string('current_weekly_rent')->nullable();
            $table->string('vehicle_value')->nullable();
            $table->enum('vehicle_type', ['owned', 'financed'])->nullable();
            $table->enum('income_type', ['weekly', 'monthly'])->nullable();
            $table->string('salary_wages_income')->nullable();
            $table->string('self_employed_income')->nullable();
            $table->string('benefit_income')->nullable();
            $table->string('other_income')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_income_expenditures', function (Blueprint $table) {
            $table->dropForeign('tenant_income_expenditures_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_income_expenditures');
    }
}
