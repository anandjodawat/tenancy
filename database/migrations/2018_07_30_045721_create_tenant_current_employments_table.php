<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantCurrentEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_current_employments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('current_employment_work_situation')->nullable();
            $table->string('current_company_name')->nullable();
            $table->string('current_manager_name')->nullable();
            $table->string('current_manager_phone')->nullable();
            $table->string('current_manager_email')->nullable();
            $table->string('current_work_position')->nullable();
            $table->integer('current_company_state')->nullable();
            $table->string('current_company_street')->nullable();
            $table->string('current_company_suburb')->nullable();
            $table->integer('current_industry_type')->nullable();
            $table->enum('current_employment_nature', ['casual', 'full-time', 'part-time'])->nullable();
            $table->string('current_employment_start_date')->nullable();
            $table->enum('current_job_status', ['still-in-the-job', 'finished-the-job'])->nullable();
            $table->string('current_gross_annual_salary')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_current_employments', function (Blueprint $table) {
            $table->dropForeign('tenant_current_employments_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_current_employments');
    }
}
