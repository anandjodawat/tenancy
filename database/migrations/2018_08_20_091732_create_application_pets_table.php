<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationPetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_pets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('pet_type')->nullable();
            $table->string('pet_breed')->nullable();
            $table->integer('application_id')->unsigned();

            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_pets', function (Blueprint $table) {
            $table->dropForeign('application_pets_application_id_foreign');
        });
        Schema::dropIfExists('application_pets');
    }
}
