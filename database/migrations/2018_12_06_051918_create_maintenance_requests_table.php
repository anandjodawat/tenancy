<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_requests', function (Blueprint $table) {
            $table->increments('id');

            //Address Maintenance/Repairs Required?
            $table->string('street_address')->nullable();
            $table->integer('state');
            $table->string('suburb')->nullable();
            $table->integer('post_code')->nullable();

            //contact details
            $table->enum('tenant', ['yes', 'no'])->nullable();
            $table->string('name')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('email')->nullable();

            // details of maintenance request
            $table->longText('details')->nullable();
            $table->string('preferred_date')->nullable();
            $table->string('preferred_time')->nullable();
            $table->string('maintenance_existed')->nullable();
            $table->string('property_manager_email');
            $table->integer('status')->default(0);

            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('maintenance_requests', function (Blueprint $table) {
            $table->dropForeign('maintenance_requests_user_id_foreign');
        });
        Schema::dropIfExists('maintenance_requests');
    }
}
