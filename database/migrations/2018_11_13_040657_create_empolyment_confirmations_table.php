<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpolymentConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empolyment_confirmations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('date')->nullable();
            $table->string('company_name')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('employee_position')->nullable();
            $table->string('work_duration_from')->nullable();
            $table->string('work_duration_to')->nullable();
            $table->string('employer_position')->nullable();
            $table->string('submission_date')->nullable();
            $table->integer('employment_id')->unsigned()->nullable();
            $table->string('completed_by')->nullable();

            $table->timestamps();

            $table->foreign('employment_id')
                ->references('id')
                ->on('tenant_current_employments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empolyment_confirmations', function (Blueprint $table) {
            $table->dropForeign('empolyment_confirmations_employment_id_foreign');
        });
        
        Schema::dropIfExists('empolyment_confirmations');
    }
}
