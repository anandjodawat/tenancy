<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->unique();
            $table->string('agency_id')->nullable();
            $table->integer('user_id')->unique()->unsigned();
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('suburb')->nullable();
            $table->integer('state')->nullable();
            $table->string('post_code')->nullable();
            $table->string('email');
            $table->string('registration_number')->nullable();
            $table->integer('status')->default(0);
            $table->text('website')->nullable();
            $table->string('preferred_color')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->dropForeign('agencies_user_id_foreign');
        });
        
        Schema::dropIfExists('agencies');
    }
}
