<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationOccupantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_occupants', function (Blueprint $table) {
            $table->increments('id');

            $table->string('occupant_name')->nullable();
            $table->integer('occupant_age')->nullable();
            $table->integer('occupant_relationship')->nullable();
            $table->enum('occupant_on_lease', ['yes', 'no'])->default('no')->nullable();
            $table->string('occupant_email')->nullable();
            $table->string('occupant_mobile_number')->nullable();
            $table->integer('application_id')->unsigned();

            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_occupants', function (Blueprint $table) {
            $table->dropForeign('application_occupants_application_id_foreign');
        });
        Schema::dropIfExists('application_occupants');
    }
}
