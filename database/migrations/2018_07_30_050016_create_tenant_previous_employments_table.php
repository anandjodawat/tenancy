<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantPreviousEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_previous_employments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('previous_employment_work_situtation')->nullable();
            $table->string('previous_company_name')->nullable();
            $table->string('previous_manager_name')->nullable();
            $table->string('previous_manager_phone')->nullable();
            $table->string('previous_manager_email')->nullable();
            $table->string('previous_work_position')->nullable();
            $table->integer('previous_company_state')->nullable();
            $table->string('previous_company_street')->nullable();
            $table->string('previous_company_suburb')->nullable();
            $table->integer('previous_industry_type')->nullable();
            $table->enum('previous_employment_nature', ['casual', 'full-time', 'part-time'])->nullable();
            $table->string('previous_employment_start_date')->nullable();
            $table->string('previous_employment_end_date')->nullable();
            $table->string('previous_gross_annual_salary')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_previous_employments', function (Blueprint $table) {
            $table->dropForeign('tenant_previous_employments_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_previous_employments');
    }
}
