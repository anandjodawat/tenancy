<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_applications', function (Blueprint $table) {
            $table->increments('id');

            $table->string('applicant_name')->nullable();
            $table->integer('application_id')->unsigned()->nullable();
            $table->string('sent_to');
            $table->integer('notify_for')->nullable();
            $table->string('subject');
            $table->integer('sent_by')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');

            $table->foreign('sent_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_applications', function (Blueprint $table) {
            $table->dropForeign('email_applications_application_id_foreign');
            $table->dropForeign('email_applications_sent_by_foreign');
        });

        Schema::dropIfExists('email_applications');
    }
}
