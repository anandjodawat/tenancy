<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_deposits', function (Blueprint $table) {
            $table->increments('id');

            $table->string('tenant_code')->nullable();
            $table->string('name');
            $table->string('address');
            $table->string('contact_number')->nullable();
            $table->string('payment_amount')->nullable();
            $table->string('send_to');
            $table->string('subject')->nullable();
            $table->longText('body');
            $table->integer('sent_by')->nullable()->unsigned();
            $table->integer('email_status')->default('0');

            $table->timestamps();

            $table->foreign('sent_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_deposits', function (Blueprint $table) {
            $table->dropForeign('request_deposits_sent_by_foreign');
        });

        Schema::dropIfExists('request_deposits');
    }
}
