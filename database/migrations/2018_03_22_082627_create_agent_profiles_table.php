<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('agency_name');
            $table->string('agency_id');
            $table->string('username')->nullable();
            $table->integer('address_street_number')->nullable();
            $table->string('address_street_unit')->nullable();
            $table->string('address_street_name')->nullable();
            $table->string('street_type')->nullable();
            $table->string('suburb')->nullable();
            $table->string('state')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('email');
            $table->string('phone')->nullable();
            // $table->string('fax')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('web_address')->nullable();
            $table->string('general_lease_terms');
            $table->integer('user_id')->unsigned();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_profiles', function (Blueprint $table) {
            $table->dropForeign('agent_profiles_user_id_foreign');
        });
        
        Schema::dropIfExists('agent_profiles');
    }
}
