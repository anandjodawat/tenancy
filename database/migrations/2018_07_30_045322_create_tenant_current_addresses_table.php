<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantCurrentAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_current_addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('current_living_arrangement')->nullable();
            $table->string('current_address_unit')->nullable();
            $table->string('current_address_street')->nullable();
            $table->string('current_address_street_name')->nullable();
            $table->integer('current_address_state')->nullable();
            $table->string('current_address_suburb')->nullable();
            $table->string('current_address_post_code')->nullable();
            $table->string('current_address_move_in')->nullable();
            $table->string('current_address_landlord_name')->nullable();
            $table->string('current_address_monthly_rent')->nullable();
            $table->text('current_address_leave_reason')->nullable();
            $table->string('current_address_landlord_phone')->nullable();
            $table->string('current_address_landlord_email')->nullable();
            $table->string('current_address_landlord_bond_refund')->nullable();
            // $table->enum('different_postal_current_address', ['yes', 'no'])->nullable();
            // $table->string('current_address_postal_address')->nullable();
            // $table->string('current_address_postal_suburb')->nullable();
            // $table->integer('current_address_postal_state')->nullable();
            // $table->string('current_address_postal_code')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_current_addresses', function (Blueprint $table) {
            $table->dropForeign('tenant_current_addresses_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_current_addresses');
    }
}
