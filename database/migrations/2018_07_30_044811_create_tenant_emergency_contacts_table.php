<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantEmergencyContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_emergency_contacts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('emergency_contact_name')->nullable();
            $table->integer('emergency_contact_relation')->nullable();
            $table->string('emergency_contact_number')->nullable();
            $table->integer('emergency_contact_state')->nullable();
            $table->string('emergency_contact_street')->nullable();
            $table->string('emergency_contact_suburb')->nullable();
            $table->string('emergency_contact_home_phone')->nullable();
            $table->string('emergency_contact_work_phone')->nullable();
            $table->string('emergency_contact_mobile')->nullable();
            $table->string('emergency_contact_email')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_emergency_contacts', function (Blueprint $table) {
            $table->dropForeign('tenant_emergency_contacts_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_emergency_contacts');
    }
}
