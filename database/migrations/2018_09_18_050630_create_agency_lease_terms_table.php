<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyLeaseTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_lease_terms', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('agency_id')->unsigned();
            $table->longText('lease_terms')->nullable();

            $table->timestamps();

            $table->foreign('agency_id')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agency_lease_terms', function (Blueprint $table) {
            $table->dropForeign('agency_lease_terms_agency_id_foreign');
        });

        Schema::dropIfExists('agency_lease_terms');
    }
}
