<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationForwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_forwards', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('application_id')->unsigned();
            $table->string('sent_to');
            $table->integer('sent_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');

            $table->foreign('sent_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_forwards', function (Blueprint $table) {
            $table->dropForeign('application_forwards_application_id_foreign');
            $table->dropForeign('application_forwards_sent_by_foreign');
        });
        
        Schema::dropIfExists('application_forwards');
    }
}
