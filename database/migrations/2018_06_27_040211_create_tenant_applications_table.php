<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            // Property Details
            $table->string('applied_for')->nullable();
            $table->integer('state')->nullable();
            $table->string('suburb')->nullable();
            $table->string('post_code')->nullable();
            $table->integer('property_type')->nullable();
            $table->integer('number_of_bedrooms')->nullable();
            $table->string('commencement_date')->nullable();
            $table->string('length_of_lease')->nullable();
            $table->string('weekly_rent')->nullable();
            $table->string('monthly_rent')->nullable();
            $table->string('bond')->nullable();
            $table->enum('bond_assistance', ['yes', 'no'])->nullable();
            $table->text('bondsure_url')->nullable();
            $table->text('docusign_url')->nullable();
            $table->integer('property_found_in')->nullable();

            // Property Manager Details
            $table->string('agency_name')->nullable();
            $table->string('property_manager_name')->nullable();
            $table->string('property_manager_email')->nullable();

            // Occupancy Details
            $table->integer('number_of_occupant')->nullable();
            $table->integer('number_of_children')->nullable();
            $table->integer('number_of_vehicles')->nullable();
            $table->integer('number_of_pets')->nullable();

            // Agent Specific
            $table->enum('is_tenant_terminated', ['yes', 'no'])->nullable();
            $table->text('tenant_terminated_details')->nullable();
            $table->enum('is_tenant_refused', ['yes', 'no'])->nullable();
            $table->text('tenant_refused_details')->nullable();
            $table->enum('is_on_debt', ['yes', 'no'])->nullable();
            $table->text('on_debt_details')->nullable();
            $table->enum('is_deduction_rental_bond', ['yes', 'no'])->nullable();
            $table->text('deduction_rental_bond_details')->nullable();
            $table->enum('affect_future_rental_payment', ['yes', 'no'])->nullable();
            $table->text('affect_future_rental_payment_details')->nullable();
            $table->enum('is_another_pending_application', ['yes', 'no'])->nullable();
            $table->enum('own_a_property', ['yes', 'no'])->nullable();
            $table->text('own_a_property_details')->nullable();
            $table->enum('buy_another_property', ['yes', 'no'])->nullable();
            $table->integer('intend_to_buy_after')->nullable();

            // Moving Service
            $table->enum('need_moving_service', ['yes', 'no'])->nullable();
            $table->enum('moving_service_agreement', ['yes', 'no'])->nullable();

            // Property Declaration
            $table->enum('inspected_this_property', ['yes', 'no'])->nullable();
            $table->string('inspection_date')->nullable();
            $table->enum('good_condition', ['yes', 'no'])->nullable();
            $table->text('not_good_condition_reason')->nullable();
            $table->string('inspection_code')->nullable();

            // Licence and aggreement
            $table->enum('licence_and_agreement', ['yes', 'no'])->nullable();

            $table->string('envelope_id')->nullable();
            $table->integer('status')->default('0');
            $table->integer('draft')->default('0');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_applications');
    }
}
