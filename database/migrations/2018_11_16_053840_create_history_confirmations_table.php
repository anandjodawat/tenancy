<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_confirmations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('date')->nullable();
            $table->string('agency')->nullable();
            // $table->string('fax')->nullable();
            $table->string('tenant_name')->nullable();
            $table->string('property_address')->nullable();
            $table->enum('listed_as_lease', ['yes', 'no'])->nullable();
            $table->string('property_duration_from')->nullable();
            $table->string('property_duration_to')->nullable();
            $table->enum('terminate_tenancy', ['yes', 'no'])->nullable();
            $table->longText('reason_for_termination')->nullable();
            $table->integer('arrears')->nullable();
            $table->enum('breach_notices', ['yes', 'no'])->nullable();
            $table->longText('reason_for_breach_notices')->nullable();
            $table->enum('damage', ['yes', 'no'])->nullable();
            $table->longText('damage_reason')->nullable();
            $table->integer('inspection_like')->nullable();
            $table->enum('pets', ['yes', 'no'])->nullable();
            $table->longText('complaints_damage')->nullable();
            $table->string('vacate_date')->nullable();
            $table->enum('bond_refund_full', ['yes', 'no'])->nullable();
            $table->longText('bond_refund_reason')->nullable();
            $table->string('rate_tenant')->nullable();
            $table->enum('rent_again', ['yes', 'no'])->nullable();
            $table->longText('additional_comments')->nullable();
            $table->string('completed_by')->nullable();
            $table->string('employer_position')->nullable();
            $table->string('submission_date')->nullable();
            $table->integer('personal_ref_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('personal_ref_id')
                ->references('id')
                ->on('tenant_personal_references')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_confirmations');
    }
}
