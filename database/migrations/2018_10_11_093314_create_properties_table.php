<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');

            $table->string('address')->nullable();
            $table->string('suburb')->nullable();
            $table->integer('state')->nullable();
            $table->string('post_code')->nullable();
            $table->string('rent_amount')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('agency_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign('properties_created_by_foreign');
        });

        Schema::dropIfExists('properties');
    }
}
