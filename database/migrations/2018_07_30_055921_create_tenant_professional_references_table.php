<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantProfessionalReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_professional_references', function (Blueprint $table) {
            $table->increments('id');

            $table->string('professional_ref_name')->nullable();
            $table->string('professional_ref_company')->nullable();
            $table->string('professional_ref_occupation')->nullable();
            $table->integer('professional_ref_relationship')->nullable();
            $table->string('professional_ref_phone')->nullable();
            $table->string('professional_ref_mobile')->nullable();
            $table->string('professional_ref_email')->nullable();
            $table->integer('professional_ref_company_state')->nullable();
            $table->string('professional_ref_company_street')->nullable();
            $table->string('professional_ref_company_suburb')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_professional_references', function (Blueprint $table) {
            $table->dropForeign('tenant_professional_references_profile_id_foreign');
        });

        Schema::dropIfExists('tenant_professional_references');
    }
}
