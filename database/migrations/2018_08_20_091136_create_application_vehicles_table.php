<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_vehicles', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('application_id')->unsigned();
            $table->string('vehicle_type');
            $table->string('vehicle_registration')->nullable();
            $table->string('make_model')->nullable();

            $table->timestamps();

            $table->foreign('application_id')
                ->references('id')
                ->on('tenant_applications')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_vehicles', function (Blueprint $table) {
            $table->dropForeign('application_vehicles_application_id_foreign');
        });
        
        Schema::dropIfExists('application_vehicles');
    }
}
