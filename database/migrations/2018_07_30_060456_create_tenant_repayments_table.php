<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_repayments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('repayment_loans')->nullable();
            $table->string('repayment_credit_cards')->nullable();
            $table->string('repayment_hire_purchase')->nullable();
            $table->string('repayment_store_card')->nullable();
            $table->string('repayment_others')->nullable();
            $table->string('repayment_expenses')->nullable();
            $table->integer('profile_id')->unsigned();

            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('tenant_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant_repayments', function (Blueprint $table) {
            $table->dropForeign('tenant_repayments_profile_id_foreign');
        });
        
        Schema::dropIfExists('tenant_repayments');
    }
}
