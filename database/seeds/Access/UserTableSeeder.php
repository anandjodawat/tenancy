<?php

use App\Models\Agency;
use Database\TruncateTable;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\DB;
use App\Models\Agency\SupportingDocument;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncateMultiple([config('access.users_table'), 'social_logins']);

        //Add the master administrator, user id of 1
        $users = [
            [
                'first_name'        => 'Super',
                'last_name'         => 'Admin',
                'email'             => 'superadmin@admin.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Agency',
                'last_name'         => 'Admin',
                'email'             => 'admin@admin.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Property',
                'last_name'         => 'Manager',
                'email'             => 'manager@manager.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'new',
                'last_name'         => 'user',
                'email'             => 'user@user.com',
                'password'          => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
        ];

        DB::table(config('access.users_table'))->insert($users);

        $this->enableForeignKeys();

        $agency = Agency::create([
            'name' => 'Agency Admin',
            'agency_id' => 'My Agency ID',
            'user_id' => '2',
            'address' => 'Agency Address',
            'phone_number' => '123456798',
            'email' => 'admin@admin.com',
            'registration_number' => '12345',
            'status' => '1',
            'suburb' => 'Test Suburb',
            'state' => '1',
            'post_code' => '1234'
        ]);

        foreach (SupportingDocument::DOCUMENTNAME as $key => $value) {
            SupportingDocument::create([
                'agency_id' => $agency->id,
                'supporting_document' => config('tenancy-application.document_description')[$key],
                'document_id' => $key
            ]);
        }

        User::find(2)->update([
            'agency_id' => $agency->id
        ]);

        User::find(3)->update([
            'agency_id' => $agency->id
        ]);
    }
}
