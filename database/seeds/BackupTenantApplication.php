<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant\TenantApplication;
use App\Models\Access\User\User;

class BackupTenantApplication extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        echo "User Data Extracting";
        $users = \DB::table('users_backup')->get();
        foreach ($users as $key => $user) {
            if ($user->Email) {
                $newUser = User::whereEmail($user->Email)->first();
                if ($newUser) {
                    $applications = \DB::table('application')->where('TenantID', $user->ID)->get();
                    foreach ($applications as $key => $application) {
                        echo " .";
                        $values = $this->reutrn_application_values($application, $newUser);
                        TenantApplication::create($values);
                    }
                }
            }
        }
    }

    private function reutrn_application_values($application, $newUser)
    {
        switch ($application->Status) {
            case '6':
            $status = '1';
            break;
            case '4':
            $status = '0';
            case '5':
            $status = '3';
            case '7':
            $status = '4';
            case '8':
            $status = '5';
            case '9':
            $status = '2';        
            default:
            $status = '5';
            break;
        }

        return [
            'applied_for'=> $application->Address,
            'state' => $application->State ? array_search($application->State, config('tenancy-application.states')) : null,
            'suburb'=> $application->Suburb,
            'post_code'=> $application->Postcode,
            'property_type'=> null,
            'number_of_bedrooms'=> $application->Bedrooms,
            'commencement_date'=> $application->CommDate,
            'length_of_lease'=> $application->LeaseLength,
            'weekly_rent'=> $application->WeeklyRent,
            'monthly_rent'=> $application->MonthlyRent,
            'bond'=> $application->Bond,
            'bond_assistance'=> $application->BondSure,
            'property_found_in'=> null,

    // Property Manager Details
            'agency_name'=> $application->Agency,
            'property_manager_name'=> $application->PM_Name,
            'property_manager_email'=> $application->PM_Email,

    // Occupancy Details
            'number_of_occupant'=> $application->Occupants,
            'number_of_children'=> $application->children,
            'number_of_vehicles'=> $application->Vehicles,
            'number_of_pets'=> $application->Pets,

    // Agent Specific
            'is_tenant_terminated'=> null,
            'tenant_terminated_details'=> null,
            'is_tenant_refused'=> null,
            'tenant_refused_details'=> null,
            'is_on_debt'=> null,
            'on_debt_details'=> null,
            'is_deduction_rental_bond'=> null,
            'deduction_rental_bond_details'=> null,
            'affect_future_rental_payment'=> null,
            'affect_future_rental_payment_details'=> null,
            'is_another_pending_application'=> null,
            'own_a_property'=> null,
            'own_a_property_details'=> null, 
            'buy_another_property'=> null,
            'intend_to_buy_after'=> null,
    // Moving Service
            'need_moving_service'=> null,
            'moving_service_agreement'=> null,

    // Property Declaration
            'inspected_this_property' => null,
            'inspection_date'=> null,
            'good_condition'=> null,
            'not_good_condition_reason'=> null,
            'inspection_code'=> $application->insecption_code,

    // Licence and aggreement
            'licence_and_agreement'=> null,
            'status'=> $status,
            'draft'=> '0',
            'user_id'=> $newUser->id,
            'bondsure_url'=> $application->bondsureLink,
            'docusign_url'=> null,
            'envelope_id'=> $application->bondsureValid
        ];
    }
}

