<?php

use Illuminate\Database\Seeder;
use App\Models\Access\User\User;
use App\Models\Tenant\UploadTenant;

class BackupUploadTenantData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	echo "Tenant Data Extracting";
    	$tenants = \DB::table('excel_tenant')->get();

    	foreach ($tenants as $key => $excel) {
	    	$user = \DB::table('users_backup')->where('ID', $excel->ManagerID)->first();
	    	$user = User::whereEmail($user->Email)->first();
    		if ($user->count() > 0) {
	    		UploadTenant::create([
	    			'name'=>$excel->name,
	    			'lease_name'=>$excel->leaseName,
	    			'email'=>$excel->email,
	    			'post_line1'=>$excel->postline1,
	    			'post_line2'=>$excel->postline2,
	    			'post_line3'=>$excel->postline3,
	    			'suburb'=>$excel->tenantSuburb,
	    			'state'=>$excel->TenantState,
	    			'post_code'=>$excel->TenantPcode,
	    			'mobile'=>$excel->mobile,
	    			'property_address1'=>$excel->propAdd1,
	    			'property_address2'=>$excel->propAdd2,
	    			'property_address3'=>$excel->propAdd3,
	    			'property_state'=>$excel->propState,
	    			'property_suburb'=>$excel->propSuburb,
	    			'property_post_code'=>$excel->propPcode,
	    			'property_alpha'=>$excel->propAlpha,
	    			'property_manager'=>$excel->propManager,
	    			'property_manager_email'=>$excel->propManagerEmail,
	    			'property_manager_mobile'=>$excel->propManagerMobile,
	    			'property_manager_first_name'=>$excel->propManFirstName,
	    			'property_manager_last_name'=>$excel->PropManLastName,
	    			'email_status'=>$excel->status,
	    			'rent_amount1'=>$excel->RentAmt1,
	    			'rent_per1'=>$excel->RentPer1,
	    			'tenant_code'=>$excel->TentCode,
	    			'agency_id'=>$user->agency_id
	    		]);
	    		echo " .";
    		}
    	}
    echo "\r\n";
    echo "Tenant Data Extracted Successfully.";
    }
}
