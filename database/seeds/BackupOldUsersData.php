<?php

use App\Models\Agency;
use Illuminate\Database\Seeder;
use App\Models\Access\User\User;
use App\Models\Tenant\TenantProfile;
use App\Models\Agency\AgencyLeaseTerm;
use App\Models\Agency\SupportingDocument;
use App\Models\Tenant\TenantRepayment;
use App\Models\Tenant\TenantCurrentAddress;
use App\Models\Tenant\TenantPreviousAddress;
use App\Models\Tenant\TenantEmergencyContact;
use App\Models\Tenant\TenantCurrentEmployment;
use App\Models\Tenant\TenantIncomeExpenditure;
use App\Models\Tenant\TenantPersonalReference;
use App\Models\Tenant\TenantPreviousEmployment;
use App\Models\Tenant\TenantProfessionalReference;

class BackupOldUsersData extends Seeder
{
/**
* Run the database seeds.
*
* @return void
*/
public function run()
{
    echo "User Data Extracting . . . .";
    $users = \DB::table('users_backup')->get();
    foreach ($users as $key => $user) {
        if ($user->Role == '1') {
            $profile = \DB::table('manager_profile')->where('UserID', $user->ID)->first();
            if (isset($profile->ID) && User::whereEmail($user->Email)->count() < 1) {
                echo " .";
                $newUser = User::create([
                   'first_name' => $profile->FirstName,
                   'last_name' => $profile->LastName,
                   'email' => $user->Email,
                   'password' => bcrypt(rand(100000, 10000000)),
                   'status' => '1',
                   'confirmed' => '1',
               ]);

                if (Agency::whereName($profile->Name)->count() > 0) {
                    User::find($newUser->id)->attachRole(3);
                    $agency = Agency::whereName($profile->Name)->first();
                } else {
                    $agency = Agency::create([
                        'name' => $profile->Name,
                        'agency_id' => $profile->AgentID,
                        'user_id' => $newUser->id,
                        'address' => $profile->StreetN." ".$profile->StreetT." ".$profile->Suburb." ".$profile->State." ". $profile->PostCode,
                        'phone_number' => $profile->Phone,
                        'email' => $profile->ComEmail,
                        'website' => $profile->WebAddress,
                        'state' => array_search($profile->State, config('tenancy-application.states')),
                        'post_code' => $profile->PostCode,
                        'suburb' => $profile->Suburb
                    ]);
                    User::find($newUser->id)->attachRole(2);

                    foreach (SupportingDocument::DOCUMENTNAME as $key => $value) {
                        $index = $key + 1;
                        $point = "point".$index;
                        $detail = $index < 7  && $profile->$point ? $profile->$point : '';
                        SupportingDocument::create([
                            'agency_id' => $agency->id,
                            'supporting_document' => $detail,
                            'document_id' => $key
                        ]);
                    }
                    AgencyLeaseTerm::create([
                        'agency_id' => $agency->id,
                        'lease_terms' => $profile->rentalrewards_fee_info
                    ]);
                }
                $newUser->update([
                    'agency_id' => $agency->id
                ]);
            }
        }
        if ($user->Role == '2') {
            $profile = \DB::table('tenant_profile')->where('UserID', $user->ID)->first();
            if (isset($profile->ID) && User::whereEmail($user->Email)->count() < 1) {
                echo " .";
                $newUser = User::create([
                    'first_name' => $profile->Fname,
                    'last_name' => $profile->Lname,
                    'email' => $user->Email,
                    'password' => bcrypt(rand(100000, 10000000)),
                    'status' => '1',
                    'confirmed' => '1'
                ]);

                $this->save_personal_information($newUser, $profile, $user);
                $this->save_emergency_contact($newUser, $profile, $user);
                $this->save_current_address($newUser, $profile, $user);
                $this->save_previous_address($newUser, $profile, $user);
                $this->save_current_employment($newUser, $profile, $user);
                $this->save_previous_employment($newUser, $profile, $user);
                $this->save_personal_reference($newUser, $profile, $user);
                $this->save_professional_reference($newUser, $profile, $user);
                $this->save_income_expenditure($newUser, $profile, $user);
                $this->save_repayments($newUser, $profile, $user);
            }
        }
    }
    echo "\r\n";
    echo "User Data Extracted Successfully.";
}

private function save_personal_information($newUser, $profile, $user)
{
    TenantProfile::create([
// personal Information
        'user_id' => $newUser->id,
        'title' => $profile->Title ? array_search($profile->Title, config('tenancy-application.title')) : null,
        'first_name' => $profile->Fname,
        'last_name' => $profile->Lname,
        'middle_name' => $profile->Mname,
        'dob' => $profile->DOB,
        'gender' => $profile->Gender ? strtolower($profile->Gender) : null,
        'marital_status' => $profile->MS ? array_search(strtolower($profile->MS), config('tenancy-application.marital')) : null,
        'home_phone' => $profile->Hnumber,
        'work_phone' => null,
        'mobile' => $profile->Mnumber,
        'email' => $user->Email,
        'preferred_time_to_be_called' => $profile->Ptime && $profile->Ptime != 'Evening' ? strtolower($profile->Ptime) : null,
        'smoke' => $profile->Smoke ? $profile->Smoke == 'Y' ? 'yes' : 'no' : null,
        'smoke_inside' => $profile->LSmoke ? $profile->LSmoke == 'Y' ? 'yes' : 'no' : null,
        'studying' => $profile->Student ? $profile->Student == 'Y' ? 'yes' : 'no' : null,
        'institution' => null,
        'enrollment_number' => $profile->EnrollmentNo,
        'course_contact_number' => $profile->CourseCName,
        'institution_phone' => $profile->PhoneNo,
        'source_of_income' => null,
        'net_weekly_amount' => null,
        'guardian_name' => null,
        'guardian_relationship' => null,
        'guardian_address' => null,
        'guardian_phone_number' => null,
        'pension' => $profile->Pension ? $profile->Pension == 'Y' ? 'yes' : 'no' : null,
        'pention_type' => $profile->Pension_Type,
        'pension_number' => $profile->Pension_Number
    ]);
}

private function save_emergency_contact($newUser, $profile, $user)
{
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;

    TenantEmergencyContact::create([
        'profile_id' => $profile_id,
        'emergency_contact_name' => null,
        'emergency_contact_relation' => null,
        'emergency_contact_number' => $profile->E_phone,
        'emergency_contact_state' => null,
        'emergency_contact_street' => null,
        'emergency_contact_suburb' => null,
        'emergency_contact_home_phone' => $profile->E_phone,
        'emergency_contact_work_phone' => null,
        'emergency_contact_mobile' => $profile->E_mobile,
        'emergency_contact_email' => $profile->E_email,

    ]);
}

private function save_current_address($newUser, $profile, $user)
{
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantCurrentAddress::create([
        'profile_id' => $profile_id,

        'current_living_arrangement' =>null, 
        'current_address_unit' => $profile->Unit,
        'current_address_street' =>$profile->Street,
        'current_address_street_name' =>$profile->StreetN,
        'current_address_state' => $profile->State ? array_search($profile->State, config('tenancy-application.states')) : null,
        'current_address_suburb' => $profile->Suburb,
        'current_address_post_code' => $profile->PostCode,
        'current_address_move_in' => $profile->DateMoveIn,
        'current_address_landlord_name' => $profile->Landlord_Name,
        'current_address_monthly_rent' =>$profile->Monthly_Rent,
        'current_address_leave_reason' => $profile->ReasonFL,
        'current_address_landlord_phone' =>  $profile->Phone,
        'current_address_landlord_email' =>$profile->Email,
        'current_address_landlord_bond_refund' => $profile->Refund,
    ]);
}

private function save_previous_address($newUser, $profile, $user)
{
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantPreviousAddress::create([
        'profile_id' => $profile_id,

        'previous_living_arrangement' => null,
        'previous_address_unit' =>  $profile->PrUnit,
        'previous_address_street' => $profile->PrStreet,
        'previous_address_street_name' =>  $profile->PrStreetN,
        'previous_address_street_type' =>  $profile->PrStreetT,
        'previous_address_state' => $profile->PrState ? array_search($profile->PrState, config('tenancy-application.states')) : null,
        'previous_address_suburb' =>  $profile->PrSuburb,
        'previous_address_postcode' =>  null,
        'previous_address_from' => $profile->PrFrmDate,
        'previous_address_to' => $profile->PrToDate,
        'previous_address_leave_reason' => $profile->PrReasonFL,
        'previous_address_agent_name' => $profile->PrAgName,
        'previous_address_rent' => null,
        'previous_address_email' => $profile->PrEmail,
        'previous_address_bond_refund' =>  $profile->PrBond,
        'previous_address_additional_information' => null,
        'previous_address_additional_information_detail' => null,
    ]);
}

private function save_current_employment($newUser, $profile, $user)
{   
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantCurrentEmployment::create([
        'profile_id' => $profile_id,

        'current_employment_work_situation' => null,
        'current_company_name' => $profile->Emcompany_name,
        'current_manager_name' => $profile->EmManager_name,
        'current_manager_phone' => $profile->EmPhone,
        'current_manager_email' => $profile->EmEmail,
        'current_work_position' =>$profile->EmOccup_Post,
        'current_company_state' => null,
        'current_company_street' => null,
        'current_company_suburb' => null,
        'current_industry_type' => null,
        'current_employment_nature' =>  null,
        'current_employment_start_date' => $profile->EmDatestart,
        'current_job_status' => null,
        'current_gross_annual_salary' => $profile->EmGross,

    ]);
}

public function save_previous_employment($newUser, $profile, $user)
{       
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantPreviousEmployment::create([
        'profile_id' => $profile_id,

        'previous_employment_work_situtation' => null,
        'previous_company_name' => $profile->Pre_company_name,
        'previous_manager_name' => $profile->Pre_Manager_name,
        'previous_manager_phone' => $profile->Pre_Phone,
        'previous_manager_email' => $profile->Pre_Email,
        'previous_work_position' => null,
        'previous_company_state' => null,
        'previous_company_street' => null,
        'previous_company_suburb' => null,
        'previous_industry_type' => null,
        'previous_employment_nature' => null,
        'previous_employment_start_date' => $profile->Pre_Datestart,
        'previous_employment_end_date' => $profile->Pre_Datefinish,
        'previous_gross_annual_salary' => $profile->Pre_Gross,
    ]);
}

private function save_personal_reference($newUser, $profile, $user)
{   
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantPersonalReference::create([
        'profile_id' => $profile_id,

        'personal_ref_name' => $profile->PerRef,
        'personal_ref_occupation' => $profile->PerOcc,
        'personal_ref_relationship' => null,
        'personal_ref_phone' => $profile->PerPh,
        'personal_ref_mobile' => $profile->PerMob,
        'personal_ref_email' => $profile->PerEmail,
        'personal_ref_state' => null,
        'personal_ref_street' => null,
        'personal_ref_suburb' => null,
    ]);
}

private function save_professional_reference($newUser, $profile, $user)
{
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantProfessionalReference::create([
        'profile_id' => $profile_id,

        'professional_ref_name' => null,
        'professional_ref_company' => null,
        'professional_ref_relationship' => null,
        'professional_ref_occupation' => null,
        'professional_ref_phone' => null,
        'professional_ref_mobile' => null,
        'professional_ref_email' => null,
        'professional_ref_company_state' => null,
        'professional_ref_company_street' => null,
        'professional_ref_company_suburb' => null 
    ]);
}

private function save_income_expenditure($newUser, $profile, $user)
{
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantIncomeExpenditure::create([
        'profile_id' => $profile_id,

        'current_weekly_rent' => $profile->Tamount,
        'vehicle_value' => $profile->Tvalue,
        'vehicle_type' => null,
        'income_type' => null,
        'salary_wages_income' => $profile->sal,
        'self_employed_income' => $profile->selfemp,
        'benefit_income' => $profile->benefit,
        'other_income' => $profile->other
    ]);
}

private function save_repayments($newUser, $profile, $user)
{
    $profile_id = TenantProfile::whereUserId($newUser->id)->first()->id;
    TenantRepayment::create([
        'profile_id' => $profile_id,
        'repayment_loans' => $profile->loan,
        'repayment_credit_cards' => $profile->Ccard,
        'repayment_hire_purchase' => $profile->Hpurchase,
        'repayment_store_card' => $profile->Scard,
        'repayment_others' => $profile->Rother,
        'repayment_expenses' => $profile->Eother
    ]);
}
}
